﻿using System;

namespace AbnApplicationPortal.Shared.Requests
{
    public class EntityFilter
    {
        public string Search { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool ApplyDate { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; } 
        public Guid ProviderId { get; set; } 
    }
}
