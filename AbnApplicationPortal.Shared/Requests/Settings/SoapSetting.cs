﻿namespace AbnApplicationPortal.Shared.Requests.Settings
{
	public class SoapSetting
	{
		public string Url { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
	}
}
