﻿using AbnApplicationPortal.Shared.Models.Settings;
using Newtonsoft.Json;

namespace AbnApplicationPortal.Shared.Requests.Settings
{
    public class SettingsRequest
    {
        public SettingKey Key { get; set; }
        public string KeyStr { get; set; }
        public dynamic Content { get; set; }

        public SettingsRequest Read(Setting setting)
        {
            var sett = new SettingsRequest
            {
                Key = setting.Key,
                KeyStr = setting.Key.ToString(),
                Content = GetContent(setting.Data, setting.Key)
            };
            return sett;
        }

        private static dynamic GetContent(string data, SettingKey key)
        {
            if (key == SettingKey.Email)
            {
                var email = JsonConvert.DeserializeObject<EmailSetting>(data);
                return email;
            }

            if (key == SettingKey.Sms)
            {
                var sms = JsonConvert.DeserializeObject<SmsSetting>(data);
                return sms;
            }
            if (key == SettingKey.Mpesa)
            {
                var mpesa = JsonConvert.DeserializeObject<MpesaSetting>(data);
                return mpesa;
            }

            return new { };
        }
    }
}
