﻿using MailKit.Security;

namespace AbnApplicationPortal.Shared.Requests.Settings
{
    public class EmailSetting
    {
        public string SmtpServer { get; set; }
        public string SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public string SenderFromEmail { get; set; }
        public string SenderFromName { get; set; }
        public SecureSocketOptions SocketOptions { get; set; }
    }
}
