﻿namespace AbnApplicationPortal.Shared.Requests.Settings
{
    public class SmsSetting
    {
        public string Username { get; set; }
        public string ApiKey { get; set; }
        public string SenderId { get; set; }
        public string ShortCode { get; set; }
    }
}
