﻿namespace AbnApplicationPortal.Shared.Requests.Settings
{
	public class MpesaSetting
	{
		public string AbnExpressUrl { get; set; }
		public string CallBackURL { get; set; }
		public string PassKey { get; set; }
		public string BusinessShortCode { get; set; }
		public string AbnClientId { get; set; }
		public string ConsumerKey { get; set; }
		public string ConsumerSecret { get; set; }
	}
}
