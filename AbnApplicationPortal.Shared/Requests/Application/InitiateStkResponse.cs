﻿namespace AbnApplicationPortal.Shared.Requests.Application
{
    public class InitiateStkResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }
    }
}
