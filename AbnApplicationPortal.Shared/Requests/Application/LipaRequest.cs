﻿namespace AbnApplicationPortal.Shared.Requests.Application
{
	public class LipaRequest
	{
		public LipaRequest()
		{

		}

		public string PhoneNumber { get; set; }
		public string CallBackURL { get; set; }
		public string AccountReference { get; set; }
		public string TransactionDesc { get; set; }
		public string ConsumerKey { get; set; }
		public string ConsumerSecret { get; set; }
		public string PassKey { get; set; }
		public string BusinessShortCode { get; set; }
		public string Amount { get; set; }
		public string ClientId { get; set; }
	}
}
