﻿namespace AbnApplicationPortal.Shared.Responses
{
    public class StkPayResponse
    {
        public StkPayResponse()
        {
            IsCompleted = false;
        }
        public bool IsCompleted { get; set; }
        public string MerchantRequestID { get; set; }
        public string CheckoutRequestID { get; set; }
        public int ResultCode { get; set; }
        public string ResultDesc { get; set; }
        public decimal Amount { get; set; }
        public string Balance { get; set; }
        public string ReceiptNumber { get; set; }
        public string TransactionDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
    }
}
