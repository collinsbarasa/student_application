﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;

namespace AbnApplicationPortal.Shared.HttpClient
{
    public class RestSharpRequests
    {
        #region REQUESTS

        public async Task<string> Get(string apiUrl, string resourceUrl)
        {
            var restClient = new RestClient(apiUrl);
            var restRequest = new RestRequest(resourceUrl, Method.GET) { RequestFormat = DataFormat.Json };
            var data = await restClient.ExecuteGetTaskAsync(restRequest);
            return data.Content;
        }

        public async Task<string> Post(string apiUrl, string resourceUrl, object entity)
        {
            var restClient = new RestClient(apiUrl);
            var restRequest = new RestRequest(resourceUrl, Method.POST) { RequestFormat = DataFormat.Json };
            restRequest.AddJsonBody(entity);
            var response = await restClient.ExecutePostTaskAsync(restRequest);
            return response.Content;
        }
        public async Task<string> PostFromForm(string url, List<FromForm> list)
        {
            var restClient = new RestClient(url);
            var restRequest = new RestRequest(url, Method.POST);
            restRequest.AddHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

            if (list.Count > 0)
            {
                foreach (var lForm in list)
                {
                    restRequest.AddParameter(lForm.Key, lForm.Value);
                }
            }
            var response = await restClient.ExecutePostTaskAsync(restRequest);
            return response.Content;
        }
        #endregion
    }
    public class FromForm
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
