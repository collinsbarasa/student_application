﻿using System;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
    public class Deferment : BaseEntity
    {
        public Deferment() { }

        public Guid Reason { get; set; }
        //public Guid DeferPeriod { get; set; }
        public Guid ApplicationId { get; set; }
        //public DateTime ExpectedDate { get; set; }
        public string Approval { get; set; }
        public string Unit { get; set; }
        public int Count { get; set; }
        public string Personel { get; set; }
        public string PersonelId { get; set; }
    }
}
