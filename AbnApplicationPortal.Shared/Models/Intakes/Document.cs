﻿using System;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
	public class Document : BaseEntity
	{
		public Document()
		{
			Code = "DOC-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");

		}
		public string Name { get; set; }
		public string Description { get; set; }
		public string Category { get; set; }
	}
}
