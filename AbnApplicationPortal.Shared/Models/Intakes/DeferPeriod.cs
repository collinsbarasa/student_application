﻿using System;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
    public class DeferPeriod : BaseEntity
    {
        public DeferPeriod() {
            Code = "DEF-PER" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Active { get; set; }
    }
}
