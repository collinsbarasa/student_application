﻿using System;
using System.Collections.Generic;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
	public class Intake : BaseEntity
	{
		public Intake()
		{
			Code = "INT-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
			Status = EntityStatus.Inactive;
		}
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime StartDate { get; set; }
		public string StartDateStr => StartDate.ToString("dd-MMM-yyyy");
		public DateTime EndDate { get; set; }
		public string AcademicYear { get; set; }
		public string EndDateStr => EndDate.ToString("dd-MMM-yyyy");
		public virtual List<IntakeGradeType> IntakeGradeTypes { get; set; }
		public bool IsActive => Status.Equals(EntityStatus.Active) && EndDate.Date >= DateTime.Now.Date;
	}
}
