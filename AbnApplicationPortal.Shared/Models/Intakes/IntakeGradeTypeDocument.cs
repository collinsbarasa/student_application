﻿using System;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
	public class IntakeGradeTypeDocument : BaseEntity
	{
		public Guid DocumentId { get; set; }
	}
}