﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
	public class IntakeGradeType : BaseEntity
	{
		public Guid IntakeId { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal ApplicationFee { get; set; }
		public int MaxUnits { get; set; }
		public string Notes { get; set; }
		public Guid GradeTypeId { get; set; }
		public virtual List<IntakeGradeTypeClassType> IntakeGradeTypeClassTypes { get; set; }
		public virtual List<IntakeGradeTypeDocument> IntakeGradeTypeDocuments { get; set; }
		public virtual List<IntakeGradeTypeCampus> IntakeGradeTypeCampuses { get; set; }
		public virtual List<IntakeGradeTypeProgramme> IntakeGradeTypeProgrammes { get; set; }

	}

	public class IntakeGradeTypeClassType : BaseEntity
	{
		public Guid ClassTypeId { get; set; }
	}
}
