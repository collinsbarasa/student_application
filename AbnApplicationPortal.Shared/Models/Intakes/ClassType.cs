﻿using System;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
    public class ClassType : BaseEntity
    {
        public ClassType()
        {
            Code = "CL-TYPE-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UniId { get; set; }
    }
}
