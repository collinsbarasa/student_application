﻿using System;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
	public class IntakeGradeTypeCampus : BaseEntity
	{
		public Guid CampusId { get; set; }
	}
}
