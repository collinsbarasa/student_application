﻿using System;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
	public class Campus : BaseEntity
	{
		public Campus()
		{
			Code = "CAMP-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
		}
		public string Name { get; set; }
		public string Description { get; set; }
		public string Contact { get; set; }
		public string Address { get; set; }
		public string Telephone { get; set; }
		public string Email { get; set; }
		public string Website { get; set; }
		public int UniId { get; set; }
	}
}
