﻿namespace AbnApplicationPortal.Shared.Models.Intakes
{
	public class ProgrammeCluster : BaseEntity
	{
		public string ProgCode { get; set; }
		public string Cluster { get; set; }
		public string Subject { get; set; }
		public string ExamType { get; set; }
		public string SubjectCode { get; set; }
		public string Grading { get; set; }
		public string MinValue { get; set; }
		public string Choice { get; set; }
		public string Notes { get; set; }
		public bool? Closed { get; set; }
	}
}
