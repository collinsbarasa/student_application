﻿using System;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
	public class Programme : BaseEntity
	{
		public Programme()
		{
			Code = "PROG-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
		}
		public string Name { get; set; }
		public string Notes { get; set; }
		public string Requirements { get; set; }
		public string Period { get; set; }
		public string GradeType { get; set; }
		public string CertType { get; set; }
		public string Department { get; set; }
		public int UniId { get; set; }
	}
}
