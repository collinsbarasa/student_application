﻿using System;

namespace AbnApplicationPortal.Shared.Models.Intakes
{
	public class GradeCertificateType : BaseEntity
	{
		public GradeCertificateType()
		{
			Code = "GR-" + DateTime.UtcNow.ToString("HHmmssffff");
		}
		public string CertType { get; set; }
		public string GradeType { get; set; }
		public string Notes { get; set; }
	}
}
