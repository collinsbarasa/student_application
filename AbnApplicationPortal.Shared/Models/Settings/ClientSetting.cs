﻿using System;

namespace AbnApplicationPortal.Shared.Models.Settings
{
	public class ClientSetting : BaseEntity
	{
		public ClientSetting()
		{
			Code = "SYS-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
			HasUnisol = true;
			SimpleProfile = false;
		}
		public string AppName { get; set; }
		public string AppVersion { get; set; }
		public string ClientName { get; set; }
		public string ClientCode { get; set; }
		public bool HasUnisol { get; set; }
		public bool SimpleProfile { get; set; }
		public string AppRefPrefix { get; set; }
		public string TagLine { get; set; }
		public string LogoUrl { get; set; }
		public string PrimaryColor { get; set; }
		public string SecondaryColor { get; set; }

		public string ImageUrl {get; set;}
		public bool AllowPartialPayments { get; set; }
		public string ContactEmail { get; set; }
		public bool IsSaga { get; set; }
		public string IndexNoLabel => IsSaga ? "TSC No, PF No or ID No." : "KCSE Index No.";
		public bool ViewSchoolOnly { get; set; }
	}
}
