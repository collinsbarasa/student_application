﻿using System;
namespace AbnApplicationPortal.Shared.Models.Settings
{
    public class Category : BaseEntity
    {
        public Category() { 
            Code =  "CAT-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Owner { get; set; }
    }
}
