﻿namespace AbnApplicationPortal.Shared.Models.Settings
{
	public class LayoutSetting : BaseEntity
	{
		public string HomePage { get; set; }
		public string CreateAccount { get; set; }
		public string FindCourse { get; set; }
		public string SubmitApplication { get; set; }
		public string ContactUs { get; set; }
		public string RecommendedCourses { get; set; }
		public string CurrentIntakes { get; set; }
	}
}
