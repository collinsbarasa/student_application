﻿using System;

namespace AbnApplicationPortal.Shared.Models.Settings
{
	public class Setting : BaseEntity
	{
		public Setting()
		{

			Code = "SET-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
		}
		public SettingKey Key { get; set; }
		public string Data { get; set; }
	}

	public enum SettingKey
	{
		Email,
		Sms,
		Mpesa,
		Soap
	}
}
