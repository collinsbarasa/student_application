﻿using System;

namespace AbnApplicationPortal.Shared.Models.Enquiry
{
    public class Enquiry : BaseEntity
    {
        public Enquiry() {
            Code = "ENQ-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }

        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Guid CategoryId { get; set; }
        public string Message { get; set; }
        public string FollowUpCode { get; set; }
    }
}
