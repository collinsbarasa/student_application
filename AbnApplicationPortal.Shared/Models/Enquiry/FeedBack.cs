﻿using System;

namespace AbnApplicationPortal.Shared.Models.Enquiry
{
    public class FeedBack : BaseEntity
    {
        public FeedBack() {
            Code = "FDK-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }
        public Guid EnquiryId { get; set; }
        public string Message { get; set; }
        public string Personel { get; set; }
    }
}
