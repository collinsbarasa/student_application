﻿namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class UserTitle : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
