﻿using System;
using System.Collections.Generic;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class Application : BaseEntity
	{
		public Application()
		{
			IsUploaded = false;
			IsFullyPaid = false;
			Status = EntityStatus.Pending;
		}
		public string Names { get; set; }
		public string NationalId { get; set; }
		public string IdDocument { get; set; }
		public DateTime Dob { get; set; }
		public string DobStr => Dob.ToString("dd-MMM-yyyy");
		public string Gender { get; set; }
		public string Marital { get; set; }
		public string Nationality { get; set; }
		public string TelNo { get; set; }
		public string HomeAddress { get; set; }
		public string Email { get; set; }
		public string Source { get; set; }
		public string Religion { get; set; }
		public string Language { get; set; }
		public string EMName { get; set; }
		public string EMRel { get; set; }
		public string EMTel { get; set; }
		public string EMEmail { get; set; }
		public string EMAddress { get; set; }
		public string EMRemarks { get; set; }
		public string KinName { get; set; }
		public string KinRel { get; set; }
		public string KinTel { get; set; }
		public string KinEmail { get; set; }
		public string KinAddress { get; set; }
		public string KinRemarks { get; set; }
		public string Special { get; set; }
		public string Activity { get; set; }
		public string PrevAdmnNo { get; set; }
		public string Notes { get; set; }
		public string Sponsor { get; set; }
		public string Disability { get; set; }
		public string IndexNo { get; set; }
		public string County { get; set; }
		public string District { get; set; }
		public string Constituency { get; set; }
		public string SubCounty { get; set; }
		public bool IsFullyPaid { get; set; }
		public bool IsUploaded { get; set; }
		public string GradeType { get; set; }
		public Guid GradeTypeId { get; set; }
		public string StudyMode { get; set; }
		public Guid ClassTypeId { get; set; }
		public string Campus { get; set; }
		public string KcseYear { get; set; }
		public string KcpeYear { get; set; }
		public string KcpeIndexNo { get; set; }
		public string KcseQualification { get; set; }
		public string Race { get; set; }
		public Guid CampusId { get; set; }
		public Guid IntakeId { get; set; }
		public DateTime? DateApproved { get; set; }
		public string ApprovedBy { get; set; }
		public List<ApplicationProgramme> ApplicationProgrammes { get; set; }
		public List<ApplicationPayment> ApplicationPayments { get; set; }
		public List<ApplicationWaiver> ApplicationWaivers { get; set; }
		public List<ApplicationComment> ApplicationComments { get; set; }
		public List<ApplicationUpload> ApplicationUploads { get; set; }
		public string UserId { get; set; }
	}
}
