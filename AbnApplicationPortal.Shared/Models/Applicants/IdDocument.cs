﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class IdDocument : BaseEntity
    {
        public IdDocument()
        {

            Code = "ID-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
