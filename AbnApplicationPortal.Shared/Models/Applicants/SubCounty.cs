﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class SubCounty : BaseEntity
    {
        public SubCounty()
        {
            Code = "SUB-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public string Name { get; set; }
    }
}
