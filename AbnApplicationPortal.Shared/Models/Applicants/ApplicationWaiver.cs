﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class ApplicationWaiver : BaseEntity
	{
		public Guid ApplicationId { get; set; }
		public string AppRef { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		public string Notes { get; set; }
	}
}
