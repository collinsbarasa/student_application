﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class Religion : BaseEntity
    {
        public Religion()
        {

            Code = "REL-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
