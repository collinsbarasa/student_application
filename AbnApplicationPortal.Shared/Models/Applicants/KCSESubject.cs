﻿namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class KcseSubject : BaseEntity
    {
        public KcseSubject()
        {
            IsCompulsory = false;
            IsMeanGrade = false;
        }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public bool IsCompulsory { get; set; }
        public bool IsMeanGrade { get; set; }
    }
}
