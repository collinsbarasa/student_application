﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class Notification : BaseEntity
	{
		public Notification()
		{
			Code = "NOT-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
			IsRead = false;
		}
		public string Message { get; set; }
		public string Title { get; set; }
		public bool IsRead { get; set; }
		public string UserId { get; set; }
	}
}
