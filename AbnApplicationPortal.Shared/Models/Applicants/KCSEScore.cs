﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class KcseScore : BaseEntity
	{
		public KcseScore()
		{
			IsMeanGrade = false;
		}
		public Guid SubjectId { get; set; }
		public string SubjectName { get; set; }
		public string SubjectCode { get; set; }
		public string Grade { get; set; }
		public Guid GradeId { get; set; }
		public int Rank { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Points { get; set; }
		public bool IsMeanGrade { get; set; }
		public string UserId { get; set; }
	}
}
