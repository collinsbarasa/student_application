﻿using System.Collections.Generic;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class County : BaseEntity
    {
        public County()
        {
            SubCounties = new List<SubCounty>();
        }
        public string Name { get; set; }
        public List<SubCounty> SubCounties { get; set; }
    }
}
