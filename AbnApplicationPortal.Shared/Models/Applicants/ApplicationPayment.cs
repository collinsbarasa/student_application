﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using AbnApplicationPortal.Shared.Responses;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class ApplicationPayment : BaseEntity
	{
		public ApplicationPayment()
		{
			Code = "PAY-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
			IsCompleted = false;
		}
		public ApplicationPayment(StkPayResponse response)
		{
			Code = "PAY-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
			IsCompleted = response.IsCompleted;
			MerchantRequestID = response.MerchantRequestID;
			CheckoutRequestID = response.CheckoutRequestID;
			ResultCode = response.ResultCode;
			ResultDesc = response.ResultDesc;
			MpesaReceiptNumber = response.ReceiptNumber;
			TransactionDate = response.TransactionDate;
			PhoneNumber = response.PhoneNumber;
		}
		public string Reference { get; set; }
		public string AppRef { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Amount { get; set; }
		public Guid ApplicationId { get; set; }
		public Guid IntakeId { get; set; }
		public bool IsCompleted { get; set; }
		public string MerchantRequestID { get; set; }
		public string CheckoutRequestID { get; set; }
		public int ResultCode { get; set; }
		public string ResultDesc { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Balance { get; set; }
		public string MpesaReceiptNumber { get; set; }
		public string TransactionDate { get; set; }
		public string PhoneNumber { get; set; }
		public string UserId { get; set; }
	}
}
