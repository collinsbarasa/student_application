﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class ApplicationUpload : BaseEntity
	{
		public ApplicationUpload()
		{
			Code = "UP-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
		}
		public string Name { get; set; }
		public string Description { get; set; }
		public string Path { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Size { get; set; }
		public Guid DocumentId { get; set; }
		public string Extension { get; set; }
	}
}
