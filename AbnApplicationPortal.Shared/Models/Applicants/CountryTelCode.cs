﻿namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class CountryTelCode : BaseEntity
    {
        public string Name { get; set; }
        public string DialCode { get; set; }
    }
}
