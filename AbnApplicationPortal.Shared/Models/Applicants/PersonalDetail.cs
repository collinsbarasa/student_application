﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class PersonalDetail : BaseEntity
	{
		public PersonalDetail()
		{
			Code = "PER-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
		}
		public string Title { get; set; }
		public string Gender { get; set; }
		public string PhoneNumber { get; set; }
		public string AlternatePhoneNumber { get; set; }
		public string PostalAddress { get; set; }
		public string AddressName { get; set; }
		public string Town { get; set; }
		public string Ethnicity { get; set; }
		public string Language { get; set; }
		public DateTime DateOfBirth { get; set; }
		public string DateOfBirthStr => DateOfBirth.ToString("dd-MMM-yyyy");
		public string NationalityName { get; set; }
		public string MaritalStatusName { get; set; }
		public string ReligionName { get; set; }
		public string IdDocumentName { get; set; }
		public string IdDocumentNumber { get; set; }
		public string CountyName { get; set; }
		public string SubCountyName { get; set; }
		public string Constituency { get; set; }
		public string DisabilityCondition { get; set; }
		public string UserId { get; set; }
		public string IndexNo { get; set; }
		public string Activities { get; set; }
		public string Special { get; set; }
		public string District { get; set; }
		public string KcseYear { get; set; }
		public string KcpeYear { get; set; }
		public string KcpeIndexNo { get; set; }
		public string KcseQualification { get; set; }
		public string Institution { get; set; }
	}
}