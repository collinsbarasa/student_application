﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
   public class WorkExperience : BaseEntity
    {
        public WorkExperience()
        {
            Code = "WORK-EXP-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public string Employer { get; set; }
        public string Designation { get; set; }
        public string AssignmentNature { get; set; }
        public DateTime StartDate { get; set; }
        public string StartDateStr => StartDate.ToString("dd-MMM-yyyy");
        public DateTime EndDate { get; set; }
        public string EndDateStr => EndDate.ToString("dd-MMM-yyyy");
        public string UserId { get; set; }
    }
}
