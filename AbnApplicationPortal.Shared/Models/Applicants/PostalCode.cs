﻿namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class PostalCode : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
