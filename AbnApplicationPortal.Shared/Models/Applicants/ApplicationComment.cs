﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class ApplicationComment : BaseEntity
	{
		public string UserId { get; set; }
		public string Content { get; set; }
		public Guid ApplicationId { get; set; }
	}
}
