﻿using System;
namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class EducationExperience : BaseEntity
	{
		public EducationExperience()
		{
			Code = "EDU-EXP-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
		}
		public Guid EducationLevelId { get; set; }
		public string EducationLevel { get; set; }
		public int EducationLevelRank { get; set; }
		public string CourseStudied { get; set; }
		public string Institution { get; set; }
		public string Grade { get; set; }
		public DateTime StartDate { get; set; }
		public string StartDateStr => StartDate.ToString("dd-MMM-yyyy");
		public DateTime EndDate { get; set; }
		public string EndDateStr => EndDate.ToString("dd-MMM-yyyy");
		public string UserId { get; set; }
		public string Notes { get; set; }
		public EducationQualificationType QualificationType { get; set; }
		public string IndexNo { get; set; }
	}

	public enum EducationQualificationType
	{
		ACADEMIC,
		PROFESSIONAL
	}
}
