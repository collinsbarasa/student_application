﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class KCSECertificate : BaseEntity
	{
		public string Description { get; set; }
		public string Extension { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Size { get; set; }
		public string PathUrl { get; set; }
		public string UserId { get; set; }
	}
}
