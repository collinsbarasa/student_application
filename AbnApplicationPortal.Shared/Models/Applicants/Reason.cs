﻿
namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class Reason : BaseEntity
    {
        public string Description { get; set; }
    }
}
