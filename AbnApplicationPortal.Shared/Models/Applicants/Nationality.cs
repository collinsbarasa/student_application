﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class Nationality : BaseEntity
    {
        public Nationality()
        {

            Code = "NAT-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
