﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class RelationShip : BaseEntity
    {
        public RelationShip()
        {
            Code = "RSHP-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
