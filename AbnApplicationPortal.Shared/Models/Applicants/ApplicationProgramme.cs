﻿using AbnApplicationPortal.Shared.Models.Intakes;
using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
	public class ApplicationProgramme : BaseEntity
	{
		public ApplicationProgramme()
		{
			Code = "APP-PROG-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
			IsApproved = false;
			IntakeProgrammeId = Id;
		}
		public bool IsApproved { get; set; }
		public bool IsMain { get; set; }
		public Guid IntakeProgrammeId { get; set; }
		public Guid ProgrammeId { get; set; }
		public virtual Programme Programme { get; set; }
	}

}
