﻿namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class MaritalStatus : BaseEntity
    {   
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
