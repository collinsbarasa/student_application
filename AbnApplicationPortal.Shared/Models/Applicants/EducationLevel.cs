﻿using System;

namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class EducationLevel : BaseEntity
    {
        public EducationLevel()
        {
            Code = "EDU-LEVEL-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Rank { get; set; }
    }
}
