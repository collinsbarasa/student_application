﻿namespace AbnApplicationPortal.Shared.Models.Applicants
{
    public class KcseGrade : BaseEntity
    {
        public string Name { get; set; }
        public int Rank { get; set; }
    }
}
