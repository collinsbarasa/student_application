﻿using System;

namespace AbnApplicationPortal.Shared.Models
{
    public class RejectReason : BaseEntity
    {
        public RejectReason()
        {
            Code = "REAS-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
