﻿using System;

namespace AbnApplicationPortal.Shared.Models
{
	public class BaseEntity
	{
		public BaseEntity()
		{
			Id = Guid.NewGuid();
			DateCreated = DateUpdated = DateTime.Now;
			Status = EntityStatus.Active;
		}
		public Guid Id { get; set; }
		public string Code { get; set; }
		public EntityStatus Status { get; set; }
		public string StatusStr => Status.ToString();
		public DateTime DateCreated { get; set; }
		public string DateCreatedStr => DateCreated.ToString("dd-MMM-yyyy");
		public DateTime DateUpdated { get; set; }
		public string DateUpdatedStr => DateUpdated.ToString("dd-MMM-yyyy");
		public string Personnel { get; set; }

	}

	public enum EntityStatus
	{
		Inactive,
		Active,
		Pending,
		Approved,
		Declined,
		Accepted,
		Defered,
		DeferDeclined,
		InProgress,
		Completed,
		Resolved
	}
}
