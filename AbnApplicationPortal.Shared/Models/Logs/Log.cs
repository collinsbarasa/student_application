﻿using System;

namespace AbnApplicationPortal.Shared.Models.Logs
{
	public class Log : BaseEntity
	{
		public Log()
		{
			Success = false;
			Code = "LOG-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
		}
		public Guid ApplicationId { get; set; }
		public LogKey Key { get; set; }
		public string KeyStr => Key.ToString();
		public string Message { get; set; }
		public bool Success { get; set; }
		public string UserId { get; set; }
	}

	public enum LogKey
	{
		AccountLogin,
		AccountCreate,
		AccountLogout,
		AccountConfirm,
		AccountChangePassword,
		AccountResetPassword,
		AccountUpdate,
		ApplicationPayment,
		ProfileUpdate,
		ApplicationAdd,
		ApplicationStatus
	}
}
