﻿using System;

namespace AbnApplicationPortal.Shared.Models.Logs
{
    public class SmsSentLog : BaseEntity
    {
        public SmsSentLog()
        {
            Success = false;
            Code = "SMS-" + DateTime.UtcNow.ToString("yyyyMMddHHmmssffff");
        }
        public bool Success { get; set; }
        public string Response { get; set; }
        public string Recipients { get; set; }
        public string Message { get; set; }
    }
}
