﻿using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Shared.Models;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Models.Logs;
using AbnApplicationPortal.Shared.Models.Settings;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AbnApplicationPortal.Core.Models.IntakeSettingsVm;
using AbnApplicationPortal.Shared.Models.Enquiry;

namespace AbnApplicationPortal.Core.Data
{
	public class AbnApplicationPortalContext : IdentityDbContext<ApplicationUser>
	{
		public AbnApplicationPortalContext(DbContextOptions<AbnApplicationPortalContext> options) : base(options)
		{

		}
		public DbSet<PersonalDetail> PersonalDetails { get; set; }
		public DbSet<Log> Logs { get; set; }
		public DbSet<IdDocument> IdDocuments { get; set; }
		public DbSet<MaritalStatus> MaritalStatuses { get; set; }
		public DbSet<Nationality> Nationalities { get; set; }
		public DbSet<RelationShip> RelationShips { get; set; }
		public DbSet<Religion> Religions { get; set; }
		public DbSet<County> Counties { get; set; }
		public DbSet<SubCounty> SubCounties { get; set; }
		public DbSet<CountryTelCode> CountryTelCodes { get; set; }
		public DbSet<Setting> Settings { get; set; }
		public DbSet<EducationLevel> EducationLevels { get; set; }
		public DbSet<Campus> Campuses { get; set; }
		public DbSet<Intake> Intakes { get; set; }
		public DbSet<Programme> Programmes { get; set; }
		public DbSet<ProgrammeCluster> ProgrammeClusters { get; set; }
		public DbSet<ClassType> ClassTypes { get; set; }
		public DbSet<GradeCertificateType> GradeCertificateTypes { get; set; }
		public DbSet<Application> Applications { get; set; }
		public DbSet<ApplicationProgramme> ApplicationProgrammes { get; set; }
		public DbSet<ApplicationPayment> ApplicationPayments { get; set; }
		public DbSet<ApplicationWaiver> ApplicationWaivers { get; set; }
		public DbSet<ApplicationUpload> ApplicationUploads { get; set; }
		public DbSet<Document> Documents { get; set; }
		public DbSet<IntakeGradeType> IntakeGradeTypes { get; set; }
		public DbSet<IntakeGradeTypeCampus> IntakeGradeTypeCampuses { get; set; }
		public DbSet<IntakeGradeTypeProgramme> IntakeGradeTypeProgrammes { get; set; }
		public DbSet<Notification> Notifications { get; set; }
		public DbSet<RejectReason> RejectReasons { get; set; }
		public DbSet<KcseScore> KcseScores { get; set; }
		public DbSet<KcseGrade> KcseGrades { get; set; }
		public DbSet<KcseSubject> KcseSubjects { get; set; }
		public DbSet<SmsSentLog> SmsSentLogs { get; set; }
		public DbSet<ClientSetting> ClientSettings { get; set; }
		public DbSet<NextOfKin> NextOfKins { get; set; }
		public DbSet<Level> Levels { get; set; }
		public DbSet<EmergencyContact> EmergencyContacts { get; set; }
		public DbSet<EducationExperience> EducationExperiences { get; set; }
		public DbSet<WorkExperience> WorkExperiences { get; set; }
		public DbSet<PostalCode> PostalCodes { get; set; }
		public DbSet<UserTitle> UserTitles { get; set; }
		public DbSet<LayoutSetting> LayoutSettings { get; set; }
		public DbSet<ApplicationComment> ApplicationComments { get; set; }
		public DbSet<KCSECertificate> KCSECertificates { get; set; }
		public DbSet<MediaSource> MediaSources { get; set; }
		public DbSet<DisabilityCondition> DisabilityConditions { get; set; }
		public DbSet<ClientImage> ClientImages { get; set; }
		public DbSet<Reason> Reasons { get; set; }
		public DbSet<Deferment> Deferments { get; set; }
		public DbSet<Enquiry> Enquiries { get; set; }
		public DbSet<FeedBack> FeedBacks { get; set; }
		public DbSet<Category> Categories { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<Setting>(entity =>
			{
				entity.HasIndex(e => e.Key).IsUnique();
			});
			modelBuilder.Entity<KcseGrade>(entity =>
			{
				entity.HasIndex(e => e.Name).IsUnique();
			});
		}

		public DbSet<DeferPeriod> DeferPeriods { get; set; }

		public DbSet<AbnApplicationPortal.Core.Models.IntakeSettingsVm.DocumentViewModel> DocumentViewModel { get; set; }
	}
}
