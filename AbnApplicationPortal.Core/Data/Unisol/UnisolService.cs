﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using AbnApplicationPortal.Core.Data.Soap;
using AbnApplicationPortal.Core.Data.Unisol.Entities;
using AbnApplicationPortal.Core.Data.Unisol.Responses;
using AbnApplicationPortal.Shared.Models;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Models.Settings;
using AbnApplicationPortal.Shared.Requests.Settings;
using AbnApplicationPortal.Shared.Responses;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace AbnApplicationPortal.Core.Data.Unisol
{
	public class UnisolService
	{
		private readonly UnisolDbContext _context;
		private readonly AbnApplicationPortalContext _portalContext;
		public UnisolService(UnisolDbContext context, AbnApplicationPortalContext portalContext)
		{
			_context = context;
			_portalContext = portalContext;
		}

		public UnisolResponse<List<UniCampus>> Campuses()
		{
			var result = new UnisolResponse<List<UniCampus>>();
			try
			{
				result.Data = _context.Campuses.Where(e => !e.Closed).ToList();
				result.Success = true;
				result.Message = "Campuses Found";
				return result;
			}
			catch (Exception e)
			{
				result.Message = "Failed to get Campuses";
				result.Error = e.Message;
				return result;
			}
		}

		public UnisolResponse<List<UniClassType>> ClassTypes()
		{
			var result = new UnisolResponse<List<UniClassType>>();
			try
			{
				result.Data = _context.ClassType.Where(e => !e.Closed).ToList();
				result.Success = true;
				result.Message = "Class Types Found";
				return result;
			}
			catch (Exception e)
			{
				result.Message = "Failed to get Class Types";
				result.Error = e.Message;
				return result;
			}
		}

		public UnisolResponse<List<UniCertificateType>> GradeTypes()
		{
			var result = new UnisolResponse<List<UniCertificateType>>
			{
				Data = new List<UniCertificateType>()
			};
			try
			{
				var grades = _context.Programme
					.Where(p => !string.IsNullOrEmpty(p.CertType))
					.Select(e => e.CertType)
					.Distinct()
					.Select(g => new UniCertificateType { GradeType = g, CertType = g })
					.ToList();
				result.Data = grades;
				result.Success = true;
				result.Message = "Grade Types Found";
				return result;
			}
			catch (Exception e)
			{
				result.Message = "Failed to get Grade Types";
				result.Error = e.Message;
				return result;
			}
		}
		public UnisolResponse<List<Guid>> UploadList(List<Application> applications, string hostingPath)
		{
			var result = new UnisolResponse<List<Guid>>();
			try
			{
				var settings = _portalContext.Settings
					.Where(s => s.Key.Equals(SettingKey.Mpesa) || s.Key.Equals(SettingKey.Soap))
					.ToList();
				result.Data = new List<Guid>();
				applications.ForEach(async a =>
				{
					var res = await AddApplicantAsync(a, settings, hostingPath);
					if (res.Success)
						result.Data.Add(a.Id);
				});
				result.Success = result.Data.Any();
				result.Message = $"{result.Data.Count} Applicants Uploaded";
				return result;
			}
			catch (Exception e)
			{
				result.Message = "Failed to add Applicants";
				result.Error = e.Message;
				return result;
			}
		}

		public UnisolResponse<List<UniProgramme>> Programmes(string gradeType = "")
		{
			var result = new UnisolResponse<List<UniProgramme>>();
			try
			{
				result.Data = _context.Programme.Where(e => !e.Closed
															&& e.GradeType.Contains(gradeType))
					.ToList();
				result.Success = true;
				result.Message = "Programmes Found";
				return result;
			}
			catch (Exception e)
			{
				result.Message = "Failed to get Programmes";
				result.Error = e.Message;
				return result;
			}
		}

		public async Task<UnisolResponse<UniApplicant>> AddApplicantAsync(Application application, List<Setting> settings, string hostingPath)
		{
			var result = new UnisolResponse<UniApplicant>();
			try
			{
				var anyApp = _context.Applicant
					.Any(a => a.email.Equals(application.Email)
					&& a.IndexNo.Equals(application.IndexNo) && a.RYear.Equals($"{DateTime.Now.Year}")
					 && a.RMonth.Equals($"{DateTime.Now.Month}"));
				if (anyApp)
				{
					result.Message = "Application already Uploaded";
					return result;
				}
				var uniApp = GetUniApplicant(application);
				if (string.IsNullOrEmpty(uniApp.Programme))
				{
					result.Message = "Programme not Found";
					return result;
				}
				_context.Applicant.Add(uniApp);
				_context.SaveChanges();

				var soapSetting = settings.FirstOrDefault(s => s.Key.Equals(SettingKey.Soap));
				if (soapSetting != null)
				{
					var mpesaSetting = settings.FirstOrDefault(s => s.Key.Equals(SettingKey.Mpesa));
					var mpesa = JsonConvert.DeserializeObject<MpesaSetting>(mpesaSetting.Data);
					var amount = application.ApplicationPayments
						.Where(p => p.IsCompleted)
						.Sum(p => p.Amount);
					if (amount > 0 && string.IsNullOrEmpty(mpesa.AbnClientId))
					{
						var payRes = await ProcessApplicantFeesAsync(uniApp.Ref, (double)amount, soapSetting);
						var data = payRes.Data;
					}
				}
				await UploadApplicatantEducationAsync(uniApp.Ref, application.UserId);
				var albumPath = _context.SysSetups.Select(s => s.AlbumPath).FirstOrDefault();
				if (albumPath != null)
				{
					foreach (var upload in application.ApplicationUploads)
					{
						var upRes = UploadApplicantFile(uniApp.Ref, albumPath, hostingPath, upload);
					}
				}

				result.Success = true;
				result.Message = "Applicant Added";
				result.Data = uniApp;
				return result;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				result.Message = "Failed to add Applicant";
				result.Error = e.Message;
				return result;
			}
		}

		public async Task<UnisolResponse<string>> AddOnlineApplicantAsync(Application application)
		{
			var res = new UnisolResponse<string>();
			try
			{
				var onlineApp = new UniOnlineApplicant(application);
				var any = await _context.OnlineApplicants.AnyAsync(a => a.Ref.Equals(onlineApp.Ref));
				if (any)
				{
					res.Message = $"Applicant {onlineApp.Ref} already exists";
					return res;
				}

				_context.OnlineApplicants.Add(onlineApp);
				await _context.SaveChangesAsync();
				res.Success = true;
				res.Message = $"Applicant {onlineApp.Ref} Added";
				res.Data = onlineApp.Ref;
				return res;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error adding unisol online applicant :- {ex}");
				res.Error = ex.Message;
				res.Message = "Error occurred. Try again later";
				return res;
			}
		}

		public async Task<UnisolResponse<List<StkPayResponse>>> GetApplicantReceiptsAsync(string appCode)
		{
			var response = new UnisolResponse<List<StkPayResponse>> { Data = new List<StkPayResponse>() };
			try
			{
				var data = await _context.Receipts.OrderByDescending(r => r.Rdate)
					.Join(_context.ReceiptDetails.Where(d => d.Notes.Equals(appCode)),
					r => r.ReceiptNo, dt => dt.ReceiptNo, (r, dt) =>
						  new StkPayResponse
						  {
							  IsCompleted = true,
							  Amount = dt.Amount,
							  ReceiptNumber = r.ModeNumber,
							  ResultCode = 1,
							  ResultDesc = dt.Account,
							  TransactionDate = r.Rdate.ToString(),
							  PhoneNumber = r.PayMode,
							  CheckoutRequestID = r.ReceiptNo,
							  MerchantRequestID = dt.Id.ToString()
						  }
					).ToListAsync();
				response.Success = data.Any();
				response.Message = response.Success ? "Found" : "Not Found";
				if (response.Success)
					response.Data = data;
				return response;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"{ex}");
				response.Error = ex.Message;
				response.Message = "Error Ocurred.Try later";
				return response;
			}
		}

		private UniApplicant GetUniApplicant(Application application)
		{
			var programme = application.ApplicationProgrammes
				.FirstOrDefault(a => a.IsApproved);
			if (programme == null)
				return new UniApplicant();

			var uniApp = new UniApplicant
			{
				Ref = GenerateUniRef(),
				AdmnNo = string.Empty,
				names = application.Names ?? "",
				nationalID = application.NationalId ?? "",
				DOB = application.Dob,
				gender = application.Gender ?? "",
				Marital = application.Marital ?? "",
				nationality = application.Nationality ?? "",
				telno = application.TelNo ?? "",
				County = application.County ?? "",
				District = application.District ?? "",
				homeaddress = application.HomeAddress ?? "",
				email = application.Email ?? "",
				Status = "Pending",
				Source = application.Source ?? "",
				religion = application.Religion ?? "",
				Programme = programme.Programme.Name ?? "",
				StudyMode = application.StudyMode ?? "",
				Campus = application.Campus ?? "",
				Language = application.Language ?? "",
				EMName = application.EMName ?? "",
				EMRel = application.EMRel ?? "",
				EMTel = application.EMTel ?? "",
				EMEmail = application.EMEmail ?? "",
				EMAddress = application.EMAddress ?? "",
				EMRemarks = application.EMRemarks ?? "",
				special = application.Special ?? "",
				activity = application.Activity ?? "",
				prevadmnno = application.PrevAdmnNo ?? "",
				notes = application.Notes ?? "",
				createdate = DateTime.Now.Date,
				personnel = application.Personnel,
				Sponsor = application.Sponsor ?? "",
				Disability = application.Disability ?? "",
				IndexNo = application.IndexNo ?? "",
				Constituency = application.Constituency ?? "",
				SubCounty = application.SubCounty ?? "",
				KcpeIndexNo = application.KcpeIndexNo ?? "",
				KcpeYear = application.KcpeYear ?? "",
				KcseYear = application.KcseYear ?? ""
			};
			return uniApp;

		}

		public UnisolResponse<List<ProgrammeCluster>> ProgrammeClusters(string programmeCode)
		{
			var result = new UnisolResponse<List<ProgrammeCluster>>
			{
				Data = new List<ProgrammeCluster>()
			};
			try
			{
				var clusters = _context.ProgClusterDetails
					.Where(pc => pc.ProgCode.Equals(programmeCode))
					.Select(pc => new ProgrammeCluster
					{
						ProgCode = pc.ProgCode,
						Code = pc.ID + "-" + pc.Code,
						Choice = pc.Choice,
						Cluster = pc.Cluster,
						ExamType = pc.ExamType,
						Grading = pc.Grading,
						SubjectCode = pc.Code,
						Subject = pc.Subject,
						MinValue = pc.MinValue,
						Notes = pc.Notes
					})
					.ToList();
				if (clusters.Any())
				{
					result.Message = "Clusters found";
					result.Success = true;
					result.Data = clusters;
					return result;
				}

				result.Message = "No clusters found";
				return result;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				result.Message = "Failed to get clusters";
				result.Error = e.Message;
				return result;
			}
		}

		public UnisolResponse<UniStudent> GetStudentByRefOrEmail(string text)
		{
			var result = new UnisolResponse<UniStudent>();
			try
			{
				UniStudent student;
				var iQueryable = _context.Students
					.Join(_context.Programme, s => s.Programme, p => p.Names, (s, p) =>
			  new UniStudent
			  {
				  AdmnNo = s.AdmnNo,
				  Names = s.Names,
				  NationalId = s.NationalId,
				  Closed = s.Closed,
				  County = s.County,
				  TelNo = s.TelNo,
				  Email = s.Email,
				  Sponsor = s.Sponsor,
				  Gender = s.Gender,
				  DOB = s.DOB,
				  Disability = s.Disability,
				  Programme = s.Programme,
				  Department = p.Department
			  });

				if (IsValidEmail(text))
					student = iQueryable.FirstOrDefault(s => s.Email.Contains(text));
				else
					student = iQueryable.FirstOrDefault(s => s.AdmnNo.Contains(text));

				result.Success = student != null;
				result.Message = result.Success ? "Found" : "Not Found";
				result.Data = student;
				return result;
			}
			catch (Exception e)
			{
				result.Message = $"Failed to get student {text}";
				result.Error = e.Message;
				return result;
			}
		}

		public UnisolResponse<UniEmployee> GetEmployeeByRefOrEmail(string text)
		{
			var result = new UnisolResponse<UniEmployee>();
			try
			{
				UniEmployee employee;
				var iQueryable = _context.Employees.Select(s => new UniEmployee
				{
					EmpNo = s.EmpNo,
					Names = s.Names,
					IdNo = s.IdNo,
					Terminated = s.Terminated,
					County = s.County,
					Cell = s.Cell,
					Wemail = s.Wemail,
					Gender = s.Gender,
					Disability = s.Disability,
					Department = s.Department
				});
				if (IsValidEmail(text))
					employee = iQueryable.FirstOrDefault(s => s.Wemail.ToLower().Contains(text.ToLower()));
				else
					employee = iQueryable.FirstOrDefault(s => s.EmpNo.ToLower().Contains(text.ToLower()));

				result.Success = employee != null;
				result.Message = result.Success ? "Found" : "Not Found";
				result.Data = employee;
				return result;
			}
			catch (Exception e)
			{
				result.Message = $"Failed to get employee {text}";
				result.Error = e.Message;
				return result;
			}
		}

		public UnisolResponse<UniSysSetup> SysSetup()
		{
			var result = new UnisolResponse<UniSysSetup>();
			try
			{
				result.Data = _context.SysSetups.FirstOrDefault();
				result.Success = true;
				result.Message = result.Data == null ? "Not Found" : "Found";
				return result;
			}
			catch (Exception e)
			{
				Console.WriteLine($"error getting sys setup :- {e}");
				result.Message = "Failed to get sys setup";
				result.Error = e.Message;
				return result;
			}
		}

		public UnisolResponse<string> GetSchool(string department)
		{
			var result = new UnisolResponse<string>();
			try
			{
				result.Data = _context.Departments.FirstOrDefault(x=>x.Names == department)?.School;
				result.Success = true;
				result.Message = result.Data == null ? "Not Found" : "Found";
				return result;
			}
			catch (Exception e)
			{
				Console.WriteLine($"error getting sys setup :- {e}");
				result.Message = "Failed to get sys setup";
				result.Error = e.Message;
				return result;
			}
		}

		public async Task<UnisolResponse<List<string>>> SchoolDepartmentsAsync(string school)
		{
			var response = new UnisolResponse<List<string>> { Data = new List<string>() };
			try
			{
				var data = await _context.Departments
				.Where(c => !string.IsNullOrEmpty(c.School) && c.School.Equals(school))
				.Select(s => s.Names)
				.ToListAsync();
				response.Success = data.Any();
				response.Message = response.Success ? "Found" : "Not Found";
				if (response.Success)
					response.Data = data;
				return response;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				response.Message = "Error ocurred. Try later";
				response.Error = ex.Message;
				return response;
			}
		}
		public async Task<UnisolResponse<List<string>>> DepartmentsAsync(string department)
		{
			var response = new UnisolResponse<List<string>> { Data = new List<string>() };
			try
			{
				var departmentSchool = _context.Departments.Where(d => d.Names.Equals(department))
					.Select(d => d.School).FirstOrDefault();
				if (!string.IsNullOrEmpty(departmentSchool))
					return await SchoolDepartmentsAsync(departmentSchool);

				var data = await _context.Departments
				.Where(c => !string.IsNullOrEmpty(c.School))
				.Select(s => s.Names)
				.ToListAsync();
				response.Success = data.Any();
				response.Message = response.Success ? "Found" : "Not Found";
				if (response.Success)
					response.Data = data;
				return response;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				response.Message = "Error ocurred. Try later";
				response.Error = ex.Message;
				return response;
			}
		}

		public async Task<UnisolResponse<List<string>>> CollegeDepartmentsAsync(string college)
		{
			var response = new UnisolResponse<List<string>> { Data = new List<string>() };
			try
			{
				var data = await _context.Schools
				.Where(c => c.College.Equals(college))
				.Join(_context.Departments.Where(dp => !string.IsNullOrEmpty(dp.School)),
				s => s.Names, d => d.School, (s, d) => d.Names)
				.ToListAsync();
				response.Success = data.Any();
				response.Message = response.Success ? "Found" : "Not Found";
				if (response.Success)
					response.Data = data;
				return response;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				response.Message = "Error ocurred. Try later";
				response.Error = ex.Message;
				return response;
			}
		}

		public UnisolResponse<string> UpdateDocuments(Guid appId, string hostingRoot)
		{
			var response = new UnisolResponse<string>();
			try
			{
				var application = _portalContext.Applications
					.Include(a => a.ApplicationUploads)
					.FirstOrDefault(a => a.Id.Equals(appId));
				if (application == null)
				{
					response.Message = "Application not found";
					return response;
				}
				var uniApplicant = _context.Applicant
					.OrderByDescending(a => a.createdate)
					.FirstOrDefault(a => a.nationalID.Equals(application.NationalId)
					&& a.email.Equals(application.Email));
				if (uniApplicant == null)
				{
					response.Message = "Unisol Application not found";
					return response;
				}

				var resData = "";
				var uploadPath = _context.SysSetups.Select(s => s.AlbumPath).FirstOrDefault();
				if (string.IsNullOrEmpty(uploadPath))
				{
					response.Message = "Unisol album path not set";
					return response;
				}
				foreach (var upload in application.ApplicationUploads)
				{
					var resUpload = UploadApplicantFile(uniApplicant.Ref, uploadPath, hostingRoot, upload);
					resData += $"{resUpload.Data} : ";
				}

				response.Success = !string.IsNullOrEmpty(resData);
				response.Message = response.Success ? "Uploaded" : "Not Uploaded";
				if (response.Success)
					response.Data = $"Uploaded {resData}";
				return response;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"error updating unisol applicant documents :-{ex}");
				response.Error = ex.Message;
				response.Message = "Error occured. Try later";
				return response;
			}
		}

		public UnisolResponse<string> UploadApplicantFile(string appRef, string uploadsPath, string hostingRoot, ApplicationUpload upload)
		{
			var response = new UnisolResponse<string>();
			try
			{
				if (string.IsNullOrEmpty(uploadsPath))
				{
					response.Message = "Upload path not set";
					return response;
				}

				var count = _context.ApplicantFiles.Count(f => f.Ref.Equals(appRef));
				var fileName = $"{appRef}_{count + 1}";
				var applicantPath = $"{uploadsPath}\\Applicant\\Files\\{fileName}{upload.Extension}";
				var filePath = System.IO.Path.Combine(hostingRoot, upload.Path);
				System.IO.File.Copy(filePath, applicantPath);
				var appFile = new UniApplicantFile
				{
					Extension = upload.Extension.Replace(".", ""),
					Ref = appRef,
					Title = upload.Name,
					Fname = fileName,
					Personnel = upload.Personnel,
					Description = upload.Description,
					Rdate = DateTime.Now.Date
				};

				_context.ApplicantFiles.Add(appFile);
				_context.SaveChanges();
				response.Message = "Uploaded";
				response.Data = upload.Name;
				response.Success = true;
				return response;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error uploading file to unisol :- {ex}");
				response.Message = "Error occurred. Try later";
				return response;
			}
		}

		private string GenerateUniRef()
		{
			try
			{
				var nextRef = "";
				var exists = true;
				var i = 0;
				while (exists)
				{
					var nextCount = _context.Applicant.Count() + 1;
					nextRef = "A" + (nextCount + i).ToString("D6");
					exists = _context.Applicant.Any(a => a.Ref.Equals(nextRef));
					i++;
				}
				return nextRef;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				var rand = new Random().Next(9, 999999999);
				return "APPR" + rand.ToString("D5");
			}
		}

		private async Task<ReturnData<string>> ProcessApplicantFeesAsync(string refNo, double amount, Setting setting)
		{
			var res = new ReturnData<string>();
			if (setting == null)
			{
				res.Message = "Settings not Added";
				return res;
			}
			var soapSettings = JsonConvert.DeserializeObject<SoapSetting>(setting.Data);
			var service = new UnisolBankService(soapSettings.UserName, soapSettings.Password, soapSettings.Url);
			var result = await service.ProcessStudentPaymentAsync(refNo, amount, DateTime.UtcNow.ToString("yyyyMMddHHmmssffff"));
			var message = result.Success ? $"{result.Data.StrRcptNo} :- {result.Data.StrMsg}" : result.Data.StrMsg;
			res.Message = $"{result.Message} {message}";
			res.Success = result.Success;
			return res;
		}

		private async Task<ReturnData<string>> UploadApplicatantEducationAsync(string refNo, string userId)
		{
			var res = new ReturnData<string>();
			try
			{
				var backgrounds = await _portalContext.EducationExperiences
				.Where(b => b.UserId.Equals(userId))
				.ToListAsync();
				foreach (var education in backgrounds)
				{
					_context.EducationBackgrounds.Add(new UniEducationBackground
					{
						Ref = refNo,
						Names = education.Institution,
						Type = $"{education.QualificationType}",
						StartYear = $"{education.StartDate.Year}",
						EndYear = $"{education.EndDate.Year}",
						Qualification = education.EducationLevel,
						Grades = education.Grade,
						IndexNo = education.IndexNo,
						Rdate = DateTime.Now.Date
					});
				}

				await _context.SaveChangesAsync();
				res.Success = true;
				res.Message = "Uploaded";
				return res;
			}
			catch (Exception ex)
			{
				Console.WriteLine($" Error uploading education background to unisol :-{ex}");
				res.Message = "Error occured. Try later";
				res.ErrorMessage = ex.Message;
				return res;
			}

		}

		public UniApplicant GetApplicantByNationalId(string id) {
			return _context.Applicant.FirstOrDefault(x=>x.nationalID == id);
		}

		private static bool IsValidEmail(string text)
		{
			try
			{
				var address = new MailAddress(text);
				return address.Address == text;
			}
			catch
			{
				return false;
			}
		}
	}

	public class UniCertificateType
	{
		public string CertType { get; set; }
		public string GradeType { get; set; }
	}
}