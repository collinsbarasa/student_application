﻿namespace AbnApplicationPortal.Core.Data.Unisol.Responses
{
    public class UnisolResponse<T>
    {
        public UnisolResponse()
        {
            Success = false;
        }
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
        public T Data { get; set; }
    }
}
