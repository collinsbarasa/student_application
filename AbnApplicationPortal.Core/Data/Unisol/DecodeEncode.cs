﻿using System;

namespace AbnApplicationPortal.Core.Data.Unisol
{
    public class DecodeEncode
    {
        public static string Decode(string text)
        {
            var icNewText = "";
            var textLength = text.Length;
            for (var i = 0; i < textLength ; i++)
            {
                var iChar = text.Substring(i, 1);

                var myChar = Convert.ToChar(iChar);
                var n = myChar;

                if (n >= 192 && n <= 217) n = (char)(n - 127);

                if (n >= 218 && n <= 243) n = (char)(n - 121);

                if (n >= 244 && n <= 253) n = (char)(n - 196);

                if (n == 32) n = (char)(32);

                var t = Convert.ToChar(n);
                icNewText = icNewText + t;
            }

            return icNewText;
        }

        public static string Encode(string text)
        {
            var icLen = text.Length;
            var icNewText = "";

            for (var i = 0; i < icLen; i++)
            {
                var icChar = text.Substring(i, 1);
                var n = Convert.ToChar(icChar);
                if (n >= 65 && n <= 90) n = (char)(n + 127);

                if (n >= 97 && n <= 122) n = (char)(n + 121);

                if (n >= 48 && n <= 57) n = (char)(n + 196);

                if (n == 32) n = (char)(32);
                 
                var t = Convert.ToChar(n);
                icNewText = icNewText + t;
            }

            return icNewText;
        }
    }
}

