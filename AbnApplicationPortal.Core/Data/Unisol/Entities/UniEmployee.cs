﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
    [Table("hrpEmployee")]
    public class UniEmployee
    {
        [Key]
        public string EmpNo { get; set; }
        public string Names { get; set; }
        public string Gender { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public string IdNo { get; set; }
        public string Wemail { get; set; }
        public string Pemail { get; set; }
        public string Disability { get; set; }
        public string Country { get; set; }
        public string County { get; set; }
        public string City { get; set; }
        public string Cell { get; set; }
        public bool Terminated { get; set; }
    }
}
