﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	[Table("AppFiles")]
	public class UniApplicantFile
	{
		public string Ref { get; set; }
		[Key]
		public string Fname { get; set; }
		public string Title { get; set; }
		public string Extension { get; set; }
		public DateTime Rdate { get; set; }
		public string Personnel { get; set; }
		public string Description { get; set; }
	}
}
