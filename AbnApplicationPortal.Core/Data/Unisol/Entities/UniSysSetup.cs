﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	[Table("SysSetup")]
	public class UniSysSetup
	{
		[Key]
		[Column("Receipt Number")]
		public int ID { get; set; }
		public string OrgName { get; set; }
		public string SubTitle { get; set; }
		public string AlbumPath { get; set; }
	}
}
