﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
    [Table("hrpRace")]
    public class UniEthnicity
    {
        public int ID { get; set; }
        public string Names { get; set; }
        public bool Closed { get; set; }
    }
}
