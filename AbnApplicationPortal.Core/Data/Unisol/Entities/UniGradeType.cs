﻿namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	public class UniGradeType
	{
		public int Id { get; set; }
		public string GradeType { get; set; }
		public string Range { get; set; }
		public string Names { get; set; }
		public string points { get; set; }
		public string Notes { get; set; }
		public decimal GPA { get; set; }
	}
}
