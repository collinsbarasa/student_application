﻿namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
    public class UniClassType
    {
        public int ID { get; set; }
        public string Names { get; set; }
        public string notes { get; set; }
        public bool Closed { get; set; }
    }
}
