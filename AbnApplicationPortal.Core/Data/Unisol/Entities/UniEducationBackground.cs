﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	[Table("AppSchools")]
	public class UniEducationBackground
	{
		[Key]
		public int Id { get; set; }
		public string Ref { get; set; }
		public string Names { get; set; }
		public string Type { get; set; }
		public string StartYear { get; set; }
		public string EndYear { get; set; }
		public string Qualification { get; set; }
		public string Grades { get; set; }
		public string IndexNo { get; set; }
		public DateTime? Rdate { get; set; }
		public string Notes { get; set; }
		public string Personnel { get; set; }
	}
}
