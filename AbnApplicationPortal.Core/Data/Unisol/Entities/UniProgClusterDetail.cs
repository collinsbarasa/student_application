﻿namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
    public class UniProgClusterDetail
    {
        public int ID { get; set; }
        public string ProgCode { get; set; }
        public string Cluster { get; set; }
        public string Subject { get; set; }
        public string ExamType { get; set; }
        public string Code { get; set; }
        public string Grading { get; set; }
        public string MinValue { get; set; }
        public string Choice { get; set; }
        public bool Closed { get; set; }
        public string Notes { get; set; }
    }
}
