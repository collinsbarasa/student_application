﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	public class UniApplicant
	{
		public UniApplicant()
		{
			RYear = DateTime.Now.ToString("yyyy");
			RMonth = DateTime.Now.ToString("MMMM");
		}
		[Key]
		public string Ref { get; set; }
		public string AdmnNo { get; set; }
		public string names { get; set; }
		public string nationalID { get; set; }
		public DateTime DOB { get; set; }
		public string gender { get; set; }
		public string Marital { get; set; }
		public string nationality { get; set; }
		public string telno { get; set; }
		public string County { get; set; }
		public string District { get; set; }
		public string homeaddress { get; set; }
		public string email { get; set; }
		public string Status { get; set; }
		public string Source { get; set; }
		public string religion { get; set; }
		public string Programme { get; set; }
		public string StudyMode { get; set; }
		public string Campus { get; set; }
		public string Language { get; set; }
		public string EMName { get; set; }
		public string EMRel { get; set; }
		public string EMTel { get; set; }
		public string EMEmail { get; set; }
		public string EMAddress { get; set; }
		public string EMRemarks { get; set; }
		public string special { get; set; }
		public string activity { get; set; }
		public string prevadmnno { get; set; }
		public string notes { get; set; }
		public DateTime createdate { get; set; }
		public string personnel { get; set; }
		public string Sponsor { get; set; }
		public string Disability { get; set; }
		public string IndexNo { get; set; }
		public string Constituency { get; set; }
		public string SubCounty { get; set; }
		public string RYear { get; set; }
		public string RMonth { get; set; }
		public string KcpeYear { get; set; }
		public string KcpeIndexNo { get; set; }
		public string KcseYear { get; set; }
		[Column("AFRN")]
		public string ReceiptNo { get; set; }
	}
}
