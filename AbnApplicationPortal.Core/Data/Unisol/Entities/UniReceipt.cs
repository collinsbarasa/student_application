﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	[Table("ReceiptBook")]
	public class UniReceipt
	{
		[Key]
		[Column("Receipt Number")]
		public string ReceiptNo { get; set; }
		public string Names { get; set; }
		[Column(TypeName = "decimal(19,4)")]
		public decimal Amount { get; set; }
		[Column(TypeName = "decimal(19,4)")]
		public decimal Credit { get; set; }
		[Column("Payment Mode")]
		public string PayMode { get; set; }
		[Column("Mode Number")]
		public string ModeNumber { get; set; }
		public DateTime Rdate { get; set; }
	}
}
