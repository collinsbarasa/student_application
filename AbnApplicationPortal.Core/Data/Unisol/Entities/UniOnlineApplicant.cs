﻿using AbnApplicationPortal.Shared.Models.Applicants;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	[Table("OnlineApplicant")]
	public class UniOnlineApplicant
	{
		public UniOnlineApplicant()
		{

		}

		public UniOnlineApplicant(Application application)
		{
			var programme = application.ApplicationProgrammes
				.FirstOrDefault(p => p.IsMain)?.Programme;
			var pName = programme == null ? "" : programme.Name;
			Ref = application.Code;
			Names = application.Names;
			NationalId = application.NationalId;
			Gender = application.Gender;
			Dob = application.Dob;
			Marital = application.Marital;
			Nationality = application.Nationality;
			TelNo = application.TelNo;
			County = application.County;
			Email = application.Email;
			Campus = application.Campus;
			CreateDate = DateTime.Now.Date;
			Programme = pName;
		}

		[Key]
		public string Ref { get; set; }
		public string Names { get; set; }
		public string NationalId { get; set; }
		public string Gender { get; set; }
		public DateTime Dob { get; set; }
		public DateTime CreateDate { get; set; }
		public string Marital { get; set; }
		public string Nationality { get; set; }
		public string TelNo { get; set; }
		public string County { get; set; }
		public string Email { get; set; }
		public string Campus { get; set; }
		public string Programme { get; set; }
	}
}
