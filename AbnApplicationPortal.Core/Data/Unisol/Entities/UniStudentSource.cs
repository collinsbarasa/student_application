﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	[Table("StudentSource")]
	public class UniStudentSource
	{
		public int Id { get; set; }
		public string Names { get; set; }
	}
}
