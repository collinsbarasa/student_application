﻿namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
    public class UniCampus
    {
        public int ID { get; set; }
        public string Names { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public bool Closed { get; set; }
        public string Notes { get; set; }
    }
}
