﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
    [Table("Register")]
    public class UniStudent
    {
        [Key]
        public string AdmnNo { get; set; }
        public string Names { get; set; }
        public string NationalId { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string Nationality { get; set; }
        public string TelNo { get; set; }
        public string County { get; set; }
        public string Email { get; set; }
        public string Sponsor { get; set; }
        public string Programme { get; set; }
        public string Faid { get; set; }
        public string Disability { get; set; }
        public string Domicile { get; set; }
        public bool Closed { get; set; }
        [NotMapped]
        public string Department { get; set; }
    }
}
