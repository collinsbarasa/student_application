﻿namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
    public class UniUser
    {
        public string UserCode { get; set; }
        public string Names { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public bool Closed { get; set; }
        public bool Applicant { get; set; }
    }
}
