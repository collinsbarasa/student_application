﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	[Table("ReceiptBookOtherDetail")]
	public class UniReceiptDetail
	{
		[Key]
		public int Id { get; set; }
		[Column("Receipt Number")]
		public string ReceiptNo { get; set; }
		public string Names { get; set; }
		[Column(TypeName = "decimal(19,4)")]
		public decimal Amount { get; set; }
		public string Notes { get; set; }
		public string Account { get; set; }
	}
}
