﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	[Table("Department")]
	public class UniDepartment
	{
		[Key]
		public int Id { get; set; }
		public string Names { get; set; }
		public bool Tuition { get; set; }
		public string School { get; set; }
		public bool Closed { get; set; }
	}
}
