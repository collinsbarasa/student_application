﻿namespace AbnApplicationPortal.Core.Data.Unisol.Entities
{
	public class UniSchool
	{
		public int Id { get; set; }
		public string Names { get; set; }
		public string College { get; set; }
	}
}
