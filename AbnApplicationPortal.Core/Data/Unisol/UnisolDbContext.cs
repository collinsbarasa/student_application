﻿using AbnApplicationPortal.Core.Data.Unisol.Entities;
using Microsoft.EntityFrameworkCore;

namespace AbnApplicationPortal.Core.Data.Unisol
{
	public class UnisolDbContext : DbContext
	{
		public UnisolDbContext(DbContextOptions<UnisolDbContext> options) : base(options)
		{

		}
		public DbSet<UniCampus> Campuses { get; set; }
		public DbSet<UniClassType> ClassType { get; set; }
		public DbSet<UniGradeType> Grading { get; set; }
		public DbSet<UniProgramme> Programme { get; set; }
		public DbSet<UniEthnicity> Ethnicities { get; set; }
		public DbSet<UniApplicant> Applicant { get; set; }
		public DbSet<UniUser> Users { get; set; }
		public DbSet<UniProgClusterDetail> ProgClusterDetails { get; set; }
		public DbSet<UniStudent> Students { get; set; }
		public DbSet<UniEmployee> Employees { get; set; }
		public DbSet<UniDepartment> Departments { get; set; }
		public DbSet<UniEducationBackground> EducationBackgrounds { get; set; }
		public DbSet<UniSysSetup> SysSetups { get; set; }
		public DbSet<UniApplicantFile> ApplicantFiles { get; set; }
		public DbSet<UniOnlineApplicant> OnlineApplicants { get; set; }
		public DbSet<UniReceipt> Receipts { get; set; }
		public DbSet<UniReceiptDetail> ReceiptDetails { get; set; }
		public DbSet<UniSchool> Schools { get; set; }
		public DbSet<UniCollege> Colleges { get; set; }
		public DbSet<AcademicYear> AcademicYear { get; set; }
		public DbSet<UniStudentSource> StudentSources { get; set; }
	}
}
