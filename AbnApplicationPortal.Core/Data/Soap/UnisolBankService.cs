﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace AbnApplicationPortal.Core.Data.Soap
{
	public class UnisolBankService
	{
		private readonly XNamespace ns = "http://schemas.xmlsoap.org/soap/envelope/";
		public readonly XNamespace myns = "unisol:bankapi";
		private readonly XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
		private readonly XNamespace xsd = "http://www.w3.org/2001/XMLSchema";

		private readonly TimeSpan _timeout = TimeSpan.FromMinutes(6);
		private readonly string _apiUrl;
		private readonly string _userName;
		private readonly string _password;

		public UnisolBankService(string userName, string password, string apiUrl)
		{
			_userName = userName;
			_password = password;
			_apiUrl = apiUrl;
		}


		private async Task<ReturnData<XDocument>> SendAsync(string soapRequest, string action)
		{
			var res = new ReturnData<XDocument>();
			try
			{
				using (var client = new HttpClient(new HttpClientHandler
				{ AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip })
				{ Timeout = _timeout })
				{
					var request = new HttpRequestMessage
					{
						RequestUri = new Uri(_apiUrl),
						Method = HttpMethod.Post,
						Content = new StringContent(soapRequest, Encoding.UTF8, "text/xml")
					};
					request.Headers.Clear();
					client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));
					request.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
					request.Headers.Add("SOAPAction", $"{myns}/{action}");

					var response = await client.SendAsync(request);
					if (!response.IsSuccessStatusCode)
					{
						res.Message = $"FAILED : Remote server exception- {response.StatusCode}";
						return res;
					}

					var streamTask = response.Content.ReadAsStreamAsync();
					var stream = streamTask.Result;
					var sr = new StreamReader(stream);
					var soapResponse = XDocument.Load(sr);

					res.Success = true;
					res.Message = "OK. Executed";
					res.Data = soapResponse;
					return res;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				res.Message = "ERROR. Server Error Occured";
				res.ErrorMessage = ex.Message;
				return res;
			}
		}


		private XDocument ProcessStudentPaymentRequest(string regNo, double amount, string strTransNo)
		{
			var sDate = DateTime.Now.ToString().Split(' ')[0];
			var soapRequest = new XDocument(
				new XDeclaration("1.0", "UTF-8", "no"),
				new XElement(ns + "Envelope",
					new XAttribute(XNamespace.Xmlns + "xsi", xsi),
					new XAttribute(XNamespace.Xmlns + "xsd", xsd),
					new XAttribute(XNamespace.Xmlns + "soap", ns),
					new XElement(ns + "Body",
						new XElement(myns + "ProcessStudentFees",
							new XElement(myns + "strUN", _userName),
							new XElement(myns + "strPWD", _password),
							new XElement(myns + "strRegNo", regNo),
							new XElement(myns + "dblAmount", amount),
							new XElement(myns + "strTransNo", strTransNo),
							new XElement(myns + "dtTransDate", sDate)
						)
					)
				));
			return soapRequest;
		}

		public async Task<ReturnData<ProcessStudentFeesResult>> ProcessStudentPaymentAsync(string regNo, double amount, string transNo)
		{
			var res = new ReturnData<ProcessStudentFeesResult>
			{
				Data = new ProcessStudentFeesResult()
			};
			try
			{
				var paymentRequest = ProcessStudentPaymentRequest(regNo, amount, transNo);
				var response = await SendAsync(paymentRequest.ToString(), "ProcessStudentFees");
				if (!response.Success)
				{
					res.Message = response.Message;
					res.ErrorMessage = response.ErrorMessage;
					return res;
				}
				var xml = response.Data.Descendants(myns + "ProcessStudentFeesResult").FirstOrDefault()?.ToString();
				var paymentResult = Deserialize<ProcessStudentFeesResult>(xml);
				res.Success = paymentResult.StrStatus.Equals("OK");
				res.Data = paymentResult;
				res.Message = paymentResult.StrMsg;
				return res;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				res.ErrorMessage = e.Message;
				res.Message = "Error occured";
				return res;
			}
		}

		public static T Deserialize<T>(string xmlStr)
		{
			var serializer = new XmlSerializer(typeof(T));
			T result;
			using (TextReader reader = new StringReader(xmlStr))
			{
				result = (T)serializer.Deserialize(reader);
			}
			return result;
		}
	}
}
