﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Data.Unisol;
using AbnApplicationPortal.Shared.Models;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Models.Settings;
using AbnApplicationPortal.Shared.Requests.Settings;
using Newtonsoft.Json;

namespace AbnApplicationPortal.Core.Data
{
	public class DbSeeder
	{
		private static string _path;
		public static void Seed(UnisolDbContext unisolDbContext, AbnApplicationPortalContext context)
		{
			_path = Path.Combine(Directory.GetCurrentDirectory(), "Resources");
			var _unisolService = new UnisolService(unisolDbContext, context);

			if (!context.RejectReasons.Any())
			{
				var reason = new RejectReason
				{
					Title = "Not Fully Paid",
					Description = "Application fee not fully paid"
				};

				context.RejectReasons.Add(reason);
				context.SaveChanges();
			}
			if (!context.Reasons.Any())
			{
				var reasons = new List<Reason>();
				reasons.Add(new Reason
				{
					Description = "Medical"
				});
				reasons.Add(new Reason
				{
					Description = "Financial"
				});
				reasons.Add(new Reason
				{
					Description = "Deceased (Guardian)"
				});

				context.Reasons.AddRange(reasons);
				context.SaveChanges();
			}
			if (!context.Campuses.Any())
			{
				var campusRes = _unisolService.Campuses();
				if (campusRes.Success)
				{
					campusRes.Data.ForEach(c =>
					{
						context.Campuses.Add(new Campus
						{
							Address = c.Address,
							Description = c.Notes,
							Website = c.Website,
							Contact = c.Contact,
							Status = c.Closed ? EntityStatus.Inactive : EntityStatus.Active,
							Name = c.Names,
							Email = c.Email,
							Telephone = c.Tel,
							UniId = c.ID
						});
					});
					context.SaveChanges();
				}
			}

			if (!context.ClassTypes.Any())
			{
				var classTypes = _unisolService.ClassTypes();
				if (classTypes.Success)
				{
					classTypes.Data.ForEach(c =>
					{
						context.ClassTypes.Add(new ClassType
						{
							Name = c.Names,
							Description = c.notes,
							Status = c.Closed ? EntityStatus.Inactive : EntityStatus.Active,
							UniId = c.ID
						});
					});
					context.SaveChanges();
				}
			}

			if (!context.Programmes.Any())
			{
				var programmes = _unisolService.Programmes();
				var gradeTyes = programmes.Data
					.Where(p => !string.IsNullOrEmpty(p.CertType))
					.Select(e => e.CertType)
					.Distinct().Select(g => new GradeCertificateType
					{
						CertType = g,
						GradeType = g
					}).ToList();
				context.GradeCertificateTypes.AddRange(gradeTyes);
				var progs = programmes.Data
					.Select(p => new Programme
					{
						Code = p.Code,
						Name = p.Names,
						Notes = p.Notes,
						CertType = p.CertType,
						GradeType = p.GradeType,
						Requirements = p.AdminReq,
						UniId = p.ID,
						Period = p.Period,
						Department = p.Department
					}).ToList();
				context.Programmes.AddRange(progs);
				context.SaveChanges();
			}

			if (!context.ClientSettings.Any())
			{
				var clientSetting = new ClientSetting
				{
					AppName = "Online Applications Portal",
					AppVersion = "v2.0",
				};
				var setupRes = _unisolService.SysSetup();
				if (setupRes.Success)
				{
					clientSetting.ClientName = setupRes.Data.OrgName;
					clientSetting.ClientCode = setupRes.Data.SubTitle;
					clientSetting.AppRefPrefix = setupRes.Data.SubTitle;
				}
				else
				{
					clientSetting.ClientName = "ABNO University";
					clientSetting.ClientCode = "ABNO";
					clientSetting.AppRefPrefix = "APP";
				};
				context.ClientSettings.Add(clientSetting);
				context.SaveChanges();
			}

			if (!context.Settings.Any())
			{
				var settings = new List<Setting>
				{
					new Setting
					{
						Key = SettingKey.Email,
						Data = JsonConvert.SerializeObject(new EmailSetting
						{
							SmtpServer = "smtp.gmail.com",
							SmtpPort = "587",
							SmtpUsername = "abnnotifier@gmail.com",
							SmtpPassword = "Shalom123.#",
							SenderFromEmail = "abnnotifier@gmail.com",
							SenderFromName = "ABN Applications Portal",
							SocketOptions=MailKit.Security.SecureSocketOptions.StartTls
						})
					},
					new Setting
					{
						Key = SettingKey.Sms,
						Data = JsonConvert.SerializeObject(new SmsSetting
						{
							Username = "ABNO",
							ApiKey = "50e190d0197d8a26844464d503bff88767dba28fdaaf9010315826dc6fdf7f44",
							SenderId = "ABNO"
						})
					},
					new Setting
					{
						Key = SettingKey.Mpesa,
						Data = JsonConvert.SerializeObject(new MpesaSetting
						{
							AbnExpressUrl = "https://abno.co.ke:7905/api/v1/",
							CallBackURL = "https://abno.co.ke:7904/IntakeApplications/ReceivePay/",
							PassKey =  "c999aca07727c47a0b4a4e0800b9c7ba0a3099f27a4285a1f426dd7014570703",
							BusinessShortCode =  "174379",
							AbnClientId = "49CA29CC-FEFF-42DF-AA4F-47FA9456D345"
						})
					},
					new Setting
					{
						Key=SettingKey.Soap,
						Data=JsonConvert.SerializeObject(new SoapSetting
						{
							UserName="BANK_A",
							Password="STRONG_PWD",
							Url="https://bank.test.com:88/ABNUnisolBankService.asmx"
						})
					}
				};
				context.Settings.AddRange(settings);
				context.SaveChanges();
			}

			/*
             * SEED RESOURCES
             */
			if (!context.PostalCodes.Any())
			{
				var codesPath = Path.Combine(_path, "PostalCodes.json");
				var text = File.ReadAllText(codesPath);
				var list = JsonConvert.DeserializeObject<List<PostalCode>>(text);
				context.PostalCodes.AddRange(list);
				context.SaveChanges();
			}

			if (!context.CountryTelCodes.Any())
			{
				var telPath = Path.Combine(_path, "CountryTelCodes.json");
				var text = File.ReadAllText(telPath);
				var list = JsonConvert.DeserializeObject<List<CountryTelCode>>(text);
				context.CountryTelCodes.AddRange(list);
				context.SaveChanges();
			}

			if (!context.IdDocuments.Any())
			{
				var idPath = Path.Combine(_path, "IdDocuments.json");
				var text = File.ReadAllText(idPath);
				var list = JsonConvert.DeserializeObject<List<IdDocument>>(text);
				context.IdDocuments.AddRange(list);
				context.SaveChanges();
			}

			if (!context.Counties.Any())
			{
				var ctyPath = Path.Combine(_path, "KeCounties.json");
				var text = File.ReadAllText(ctyPath);
				var list = JsonConvert.DeserializeObject<List<County>>(text);
				context.Counties.AddRange(list);
				context.SaveChanges();
			}

			if (!context.MaritalStatuses.Any())
			{
				var marPath = Path.Combine(_path, "MaritalStatuses.json");
				var text = File.ReadAllText(marPath);
				var list = JsonConvert.DeserializeObject<List<MaritalStatus>>(text);
				context.MaritalStatuses.AddRange(list);
				context.SaveChanges();
			}

			if (!context.Nationalities.Any())
			{
				var natPath = Path.Combine(_path, "Nationalities.json");
				var text = File.ReadAllText(natPath);
				var list = JsonConvert.DeserializeObject<List<string>>(text);
				var nationalities = new List<Nationality>();
				list.ForEach(l => nationalities.Add(new Nationality { Name = l }));
				context.Nationalities.AddRange(nationalities);
				context.SaveChanges();
			}

			if (!context.RelationShips.Any())
			{
				var relPath = Path.Combine(_path, "Relationships.json");
				var text = File.ReadAllText(relPath);
				var list = JsonConvert.DeserializeObject<List<RelationShip>>(text);
				context.RelationShips.AddRange(list);
				context.SaveChanges();
			}

			if (!context.Religions.Any())
			{
				var relPath = Path.Combine(_path, "Religions.json");
				var text = File.ReadAllText(relPath);
				var list = JsonConvert.DeserializeObject<List<string>>(text);
				var religions = new List<Religion>();
				list.ForEach(l => religions.Add(new Religion { Name = l }));
				context.Religions.AddRange(religions);
				context.SaveChanges();
			}

			if (!context.MediaSources.Any())
			{
				var relPath = Path.Combine(_path, "MediaSources.json");
				var text = File.ReadAllText(relPath);
				var sources = JsonConvert.DeserializeObject<List<MediaSource>>(text);
				context.MediaSources.AddRange(sources);
				context.SaveChanges();
			}

			if (!context.DisabilityConditions.Any())
			{
				var relPath = Path.Combine(_path, "DisabilityConditions.json");
				var text = File.ReadAllText(relPath);
				var list = JsonConvert.DeserializeObject<List<DisabilityCondition>>(text);
				context.DisabilityConditions.AddRange(list);
				context.SaveChanges();
			}

			if (!context.EducationLevels.Any())
			{
				var relPath = Path.Combine(_path, "EducationLevels.json");
				var text = File.ReadAllText(relPath);
				var list = JsonConvert.DeserializeObject<List<EducationLevel>>(text);
				context.EducationLevels.AddRange(list);
				context.SaveChanges();
			}

			if (!context.KcseSubjects.Any())
			{
				var relPath = Path.Combine(_path, "KcseSubjects.json");
				var text = File.ReadAllText(relPath);
				var list = JsonConvert.DeserializeObject<List<KcseSubject>>(text);
				list.Add(new KcseSubject
				{
					Name = "MEAN GRADE",
					Code = "MN",
					Abbreviation = "MEAN",
					IsMeanGrade = true
				});
				context.KcseSubjects.AddRange(list);
				context.SaveChanges();
			}

			if (!context.UserTitles.Any())
			{
				var titles = new List<UserTitle> {
					new UserTitle{Name="Mr",Description="Mr."},
					new UserTitle{Name="Mrs",Description="Mrs"},
					new UserTitle{Name="Ms",Description="Ms"},
					new UserTitle{Name="Prof",Description="Professor"},
					new UserTitle{Name="Dr",Description="Doctor"}
				};
				context.UserTitles.AddRange(titles);
				context.SaveChanges();
			}

			if (!context.KcseGrades.Any())
			{
				var grades = new List<KcseGrade>
				{
					new KcseGrade
					{
						Name = "A",
						Rank = 12
					},new KcseGrade
					{
						Name = "A-",
						Rank = 11
					},new KcseGrade
					{
						Name = "B+",
						Rank = 10
					},new KcseGrade
					{
						Name = "B",
						Rank = 9
					},new KcseGrade
					{
						Name = "B-",
						Rank = 8
					},new KcseGrade
					{
						Name = "C+",
						Rank = 7
					},new KcseGrade
					{
						Name = "C",
						Rank = 6
					},new KcseGrade
					{
						Name = "C-",
						Rank = 5
					},new KcseGrade
					{
						Name = "D+",
						Rank = 4
					},new KcseGrade
					{
						Name = "D",
						Rank = 3
					},new KcseGrade
					{
						Name = "D-",
						Rank = 2
					},new KcseGrade
					{
						Name = "E",
						Rank = 1
					}
				};
				context.KcseGrades.AddRange(grades);
				context.SaveChanges();
			}
		}
	}
}
