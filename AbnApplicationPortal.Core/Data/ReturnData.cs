﻿namespace AbnApplicationPortal.Core.Data
{
    public class ReturnData<T>
    {
        public ReturnData()
        {
            Success = false;
        }
        public bool Success { get; set; }
        public string Message { get; set; }
        public string ErrorMessage { get; set; }
        public T Data { get; set; }
    }
}
