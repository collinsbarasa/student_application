﻿using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Data
{
	public class IdentitySeeder
	{
		public static async Task Seed(IServiceProvider serviceProvider, IConfiguration Configuration)
		{
			var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
			var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
			var roleNames = new List<string>
			{ PrimaryRole.SuperAdmin, PrimaryRole.Admin, PrimaryRole.Staff, PrimaryRole.Applicant, PrimaryRole.Student };
			IdentityResult roleResult;
			foreach (var roleName in roleNames)
			{
				var roleExist = await roleManager.RoleExistsAsync(roleName);
				if (!roleExist)
				{
					roleResult = await roleManager.CreateAsync(new IdentityRole(roleName));
				}
			}

			var userEmail = Configuration["SystemAdmin:Email"];
			var poweruser = new ApplicationUser
			{
				UserName = userEmail,
				Email = userEmail,
				PrimaryRole = PrimaryRole.SuperAdmin,
				OtherNames = "ABN-ADMIN",
				PhoneNumber = "0733208119",
				Department = "SUPERADMIN"
			};

			string userPassword = Configuration["SystemAdmin:Password"];
			var user = await userManager.FindByEmailAsync(userEmail);
			if (user == null)
			{
				var createPowerUser = await userManager.CreateAsync(poweruser, userPassword);
				if (createPowerUser.Succeeded)
				{
					await userManager.AddToRoleAsync(poweruser, PrimaryRole.SuperAdmin);
					await userManager.AddToRoleAsync(poweruser, PrimaryRole.Admin);
				}
			}
		}
	}
}
