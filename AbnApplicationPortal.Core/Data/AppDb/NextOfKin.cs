﻿using AbnApplicationPortal.Shared.Models;
using System;

namespace AbnApplicationPortal.Core.Data.AppDb
{
	public class NextOfKin : BaseEntity
	{
		public NextOfKin()
		{
			Code = $"KIN-{DateTime.UtcNow:yyyyMMddHHmmssffff}";
		}
		public string Title { get; set; }
		public string Gender { get; set; }
		public string IdDocumentName { get; set; }
		public string IdDocumentNumber { get; set; }
		public string OtherNames { get; set; }
		public string UserId { get; set; }
		public string SurName { get; set; }
		public string EmailAddress { get; set; }
		public string PhoneNumber { get; set; }
		public string PostalAddress { get; set; }
		public string AddressName { get; set; }
		public string Town { get; set; }
		public string RelationShipName { get; set; }
		public string Notes { get; set; }
	}
}
