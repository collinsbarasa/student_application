﻿using AbnApplicationPortal.Shared.Models;
using System;

namespace AbnApplicationPortal.Core.Data.AppDb
{
	public class DisabilityCondition : BaseEntity
	{
		public DisabilityCondition()
		{

			Code = "DIS-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
		}
		public string Name { get; set; }
		public string Description { get; set; }
		public int Rank { get; set; }
	}
}

