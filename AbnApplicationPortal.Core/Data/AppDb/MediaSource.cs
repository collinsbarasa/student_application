﻿using AbnApplicationPortal.Shared.Models;

namespace AbnApplicationPortal.Core.Data.AppDb
{
	public class MediaSource : BaseEntity
	{
		public string Title { get; set; }
		public string Description { get; set; }
	}
}
