﻿using System;

namespace AbnApplicationPortal.Core.Data.AppDb
{
    public class EmergencyContact : NextOfKin
    {
       public EmergencyContact() {
            Code = $"EM-{DateTime.UtcNow:yyyyMMddHHmmssffff}";
        }

        public bool SameAsKin { get; set; }
    }
}
