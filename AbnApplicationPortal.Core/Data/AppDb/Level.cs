﻿using AbnApplicationPortal.Shared.Models;
using System;
using System.Collections.Generic;

namespace AbnApplicationPortal.Core.Data.AppDb
{
    public class Level : BaseEntity
    {
        public Level()
        {
            Code = "EDU-LEV" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }
        public string UserId { get; set; }
        public string ApplicationLevel { get; set; }
        public string ResearchInsitute { get; set; }
        public string Financing { get; set; }
        public Guid? ApplicationId { get; set; }
        public List<Referee> Referees { get; set; } 
    }
}
