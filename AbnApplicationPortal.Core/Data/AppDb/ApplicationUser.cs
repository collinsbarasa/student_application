﻿using AbnApplicationPortal.Shared.Models.Applicants;
using Microsoft.AspNetCore.Identity;
using System;

namespace AbnApplicationPortal.Core.Data.AppDb
{
	public class ApplicationUser : IdentityUser
	{
		public ApplicationUser()
		{
			PersonalDetail = new PersonalDetail();
			NextOfKin = new NextOfKin();
			DateCreated = DateUpdated = DateTime.Now;
		}
		public string Names => $"{SurName} {OtherNames}";
		public string RefNo { get; set; }
		public string PrimaryRole { get; set; }
		public string Department { get; set; }
		public string SurName { get; set; }
		public string OtherNames { get; set; }
		public virtual NextOfKin NextOfKin { get; set; }
		public virtual PersonalDetail PersonalDetail { get; set; }
		public string ImageUrl { get; set; }
		public string SignatureUrl { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateUpdated { get; set; }
		public string Personnel { get; set; }

	}
}
