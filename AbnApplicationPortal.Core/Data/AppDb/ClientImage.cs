﻿using AbnApplicationPortal.Shared.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Data.AppDb
{
	public class ClientImage : BaseEntity
	{
		public ClientImage()
		{
			Code = "IMG-" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
		}
		public string FileName { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string Path { get; set; }
		[Column(TypeName = "decimal(18,2)")]
		public decimal Size { get; set; }
		public string Extension { get; set; }
	}
}
