﻿using AbnApplicationPortal.Shared.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Data.AppDb
{
    public class Referee : BaseEntity
    {
        public Referee()
        {
            Code = "AC-REF" + DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }
        [Required]
        [Display(Name = "Name of Referee.")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Phone of Referee.")]
        public string Phone { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email of Referee.")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Title of Referee.")]
        public string Title { get; set; }
    }
}
