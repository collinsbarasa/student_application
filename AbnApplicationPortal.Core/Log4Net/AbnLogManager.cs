﻿using System;
using log4net;
using System.IO;
using System.Reflection;
using log4net.Config;

namespace AbnApplicationPortal.Core.Log4Net
{
    public static class AbnLogManager
    {
        private static ILog _logger;
        public static ILog Logger(Type type)
        {
            var cDirectory = Directory.GetCurrentDirectory();
            var log4NetConfigFilePath = Path.Combine(cDirectory, "Log4Net", "log4net.config");
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());

            XmlConfigurator.Configure(logRepository, new FileInfo(log4NetConfigFilePath));
            _logger = LogManager.GetLogger(type);

            return _logger;
        }
    }



}