﻿using AbnApplicationPortal.Shared.Models.Applicants;
using System;

namespace AbnApplicationPortal.Core.Models.PaymentVm
{
	public class PaymentViewModel : BaseViewModel
	{
		public PaymentViewModel(ApplicationPayment payment)
		{
			Reference = payment.Reference;
			AppRef = payment.AppRef;
			Amount = payment.Amount;
			MpesaReceiptNumber = payment.MpesaReceiptNumber;
			TransactionDate = payment.TransactionDate;
			PhoneNumber = payment.PhoneNumber;
			IsCompleted = payment.IsCompleted;
			DateStr = payment.DateCreatedStr;
			DateCreated = payment.DateCreated;
			ApplicationId = payment.ApplicationId;
		}
		public string Reference { get; set; }
		public string AppRef { get; set; }
		public decimal Amount { get; set; }
		public string MpesaReceiptNumber { get; set; }
		public string TransactionDate { get; set; }
		public string PhoneNumber { get; set; }
		public string DateStr { get; set; }
		public DateTime DateCreated { get; set; }
		public bool IsCompleted { get; set; }
		public Guid ApplicationId { get; set; }
	}
}
