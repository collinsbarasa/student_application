﻿using AbnApplicationPortal.Shared.Models.Applicants;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
	public class EducationDetailViewModel : BaseViewModel
	{
		public EducationDetailViewModel()
		{
			EducationExperiences = new List<EducationExperience>();
			IsEditMode = false;
			StartDate = EndDate = DateTime.Now;
		}

		[Required]
		[Display(Name = "Education Level")]
		public string EducationLevelId { get; set; }
		[Required]
		public string Grade { get; set; }
		public string StartDateStr { get; set; }
		[Required]
		[Display(Name = "Start Date")]
		public DateTime StartDate { get; set; }
		public string EndDateStr { get; set; }
		[Required]
		[Display(Name = "End Date")]
		public DateTime EndDate { get; set; }
		[Required]
		[Display(Name = "Course/Certificate")]
		public string CourseStudied { get; set; }
		[Required]
		public string Institution { get; set; }
		public List<EducationExperience> EducationExperiences { get; set; }
		[HiddenInput]
		public Guid Id { get; set; }
		public string Notes { get; set; }
		[Display(Name = "Exam/Index No.")]
		public string IndexNo { get; set; }
		public EducationQualificationType Type { get; set; }
		[HiddenInput]
		public Guid? ApplicationId { get; set; }
	}
}
