﻿using AbnApplicationPortal.Core.Data.AppDb;

namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
    public class EmergencyContactDetailViewModel : NextOfKinDetailViewModel
    {
		public bool SameAsKin { get; set; }
		public EmergencyContactDetailViewModel()
		{

		}
		public EmergencyContactDetailViewModel(EmergencyContact emergencyContact)
		{
			Title = emergencyContact.Title;
			Gender = emergencyContact.Gender;
			IdDocumentName = emergencyContact.IdDocumentName;
			IdDocumentNumber = emergencyContact.IdDocumentNumber;
			OtherNames = emergencyContact.OtherNames;
			SurName = emergencyContact.OtherNames;
			SameAsKin = emergencyContact.SameAsKin;
			SurName = emergencyContact.SurName;
			EmailAddress = emergencyContact.EmailAddress;
			PhoneNumber = emergencyContact.PhoneNumber;
			PostalAddress = emergencyContact.PostalAddress;
			AddressName = emergencyContact.AddressName;
			Town = emergencyContact.Town;
			RelationShipName = emergencyContact.RelationShipName;
			UserId = emergencyContact.UserId;
			Success = true;
			Notes = emergencyContact.Notes;
		}
	}
}
