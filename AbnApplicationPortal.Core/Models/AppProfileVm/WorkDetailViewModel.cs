﻿using System;
using System.Collections.Generic;
using AbnApplicationPortal.Shared.Models.Applicants;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
    public class WorkDetailViewModel : BaseViewModel
    {
		public WorkDetailViewModel()
		{
            WorkExperiences = new List<WorkExperience>();
			IsEditMode = false;
			StartDate = EndDate = DateTime.Now;
		}
        [Required]
        public string Employer { get; set; }
        [Required]
        public string Designation { get; set; }
        [Required]
        [Display(Name = "Nature of Assignment")]
        public string AssignmentNature { get; set; }
        [Required]
        [Display(Name = "From")]
        public DateTime StartDate { get; set; }
        public string StartDateStr { get; set; }
        [Required]
        [Display(Name = "To")]
        public DateTime EndDate { get; set; }
        public string EndDateStr { get; set; }
        public List<WorkExperience> WorkExperiences { get; set; }
        public Guid? ApplicationId { get; set; }
        [HiddenInput]
        public Guid Id { get; set; }
    }
}
