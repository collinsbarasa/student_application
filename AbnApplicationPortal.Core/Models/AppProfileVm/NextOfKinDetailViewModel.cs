﻿using AbnApplicationPortal.Core.Data.AppDb;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
	public class NextOfKinDetailViewModel : BaseViewModel
	{
		public NextOfKinDetailViewModel()
		{

		}

		public NextOfKinDetailViewModel(NextOfKin kin)
		{
			Title = kin.Title;
			Gender = kin.Gender;
			IdDocumentName = kin.IdDocumentName;
			IdDocumentNumber = kin.IdDocumentNumber;
			OtherNames = kin.OtherNames;
			SurName = kin.OtherNames;
			SurName = kin.SurName;
			EmailAddress = kin.EmailAddress;
			PhoneNumber = kin.PhoneNumber;
			PostalAddress = kin.PostalAddress;
			AddressName = kin.AddressName;
			Town = kin.Town;
			RelationShipName = kin.RelationShipName;
			UserId = kin.UserId;
			Success = true;
			Notes = kin.Notes;
		}

		[Required]
		public string Title { get; set; }
		[Required]
		public string Gender { get; set; }
		[Required]
		[Display(Name = "Identification Document")]
		public string IdDocumentName { get; set; }
		[Required]
		[Display(Name = "Identification Document No.")]
		public string IdDocumentNumber { get; set; }
		[Display(Name = "Other Names")]
		public string OtherNames { get; set; }
		[Required]
		[Display(Name = "Surname")]
		public string SurName { get; set; }
		[DataType(DataType.EmailAddress)]
		public string EmailAddress { get; set; }
		[Required]
		[MinLength(9)]
		[MaxLength(12)]
		[Display(Name = "Phone Number")]
		public string PhoneNumber { get; set; }
		[Display(Name = "Postal Code")]
		public string PostalAddress { get; set; }
		[Display(Name = "Postal Address")]
		public string AddressName { get; set; }
		public string Town { get; set; }
		[Display(Name = "Relationship")]
		public string RelationShipName { get; set; }
		public string UserId { get; set; }
		public string Notes { get; set; }
		[HiddenInput]
		public Guid? ApplicationId { get; set; }
	}
}
