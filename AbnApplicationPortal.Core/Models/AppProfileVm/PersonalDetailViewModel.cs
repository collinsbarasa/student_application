﻿using AbnApplicationPortal.Shared.Models.Applicants;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
	public class PersonalDetailViewModel : BaseViewModel
	{
		public PersonalDetailViewModel()
		{
			DateOfBirth = DateTime.Now;
		}
		public PersonalDetailViewModel(PersonalDetail personal)
		{
			Title = personal.Title;
			Gender = personal.Gender;
			AlternatePhoneNumber = personal.AlternatePhoneNumber;
			Language = personal.Language;
			Ethnicity = personal.Ethnicity;
			DateOfBirth = personal.DateOfBirth;
			NationalityName = personal.NationalityName;
			MaritalStatusName = personal.MaritalStatusName;
			ReligionName = personal.ReligionName;
			IdDocumentName = personal.IdDocumentName;
			IdDocumentNumber = personal.IdDocumentNumber;
			DisabilityCondition = personal.DisabilityCondition;
			Success = true;
			UserId = personal.UserId;
			IndexNo = personal.IndexNo;
			DateOfBirthStr = personal.DateOfBirthStr;
			Activities = personal.Activities;
			KcseYear = personal.KcseYear;
			KcpeIndexNo = personal.KcpeIndexNo;
			KcpeYear = personal.KcpeYear;
			Special = personal.Special;
			KcseQualification = personal.KcseQualification;
			Institution = personal.Institution;
		}

		[Required]
		public string Title { get; set; }
		[Required]
		public string Gender { get; set; }
		//[Required]
		public string Ethnicity { get; set; }
		[Display(Name = "Alternate PhoneNumber")]
		[MinLength(9)]
		[MaxLength(12)]
		public string AlternatePhoneNumber { get; set; }
		[Required]
		[MinLength(9)]
		[MaxLength(12)]
		[Display(Name = "Primary PhoneNumber")]
		public string PrimaryPhoneNumber { get; set; }
		[Display(Name = "Official Language(s)")]
		public string Language { get; set; }
		//[Required]
		[Display(Name = "Date of Birth")]
		public DateTime DateOfBirth { get; set; }
		public string DateOfBirthStr { get; set; }
		//[Required]
		[Display(Name = "Nationality")]
		public string NationalityName { get; set; }
		//[Required]
		[Display(Name = "Marital Status")]
		public string MaritalStatusName { get; set; }
		//[Required]
		[Display(Name = "Religion")]
		public string ReligionName { get; set; }
		[Required]
		[Display(Name = "Identification Document")]
		public string IdDocumentName { get; set; }
		[Required]
		[Display(Name = "Identification Document No.")]
		public string IdDocumentNumber { get; set; }
		public bool HasDisability => !string.IsNullOrEmpty(DisabilityCondition);
		[Display(Name = "Disability Condition")]
		public string DisabilityCondition { get; set; }
		[Required]
		[Display(Name = "Surname")]
		public string SurName { get; set; }
		[Required]
		[Display(Name = "Other Names")]
		public string OtherNames { get; set; }
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Alternate Email Address")]
		public string AlternateEmailAddress { get; set; }
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Primary Email Address")]
		public string PrimaryEmailAddress { get; set; }
		public string UserId { get; set; }
		//[Required]
		[Display(Name = "KCSE Index No.")]
		public string IndexNo { get; set; }
		[Display(Name = "Co-curriculum Activities")]
		public string Activities { get; set; }
		[Display(Name = "KCSE Year")]
		[MinLength(4)]
		[MaxLength(4)]
		public string KcseYear { get; set; }
		[Display(Name = "KCPE Year")]
		[MinLength(4)]
		[MaxLength(4)]
		public string KcpeYear { get; set; }
		[Display(Name = "KCPE Index No.")]
		public string KcpeIndexNo { get; set; }
		[Display(Name = "KCSE Qualification")]
		public string KcseQualification { get; set; }
		[Display(Name = "Medical Information")]
		public string Special { get; set; }
		[Display(Name = "Where you work")]
		public string Institution { get; set; }
		[HiddenInput]
		public Guid? ApplicationId { get; set; }
	}
}
