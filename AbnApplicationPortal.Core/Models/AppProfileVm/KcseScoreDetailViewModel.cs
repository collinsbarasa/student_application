﻿using AbnApplicationPortal.Shared.Models.Applicants;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
	public class KcseScoreDetailViewModel : BaseViewModel
	{
		public KcseScoreDetailViewModel()
		{
			KcseScores = new List<KcseScore>();
		}

		[Required]
		[Display(Name = "Subject")]
		public string SubjectId { get; set; }
		[Required]
		[Display(Name = "Grade")]
		public string GradeId { get; set; }
		public List<KcseScore> KcseScores { get; set; }
		[HiddenInput]
		public Guid Id { get; set; }
		[Range(1, 99)]
		public decimal Points { get; set; }
	}
}
