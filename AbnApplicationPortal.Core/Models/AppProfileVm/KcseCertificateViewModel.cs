﻿using AbnApplicationPortal.Shared.Models.Applicants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
	public class KcseCertificateViewModel : BaseViewModel
	{
		public KcseCertificateViewModel()
		{

		}

		public KcseCertificateViewModel(KCSECertificate certificate)
		{
			Description = certificate.Description;
			Extension = certificate.Extension;
			Size = certificate.Size;
			PathUrl = certificate.PathUrl;
			IsEditMode = true;
			Id = certificate.Id;
		}

		public string Description { get; set; }
		public string Extension { get; set; }
		public decimal Size { get; set; }
		[Display(Name = "KCSE Document")]
		public string PathUrl { get; set; }
		public IFormFile File { get; set; }
		[HiddenInput]
		public string FullPathUrl { get; set; }
		public string ContentType { get; set; }
		public Guid Id { get; set; }
	}
}
