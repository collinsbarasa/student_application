﻿using AbnApplicationPortal.Shared.Models.Applicants;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
	public class LocationViewModel : BaseViewModel
	{
		public LocationViewModel()
		{

		}
		public LocationViewModel(PersonalDetail detail)
		{
			CountyName = detail.CountyName;
			SubCountyName = detail.SubCountyName;
			Constituency = detail.Constituency;
			Town = detail.Town;
			PostalAddress = detail.PostalAddress;
			AddressName = detail.AddressName;
			UserId = detail.UserId;
			Success = true;
		}

		[Required]
		[Display(Name = "County")]
		public string CountyName { get; set; }
		[Required]
		[Display(Name = "Sub-County")]
		public string SubCountyName { get; set; }
		public string Constituency { get; set; }
		[Display(Name = "Town")]
		public string Town { get; set; }
		[Display(Name = "Postal Code")]
		public string PostalAddress { get; set; }
		[Display(Name = "Postal Address")]
		public string AddressName { get; set; }
		public string UserId { get; set; }
		[HiddenInput]
		public Guid? ApplicationId { get; set; }
	}
}
