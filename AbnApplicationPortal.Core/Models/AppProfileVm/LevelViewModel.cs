﻿using AbnApplicationPortal.Core.Data.AppDb;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
    public class LevelViewModel : BaseViewModel
    {
        public LevelViewModel() {}
        public LevelViewModel(Level level) {
            Success = true;
            ApplicationLevel = level.ApplicationLevel;
            Financing = level.Financing;
            ResearchInsitute = level.ResearchInsitute;
            UserId = level.UserId;
            Referees = level.Referees;
        }
        [Required]
        [Display(Name ="Application Academic Level")]
        public string ApplicationLevel { get; set; }
        [Required]
        [Display(Name ="Reseach Institute to undertake your work.")]
        public string ResearchInsitute { get; set; }
        [Required]
        [Display(Name = "How will you finance your studies.")]
        public string Financing { get; set; }
        public string UserId { get; set; }
        public Guid? ApplicationId { get; set; }
        public List<Referee> Referees { get; set; }

        
    }
}
