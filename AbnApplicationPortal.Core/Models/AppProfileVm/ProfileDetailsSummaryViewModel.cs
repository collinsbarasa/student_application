﻿namespace AbnApplicationPortal.Core.Models.AppProfileVm
{
    public class ProfileDetailsSummaryViewModel : BaseViewModel
    {
        public ProfileDetailsSummaryViewModel()
        {
            Education = new EducationDetailViewModel();
            Location = new LocationViewModel();
            NextOfKin = new NextOfKinDetailViewModel();
            PersonalDetail = new PersonalDetailViewModel();
            Work = new WorkDetailViewModel();
            Level = new LevelViewModel();
            Emergency = new EmergencyContactDetailViewModel();
        }
        public EducationDetailViewModel Education { get; set; }
        public WorkDetailViewModel Work { get; set; }
        public LocationViewModel Location { get; set; }
        public NextOfKinDetailViewModel NextOfKin { get; set; }
        public PersonalDetailViewModel PersonalDetail { get; set; }
        public EmergencyContactDetailViewModel Emergency { get; set; }
        public LevelViewModel Level { get; set; }
        public bool IsComplete => PersonalDetail.Success && Level.Success && 
            NextOfKin.Success && Location.Success && Emergency.Success;
    }
}
