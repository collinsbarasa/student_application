﻿using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
	public class TestSmsViewModel : BaseViewModel
	{
		[Required]
		public string Phone { get; set; }
		[Required]
		public string Content { get; set; }
	}
}
