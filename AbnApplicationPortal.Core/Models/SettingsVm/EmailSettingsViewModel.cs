﻿using AbnApplicationPortal.Shared.Requests.Settings;
using System.ComponentModel.DataAnnotations;
using AbnApplicationPortal.Shared.Models.Settings;
using MailKit.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
    public class EmailSettingsViewModel : BaseViewModel
    {
        public EmailSettingsViewModel()
        {

        }
        public EmailSettingsViewModel(EmailSetting email)
        {
            SmtpPort = email.SmtpPort;
            SmtpPassword = email.SmtpPassword;
            SmtpUsername = email.SmtpUsername;
            SmtpServer = email.SmtpServer;
            SenderFromEmail = email.SenderFromEmail;
            SenderFromName = email.SenderFromName;
            SocketOptions = email.SocketOptions;
        }
        [Required]
        [MaxLength(50)]
        public string SmtpServer { get; set; }
        [Required]
        [MaxLength(50)]
        public string SmtpPort { get; set; }
        [Required]
        [MaxLength(70)]
        public string SmtpUsername { get; set; }
        [Required]
        public string SmtpPassword { get; set; }
        [Required]
        [MaxLength(70)]
        [DataType(DataType.EmailAddress)]
        public string SenderFromEmail { get; set; }
        public string SenderFromName { get; set; }
        public SettingKey Key => SettingKey.Email;
        public SecureSocketOptions SocketOptions { get; set; }
        public List<FormSelectViewModel> Options()
        {
            var statusesList = Enum.GetValues(typeof(SecureSocketOptions))
                .Cast<SecureSocketOptions>()
                .ToList();
            return statusesList.Select(s => new FormSelectViewModel
            {
                Id = s.ToString(),
                Text = s.ToString()
            }).ToList();
        }
    }
}
