﻿using AbnApplicationPortal.Shared.Models.Settings;
using AbnApplicationPortal.Shared.Requests.Settings;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
	public class SoapSettingViewModel : BaseViewModel
	{
		public SoapSettingViewModel()
		{

		}

		public SoapSettingViewModel(SoapSetting setting)
		{
			Url = setting.Url;
			UserName = setting.UserName;
			Password = setting.Password;
		}
		public SettingKey Key => SettingKey.Soap;
		[Required]
		public string Url { get; set; }
		[Required]
		public string UserName { get; set; }
		[Required]
		public string Password { get; set; }

	}
}
