﻿using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
	public class TestSoapViewModel : BaseViewModel
	{
		[Required]
		[Display(Name = "Reg No.")]
		public string RegNo { get; set; }
		[Required]
		[Display(Name = "Mode No.")]
		public string ModeNo { get; set; }
		public double Amount { get; set; }
	}
}
