﻿using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
	public class TestStkViewModel : BaseViewModel
	{
		public TestStkViewModel()
		{
			Amount = 1;
		}
		[Required]
		[MinLength(12)]
		public string Phone { get; set; }
		public int Amount { get; set; }
	}
}
