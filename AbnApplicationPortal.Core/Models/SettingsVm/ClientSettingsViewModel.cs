﻿using AbnApplicationPortal.Shared.Models.Settings;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
	public class ClientSettingsViewModel : BaseViewModel
	{
		public ClientSettingsViewModel()
		{

		}

		public ClientSettingsViewModel(ClientSetting client)
		{
			AppVersion = client.AppVersion;
			ClientName = client.ClientName;
			ClientCode = client.ClientCode;
			HasUnisol = client.HasUnisol;
			SimpleProfile = client.SimpleProfile;
			AppRefPrefix = client.AppRefPrefix;
			TagLine = client.TagLine;
			LogoUrl = client.LogoUrl;
			PrimaryColor = client.PrimaryColor;
			SecondaryColor = client.SecondaryColor;
			AllowPartialPayments = client.AllowPartialPayments;
			IsEditMode = true;
			AppName = client.AppName;
			ImageUrl = client.ImageUrl;
			ContactEmail = client.ContactEmail;
			IsSaga = client.IsSaga;
			ViewSchoolOnly = client.ViewSchoolOnly;
		}
		[Display(Name = "Version")]
		public string AppVersion { get; set; }
		[Display(Name = "Applicaton Name")]
		public string AppName { get; set; }
		[Display(Name = "Names")]
		public string ClientName { get; set; }
		[Display(Name = "Initials")]
		public string ClientCode { get; set; }
		public bool HasUnisol { get; set; }
		public bool SimpleProfile { get; set; }
		[Display(Name = "Prefix")]
		public string AppRefPrefix { get; set; }
		[Display(Name = "Tag Line")]
		public string TagLine { get; set; }
		[Display(Name = "Logo Image")]
		public string LogoUrl { get; set; }
		public string PrimaryColor { get; set; }
		public string SecondaryColor { get; set; }
		[Display(Name ="Letter Footer Image")]
		public string ImageUrl{get; set;}
		public bool AllowPartialPayments { get; set; }
		[EmailAddress]
		[Display(Name = "Contact Email")]
		public string ContactEmail { get; set; }
		[Display(Name = "Phone Number")]
        public IFormFile File { get; set; }
		public IFormFile LFImage { get; set; }
		public bool IsNewLFImg { get; set; }
		public bool IsNewImg { get; set; }
		public bool IsSaga { get; set; }
		public bool ViewSchoolOnly { get; set; }
	}
}
