﻿using System.ComponentModel.DataAnnotations;
using AbnApplicationPortal.Shared.Models.Settings;
using AbnApplicationPortal.Shared.Requests.Settings;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
    public class SmsSettingsViewModel : BaseViewModel
    {
        public SmsSettingsViewModel()
        {

        }

        public SmsSettingsViewModel(SmsSetting sms)
        {
            Username = sms.Username;
            ApiKey = sms.ApiKey;
            SenderId = sms.SenderId;
            ShortCode = sms.ShortCode;
        }
        [Required]
        public string Username { get; set; }
        [Required]
        public string ApiKey { get; set; }
        public string SenderId { get; set; }
        public string ShortCode { get; set; }
        public SettingKey Key => SettingKey.Sms;
    }
}
