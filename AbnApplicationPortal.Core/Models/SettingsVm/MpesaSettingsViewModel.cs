﻿using System.ComponentModel.DataAnnotations;
using AbnApplicationPortal.Shared.Models.Settings;
using AbnApplicationPortal.Shared.Requests.Settings;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
	public class MpesaSettingsViewModel : BaseViewModel
	{
		public MpesaSettingsViewModel()
		{

		}

		public MpesaSettingsViewModel(MpesaSetting mpesa)
		{
			AbnExpressUrl = mpesa.AbnExpressUrl;
			CallBackURL = mpesa.CallBackURL;
			PassKey = mpesa.PassKey;
			BusinessShortCode = mpesa.BusinessShortCode;
			AbnClientId = mpesa.AbnClientId;
			ConsumerKey = mpesa.ConsumerKey;
			ConsumerSecret = mpesa.ConsumerSecret;
		}

		[Required]
		[DataType(DataType.Url)]
		[Display(Name = "ABN Express URL")]
		public string AbnExpressUrl { get; set; }
		[Required]
		[DataType(DataType.Url)]
		[Display(Name = "CallBack URL")]
		public string CallBackURL { get; set; }
		[Required]
		[Display(Name = "Pass Key")]
		public string PassKey { get; set; }
		[MaxLength(8)]
		[Display(Name = "Business Short Code")]
		public string BusinessShortCode { get; set; }
		[Display(Name = "ABN Client Key")]
		public string AbnClientId { get; set; }
		public string ConsumerKey { get; set; }
		public string ConsumerSecret { get; set; }
		public SettingKey Key => SettingKey.Mpesa;
	}
}
