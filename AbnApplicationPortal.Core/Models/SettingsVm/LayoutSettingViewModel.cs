﻿using AbnApplicationPortal.Shared.Models.Settings;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
	public class LayoutSettingViewModel
	{
		public LayoutSettingViewModel()
		{

		}
		public LayoutSettingViewModel(LayoutSetting layout)
		{
			HomePage = layout.HomePage?.Replace("\n", "<br />");
			CreateAccount = layout.CreateAccount?.Replace("\n", "<br />");
			FindCourse = layout.FindCourse?.Replace("\n", "<br />");
			SubmitApplication = layout.SubmitApplication?.Replace("\n", "<br />");
			ContactUs = layout.ContactUs?.Replace("\n", "<br />");
			RecommendedCourses = layout.RecommendedCourses?.Replace("\n", "<br />");
			CurrentIntakeImage = layout.CurrentIntakes;
		}

		[Display(Name = "Home Page")] 
		public string HomePage { get; set; }
		[Display(Name = "Create Account")]
		public string CreateAccount { get; set; }
		[Display(Name = "Find Course")]
		public string FindCourse { get; set; }
		[Display(Name = "Submit Application")]
		public string SubmitApplication { get; set; }
		[Display(Name = "Contact Us")]
		public string ContactUs { get; set; }
		[Display(Name = "Recommended Courses")]
		public string RecommendedCourses { get; set; }
		[Display(Name = "Home Intake Image")]
		public string CurrentIntakeImage { get; set; }
		public IFormFile PdfFImage{get; set;}
		public IFormFile File { get; set; }
		public bool IsNewImg { get; set; }
	}
}
