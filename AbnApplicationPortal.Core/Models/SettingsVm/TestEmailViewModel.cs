﻿using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.SettingsVm
{
	public class TestEmailViewModel : BaseViewModel
	{
		[Required]
		public string Names { get; set; }
		[EmailAddress]
		public string Email { get; set; }
		[Required]
		public string Subject { get; set; }
		[Required]
		public string Content { get; set; }
	}
}
