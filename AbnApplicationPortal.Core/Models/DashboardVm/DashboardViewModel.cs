﻿using AbnApplicationPortal.Core.Models.ApplicationVm;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Models.DashboardVm
{
    public class DashboardViewModel
    {
       
        public Deferment Deferments { get; set; }
        public Application Applications { get; set; }
        public Programme Programmes { get; set; }
        public ApplicationPayment ApplicationPayments { get; set; }
        public int DefermentCount { get; set; }
        public int ApplicationCount { get; set; }
        public int ProgramCount { get; set; }
        public int PendingCount { get; set; }
        public int ApprovedCount { get; set; }
        
    }
}
