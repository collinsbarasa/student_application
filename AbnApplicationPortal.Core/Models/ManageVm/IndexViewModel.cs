﻿using AbnApplicationPortal.Core.Models.AccountVm;
using AbnApplicationPortal.Shared.Models.Applicants;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace AbnApplicationPortal.Core.Models.ManageVm
{
	public class IndexViewModel
	{
		public bool IsEmailConfirmed { get; set; }
		[Required]
		[EmailAddress]
		[Display(Name = "Email Address")]
		public string Email { get; set; }
		[Phone]
		[Required]
		[Display(Name = "Phone Number")]
		public string PhoneNumber { get; set; }
		public DateTime CreatedAt { get; set; }
		public string Names { get; set; }
		[Display(Name = "Passport Picture")]
		public string ProfilePhotoPath { get; set; }
		[Display(Name = "Signature Picture")]
		public string SignaturePhotoPath { get; set; }
		public IFormFile File { get; set; }
		public ResetPasswordViewModel resetPasswordViewModel { get; set; }
		public ChangePasswordViewModel changePasswordViewModel { get; set; }
		public List<Notification> Notifications { get; set; }
	}
}
