﻿using Microsoft.AspNetCore.Mvc;

namespace AbnApplicationPortal.Core.Models
{
	public class BaseViewModel
	{
		public BaseViewModel()
		{
			Success = false;
		}
		public bool Success { get; set; }
		public string Message { get; set; }
		public string ErrorMessage { get; set; }
		[HiddenInput]
		public bool IsEditMode { get; set; }
	}
}
