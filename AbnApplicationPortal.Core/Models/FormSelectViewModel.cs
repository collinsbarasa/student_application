﻿namespace AbnApplicationPortal.Core.Models
{
    public class FormSelectViewModel
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }
}
