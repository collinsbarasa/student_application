﻿using AbnApplicationPortal.Shared.Models;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
	public class FilterApplicationsViewModel
	{
		public string SortOrder { get; set; }
		public string CurrentFilter { get; set; }
		public string SearchString { get; set; }
		public int? PageNumber { get; set; }
		public int? ItemsPerPage { get; set; }
		public string IntakeId { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
		public string GradeTypeId { get; set; }
		public EntityStatus? Status { get; set; }
		public string ClassTypeId { get; set; }
		public string Paid { get; set; }
		public string Pushed { get; set; }
		public string Disability { get; set; }
		public string Gender { get; set; }
		public string ProgrammeId { get; set; }
	}
}
