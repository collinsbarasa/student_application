﻿using System;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
	public class CommentViewModel
	{
		public Guid AppId { get; set; }
		public string Comment { get; set; }
	}
}
