﻿using System;
using System.ComponentModel.DataAnnotations;
using AbnApplicationPortal.Shared.Models.Intakes;
using Microsoft.AspNetCore.Mvc;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
    public class DefermentViewModel : BaseViewModel
    {
        public DefermentViewModel() {
            ExpectedDate = DateTime.Now;
        }
        public DefermentViewModel(Deferment deferment) {
            Id = deferment.Id;
            Reason = deferment.Reason;
            //DeferPeriod = deferment.DeferPeriod;
            ApplicationId = deferment.ApplicationId;
            //ExpectedDate = deferment.ExpectedDate;
            Approval = deferment.Approval;
            Count = deferment.Count;
            Unit = deferment.Unit;
        }
        public Guid Id { get; set; }
        [Display(Name="Reason for Defering")]
        public Guid Reason { get; set; }
        [Display(Name = "Defering Period")]
        public Guid DeferPeriod { get; set; }
        [HiddenInput]
        public Guid ApplicationId { get; set; }
        [Display(Name = "Expected Reporting Date")]
        [DataType(DataType.Date)]
        public DateTime ExpectedDate { get; set; } 
        public string Approval { get; set; }
        public string PeriodString { get; set; }
        public string ReasonString { get; set; }
        [Display(Name = "Select Defer Semester or Academic Year")]
        public string Unit { get; set; }
        [Display(Name = "How Many")]
        public int Count { get; set; }
    }
}
