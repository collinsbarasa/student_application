﻿using AbnApplicationPortal.Core.Models.IntakeVm;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using System.Collections.Generic;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
	public class ApplicationViewModel : BaseViewModel
	{
		public ApplicationViewModel()
		{
			Documents = new List<IntakeGradeTypeDocumentViewModel>();
			Programmes = new List<IntakeGradeTypeProgrammeViewModel>();
			Deferments = new List<DefermentViewModel>();
		}
		public Application Application { get; set; }
		public List<IntakeGradeTypeDocumentViewModel> Documents { get; set; }
		public List<IntakeGradeTypeProgrammeViewModel> Programmes { get; set; }
		public List<DefermentViewModel> Deferments { get; set; }
		public IntakeGradeTypeViewModel GradeType { get; set; }
		public decimal Balance => TotalExpected - (TotalPaid + TotalWaiver);
		public string Paybill { get; set; }
		public bool PaybillIsSelf { get; set; }
		public decimal TotalPaid { get; set; }
		public decimal TotalWaiver { get; set; }
		public decimal TotalExpected { get; set; }
		public string UniStatus { get; set; }
		public Intake Intake { get; set; }
	}
}
