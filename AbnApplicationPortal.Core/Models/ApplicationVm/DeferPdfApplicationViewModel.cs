﻿using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Shared.Models.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
    public class DeferPdfApplicationViewModel
    {
        public string Message { get; set; }
        public bool Success { get; set; }
        public ClientSetting ClientSetting { get; set; }
        public List<DeferApplicationViewModel> DeferApplications { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public string SignaturePathUrl { get; set; }
    }
}
