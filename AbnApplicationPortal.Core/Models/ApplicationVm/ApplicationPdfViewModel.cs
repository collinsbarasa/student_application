﻿using AbnApplicationPortal.Shared.Models.Applicants;
using System.Collections.Generic;
namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
    public class ApplicationPdfViewModel: BaseViewModel
    {
        public Application Application { get; set; }
        public List<WorkExperience> WorkExperiences { get; set; }
        public List<EducationExperience> EducationExperiences { get; set; }
        public List<KcseScore> KcseScores { get; set; }
        public string Level { get; set; }
    }
}
