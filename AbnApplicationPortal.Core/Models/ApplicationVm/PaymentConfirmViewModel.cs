﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
	public class PaymentConfirmViewModel : BaseViewModel
	{
		public Guid ApplicationId { get; set; }
		public decimal Balance { get; set; }
		[MinLength(12)]
		public string PhoneNumber { get; set; }
		public string Names { get; set; }
		public string RefNo { get; set; }
		public string Paybill { get; set; }
		public bool PaybillISelf { get; set; }
	}
}
