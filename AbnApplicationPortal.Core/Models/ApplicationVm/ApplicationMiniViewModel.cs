﻿using AbnApplicationPortal.Shared.Models;
using AbnApplicationPortal.Shared.Models.Applicants;
using System;
using System.Collections.Generic;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
	public class ApplicationMiniViewModel : BaseViewModel
	{
		public ApplicationMiniViewModel()
		{
			Programmes = new List<ProgrammeViewModel>();
		}

		public ApplicationMiniViewModel(Application application)
		{
			IndexNo = application.IndexNo;
			RefNo = application.Code;
			DateAdded = application.DateCreatedStr;
			GradeCertTypeName = application.GradeType;
			IsFullyPaid = application.IsFullyPaid;
			IsUploaded = application.IsUploaded;
			Status = application.Status;
			Programmes = new List<ProgrammeViewModel>();
			Id = application.Id;
			EmailAddress = application.Email;
			PhoneNumber = application.TelNo;
			IdNumber = application.NationalId;
		}

		public Guid Id { get; set; }
		public string IndexNo { get; set; }
		public string RefNo { get; set; }
		public string DateAdded { get; set; }
		public string IntakeCode { get; set; }
		public string IntakeName { get; set; }
		public string IntakeDescription { get; set; }
		public string IntakePeriod { get; set; }
		public string GradeCertTypeName { get; set; }
		public EntityStatus Status { get; set; }
		public bool IsFullyPaid { get; set; }
		public bool IsUploaded { get; set; }
		public string PhoneNumber { get; }
		public string EmailAddress { get; }
		public string IdNumber { get; }
		public List<ProgrammeViewModel> Programmes { get; set; }
		public string IntakeStatus { get; set; }
		public BaseEntity BaseEntity {get; set;}

	}
}
