﻿using AbnApplicationPortal.Shared.Models.Applicants;
using System;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
    public class UploadViewModel : BaseViewModel
    {
        public UploadViewModel()
        {

        }
        public UploadViewModel(ApplicationUpload upload)
        {
            Id = upload.Id;
            Name = upload.Name;
            Description = upload.Description;
            Path = upload.Path;
            Size = upload.Size;
            Extension = upload.Extension;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public decimal Size { get; set; }
        public string Extension { get; set; }
    }
}
