﻿using AbnApplicationPortal.Shared.Models.Applicants;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
    public class DeferApplicationViewModel
    {
        public DeferApplicationViewModel(Application application, DefermentViewModel deferment) {
            Application = application;
            Deferment = deferment;
        }
        public Application Application { get; set; }
        public DefermentViewModel Deferment { get; set; }
        public string AdmNo { get; set; }
    }
}
