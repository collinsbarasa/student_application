﻿using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Data.Unisol.Entities;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Models.Settings;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
    public class DeferLeterViewModel
    {
        public Application Application { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public ClientSetting Client { get; set; }
        public string Message { get; set; }
        public string ReportMessage { get; set; }
        public UniApplicant UniApplicant { get; set; }
        public Deferment Deferment { get; set; }
        public string SignaturePathUrl { get; set; }
    }
}
