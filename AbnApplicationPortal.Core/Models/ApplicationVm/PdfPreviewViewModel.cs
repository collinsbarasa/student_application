﻿using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Models.Settings;
using System.Collections.Generic;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
    public class PdfPreviewViewModel
    {
        public List<ApplicationPdfViewModel> Applications { get; set; }
        public DefermentViewModel DefermentViewModel{ get; set; }
        public ClientSetting ClientSetting { get; set; }
        public Programme Programme { get; set; }
        public Intake Intake { get; set; }
        public string School { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
        public bool IsPostGrad { get; set; }
    }
}
