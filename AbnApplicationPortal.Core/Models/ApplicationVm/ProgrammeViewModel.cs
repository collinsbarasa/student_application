﻿using AbnApplicationPortal.Shared.Models.Intakes;
using System;

namespace AbnApplicationPortal.Core.Models.ApplicationVm
{
	public class ProgrammeViewModel : BaseViewModel
	{
		public ProgrammeViewModel()
		{

		}

		public ProgrammeViewModel(Programme programme)
		{
			Code = programme.Code;
			Name = programme.Name;
			Requirements = programme.Requirements;
			Period = programme.Period;
			GradeType = programme.GradeType;
			CertType = programme.CertType;
			Department = programme.Department;
			Id = programme.Id;
		}

		public string Code { get; set; }
		public string Name { get; set; }
		public string Requirements { get; set; }
		public string Period { get; set; }
		public string GradeType { get; set; }
		public string CertType { get; set; }
		public string Department { get; set; }
		public bool IsMain { get; set; }
		public bool IsApproved { get; set; }
		public Guid Id { get; set; }
	}
}
