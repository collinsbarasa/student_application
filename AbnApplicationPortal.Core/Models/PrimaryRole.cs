﻿namespace AbnApplicationPortal.Core.Models
{
	public class PrimaryRole
	{
		public const string SuperAdmin = "SuperAdmin";
		public const string Admin = "Admin";
		public const string Staff = "Staff";
		public const string Student = "Student";
		public const string Applicant = "Applicant";
	}
}
