﻿using AbnApplicationPortal.Shared.Models.Intakes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
	public class DocumentViewModel : BaseViewModel
	{
		public DocumentViewModel()
		{

		}

		public DocumentViewModel(Document document)
		{
			Id = document.Id;
			Name = document.Name;
			Description = document.Description;
			Category = document.Category;
			IsEditMode = true;
		}
		[HiddenInput]
		public Guid Id { get; set; }
		[Required]
		public string Name { get; set; }
		public string Description { get; set; }
		public string Category { get; set; }
	}
}
