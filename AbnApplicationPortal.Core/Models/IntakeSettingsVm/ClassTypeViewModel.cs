﻿using AbnApplicationPortal.Shared.Models.Intakes;
using System;

namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
	public class ClassTypeViewModel : BaseViewModel
	{
		public ClassTypeViewModel()
		{

		}

		public ClassTypeViewModel(ClassType type)
		{
			Id = type.Id;
			Code = type.Code;
			Name = type.Name;
			Description = type.Description;
			IsEditMode = true;
		}

		public Guid Id { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
	}
}
