﻿using AbnApplicationPortal.Shared.Models.Intakes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
    public class DeferPeriodViewModel : BaseViewModel
    {
        public DeferPeriodViewModel() {
            StartDate = DateTime.Now;
            EndDate = DateTime.Now.AddDays(30);
        }

        public DeferPeriodViewModel(DeferPeriod deferPeriod) {
            Id = deferPeriod.Id;
            Name = deferPeriod.Name;
            StartDate = deferPeriod.StartDate;
            EndDate = deferPeriod.EndDate;
            Active = deferPeriod.Active;
        }
        [HiddenInput]
        public Guid Id { get; set; }
        public string Name { get; set; }
        [Display(Name = "Period Start Date")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Display(Name = "Period End Date")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        public bool Active { get; set; }
    }
}
