﻿using AbnApplicationPortal.Shared.Models.Intakes;
using Microsoft.AspNetCore.Mvc;
using System;

namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
	public class GradeTypeViewModel : BaseViewModel
	{
		public GradeTypeViewModel()
		{

		}

		public GradeTypeViewModel(GradeCertificateType type)
		{
			Id = type.Id;
			Code = type.Code;
			Notes = type.Notes;
			IsEditMode = true;
			Names = type.GradeType;
			CertType = type.CertType;
		}

		[HiddenInput]
		public Guid Id { get; set; }
		public string Code { get; set; }
		public string Names { get; set; }
		public string Notes { get; set; }
		public string CertType { get; set; }
	}
}
