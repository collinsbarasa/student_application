﻿using AbnApplicationPortal.Shared.Models.Intakes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
	public class CampusViewModel : BaseViewModel
	{
		public CampusViewModel()
		{

		}

		public CampusViewModel(Campus campus)
		{
			Code = campus.Code;
			Id = campus.Id;
			Name = campus.Name;
			Description = campus.Description;
			Contact = campus.Contact;
			Address = campus.Address;
			Telephone = campus.Telephone;
			Email = campus.Email;
			Website = campus.Website;
		}

		[HiddenInput]
		public Guid Id { get; set; }
		[Required]
		public string Code { get; set; }
		[Required]
		public string Name { get; set; }
		public string Description { get; set; }
		public string Contact { get; set; }
		public string Address { get; set; }
		public string Telephone { get; set; }
		public string Email { get; set; }
		public string Website { get; set; }
	}
}
