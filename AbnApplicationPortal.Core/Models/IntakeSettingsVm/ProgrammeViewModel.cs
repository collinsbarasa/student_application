﻿using AbnApplicationPortal.Shared.Models.Intakes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
	public class ProgrammeViewModel : BaseViewModel
	{
		public ProgrammeViewModel()
		{

		}

		public ProgrammeViewModel(Programme programme)
		{
			Id = programme.Id;
			Name = programme.Name;
			Code = programme.Code;
			Description = programme.Notes;
			Requirements = programme.Requirements;
			Period = programme.Period;
			GradeType = programme.GradeType;
			Department = programme.Department;
		}

		[HiddenInput]
		public Guid Id { get; set; }
		[Required]
		public string Code { get; set; }
		[Required]
		public string Name { get; set; }
		public string Description { get; set; }
		public string Requirements { get; set; }
		public string Period { get; set; }
		public string GradeType { get; set; }
		public string Department { get; set; }
	}
}
