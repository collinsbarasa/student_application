﻿using AbnApplicationPortal.Shared.Models.Settings;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
    public class CategoryViewModel : BaseViewModel
    {
        public CategoryViewModel() { }

        public CategoryViewModel(Category category) {
            Id = category.Id;
            Name = category.Name;
            Description = category.Description;
            Owner = category.Owner;
        }
        [HiddenInput]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Owner { get; set; }
    }
}
