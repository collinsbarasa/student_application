﻿using AbnApplicationPortal.Shared.Models.Enquiry;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
    public class EnquiryViewModel : BaseViewModel
    {
        public EnquiryViewModel() { }

        public EnquiryViewModel(Enquiry enquiry) {
            Id = enquiry.Id;
            Code = enquiry.Code;
            Name = enquiry.Name;
            Phone = enquiry.Phone;
            Email = enquiry.Email;
            CategoryId = enquiry.CategoryId;
            Message = enquiry.Message;
            FollowUpCode = enquiry.FollowUpCode;
        }
        [HiddenInput]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Code { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Guid CategoryId { get; set; }
        public new string Message { get; set; }
        public string FollowUpCode { get; set; }
        public string CategoryStr { get; set; }
    }
}
