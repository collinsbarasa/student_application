﻿namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
	public class IntakeSettingsSummaryViewModel : BaseViewModel
	{
		public int Campuses { get; set; }
		public int Programmes { get; set; }
		public int ClassTypes { get; set; }
		public int GradeTypes { get; set; }
		public int Documents { get; set; }
		public int KcseSubjects { get; set; }
	}
}
