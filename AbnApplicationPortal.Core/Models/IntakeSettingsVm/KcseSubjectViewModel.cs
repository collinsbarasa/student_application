﻿using AbnApplicationPortal.Shared.Models.Applicants;
using System;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.IntakeSettingsVm
{
	public class KcseSubjectViewModel : BaseViewModel
	{
		public KcseSubjectViewModel()
		{

		}
		public KcseSubjectViewModel(KcseSubject subject)
		{
			Id = subject.Id;
			Code = subject.Code;
			Name = subject.Name;
			Abbreviation = subject.Abbreviation;
			IsCompulsory = subject.IsCompulsory;
			IsMeanGrade = subject.IsMeanGrade;
			IsEditMode = true;
		}
		public Guid Id { get; set; }
		[Required]
		public string Code { get; set; }
		[Required]
		public string Name { get; set; }
		public string Abbreviation { get; set; }
		public bool IsCompulsory { get; set; }
		public bool IsMeanGrade { get; set; }
	}
}
