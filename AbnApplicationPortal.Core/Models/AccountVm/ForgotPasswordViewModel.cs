﻿using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AccountVm
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
