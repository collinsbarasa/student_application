﻿using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AccountVm
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
