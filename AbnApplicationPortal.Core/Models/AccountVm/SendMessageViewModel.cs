﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AccountVm
{
	public class SendMessageViewModel
	{
		[Required]
		public string Name { get; set; }
		[EmailAddress]
		public string Email { get; set; }
		[Required]
		public string Message { get; set; }
		[Required]
		public string Result { get; set; }
		[HiddenInput]
		public string IntResult { get; set; }
	}
}
