﻿namespace AbnApplicationPortal.Core.Models.AccountVm
{
	public class AccountCountViewModel
	{
		public int StaffAccounts { get; set; }
		public int ApplicantAccounts { get; set; }
		public int AdminAccounts { get; set; }
	}
}
