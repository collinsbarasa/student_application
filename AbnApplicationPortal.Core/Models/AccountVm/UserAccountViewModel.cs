﻿using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Data.Unisol.Entities;

namespace AbnApplicationPortal.Core.Models.AccountVm
{
	public class UserAccountViewModel
	{
		public UserAccountViewModel()
		{

		}

		public UserAccountViewModel(UniStudent student)
		{
			UserName = student.Email;
			EmailAddress = student.Email;
			IdNumber = student.NationalId;
			PhoneNumber = student.TelNo;
			Names = student.Names;
			RefNo = student.AdmnNo;
			Department = student.Department;
			Gender = student.Gender;
			PrimaryType = PrimaryRole.Student;
		}

		public UserAccountViewModel(UniEmployee employee)
		{
			UserName = employee.Wemail;
			EmailAddress = employee.Wemail;
			IdNumber = employee.IdNo;
			PhoneNumber = employee.Cell;
			Names = employee.Names;
			RefNo = employee.EmpNo;
			Department = employee.Department;
			Gender = employee.Gender;
			PrimaryType = PrimaryRole.Staff;
		}

		public UserAccountViewModel(ApplicationUser user)
		{
			UserName = user.UserName;
			EmailAddress = user.Email;
			PhoneNumber = user.PhoneNumber;
			Names = user.Names;
			RefNo = user.RefNo;
			Department = user.Department;
			Id = user.Id;
			PrimaryType = user.PrimaryRole;
			EmailConfirmed = user.EmailConfirmed;
		}

		public string UserName { get; set; }
		public string EmailAddress { get; set; }
		public string PhoneNumber { get; set; }
		public bool EmailConfirmed { get; set; }
		public string Id { get; set; }
		public string IdNumber { get; set; }
		public string PrimaryType { get; set; }
		public string RefNo { get; set; }
		public string Names { get; set; }
		public string Department { get; set; }
		public string Gender { get; set; }
	}
}
