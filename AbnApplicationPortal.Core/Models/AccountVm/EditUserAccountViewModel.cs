﻿using System.ComponentModel.DataAnnotations;
using AbnApplicationPortal.Core.Data.AppDb;
using Microsoft.AspNetCore.Mvc;

namespace AbnApplicationPortal.Core.Models.AccountVm
{
	public class EditUserAccountViewModel
	{
		public EditUserAccountViewModel()
		{

		}

		public EditUserAccountViewModel(ApplicationUser user)
		{
			Id = user.Id;
			PhoneNumber = user.PhoneNumber;
			UserName = user.UserName;
			EmailConfirmed = user.EmailConfirmed;
			EmailAddress = user.Email;
			Role = user.PrimaryRole;
		}

		[Required]
		[Display(Name = "Phone Number")]
		public string PhoneNumber { get; set; }
		[Required]
		public string Role { get; set; }
		public string UserName { get; set; }
		[Display(Name = "Email Address")]
		[Required]
		[DataType(DataType.EmailAddress)]
		public string EmailAddress { get; set; }
		[Display(Name = "Email Confirmed ")]
		public bool EmailConfirmed { get; set; }
		[HiddenInput]
		public string Id { get; set; }
	}
}
