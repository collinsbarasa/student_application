﻿using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AccountVm
{
	public class RegisterViewModel
	{
		[Required]
		[EmailAddress]
		[Display(Name = "Email Address")]
		public string Email { get; set; }
		[Required]
		[StringLength(20, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 4)]
		[DataType(DataType.Password)]
		[Display(Name = "Create Password")]
		public string Password { get; set; }
		[DataType(DataType.Password)]
		[Display(Name = "Confirm password")]
		[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
		public string ConfirmPassword { get; set; }
		[Required]
		public string Role { get; set; }
		public string Department { get; set; }
		[Display(Name = "Sur Name")]
		public string SurName { get; set; }
		[Display(Name = "Other Names")]
		public string OtherNames { get; set; }
		public string KcseIndexNo { get; set; }
		[Display(Name = "Identification Document")]
		public string IdDocumentName { get; set; }
		[Display(Name = "Identification Document No.")]
		public string IdDocumentNumber { get; set; }
	}
}
