﻿using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.AccountVm
{
    public class LoginWithRecoveryCodeViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Recovery Code")]
        public string RecoveryCode { get; set; }
    }
}
