﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Models.MessagesVm
{
    public class MessageViewModel
    {
        public string Message { get; set; }
        public string TimeStamp { get; set; }
        public string SenderName { get; set; }
        public string SenderId { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverId { get; set; }
        public bool IsIncoming { get; set; }
    }
}
