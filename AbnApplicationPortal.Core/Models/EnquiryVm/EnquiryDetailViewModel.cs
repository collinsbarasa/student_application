﻿using AbnApplicationPortal.Core.Models.IntakeSettingsVm;
using AbnApplicationPortal.Shared.Models.Enquiry;
using System.Collections.Generic;

namespace AbnApplicationPortal.Core.Models.EnquiryVm
{
    public class EnquiryDetailViewModel : BaseViewModel
    {
        public EnquiryViewModel Enquiry { get; set; }
        public List<FeedBack> EnquiryFeedBacks { get; set; }
    }
}
