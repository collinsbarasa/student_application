﻿using System.Collections.Generic;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
    public class IntakeListViewModel
    {
        public List<IntakeSetupViewModel> Intakes { get; set; }
    }
}
