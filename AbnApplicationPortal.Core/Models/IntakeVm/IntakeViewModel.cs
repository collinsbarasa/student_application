﻿using AbnApplicationPortal.Shared.Models;
using AbnApplicationPortal.Shared.Models.Intakes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
	public class IntakeViewModel : BaseViewModel
	{
		public IntakeViewModel()
		{
			IntakeGradeTypes = new List<IntakeGradeType>();
			StartDate = EndDate = DateTime.Now;
		}

		public IntakeViewModel(Intake intake)
		{
			Id = intake.Id;
			Code = intake.Code;
			Active = intake.IsActive;
			Name = intake.Name;
			Description = intake.Description;
			StartDate = intake.StartDate;
			EndDate = intake.EndDate;
			IsEditMode = true;
			StartDateStr = intake.StartDateStr;
			EndDateStr = intake.EndDateStr;
			StatusStr = intake.StatusStr;
			IntakeGradeTypes = intake.IntakeGradeTypes;
			AcademicYear = intake.AcademicYear;
		}

		[HiddenInput]
		public Guid Id { get; set; }
		[Required]
		public string Code { get; set; }
		public bool Active { get; set; }
		[Required]
		public string Name { get; set; }
		[Required]
		[Display(Name = "Academic Year")]
		public string AcademicYear { get; set; }
		public string Description { get; set; }
		[Required]
		[Display(Name = "Start Date")]
		public DateTime StartDate { get; set; }
		public string StartDateStr { get; set; }
		[Required]
		[Display(Name = "End Date")]
		public DateTime EndDate { get; set; }
		public string EndDateStr { get; set; }
		public string StatusStr { get; set; }
		public List<IntakeGradeType> IntakeGradeTypes { get; set; }
	}
}
