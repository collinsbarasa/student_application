﻿using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
	public class IntakeGradeTypeViewModel : BaseViewModel
	{
		public IntakeGradeTypeViewModel()
		{
			ClassTypes = new List<IntakeGradeTypeClassTypeViewModel>();
			Documents = new List<IntakeGradeTypeDocumentViewModel>();
			Campuses = new List<IntakeGradeTypeCampusViewModel>();
			Programmes = new List<IntakeGradeTypeProgrammeViewModel>();
			MaxUnits = 1;
			ApplicationFee = 1;
		}
		public IntakeSetupViewModel IntakeSetupViewModel { get; set; }
		public List<Document> documents { get; set; }
		public List<ApplicationUpload> ApplicationUploads { get; set; }
		public Guid GradeTypeId { get; set; }
		public Guid IntakeId { get; set; }
		[Display(Name = "Application Fee")]
		public decimal ApplicationFee { get; set; }
		[Display(Name = "Maximum Programmes")]
		public int MaxUnits { get; set; }
		public string Name { get; set; }
		public string Notes { get; set; }
		public string Category { get; set; }
		public string CertType { get; set; }
		public bool IsAdded { get; set; }
		public virtual List<IntakeGradeTypeClassTypeViewModel> ClassTypes { get; set; }
		public virtual List<IntakeGradeTypeDocumentViewModel> Documents { get; set; }
		public virtual List<IntakeGradeTypeCampusViewModel> Campuses { get; set; }
		public virtual List<IntakeGradeTypeProgrammeViewModel> Programmes { get; set; }

		public List<UploadDocument> UploadDocuments { get; set; }
		[Display(Name = "Study Mode")]
		public string ClassTypeId { get; set; }
		[Display(Name = "Campus")]
		public string CampusId { get; set; }
		[Display(Name = "Previous Adm No.")]
		public string PrevAdmNo { get; set; }
		public string Source { get; set; }
		public Guid? ApplicationId { get; set; }
		public Guid? DocumentId { get; set; }
	}
}
