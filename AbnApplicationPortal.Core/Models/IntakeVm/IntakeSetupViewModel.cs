﻿using System;
using System.Collections.Generic;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
	public class IntakeSetupViewModel : BaseViewModel
	{
		public IntakeSetupViewModel()
		{
			IntakeGradeTypes = new List<IntakeGradeTypeViewModel>();
		}
		public IntakeViewModel Intake { get; set; }
		public List<IntakeGradeTypeViewModel> IntakeGradeTypes { get; set; }
		public Guid? ApplicationId { get; set; } 
	}
}
