﻿using System;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
	public class IntakeGradeTypeDocumentViewModel : BaseViewModel
	{
		public Guid DocumentId { get; set; }
		public Guid GradeTypeId { get; set; }
		public string Name { get; set; }
		public string Category { get; set; }
		public string Notes { get; set; }
		public bool IsAdded { get; set; }
	}
}
