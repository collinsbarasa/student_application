﻿using System;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
	public class IntakeGradeTypeProgrammeViewModel : BaseViewModel
	{
		public string Code { get; set; }
		public string Name { get; set; }
		public string GradeType { get; set; }
		public string Requirements { get; set; }
		public string Period { get; set; }
		public string Department { get; set; }
		public string CertType { get; set; }
		public Guid ProgrammeId { get; set; }
		public Guid GradeTypeId { get; set; }
		public bool IsAdded { get; set; }

		public bool IsApproved { get; set; }
		public bool IsMain { get; set; }
	}
}
