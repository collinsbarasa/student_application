﻿using System;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
	public class IntakeGradeTypeCampusViewModel
	{
		public Guid GradeTypeId { get; set; }
		public Guid CampusId { get; set; }
		public string Name { get; set; }
		public bool IsAdded { get; set; }
	}
}
