﻿namespace AbnApplicationPortal.Core.Models.IntakeVm
{
	public class IntakeSummaryViewModel : BaseViewModel
	{
		public int Active { get; set; }
		public int InActive { get; set; }
		public int TotalApplicants { get; set; }
		public int TodayApplicants { get; set; }
	}
}
