﻿using System;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
	public class IntakeGradeTypeClassTypeViewModel : BaseViewModel
	{
		public Guid GradeTypeId { get; set; }
		public string Name { get; set; }
		public Guid ClassTypeId { get; set; }
		public bool IsAdded { get; set; }
	}
}
