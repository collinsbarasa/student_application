﻿using Microsoft.AspNetCore.Http;
using System;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
	public class UploadDocumentViewModel
	{
		public IFormFile File { get; set; }
		public Guid DocumentId { get; set; }
		public Guid ApplicationId { get; set; }
		public string Notes { get; set; }
		public bool IsEditMode { get; set; }
	}
}
