﻿using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;

namespace AbnApplicationPortal.Core.Models.IntakeVm
{
    public class UploadDocument
    {
        public Document Document { get; set; }

        public ApplicationUpload Upload { get; set; }
    }
}
