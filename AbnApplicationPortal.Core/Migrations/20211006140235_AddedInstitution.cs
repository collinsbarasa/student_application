﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AbnApplicationPortal.Core.Migrations
{
    public partial class AddedInstitution : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Institution",
                table: "PersonalDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Institution",
                table: "PersonalDetails");
        }
    }
}
