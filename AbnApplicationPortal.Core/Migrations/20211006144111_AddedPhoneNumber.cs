﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AbnApplicationPortal.Core.Migrations
{
    public partial class AddedPhoneNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "PersonalDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "PersonalDetails");
        }
    }
}
