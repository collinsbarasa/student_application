﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AbnApplicationPortal.Core.Migrations
{
    public partial class AddedSimpleProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "SimpleProfile",
                table: "ClientSettings",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SimpleProfile",
                table: "ClientSettings");
        }
    }
}
