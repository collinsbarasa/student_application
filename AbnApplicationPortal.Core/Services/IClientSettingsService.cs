﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Models.SettingsVm;
using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Models.Settings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AbnApplicationPortal.Core.Services
{
	public interface IClientSettingsService
	{
		ReturnData<ClientSetting> Read();
		ReturnData<ClientSettingsViewModel> Update(ClientSettingsViewModel setting, string personnel);
		ReturnData<List<Programme>> RecommendedProgrammes();
		ReturnData<LayoutSettingViewModel> Layout();
	}

	public class ClientSettingsService : IClientSettingsService
	{
		private readonly AbnApplicationPortalContext _context;
		private readonly MemCacheService _cache;

		public ClientSettingsService(AbnApplicationPortalContext context, IMemoryCache cache)
		{
			_context = context;
			_cache = new MemCacheService(cache);
		}

		public ReturnData<LayoutSettingViewModel> Layout()
		{
			var result = new ReturnData<LayoutSettingViewModel>
			{ Data = new LayoutSettingViewModel() };
			try
			{
				var layout = _context.LayoutSettings.FirstOrDefault();
				result.Success = layout != null;
				result.Message = result.Success ? "Found" : "Not Found";
				if (result.Success)
					result.Data = new LayoutSettingViewModel(layout);
				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				result.ErrorMessage = ex.Message;
				result.Message = "Error occured. Try later";
				return result;
			}
		}

		public ReturnData<ClientSetting> Read()
		{
			var res = new ReturnData<ClientSetting>
			{
				Data = new ClientSetting()
			};
			try
			{
				var cached = _cache.Read(MemCacheKey.ClientSettings);
				if (cached.Success)
				{
					res.Data = JsonConvert.DeserializeObject<ClientSetting>(cached.Data);
					res.Message = "Found:-Cached";
					res.Success = true;
					return res;
				}
				var dbSetting = _context.ClientSettings.FirstOrDefault() ?? new ClientSetting();
				_cache.Store(MemCacheKey.ClientSettings, JsonConvert.SerializeObject(dbSetting));
				res.Data = dbSetting;
				res.Success = true;
				res.Message = "Found";
				return res;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				res.Message = "Error Occured. Try again";
				res.ErrorMessage = e.Message;
				return res;
			}
		}

		public ReturnData<List<Programme>> RecommendedProgrammes()
		{
			var result = new ReturnData<List<Programme>> { Data = new List<Programme>() };
			try
			{
				var appliedProgrammes = _context.ApplicationProgrammes
					.OrderByDescending(p => p.DateCreated)
					.Include(p => p.Programme)
					.Select(p => p.Programme)
					.Distinct()
					.OrderBy(p => Guid.NewGuid())
					.Take(6)
					.ToList();
				if (appliedProgrammes.Count < 6)
				{
					var appIds = appliedProgrammes.Select(p => p.Id).ToList();
					var remaining = 6 - appliedProgrammes.Count;
					var programmes = _context.Programmes
						.Where(p => !appIds.Contains(p.Id))
						.OrderBy(p => Guid.NewGuid())
						.Take(remaining)
						.ToList();
					programmes.ForEach(p =>
					{
						appliedProgrammes.Add(p);
					});
				}
				result.Success = true;
				result.Message = "Found";
				result.Data = appliedProgrammes;
				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				result.ErrorMessage = ex.Message;
				result.Message = "Error occured. Try later";
				return result;
			}
		}

		public ReturnData<ClientSettingsViewModel> Update(ClientSettingsViewModel setting, string personnel)
		{
			var res = new ReturnData<ClientSettingsViewModel>();
			try
			{
				var dbSetting = _context.ClientSettings.First();
				dbSetting.AppVersion = setting.AppVersion;
				dbSetting.AppName = setting.AppName;
				dbSetting.ClientName = setting.ClientName;
				dbSetting.ClientCode = setting.ClientCode;
				dbSetting.HasUnisol = setting.HasUnisol;
				dbSetting.SimpleProfile = setting.SimpleProfile;
				dbSetting.AppRefPrefix = setting.AppRefPrefix;
				dbSetting.TagLine = setting.TagLine;
				dbSetting.PrimaryColor = setting.PrimaryColor;
				dbSetting.SecondaryColor = setting.SecondaryColor;
				dbSetting.AllowPartialPayments = setting.AllowPartialPayments;
				dbSetting.ContactEmail = setting.ContactEmail;
				dbSetting.LogoUrl = setting.IsNewImg ? setting.LogoUrl
					: dbSetting.LogoUrl;
				dbSetting.DateUpdated = DateTime.Now;
				dbSetting.ImageUrl = setting.IsNewLFImg? setting.ImageUrl
				     :dbSetting.ImageUrl;
				dbSetting.Personnel = personnel;
				dbSetting.IsSaga = setting.IsSaga;
				dbSetting.ViewSchoolOnly = setting.ViewSchoolOnly;

				var resModel = new ClientSettingsViewModel(dbSetting);
				_cache.Store(MemCacheKey.ClientSettings, JsonConvert.SerializeObject(resModel));
				_context.SaveChanges();

				res.Data = resModel;
				res.Message = "Updated";
				res.Success = true;
				return res;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				res.Message = "Error Occured. Try again";
				res.ErrorMessage = e.Message;
				return res;
			}
		}
	}
}
