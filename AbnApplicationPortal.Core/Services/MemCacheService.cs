﻿using AbnApplicationPortal.Core.Data;
using Microsoft.Extensions.Caching.Memory;
using System;

namespace AbnApplicationPortal.Core.Services
{
	public class MemCacheService
	{
		private IMemoryCache _cache;

		public MemCacheService(IMemoryCache cache)
		{
			_cache = cache;
		}

		public ReturnData<string> Store(string key, string content)
		{
			var res = new ReturnData<string>();
			_cache.Set(key, content, new MemoryCacheEntryOptions
			{
				AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(30),
				SlidingExpiration = TimeSpan.FromDays(7)
			});
			res.Data = content;
			res.Success = true;
			res.Message = "Stored";
			return res;
		}

		public ReturnData<string> Read(string key)
		{
			var res = new ReturnData<string>();
			var hasValue = _cache.TryGetValue<string>(key, out var settings);
			res.Success = hasValue;
			res.Data = settings;
			res.Message = hasValue ? "Found" : "Not Found";
			return res;
		}
	}
}
