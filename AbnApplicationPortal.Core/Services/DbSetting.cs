﻿using AbnApplicationPortal.Shared.Services;
using Microsoft.Extensions.Configuration;

namespace AbnApplicationPortal.Core.Services
{
	public class DbSetting
	{
		public static string ConnectionString(IConfiguration configuration, string key)
		{
			var serverIp = configuration["Database:" + key + ":ServerIp"];
			var dbUser = configuration["Database:" + key + ":DbUser"];
			var dbName = configuration["Database:" + key + ":DbName"];
			var dbPassword = configuration["Database:" + key + ":DbPassword"];

			var realDbUser = Encrypter.Decrypt(dbUser);
			var realDbPassword = Encrypter.Decrypt(dbPassword);

			var conString = $"Server={serverIp} ;Database={dbName};User Id ={realDbUser};password ={realDbPassword};";
			return conString;
		}
	}
}
