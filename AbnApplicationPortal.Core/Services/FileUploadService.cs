﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Models.ApplicationVm;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Services
{
	public static class FileUploadService
	{
		public static async Task<ReturnData<UploadViewModel>> Upload(IFormFile file, string webRootPath)
		{
			var res = new ReturnData<UploadViewModel>
			{
				Data = new UploadViewModel()
			};
			try
			{
				string folder = "uploads";
				var allowed = new List<string> { ".png", ".jpg", ".jpeg", ".pdf" };
				var fileName = file.FileName;
				var ext = Path.GetExtension(fileName);
				if (!allowed.Contains(ext.ToLower()))
				{
					res.Message = $"Extension {ext} is not Allowed";
					return res;
				}

				var size = file.Length;
				//if (size > 1 * 1000000)
				//{
				//	res.Message = $"Ensure file size is below 9Mbs";
				//	return res;
				//}

				var filePath = Path.Combine(webRootPath, folder);
				if (!Directory.Exists(filePath))
					Directory.CreateDirectory(filePath);

				var uniqueFileName = $"{Guid.NewGuid()}{ext}";
				var fullPath = Path.Combine(filePath, uniqueFileName);
				using (var stream = new FileStream(fullPath, FileMode.Create))
				{
					await file.CopyToAsync(stream);
				}

				//if (!string.IsNullOrEmpty(prevFile))
				//{
				//	File.Delete(prevFile);
				//}

				var fileUrl = Path.Combine(folder, uniqueFileName);
				res.Success = true;
				res.Message = "Uploaded";
				res.Data = new UploadViewModel
				{
					Size = size,
					Extension = ext,
					Path = fileUrl,
					Name = fileName
				};
				return res;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				res.Message = "File upload Error Occured. Try again later";
				res.ErrorMessage = e.Message;
				return res;
			}
		}

	}
}
