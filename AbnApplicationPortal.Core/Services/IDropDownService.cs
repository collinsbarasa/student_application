﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Data.Unisol;
using AbnApplicationPortal.Core.Data.Unisol.Entities;
using AbnApplicationPortal.Core.Models;
using AbnApplicationPortal.Core.Models.IntakeSettingsVm;
using AbnApplicationPortal.Shared.Models;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Models.Settings;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Services
{
	public interface IDropDownService
	{
		Task<List<IdDocument>> IdDocuments();
		Task<List<Notification>> Notifications(string userId);
		Task<List<MaritalStatus>> MaritalStatuses();
		Task<List<Nationality>> Nationalities();
		Task<List<UniEthnicity>> Ethnicities();
		Task<List<RelationShip>> RelationShips();
		Task<List<Religion>> Religions();
		Task<List<County>> Counties();
		Task<List<CountryTelCode>> CountryTelCodes();
		Task<List<UserTitle>> UserTitles();
		Task<List<PostalCode>> PostalCodes();
		Task<List<EducationLevel>> EducationLevels();
		Task<List<KcseGrade>> KcseGrades();
		Task<List<Reason>> DeferReasons();
		Task<List<KcseSubject>> KcseSubjects();
		Task<List<DisabilityCondition>> DisabilityConditions();
		Task<List<string>> UnisolSchools();
		Task<List<string>> UnisolColleges();
		Task<List<GradeCertificateType>> GradeCertificateTypes();
		Task<List<FormSelectViewModel>> Intakes();
		List<FormSelectViewModel> Statuses();
		Task<List<ClassType>> ClassTypes();
		Task<List<DeferPeriod>> DeferPeriods();
		Task<List<ProgrammeViewModel>> Programmes();
		Task<List<AcademicYear>> AcademicYears();
		Task<List<Category>> EnquiryCategories();
		Task<List<Category>> AppointmentCategories();
		Task<List<string>> UnisolStudentSources();
	}

	public class DropDownService : IDropDownService
	{
		private readonly AbnApplicationPortalContext _context;
		private readonly UnisolDbContext _uniContext;
		public DropDownService(AbnApplicationPortalContext context, UnisolDbContext unisol)
		{
			_context = context;
			_uniContext = unisol;
		}

		public async Task<List<ClassType>> ClassTypes()
		{
			try
			{
				var data = await _context.ClassTypes
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting ClassTypes :- {ex}");
				return new List<ClassType>();
			}
		}

		public async Task<List<UniEthnicity>> Ethnicities()
		{
			try
			{
				var data = await _uniContext.Ethnicities.ToListAsync(); ;
				return data;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting ClassTypes :- {ex}");
				return new List<UniEthnicity>();
			}
		}

		public async Task<List<County>> Counties()
		{
			try
			{
				var data = await _context.Counties
					.Include(c => c.SubCounties)
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting counties :- {ex}");
				return new List<County>();
			}
		}

		public async Task<List<CountryTelCode>> CountryTelCodes()
		{
			try
			{
				var data = await _context.CountryTelCodes
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting CountryTelCodes :- {ex}");
				return new List<CountryTelCode>();
			}
		}

		public async Task<List<DisabilityCondition>> DisabilityConditions()
		{
			try
			{
				var data = await _context.DisabilityConditions
					.OrderBy(c => c.Rank)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting DisabilityConditions :- {ex}");
				return new List<DisabilityCondition>();
			}
		}

		public async Task<List<Notification>> Notifications(string userId)
		{
			try
			{
				var notifications = await _context.Notifications
					.Where(n => n.UserId.Equals(userId))
					.OrderByDescending(n => n.DateUpdated)
					.ToListAsync();

				return notifications;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting DisabilityConditions :- {ex}");
				return new List<Notification>();
			}
		}

		public async Task<List<EducationLevel>> EducationLevels()
		{
			try
			{
				var data = await _context.EducationLevels
					.OrderBy(c => c.Rank)
					.ToListAsync();
				return data;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting EducationLevels :- {ex}");
				return new List<EducationLevel>();
			}
		}

		public async Task<List<GradeCertificateType>> GradeCertificateTypes()
		{
			try
			{
				var data = await _context.GradeCertificateTypes
					.OrderBy(c => c.GradeType)
					.ToListAsync();
				return data;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting GradeCertificateTypes :- {ex}");
				return new List<GradeCertificateType>();
			}
		}

		public async Task<List<IdDocument>> IdDocuments()
		{
			try
			{
				var data = await _context.IdDocuments
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting IdDocuments :- {ex}");
				return new List<IdDocument>();
			}
		}

		public async Task<List<FormSelectViewModel>> Intakes()
		{
			try
			{
				var data = await _context.Intakes
					.OrderByDescending(c => c.DateUpdated)
					.Select(c => new FormSelectViewModel { Id = c.Id.ToString(), Text = c.Name })
					.ToListAsync();
				return data;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting dropdown Intakes :- {ex}");
				return new List<FormSelectViewModel>();
			}
		}

		public async Task<List<KcseGrade>> KcseGrades()
		{
			try
			{
				var data = await _context.KcseGrades
					.OrderByDescending(c => c.Rank)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting KcseGrades :- {ex}");
				return new List<KcseGrade>();
			}
		}

		public async Task<List<KcseSubject>> KcseSubjects()
		{
			try
			{
				var data = await _context.KcseSubjects
					.OrderBy(c => c.Name)
					.OrderByDescending(c => c.IsMeanGrade)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting KcseSubjects :- {ex}");
				return new List<KcseSubject>();
			}
		}

		public async Task<List<MaritalStatus>> MaritalStatuses()
		{
			try
			{
				var data = await _context.MaritalStatuses
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting MaritalStatuses :- {ex}");
				return new List<MaritalStatus>();
			}
		}

		public async Task<List<Nationality>> Nationalities()
		{
			try
			{
				var data = await _context.Nationalities
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting Nationalities :- {ex}");
				return new List<Nationality>();
			}
		}

		public async Task<List<PostalCode>> PostalCodes()
		{
			try
			{
				var data = await _context.PostalCodes
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting PostalCodes :- {ex}");
				return new List<PostalCode>();
			}
		}

		public async Task<List<RelationShip>> RelationShips()
		{
			try
			{
				var data = await _context.RelationShips
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting RelationShips :- {ex}");
				return new List<RelationShip>();
			}
		}

		public async Task<List<Religion>> Religions()
		{
			try
			{
				var data = await _context.Religions
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting Religions :- {ex}");
				return new List<Religion>();
			}
		}

		public List<FormSelectViewModel> Statuses()
		{
			var statusesList = Enum.GetValues(typeof(EntityStatus))
				.Cast<EntityStatus>()
				.ToList();
			return statusesList.Select(s => new FormSelectViewModel
			{
				Id = s.ToString(),
				Text = s.ToString()
			}).ToList();
		}

		public async Task<List<string>> UnisolColleges()
		{
			try
			{
				var data = await _uniContext.Colleges
					.OrderBy(c => c.Names)
					.Select(c => c.Names)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting Unisol Colleges :- {ex}");
				return new List<string>();
			}
		}

		public async Task<List<string>> UnisolSchools()
		{
			try
			{
				var data = await _uniContext.Schools
					.OrderBy(c => c.Names)
					.Select(c => c.Names)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting Unisol Schools :- {ex}");
				return new List<string>();
			}
		}

		public async Task<List<AcademicYear>> AcademicYears()
		{
			try
			{
				var data = await _uniContext.AcademicYear
					.OrderBy(c => c.Names)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting Unisol Schools :- {ex}");
				return new List<AcademicYear>();
			}
		}

		public async Task<List<string>> UnisolStudentSources()
		{
			try
			{
				var data = await _uniContext.StudentSources
					.OrderBy(c => c.Names)
					.Select(c => c.Names)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting Unisol student sources :- {ex}");
				return new List<string>();
			}
		}

		public async Task<List<UserTitle>> UserTitles()
		{
			try
			{
				var data = await _context.UserTitles
					.OrderBy(c => c.Name)
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting UserTitles :- {ex}");
				return new List<UserTitle>();
			}
		}

        public async Task<List<Reason>> DeferReasons()
        {
			try
			{
				var data = await _context.Reasons
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting defer reasons :- {ex}");
				return new List<Reason>();
			}
		}

        public async Task<List<DeferPeriod>> DeferPeriods()
        {
			try
			{
				var data = await _context.DeferPeriods
					.ToListAsync();
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting defer periods :- {ex}");
				return new List<DeferPeriod>();
			}
		}

		public async Task<List<ProgrammeViewModel>> Programmes()
		{
			try
			{
				var data = await _context.Programmes
					.OrderBy(x=>x.Name)
					.Select(p => new ProgrammeViewModel(p))
						.ToListAsync(); ;
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting defer periods :- {ex}");
				return new List<ProgrammeViewModel>();
			}
		}

		public async Task<List<Category>> EnquiryCategories()
		{
			try
			{
				var data = await _context.Categories
					.Where(i=>i.Owner == "Enquiry")
					.OrderBy(x => x.Name)
					.ToListAsync(); ;
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting defer periods :- {ex}");
				return new List<Category>();
			}
		}

		public async Task<List<Category>> AppointmentCategories()
		{
			try
			{
				var data = await _context.Categories
					.Where(i => i.Owner == "Appointment")
					.OrderBy(x => x.Name)
					.ToListAsync(); ;
				return data;

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error getting defer periods :- {ex}");
				return new List<Category>();
			}
		}
	}
}
