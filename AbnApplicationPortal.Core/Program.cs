﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.Unisol;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Services;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AbnApplicationPortal.Core
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var logger = AbnLogManager.Logger(typeof(Program));
			var host = BuildWebHost(args);
			string env;
			using (var scope = host.Services.CreateScope())
			{
				var services = scope.ServiceProvider;
				var environment = scope.ServiceProvider.GetService<IHostingEnvironment>();
				env = environment.EnvironmentName;
				try
				{
					var serviceProvider = services.GetRequiredService<IServiceProvider>();
					var configuration = services.GetRequiredService<IConfiguration>();
					IdentitySeeder.Seed(serviceProvider, configuration).Wait();

					var context = services.GetRequiredService<AbnApplicationPortalContext>();
					var unisolContext = services.GetRequiredService<UnisolDbContext>();
					DbSeeder.Seed(unisolContext, context);
				}
				catch (Exception exception)
				{
					logger.Error($"An error occurred while creating roles:- {exception.Message}");
				}
			}
			logger.Info($"ABN Application Portal Service has Started. Env- {env}");

			host.Run();
		}

		public static IWebHost BuildWebHost(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.Build();
	}
}
