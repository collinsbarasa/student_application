﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models.IntakeVm;
using AbnApplicationPortal.Shared.Models;
using AbnApplicationPortal.Shared.Models.Intakes;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers.Applications
{
	[Authorize]
	[Route("[controller]/[action]")]
	public class ApplicationIntakesController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly AbnApplicationPortalContext _context;
		private readonly ILog _logger;

		public ApplicationIntakesController(AbnApplicationPortalContext context, UserManager<ApplicationUser> userManager)
		{
			_logger = AbnLogManager.Logger(typeof(ApplicationPaymentsController));
			_context = context;
			_userManager = userManager;
		}

		public async Task<IActionResult> Index()
		{

			var model = new IntakeSummaryViewModel();
			try
			{
				model.Active = await _context.Intakes.CountAsync(i => i.IsActive);
				model.InActive = await _context.Intakes.CountAsync(i => !i.IsActive);
				model.TotalApplicants = await _context.Applications.CountAsync();
				model.TodayApplicants = await _context.Applications.CountAsync(a => a.DateCreated.Date.Equals(DateTime.Now.Date));
				return View(model);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Intakes Summary:-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Summary", message);
				return View(model);
			}
		}

		public async Task<IActionResult> ViewAll(EntityStatus status)
		{
			var model = new List<IntakeViewModel>();
			try
			{

				ViewData["CPage"] = status.ToString();
				var intakes = await _context.Intakes.OrderByDescending(i => i.DateUpdated)
				.Where(i => i.Status.Equals(status))
						.Select(c => new IntakeViewModel(c))
						.ToListAsync();
				if (!intakes.Any())
				{
					TempData.SetData(AlertLevel.Warning, "Intakes", "Not Found");
					return View(model);
				}
				return View(intakes);

			}
			catch (Exception ex)
			{
				ViewData["CPage"] = "All";
				Console.WriteLine(ex);
				_logger.Error($"Error getting {status} Intakes :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, $"{status} Intakes", message);
				return View(model);
			}
		}

		[Authorize(Roles = "Admin,SuperAdmin,Staff")]
		public async Task<IActionResult> Edit(Guid? id)
		{
			try
			{
				if (id == null)
				{
					return View(new IntakeViewModel());
				}

				var intake = await _context.Intakes.FirstOrDefaultAsync(i => i.Id.Equals(id));
				if (intake == null)
				{
					TempData.SetData(AlertLevel.Error, "Intakes", "Not Found");
					return View(new IntakeViewModel() { Message = "Not Found" });
				}
				return View(new IntakeViewModel(intake) { Message = "Found" });
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Intake :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Intakes", message);
				return View(new IntakeViewModel() { Message = message });
			}
		}

		[HttpPost]
		[Authorize(Roles = "Admin,SuperAdmin,Staff")]
		public async Task<IActionResult> Edit(IntakeViewModel model)
		{
			try
			{
				var personnel = User.Identity.Name;
				if (!ModelState.IsValid)
				{
					TempData.SetData(AlertLevel.Warning, "Edit Intake", "Invalid Request");
					return View(model);
				}

				if (model.IsEditMode)
				{
					var dbIntake = await _context.Intakes.FirstOrDefaultAsync(i => i.Id.Equals(model.Id));
					if (dbIntake == null)
					{
						TempData.SetData(AlertLevel.Warning, "Edit Intake", "Not ");
						return View(model);
					}

					dbIntake.Code = model.Code;
					dbIntake.Name = model.Name;
					dbIntake.Description = model.Description;
					dbIntake.AcademicYear = model.AcademicYear;
					dbIntake.EndDate = DateTime.Parse(model.EndDate.ToString());
					dbIntake.StartDate = DateTime.Parse(model.StartDate.ToString());
					dbIntake.DateUpdated = DateTime.Now;
					dbIntake.Status = model.Active ? EntityStatus.Active : EntityStatus.Inactive;
					await _context.SaveChangesAsync();

					TempData.SetData(AlertLevel.Success, "Edit Intake", "Intake Updated");
					return RedirectToAction(nameof(ViewAll), new { status = dbIntake.Status });
				}

				var intake = new Intake
				{
					Code = model.Code,
					Name = model.Name,
					Description = model.Description,
					AcademicYear = model.AcademicYear,
					StartDate = DateTime.Parse(model.StartDate.ToString()),
					EndDate = DateTime.Parse(model.EndDate.ToString()),
					Personnel = personnel,
					Status = model.Active ? EntityStatus.Active : EntityStatus.Inactive
				};

				_context.Intakes.Add(intake);
				await _context.SaveChangesAsync();

				TempData.SetData(AlertLevel.Success, "Edit Intake", "Intake Added");
				return RedirectToAction(nameof(ViewAll), new { status = intake.Status });
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error  :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Edit Intake", model.Message);
				return View(model);
			}
		}

		[Authorize(Roles = "Admin,SuperAdmin,Staff")]
		public async Task<IActionResult> EditIntakeSetup(Guid id)
		{
			var model = new IntakeSetupViewModel();
			try
			{
				var intake = await _context.Intakes.FirstOrDefaultAsync(i => i.Id.Equals(id));
				if (intake == null)
				{
					TempData.SetData(AlertLevel.Warning, "Intake Setup", "Intake Not Found");
					return View(model);
				}
				ViewData["cPage"] = intake.StatusStr;
				model.Success = true;
				model.Intake = new IntakeViewModel(intake);
				var gradeTypes = await _context.GradeCertificateTypes.ToListAsync();
				if (!gradeTypes.Any())
				{
					TempData.SetData(AlertLevel.Warning, "Intake Setup", "No grade type Found");
					return View(model);
				}

				var gradeTypeViewModels = new List<IntakeGradeTypeViewModel>();
				var intakeGrades = await _context.IntakeGradeTypes
					.Include(g => g.IntakeGradeTypeClassTypes)
					.Include(g => g.IntakeGradeTypeDocuments)
					.Include(g => g.IntakeGradeTypeCampuses)
					.Include(g => g.IntakeGradeTypeProgrammes)
					.Where(g => g.IntakeId.Equals(id)).ToListAsync();

				var classTypes = await _context.ClassTypes.ToListAsync();
				var documents = await _context.Documents.ToListAsync();
				var campuses = await _context.Campuses.ToListAsync();
				var programmes = await _context.Programmes.ToListAsync();
				foreach (var gradeType in gradeTypes)
				{
					var inGrade = intakeGrades.FirstOrDefault(i => i.GradeTypeId.Equals(gradeType.Id));
					var gradeTypeAllProgrammes = programmes.Where(p => p.CertType.Equals(gradeType.GradeType)).ToList();
					var gradeStudyTpes = new List<IntakeGradeTypeClassType>();
					var gradeDocuments = new List<IntakeGradeTypeDocument>();
					var gradeCampuses = new List<IntakeGradeTypeCampus>();
					var gradeProgrammes = new List<IntakeGradeTypeProgramme>();
					var gradeModel = new IntakeGradeTypeViewModel
					{
						IntakeId = id,
						GradeTypeId = gradeType.Id,
						Name = gradeType.GradeType,
						Notes = gradeType.Notes,
						CertType = gradeType.CertType
					};
					if (inGrade != null)
					{
						gradeModel.ApplicationFee = inGrade.ApplicationFee;
						gradeModel.MaxUnits = inGrade.MaxUnits;
						gradeModel.IsAdded = true;
						gradeModel.Notes = inGrade.Notes;
						gradeStudyTpes = inGrade.IntakeGradeTypeClassTypes;
						gradeDocuments = inGrade.IntakeGradeTypeDocuments;
						gradeCampuses = inGrade.IntakeGradeTypeCampuses;
						gradeProgrammes = inGrade.IntakeGradeTypeProgrammes;
					}

					foreach (var studyType in classTypes)
					{
						var gStype = gradeStudyTpes.FirstOrDefault(g => g.ClassTypeId.Equals(studyType.Id));
						var smodel = new IntakeGradeTypeClassTypeViewModel
						{
							GradeTypeId = gradeType.Id,
							ClassTypeId = studyType.Id,
							Name = studyType.Name
						};
						if (gStype != null)
							smodel.IsAdded = true;
						gradeModel.ClassTypes.Add(smodel);
					}

					foreach (var document in documents)
					{
						var gDoc = gradeDocuments.FirstOrDefault(g => g.DocumentId.Equals(document.Id));
						var dmodel = new IntakeGradeTypeDocumentViewModel
						{
							GradeTypeId = gradeType.Id,
							DocumentId = document.Id,
							Name = document.Name,
							Notes = document.Description
						};
						if (gDoc != null)
							dmodel.IsAdded = true;
						gradeModel.Documents.Add(dmodel);
					}

					foreach (var campus in campuses)
					{
						var gCampus = gradeCampuses.FirstOrDefault(g => g.CampusId.Equals(campus.Id));
						var cmodel = new IntakeGradeTypeCampusViewModel
						{
							GradeTypeId = gradeType.Id,
							CampusId = campus.Id,
							Name = campus.Name
						};
						if (gCampus != null)
							cmodel.IsAdded = true;
						gradeModel.Campuses.Add(cmodel);
					}

					foreach (var programme in gradeTypeAllProgrammes)
					{
						var gProgramme = gradeProgrammes.FirstOrDefault(g => g.ProgrammeId.Equals(programme.Id));
						var pmodel = new IntakeGradeTypeProgrammeViewModel
						{
							GradeTypeId = gradeType.Id,
							ProgrammeId = programme.Id,
							Name = programme.Name,
							CertType = programme.CertType,
							GradeType = programme.GradeType,
							Department = programme.Department,
							Requirements = programme.Requirements,
							Period = programme.Period,
							Code = programme.Code
						};
						if (gProgramme != null)
							pmodel.IsAdded = true;
						gradeModel.Programmes.Add(pmodel);
					}
					gradeTypeViewModels.Add(gradeModel);
				}

				model.IntakeGradeTypes = gradeTypeViewModels;
				return View(model);
			}
			catch (Exception ex)
			{
				ViewData["CPage"] = "All";
				Console.WriteLine(ex);
				_logger.Error($"Error getting intake for setup :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, $"Intake Setup", message);
				return View(model);
			}
		}

		[HttpPost]
		[Authorize(Roles = "Admin,SuperAdmin,Staff")]
		public async Task<IActionResult> EditIntakeSetup(IntakeGradeTypeViewModel grade)
		{
			try
			{
				var personnel = User.Identity.Name;
				if (grade == null)
				{
					TempData.SetData(AlertLevel.Warning, $"Intake Setup", "Intake Grade is Missing. Retry");
					return RedirectToAction(nameof(EditIntakeSetup), new { id = grade.IntakeId });
				}
				var intake = await _context.Intakes
					.Include(e => e.IntakeGradeTypes)
					.ThenInclude(e => e.IntakeGradeTypeProgrammes)
					.Include(e => e.IntakeGradeTypes)
					.ThenInclude(e => e.IntakeGradeTypeProgrammes)
					.Include(e => e.IntakeGradeTypes)
					.ThenInclude(e => e.IntakeGradeTypeClassTypes)

					.Include(e => e.IntakeGradeTypes)
					.ThenInclude(e => e.IntakeGradeTypeCampuses)

					.Include(e => e.IntakeGradeTypes)
					.ThenInclude(e => e.IntakeGradeTypeDocuments)

					.FirstOrDefaultAsync(i => i.Id.Equals(grade.IntakeId));
				if (intake == null)
				{
					TempData.SetData(AlertLevel.Warning, $"Intake Setup", "Intake not Found");
					return RedirectToAction(nameof(EditIntakeSetup), new { id = grade.IntakeId });
				}

				var inGradeTypes = intake.IntakeGradeTypes;
				var mgClassTypes = grade.ClassTypes
						.Where(c => c.IsAdded)
						.Select(c => new IntakeGradeTypeClassType { ClassTypeId = c.ClassTypeId, Personnel = personnel })
						.ToList();
				var classTypes = mgClassTypes.Any() ? mgClassTypes : new List<IntakeGradeTypeClassType>();
				var mgDocuments = grade.Documents
					.Where(d => d.IsAdded)
					.Select(d => new IntakeGradeTypeDocument { DocumentId = d.DocumentId, Personnel = personnel })
					.ToList();
				var documents = mgDocuments.Any() ? mgDocuments : new List<IntakeGradeTypeDocument>();
				var mgCampuses = grade.Campuses
					.Where(c => c.IsAdded)
					.Select(c => new IntakeGradeTypeCampus
					{
						CampusId = c.CampusId,
						Personnel = personnel
					})
					.ToList();
				var campuses = mgCampuses.Any() ? mgCampuses : new List<IntakeGradeTypeCampus>();
				var mgProgrammes = grade.Programmes
					.Where(p => p.IsAdded)
					.Select(p => new IntakeGradeTypeProgramme
					{
						ProgrammeId = p.ProgrammeId,
						Personnel = personnel
					})
					.ToList();
				var programmes = mgProgrammes.Any() ? mgProgrammes : new List<IntakeGradeTypeProgramme>();
				var currType = inGradeTypes.FirstOrDefault(i => i.GradeTypeId.Equals(grade.GradeTypeId));
				if (currType == null)
				{
					var nwGradeType = new IntakeGradeType
					{
						IntakeId = intake.Id,
						ApplicationFee = grade.ApplicationFee,
						MaxUnits = grade.MaxUnits,
						GradeTypeId = grade.GradeTypeId,
						Notes = grade.Notes,
						IntakeGradeTypeClassTypes = classTypes,
						IntakeGradeTypeDocuments = documents,
						IntakeGradeTypeCampuses = campuses,
						IntakeGradeTypeProgrammes = programmes,
						Personnel = personnel
					};
					inGradeTypes.Add(nwGradeType);
				}
				else
				{
					currType.ApplicationFee = grade.ApplicationFee;
					currType.MaxUnits = grade.MaxUnits;
					currType.Notes = grade.Notes;
					var uniqueClassTypeIds = classTypes.Select(c => c.ClassTypeId).ToList();
					currType.IntakeGradeTypeClassTypes.AddRange(classTypes);
					currType.IntakeGradeTypeClassTypes.RemoveAll(c => !uniqueClassTypeIds.Contains(c.ClassTypeId));

					var uniqueDocIds = documents.Select(d => d.DocumentId).ToList();
					currType.IntakeGradeTypeDocuments.AddRange(documents);
					currType.IntakeGradeTypeDocuments.RemoveAll(d => !uniqueDocIds.Contains(d.DocumentId));

					var uniqueCampusIds = campuses.Select(d => d.CampusId).ToList();
					currType.IntakeGradeTypeCampuses.AddRange(campuses);
					currType.IntakeGradeTypeCampuses.RemoveAll(c => !uniqueCampusIds.Contains(c.CampusId));

					var uniqueProgIds = programmes.Select(p => p.ProgrammeId).ToList();
					currType.IntakeGradeTypeProgrammes.AddRange(programmes);
					currType.IntakeGradeTypeProgrammes.RemoveAll(p => !uniqueProgIds.Contains(p.ProgrammeId));
				}

				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, $"Intake Setup", "Intake details Updated");
				return RedirectToAction(nameof(EditIntakeSetup), new { id = grade.IntakeId });
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error editing intake for setup :-{ex.Message}");
				var message = "Error occurred. Try again";
				TempData.SetData(AlertLevel.Error, $"Intake Setup", message);
				return RedirectToAction(nameof(EditIntakeSetup), new { id = grade.IntakeId });
			}
		}
	}
}
