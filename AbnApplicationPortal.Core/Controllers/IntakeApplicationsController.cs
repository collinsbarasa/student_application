﻿using AbnApplicationPortal.Core.CommProviders.Email;
using AbnApplicationPortal.Core.CommProviders.Sms;
using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Data.Unisol;
using AbnApplicationPortal.Core.Data.Unisol.Entities;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models;
using AbnApplicationPortal.Core.Models.ApplicationVm;
using AbnApplicationPortal.Core.Models.AppProfileVm;
using AbnApplicationPortal.Core.Models.IntakeVm;
using AbnApplicationPortal.Core.Services;
using AbnApplicationPortal.Shared.Models;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Models.Logs;
using AbnApplicationPortal.Shared.Models.Settings;
using AbnApplicationPortal.Shared.Requests.Application;
using AbnApplicationPortal.Shared.Requests.Settings;
using AbnApplicationPortal.Shared.Responses;
using AbnApplicationPortal.Shared.Services;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers.Applications
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class IntakeApplicationsController : Controller
    {
        private readonly AbnApplicationPortalContext _context;
        private readonly ILog _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationProfileController _profileController;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UnisolService _unisolService;
        private readonly IEmailSender _emailSender;
        private readonly ISmsService _smsService;
        private readonly IDropDownService _dropDownService;
        private readonly IClientSettingsService _clientSettingsService;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public IntakeApplicationsController(AbnApplicationPortalContext context, UnisolDbContext unisolDbContext,
            UserManager<ApplicationUser> userManager, IHostingEnvironment hostingEnvironment,
            IEmailSender emailSender, ISmsService smsService, IClientSettingsService clientSettingsService,
            IDropDownService dropDownService, UnisolDbContext ucontext)
        {
            _logger = AbnLogManager.Logger(typeof(IntakeApplicationsController));
            _context = context;
            _userManager = userManager;
            _profileController = new ApplicationProfileController(context, userManager);
            _hostingEnvironment = hostingEnvironment;
            _unisolService = new UnisolService(unisolDbContext, context);
            _emailSender = emailSender;
            _smsService = smsService;
            _clientSettingsService = clientSettingsService;
            _dropDownService = dropDownService;
        }

        public IActionResult Index()
        {
            if (User.IsInRole(PrimaryRole.Applicant))
            {
                return RedirectToAction(nameof(Mine));
            }
            return RedirectToAction(nameof(All));
        }

        public async Task<IActionResult> Mine()
        {
            var data = new List<ApplicationMiniViewModel>();
            try
            {
                var applications = await _context.Applications
                    .Include(a => a.ApplicationProgrammes)
                    .Where(a => a.UserId.Equals(_userManager.GetUserId(User)))
                    .OrderByDescending(a => a.DateCreated)
                    .ToListAsync();
                var uniqueIds = applications.Select(a => a.IntakeId).Distinct().ToList();
                var intakes = _context.Intakes.Where(i => uniqueIds.Contains(i.Id)).ToList();
                foreach (var application in applications)
                {
                    var intake = intakes.FirstOrDefault(i => i.Id.Equals(application.IntakeId));
                    var model = new ApplicationMiniViewModel(application)
                    {
                        IntakeCode = intake?.Code,
                        IntakeName = intake?.Name,
                        IntakeDescription = intake?.Description,
                        IntakeStatus = application.StatusStr,
                        IntakePeriod = $"{intake?.StartDateStr} - {intake?.EndDateStr}"
                    };

                    var appliedProgs = application.ApplicationProgrammes;
                    var progIds = appliedProgs.Select(p => p.ProgrammeId).Distinct().ToList();
                    var programmes = _context.Programmes.Where(p => progIds.Contains(p.Id)).ToList();
                    foreach (var programme in appliedProgs)
                    {
                        var prog = programmes.FirstOrDefault(p => p.Id.Equals(programme.ProgrammeId));
                        if (prog != null)
                        {
                            model.Programmes.Add(new ProgrammeViewModel(prog)
                            {
                                IsApproved = programme.IsApproved,
                                IsMain = programme.IsMain
                            });
                        }
                    }
                    data.Add(model);
                }
                return View(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error getting my applications :-{ex.Message}");
                var message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, "My Applications", message);
                return View(data);
            }
        }

        [Authorize(Roles = "Admin,Staff,SuperAdmin")]
        public async Task<IActionResult> All(FilterApplicationsViewModel filter)
        {
            var tag = "Applications";
            try
            {
                filter.ItemsPerPage = 10;
                var applications = await GetApplicationsAsync(filter);
                return View(applications);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error getting {tag} :-{ex.Message}");
                var message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, tag, message);
                var res = new PaginatedList<Application>(new List<Application>(), 1, 1, 1);
                return View(res);
            }
        }

        public async Task<IActionResult> ApplyIntake(Guid? intId, Guid? grtId, Guid? appId)
        {
            var tag = "Apply Intake";
            var model = new IntakeSetupViewModel();
            try
            {
                ViewData["IntId"] = intId;
                ViewData["GrtId"] = grtId;
                ViewData["AppId"] = appId;
                var profile = await _profileController.ProfileDetailsSummary(_userManager.GetUserId(User));
                if (!profile.IsComplete)
                {
                    var msg = $"Incomplete Profile:- {profile.Message}";
                    TempData.SetData(AlertLevel.Warning, tag, msg);
                    return RedirectToAction("PersonalDetails", "ApplicationProfile");
                }

                if (intId == null)
                {
                    var intake = await _context.Intakes
                    .Include(i => i.IntakeGradeTypes)
                    .FirstOrDefaultAsync(i => i.IsActive);
                    if (intake == null)
                    {
                        model.Message = "No active intake found";
                        TempData.SetData(AlertLevel.Warning, tag, model.Message);
                        return View(model);
                    }
                    model = new IntakeSetupViewModel
                    {
                        Intake = new IntakeViewModel(intake)
                    };
                }
                else
                {
                    var appIntake = await _context.Intakes.Include(i => i.IntakeGradeTypes)
                        .FirstOrDefaultAsync(i => i.Id.Equals(intId));
                    if (appIntake == null)
                    {
                        model.Message = "Provided intake not found";
                        TempData.SetData(AlertLevel.Warning, tag, model.Message);
                        return View(model);
                    }
                    model = new IntakeSetupViewModel
                    {
                        IsEditMode = true,
                        Intake = new IntakeViewModel(appIntake)
                    };
                }
                var grades = model.Intake.IntakeGradeTypes;
                var uniqueGrIds = grades.Select(gr => gr.GradeTypeId).ToList();
                var dbGradeTypes = _context.GradeCertificateTypes
                .Where(g => uniqueGrIds.Contains(g.Id))
                    .ToList();
                foreach (var item in grades)
                {
                    var type = dbGradeTypes.FirstOrDefault(g => g.Id.Equals(item.GradeTypeId));
                    var gradeTypeVm = new IntakeGradeTypeViewModel
                    {
                        GradeTypeId = item.GradeTypeId,
                        IntakeId = item.IntakeId,
                        ApplicationFee = item.ApplicationFee,
                        MaxUnits = item.MaxUnits,
                        Notes = item.Notes,
                        Name = type?.GradeType,
                        CertType = type?.CertType
                    };
                    if (grtId != null)
                        gradeTypeVm.IsAdded = gradeTypeVm.GradeTypeId.Equals(grtId);
                    model.IntakeGradeTypes.Add(gradeTypeVm);
                }
                model.Success = true;
                model.Message = "Found";
                model.ApplicationId = appId;
                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error getting intakes to  {tag} :-{ex.Message}");
                model.Message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, tag, model.Message);
                return View(model);
            }
        }

        public async Task<IActionResult> ActiveIntakes()
        {
            try
            {
                var intakesViewList = new List<IntakeSetupViewModel>();
                await _context.Intakes
                    .Include(i => i.IntakeGradeTypes)
                    .Where(i => i.IsActive).ForEachAsync(x => {
                        var model = new IntakeSetupViewModel();
                        model = new IntakeSetupViewModel
                        {
                            Intake = new IntakeViewModel(x)
                        };
                        intakesViewList.Add(model);
                    });
                var viewModel = new IntakeListViewModel()
                {
                    Intakes = intakesViewList
                };
                return View(viewModel);
            }
            catch (Exception ex)
            {
                var tag = "Active Intakes";
                Console.WriteLine(ex);
                _logger.Error($"Error getting intakes to  {tag} :-{ex.Message}");
                TempData.SetData(AlertLevel.Error, tag, "An error occured while getting active intakes");
                var viewModel = new IntakeListViewModel()
                {
                    Intakes = new List<IntakeSetupViewModel>()
                };
                return View(viewModel);
            }
        }

        public async Task<IActionResult> ApplyProgrammes(Guid intId, Guid grtId, Guid? appId)
        {
            var model = new IntakeGradeTypeViewModel();
            var tag = "Apply Programme";

            try
            {
                ViewData["IntId"] = intId;
                ViewData["GrtId"] = grtId;
                ViewData["AppId"] = appId;
                var dbGrade = await _context.IntakeGradeTypes
                    .Include(g => g.IntakeGradeTypeCampuses)
                    .Include(g => g.IntakeGradeTypeClassTypes)
                    .Include(g => g.IntakeGradeTypeProgrammes)
                    .FirstOrDefaultAsync(g => g.IntakeId.Equals(intId) && g.GradeTypeId.Equals(grtId));
                if (dbGrade == null)
                {
                    model.Message = "Provided intake grade not found";
                    TempData.SetData(AlertLevel.Warning, tag, model.Message);
                    return View(model);
                }

                var application = new Application
                {
                    ApplicationProgrammes = new List<ApplicationProgramme>()
                };
                if (appId != null)
                {
                    var app = await _context.Applications
                        .Include(a => a.ApplicationProgrammes)
                        .FirstOrDefaultAsync(a => a.Id.Equals(appId));
                    if (app != null)
                    {
                        application = app;
                        model.IsEditMode = true;
                        model.ApplicationId = app.Id;
                        model.Source = app.Source;
                    }
                }
                else
                {
                    var dbApp = await _context.Applications
                    .Include(a => a.ApplicationProgrammes)
                    .FirstOrDefaultAsync(a => a.IntakeId.Equals(intId) && a.GradeTypeId.Equals(grtId)
                    && a.UserId.Equals(_userManager.GetUserId(User)));
                    if (dbApp != null)
                    {
                        application = dbApp;
                        model.IsEditMode = true;
                        model.ApplicationId = dbApp.Id;
                        model.Source = dbApp.Source;
                    }
                }
                var type = _context.GradeCertificateTypes.FirstOrDefault(t => t.Id.Equals(dbGrade.GradeTypeId));
                model.IntakeId = dbGrade.IntakeId;
                model.GradeTypeId = dbGrade.GradeTypeId;
                model.ApplicationFee = dbGrade.ApplicationFee;
                model.MaxUnits = dbGrade.MaxUnits;
                model.Notes = dbGrade.Notes;
                model.Name = type?.GradeType;
                model.CertType = type?.CertType;


                var uniqueClIds = dbGrade.IntakeGradeTypeClassTypes.Select(c => c.ClassTypeId).Distinct().ToList();
                var classTypes = _context.ClassTypes.Where(c => uniqueClIds.Contains(c.Id)).ToList();
                foreach (var classType in classTypes)
                {
                    var dbClType = classTypes.FirstOrDefault(c => c.Id.Equals(classType.Id));
                    var clTypeModel = new IntakeGradeTypeClassTypeViewModel
                    {
                        GradeTypeId = dbGrade.GradeTypeId,
                        Name = dbClType?.Name,
                        ClassTypeId = classType.Id
                    };
                    clTypeModel.IsAdded = application.ClassTypeId.Equals(clTypeModel.ClassTypeId);
                    model.ClassTypes.Add(clTypeModel);
                }

                var uniqueCampusIds = dbGrade.IntakeGradeTypeCampuses.Select(c => c.CampusId).Distinct().ToList();
                var campuses = _context.Campuses.Where(c => uniqueCampusIds.Contains(c.Id)).ToList();
                foreach (var campus in campuses)
                {
                    var dbCampus = campuses.FirstOrDefault(c => c.Id.Equals(campus.Id));
                    var campModel = new IntakeGradeTypeCampusViewModel
                    {
                        CampusId = campus.Id,
                        GradeTypeId = dbGrade.GradeTypeId,
                        Name = dbCampus?.Name,
                        IsAdded = false
                    };
                    campModel.IsAdded = application.CampusId.Equals(campModel.CampusId);
                    model.Campuses.Add(campModel);
                }

                var uniqueProgIds = dbGrade.IntakeGradeTypeProgrammes.Select(p => p.ProgrammeId).Distinct().ToList();
                var programmes = _context.Programmes.Where(p => uniqueProgIds.Contains(p.Id)).ToList();



                //var qualifiedPrograms = new List<IntakeGradeTypeProgramme>();
                //var scores = _context.KcseScores.Where(k => k.ProfileId.Equals(profileId)).ToList();
                //var grades = _context.KcseGrades.ToList();

                //foreach (var programme in programmes)
                //{
                //  if (!programme.Clusters.Any())
                //      qualifiedPrograms.Add(programme);
                //  if (IsQualified(programme.Clusters, scores, grades))
                //      qualifiedPrograms.Add(programme);
                //}




                foreach (var programme in programmes)
                {
                    var dbProg = programmes.FirstOrDefault(p => p.Id.Equals(programme.Id));
                    var progModel = new IntakeGradeTypeProgrammeViewModel
                    {
                        GradeTypeId = dbGrade.GradeTypeId,
                        ProgrammeId = programme.Id,
                        Code = dbProg?.Code,
                        Name = dbProg?.Name,
                        GradeType = dbProg?.GradeType,
                        CertType = dbProg?.CertType,
                        Requirements = dbProg?.Requirements,
                        Period = dbProg?.Period
                    };
                    progModel.IsAdded = application.ApplicationProgrammes.Any(p => p.ProgrammeId.Equals(progModel.ProgrammeId));
                    model.Programmes.Add(progModel);
                }
                model.Success = true;
                model.Message = "Found";
                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error getting grade type to  {tag} :-{ex.Message}");
                model.Message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, tag, model.Message);
                return View(model);
            }
        }

        public async Task<IActionResult> ApplyDeferment(Guid appId, Guid? defId)
        {
            ViewData["appId"] = appId;
            var model = new DefermentViewModel();
            if (defId == null)
            {
                defId = Guid.NewGuid();
            }
            var existing = await _context.Deferments.FirstOrDefaultAsync(x => x.Id == defId);
            if (existing != null)
            {
                model = new DefermentViewModel(existing);
                model.IsEditMode = true;
            }
            else
            {
                model.ApplicationId = appId;
            }

            return View(model);
        }

        public async Task<IActionResult> DeferLetter(Guid appId, Guid defId)
        {
            ViewData["appId"] = appId;

            var application = _context.Applications.FirstOrDefault(x => x.Id == appId);
            var client = await _context.ClientSettings.FirstOrDefaultAsync();
            var defer = await _context.Deferments.FirstOrDefaultAsync(x => x.Id == defId);
            var unisolApp = _unisolService.GetApplicantByNationalId(application.NationalId);
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(defer.PersonelId));

            var message = "";
            var reportMessage = "";
            if (defer.Unit == "Academic Year")
            {
                message = "for " + ones(defer.Count.ToString()) + " " + defer.Unit + "(s) from the First Semester, "
                    + unisolApp.RYear + "/" + (Convert.ToInt32(unisolApp.RYear) + 1) + " Academic Year to the First Semester, "
                    + (Convert.ToInt32(unisolApp.RYear) + defer.Count) + "/" + (Convert.ToInt32(unisolApp.RYear) + (defer.Count + 1))
                    + " Academic Year has been approved.";
                reportMessage = unisolApp.RMonth + ", " + (Convert.ToInt32(unisolApp.RYear) + defer.Count);
            }
            if (defer.Unit == "Semester")
            {
                var toDate = SemCalculator(defer, unisolApp);
                message = "for " + ones(defer.Count.ToString()) + " " + defer.Unit + "(s) from the First Semester, "
                    + unisolApp.RYear + "/" + (Convert.ToInt32(unisolApp.RYear) + 1) + " Academic Year to the First Semester, " + toDate.Year + "/" + (Convert.ToInt32(toDate.Year) + 1)
                    + " Academic Year has been approved.";
                reportMessage = toDate.ToString("MMMM yyyy");
            }
            var users = await _userManager.GetUserAsync(User);

            var model = new DeferLeterViewModel()
            {
                Application = application,
                Deferment = defer,
                Message = message,
                ApplicationUser = users,
                ReportMessage = reportMessage,
                UniApplicant = unisolApp,
                Client = client,
                SignaturePathUrl = users?.SignatureUrl
            };
            return View(model);
        }

        public async Task<IActionResult> CancelDeferLetter(Guid appId, Guid defId)
        {
            ViewData["appId"] = appId;

            var application = _context.Applications.FirstOrDefault(x => x.Id == appId);
            var client = await _context.ClientSettings.FirstOrDefaultAsync();
            var defer = await _context.Deferments.FirstOrDefaultAsync(x => x.Id == defId);
            var unisolApp = _unisolService.GetApplicantByNationalId(application.NationalId);
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(defer.PersonelId));

            var message = "";
            if (defer.Unit == "Academic Year")
            {
                message = "for " + ones(defer.Count.ToString()) + " " + defer.Unit + "(s) from the First Semester, "
                    + unisolApp.RYear + "/" + (Convert.ToInt32(unisolApp.RYear) + 1) + " Academic Year to the First Semester, "
                    + (Convert.ToInt32(unisolApp.RYear) + defer.Count) + "/" + (Convert.ToInt32(unisolApp.RYear) + (defer.Count + 1))
                    + " Academic Year has been approved.";
            }
            if (defer.Unit == "Semester")
            {
                var toDate = SemCalculator(defer, unisolApp);
                message = "for " + ones(defer.Count.ToString()) + " " + defer.Unit + "(s) from the First Semester, "
                    + unisolApp.RYear + "/" + (Convert.ToInt32(unisolApp.RYear) + 1) + " Academic Year to the First Semester, " + toDate.Year + "/" + (Convert.ToInt32(toDate.Year) + 1)
                    + " Academic Year has been approved.";

            }
            var model = new DeferLeterViewModel()
            {
                Application = application,
                Deferment = defer,
                Message = message,
                UniApplicant = unisolApp,
                Client = client,
                SignaturePathUrl = user?.SignatureUrl
            };
            return View(model);
        }

        public async Task<IActionResult> ApproveDeferment(Guid id, string status)
        {
            var existing = await _context.Deferments.FirstOrDefaultAsync(x => x.Id == id);
            var user = await _userManager.GetUserAsync(User);
            if (existing != null)
            {
                var application = _context.Applications.FirstOrDefault(x => x.Id == existing.ApplicationId);
                if (status == "Approve")
                {
                    application.Status = EntityStatus.Defered;
                    existing.Approval = "Approved";
                }
                if (status == "Cancel")
                {
                    application.Status = EntityStatus.DeferDeclined;
                    existing.Approval = "Cancelled";
                }
                existing.PersonelId = user.Id;
                existing.Personel = user.Names + " " + user.OtherNames;
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Deferments));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApplyDeferment(DefermentViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                if (model.IsEditMode)
                {
                    var edit = await _context.Deferments.FirstOrDefaultAsync();
                    //edit.ExpectedDate = model.ExpectedDate;
                    edit.DateUpdated = DateTime.Now;
                    //edit.DeferPeriod = model.DeferPeriod;
                    edit.Reason = model.Reason;
                    edit.Count = model.Count;
                    edit.Unit = model.Unit;
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(ViewApplication), new { id = model.ApplicationId, isInit = false });
                }
                var detail = new Deferment
                {
                    ApplicationId = model.ApplicationId,
                    //DeferPeriod = model.DeferPeriod,
                    //ExpectedDate = model.ExpectedDate,
                    Count = model.Count,
                    Unit = model.Unit,
                    Approval = "Pending",
                    Reason = model.Reason,
                };

                await _context.Deferments.AddAsync(detail);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(ViewApplication), new { id = model.ApplicationId, isInit = false });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error applying for deferment :-{ex.Message}");
                model.Message = "Error occured. Try again";
                model.ErrorMessage = ex.Message;
                return View(model);
            }
        }

        [Authorize(Roles = "Admin,Staff,SuperAdmin")]
        public IActionResult Deferments(DateTime? startDate, DateTime? endDate)
        {
            var tag = "Defer Applications";
            try
            {
                var deferments = new List<DeferApplicationViewModel>();
                if (startDate == null && endDate == null)
                {
                    startDate = DateTime.Now.AddMonths(-4);
                    endDate = DateTime.Today;
                }
                _context.Deferments
                    .Where(x => x.DateCreated >= startDate && x.DateCreated <= endDate)
                   .Select(x => new DefermentViewModel(x)).ToList().ForEach(item =>
                   {
                       var reason = _context.Reasons.FirstOrDefault(x => x.Id == item.Reason);
                       //var period = _context.DeferPeriods.FirstOrDefault(x => x.Id == item.DeferPeriod);
                       var application = _context.Applications.FirstOrDefault(x => x.Id == item.ApplicationId);
                       var unisolApp = _unisolService.GetApplicantByNationalId(application.NationalId);

                       //item.PeriodString = period?.Name;
                       item.ReasonString = reason?.Description;
                       var dModel = new DeferApplicationViewModel(application, item);
                       if (unisolApp != null) {
                           dModel.AdmNo = unisolApp.AdmnNo;
                       }
                       
                       deferments.Add(dModel);
                   });
                return View(new PaginatedList<DeferApplicationViewModel>(deferments, 20, 1, 20));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error getting {tag} :-{ex.Message}");
                var message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, tag, message);
                var res = new PaginatedList<DeferApplicationViewModel>(new List<DeferApplicationViewModel>(), 1, 1, 1);
                return View(res);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApplyProgrammes(IntakeGradeTypeViewModel model)
        {
            var opt = model.IsEditMode ? "Update" : "Add";
            var tag = $"{opt} Application";
            var settings = _context.ClientSettings.FirstOrDefault();
            try
            {
                var res = await AddOrUpdateApplicationAsync(model);
                if (!res.Success)
                {
                    model.Message = $"{opt} Failed :- {res.Message} ";
                    TempData.SetData(AlertLevel.Warning, tag, model.Message);
                    return View(model);
                }
                if (settings.SimpleProfile) {
                    return RedirectToAction(nameof(ViewApplication), new { id = res.Data, isInit = true});
                }
                return RedirectToAction(nameof(ApplyDocuments),
                    new { appId = res.Data });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error posting grade type to  {tag} :-{ex.Message}");
                model.Message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, tag, model.Message);
                return View(model);
            }
        }


        public async Task<IActionResult> ApplyDocuments(Guid appId)
        {
            var tag = "Apply Documents";
            var model = new IntakeGradeTypeViewModel();

            try
            {
                var application = await _context.Applications
                    .Include(a => a.ApplicationUploads)
                    .FirstOrDefaultAsync(a => a.Id.Equals(appId));
                if (application == null)
                {
                    model.Message = "Application not Found";
                    TempData.SetData(AlertLevel.Warning, tag, model.Message);
                    return View(model);
                }


                ViewData["IntId"] = application.IntakeId;
                ViewData["GrtId"] = application.GradeTypeId;
                ViewData["AppId"] = appId;



                model.ApplicationId = appId;
                model.IntakeId = application.IntakeId;
                model.GradeTypeId = application.GradeTypeId;
                var uploaded = application.ApplicationUploads.Join(_context.Documents,
                    u => u.DocumentId,
                    d => d.Id,
                    (u, d) => new UploadDocument
                    {
                        Document = d,
                        Upload = u
                    }).ToList();

                model.UploadDocuments = uploaded;

                var types = _context.Documents.ToList();
                ViewBag.Types = types;

                model.Success = true;
                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error getting application documents to  {tag} :-{ex.Message}");
                model.Message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, tag, model.Message);
                return View(model);
            }
        }

        public async Task<IActionResult> ViewUpload(Guid appId, Guid dId)
        {
            var model = new KcseCertificateViewModel();
            var tag = "View Upload";
            try
            {
                var application = await _context.Applications
                    .Include(a => a.ApplicationUploads)
                    .FirstOrDefaultAsync(a => a.Id.Equals(appId));
                if (application == null)
                {
                    model.Message = "Application not Found";
                    TempData.SetData(AlertLevel.Warning, tag, model.Message);
                    return View(model);
                }

                var document = application.ApplicationUploads
                    .FirstOrDefault(a => a.DocumentId.Equals(dId));
                if (document == null)
                {
                    model.Message = "Document not Found";
                    TempData.SetData(AlertLevel.Warning, tag, model.Message);
                    return View(model);
                }

                var realDoc = _context.Documents.FirstOrDefault(d => d.Id.Equals(document.DocumentId));
                model.Description = $"{realDoc?.Name} [{realDoc?.Description}]";
                model.Success = true;
                model.FullPathUrl = FullPathUrl(document.Path);
                model.PathUrl = document.Path;
                model.ContentType = GetContentType(document.Extension);
                model.Id = document.Id;
                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error getting document to view :-{ex.Message}");
                model.Message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, tag, model.Message);
                return View(model);
            }
        }

        public async Task<FileContentResult> Download(Guid id)
        {
            try
            {
                var upload = await _context.ApplicationUploads
                    .FirstOrDefaultAsync(u => u.Id.Equals(id));
                if (upload == null)
                    return null;
                var filepath = Path.Combine(_hostingEnvironment.WebRootPath, upload.Path);
                var ext = Path.GetExtension(upload.Path).ToLowerInvariant();

                byte[] fileBytes;
                if (System.IO.File.Exists(filepath))
                    fileBytes = System.IO.File.ReadAllBytes(filepath);
                else
                    return null;

                var fileName = Regex.Replace(upload.Name, @"s", "");
                var dName = $"{fileName}_{id}".ToLower();
                Response.Headers.Add("content-disposition", "inline");
                return new FileContentResult(fileBytes, GetContentType(upload.Extension))
                {
                    FileDownloadName = $"{dName}{ext}"
                };
            }
            catch (Exception e)
            {
                Console.WriteLine($"error getting upload :-{e.Message}");
                return null;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(UploadDocumentViewModel model)
        {
            var tag = "Upload";
            try
            {
                var application = await _context.Applications
                          .Include(a => a.ApplicationUploads)
                                     .FirstOrDefaultAsync(a => a.Id.Equals(model.ApplicationId));
                if (application == null)
                {
                    TempData.SetData(AlertLevel.Warning, tag, "Provided application not Found");
                    return RedirectToAction(nameof(ApplyDocuments),
                    new { appId = model.ApplicationId });
                }
                if (model.IsEditMode)
                {
                    var dbUpload = application.ApplicationUploads
                        .FirstOrDefault(d => d.DocumentId.Equals(model.DocumentId));
                    if (dbUpload != null)
                    {
                        var upRes = await FileUploadService.Upload(model.File, _hostingEnvironment.WebRootPath);
                        if (!upRes.Success)
                        {
                            TempData.SetData(AlertLevel.Warning, tag, $"Not uploaded. {upRes.Message}");
                            return View(model);
                        }
                        dbUpload.Path = upRes.Data.Path;
                        dbUpload.Size = upRes.Data.Size;
                        dbUpload.Extension = upRes.Data.Extension;
                        dbUpload.Description = model.Notes;
                        dbUpload.DateUpdated = DateTime.Now;
                        await _context.SaveChangesAsync();
                        TempData.SetData(AlertLevel.Success, tag, "Updated");
                        return RedirectToAction(nameof(ApplyDocuments),
                                    new { appId = model.ApplicationId });
                    }

                    TempData.SetData(AlertLevel.Warning, tag, "Provided document not Found");
                    return RedirectToAction(nameof(ApplyDocuments),
                    new { appId = model.ApplicationId });
                }

                var upRes_ = await FileUploadService.Upload(model.File, _hostingEnvironment.WebRootPath);
                if (!upRes_.Success)
                {
                    var msg = $"Unable to upload Document :- {upRes_.Message}";
                    TempData.SetData(AlertLevel.Warning, tag, msg);
                    return RedirectToAction(nameof(ApplyDocuments),
                    new { appId = model.ApplicationId });
                }

                var data = upRes_.Data;
                var upload = new ApplicationUpload
                {
                    Extension = data.Extension,
                    Path = data.Path,
                    Size = data.Size,
                    Description = model.Notes,
                    Name = data.Name,
                    DocumentId = model.DocumentId,
                    Personnel = User.Identity.Name
                };

                application.ApplicationUploads.Add(upload);
                await _context.SaveChangesAsync();
                TempData.SetData(AlertLevel.Success, tag, "Uploaded");

                return RedirectToAction(nameof(ApplyDocuments),
                    new { appId = model.ApplicationId });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error uploading application document :-{ex.Message}");
                var message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, tag, message);
                return RedirectToAction(nameof(ApplyDocuments),
                    new { appId = model.ApplicationId });
            }
        }

        public async Task<IActionResult> ViewApplication(Guid id, bool isInit = false)
        {
            var model = await ApplicationViewModelAsync(id);
            var personnel = User.Identity.Name;
            var seconds = Math.Floor(model.Application.DateUpdated.Subtract(model.Application.DateCreated).TotalSeconds);
            var settings = _context.ClientSettings.FirstOrDefault();
            if (model.Application.Personnel.Equals(personnel) && seconds.Equals(0) && isInit)
            {
                var notification = new Notification
                {
                    UserId = model.Application.UserId,
                    Personnel = personnel,
                    Title = "Application Added",
                    Message = $"Dear {model.Application.Names}, your application has been Received. Reference Number is {model.Application.Code}."
                };
                _context.Notifications.Add(notification);
                await _context.SaveChangesAsync();

                var smsTail = $" Proceed to pay application fee of {model.Balance}.";
                _smsService.Send(new List<string> { model.Application.TelNo }, $"{notification.Message}{smsTail}");
                notification.Message = $"{notification.Message}<br/>{smsTail} <br/><br/> Be checking on the portal for Updates.";
                await _emailSender.SendEmailAsync(model.Application.Email, $"{model.Application.Code} Application Received", notification.Message);
                TempData.SetData(AlertLevel.Success, "Application Added", $"Application {model.Application.Code} Added");
            }

            if (!settings.SimpleProfile) {
                if (model.Application.UserId.Equals(_userManager.GetUserId(User)))
                {
                    if (model.Application.Status.Equals(EntityStatus.Pending) && !model.Application.ApplicationUploads.Any())
                    {
                        TempData.SetData(AlertLevel.Warning, "Uploads", "You have not uploaded documents");
                        return RedirectToAction(nameof(ApplyDocuments), new { appId = id });
                    }
                }
            }
            
            return View(model);
        }

        public async Task<IActionResult> ApproveProgramme(Guid id, Guid appId)
        {
            var tag = "Approve";
            try
            {
                var application = await _context.Applications
                    .Include(a => a.ApplicationProgrammes)
                    .ThenInclude(a => a.Programme)
                    .FirstOrDefaultAsync(a => a.Id.Equals(appId));
                if (application == null)
                {
                    TempData.SetData(AlertLevel.Warning, tag, "Application not Found");
                    return RedirectToAction(nameof(ViewApplication), new { id = appId });
                }
                application.ApplicationProgrammes.ForEach(p =>
                {
                    p.IsApproved = false; p.DateUpdated = DateTime.Now;
                });
                var programme = application.ApplicationProgrammes
                    .FirstOrDefault(p => p.ProgrammeId.Equals(id));
                if (programme == null)
                {
                    TempData.SetData(AlertLevel.Warning, tag, "Programme not Found");
                    return RedirectToAction(nameof(ViewApplication), new { id = appId });
                }

                programme.IsApproved = true;
                application.Status = EntityStatus.Accepted;
                application.DateApproved = DateTime.Now;
                application.ApprovedBy = User.Identity.Name;
                application.DateUpdated = DateTime.Now;

                var notification = new Notification
                {
                    UserId = application.UserId,
                    Personnel = User.Identity.Name,
                    Title = "Application Accepted",
                    Message = $"Dear {application.Names}, your application {application.Code} has been Accepted awaiting Approval."
                };
                _context.Notifications.Add(notification);
                await _context.SaveChangesAsync();

                _smsService.Send(new List<string> { application.TelNo }, notification.Message);
                var progName = $"{programme.Programme?.Code } {programme.Programme?.Name} - [{programme.Programme?.CertType}]";
                notification.Message = $"{notification.Message} <br/> Programme : {progName} <br/><br/> Be checking on the portal for Updates.";
                await _emailSender.SendEmailAsync(application.Email, $"{application.Code} Application Accepted", notification.Message);

                TempData.SetData(AlertLevel.Success, tag, "Programme Approved");
                return RedirectToAction(nameof(ViewApplication), new { id = appId });
            }
            catch (Exception ex)
            {
                _logger.Error($"error approving program :- {ex}");
                TempData.SetData(AlertLevel.Error, tag, "Error occured. Try again");
                return RedirectToAction(nameof(ViewApplication), new { id = appId });
            }
        }

        public async Task<IActionResult> UploadSingleToUnisol(Guid id)
        {
            var tag = "Upload";
            try
            {
                var application = await _context.Applications
                    .Include(a => a.ApplicationProgrammes)
                    .ThenInclude(a => a.Programme)
                    .Include(a => a.ApplicationPayments)
                    .Include(a => a.ApplicationUploads)
                    .FirstOrDefaultAsync(a => a.Id.Equals(id));
                if (application == null)
                {
                    TempData.SetData(AlertLevel.Warning, tag, "Application not Found");
                    return RedirectToAction(nameof(ViewApplication), new { id });
                }
                var settings = await _context.Settings
                    .Where(s => s.Key.Equals(SettingKey.Mpesa) || s.Key.Equals(SettingKey.Soap))
                    .ToListAsync();
                var result = await _unisolService.AddApplicantAsync(application, settings, _hostingEnvironment.WebRootPath);
                if (result.Success)
                {
                    application.IsUploaded = true;
                    application.DateUpdated = DateTime.Now;
                    await _context.SaveChangesAsync();
                }
                TempData.SetData(result.Success ? AlertLevel.Success : AlertLevel.Warning, tag, result.Message);
                return RedirectToAction(nameof(ViewApplication), new { id });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"error uploading application :- {ex}");
                TempData.SetData(AlertLevel.Error, tag, "Error occured. Try again");
                return RedirectToAction(nameof(ViewApplication), new { id });
            }
        }

        public async Task<IActionResult> UploadAllApproved(Guid intakeId)
        {
            var tag = "Upload All";
            try
            {
                var applications = await _context.Applications
                    .Where(a => a.IntakeId.Equals(intakeId) && a.Status.Equals(EntityStatus.Approved) && !a.IsUploaded)
                    .Include(p => p.ApplicationProgrammes)
                    .ThenInclude(p => p.Programme)
                    .Include(a => a.ApplicationPayments)
                    .ToListAsync();
                if (!applications.Any())
                {
                    TempData.SetData(AlertLevel.Warning, tag, "No Approved applications to Upload");
                }
                var result = _unisolService.UploadList(applications, _hostingEnvironment.WebRootPath);
                if (result.Success)
                {
                    applications.ForEach(application =>
                    {
                        application.IsUploaded = true;
                        application.DateUpdated = DateTime.Now;
                    });
                    await _context.SaveChangesAsync();
                }
                TempData.SetData(result.Success ? AlertLevel.Success : AlertLevel.Warning, tag, result.Message);
                return RedirectToAction(nameof(All), new { intId = intakeId });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"error uploading intake approved application :- {ex}");
                TempData.SetData(AlertLevel.Error, tag, "Error occured. Try again");
                return RedirectToAction(nameof(All), new { intId = intakeId });
            }
        }

        public async Task<IActionResult> PayApplication(Guid id)
        {
            var application = await ApplicationViewModelAsync(id);
            var phone = $"254{application.Application.TelNo.Substring(application.Application.TelNo.Length - 9)}";
            var model = new PaymentConfirmViewModel
            {
                ApplicationId = application.Application.Id,
                Balance = application.Balance,
                PhoneNumber = phone,
                RefNo = application.Application.Code,
                Names = application.Application.Names,
                Paybill = application.Paybill,
                PaybillISelf = application.PaybillIsSelf
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PayApplication(PaymentConfirmViewModel model)
        {
            var setting = await _context.Settings.FirstOrDefaultAsync(s => s.Key.Equals(SettingKey.Mpesa));
            var mpesa = JsonConvert.DeserializeObject<MpesaSetting>(setting.Data);
            var lipa = new LipaRequest()
            {
                CallBackURL = $"{mpesa.CallBackURL}?code={Encrypter.Encrypt(model.ApplicationId.ToString())}",
                PassKey = mpesa.PassKey,
                BusinessShortCode = mpesa.BusinessShortCode,
                TransactionDesc = model.RefNo,
                ClientId = mpesa.AbnClientId,
                ConsumerKey = mpesa.ConsumerKey,
                ConsumerSecret = mpesa.ConsumerSecret,
                AccountReference = model.RefNo,
                Amount = $"{model.Balance}",
                PhoneNumber = model.PhoneNumber
            };

            var expressUrl = mpesa.AbnExpressUrl;
            var client = new RestClient(expressUrl);
            var expressRequest = new RestRequest("pay", Method.POST) { RequestFormat = DataFormat.Json };
            expressRequest.AddJsonBody(lipa);
            var data = await client.ExecutePostTaskAsync(expressRequest);

            if (data.StatusCode.Equals(HttpStatusCode.OK))
            {
                var content = data.Content;
                var result = JsonConvert.DeserializeObject<InitiateStkResponse>(content);
                model.Success = result.Success;
                //model.Data = result.Data;
                model.Message = result.Success ? "STK Push Success" : $"STK Failed :-{result.Message}";
            }
            else
            {
                model.Message = "STK Push Failed:- Incomplete";
            }
            TempData["StkResponse"] = JsonConvert.SerializeObject(model);
            return RedirectToAction(nameof(PaymentConfirmation));
        }

        public IActionResult PaymentConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<JsonResult> ReceivePay(string code)
        {
            var data = new StkPayResponse();
            try
            {
                string body;
                using (var stream = new StreamReader(HttpContext.Request.Body))
                {
                    body = await stream.ReadToEndAsync();
                }
                Console.WriteLine(body);
                var appId = Guid.Parse(Encrypter.Decrypt(code));
                var stkPayResponse = JsonConvert.DeserializeObject<StkDarajaResponse.CallBackResponse>(body);
                var stkCallback = stkPayResponse.Body?.stkCallback;
                if (stkCallback == null)
                {
                    data.IsCompleted = false;
                    data.Message = "Received data. Callback is Null";
                    return Json(await ReceiptAsync(data, appId));
                }
                data.CheckoutRequestID = stkCallback.CheckoutRequestID;
                data.MerchantRequestID = stkCallback.MerchantRequestID;
                data.ResultCode = stkCallback.ResultCode;
                data.ResultDesc = stkCallback.ResultDesc;
                var items = stkCallback.CallbackMetadata?.Item;
                if (items == null)
                {
                    data.IsCompleted = false;
                    data.Message = "Received data. Items is Null";
                    return Json(await ReceiptAsync(data, appId));
                }
                data.IsCompleted = stkCallback.ResultCode.Equals(0);
                for (var i = 0; i < items.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            data.Amount = Convert.ToDecimal(items[i]?.Value);
                            break;
                        case 1:
                            data.ReceiptNumber = items[i]?.Value;
                            break;
                        case 3:
                            data.TransactionDate = items[i]?.Value;
                            break;
                        case 4:
                            data.PhoneNumber = items[i]?.Value;
                            break;
                    }
                }
                return Json(await ReceiptAsync(data, appId));
            }
            catch (Exception e)
            {
                data.IsCompleted = false;
                data.Message = "Failed to process STK data :-" + e.Message;
                return Json(await ReceiptAsync(data, Guid.Empty));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditComment(CommentViewModel model, bool decline = false)
        {
            var tag = "Comment";
            var application = await _context.Applications
                .Include(a => a.ApplicationComments)
                .FirstOrDefaultAsync(a => a.Id.Equals(model.AppId));
            if (application == null)
            {
                TempData.SetData(AlertLevel.Warning, tag, "Application not Found");
            }
            else
            {
                application.ApplicationComments.Add(new ApplicationComment
                {
                    ApplicationId = application.Id,
                    Content = model.Comment,
                    UserId = application.UserId,
                    Personnel = User.Identity.Name,
                    Status = application.Status
                });

                _context.Notifications.Add(new Notification
                {
                    Message = model.Comment,
                    Title = $"Application {application.Code}",
                    UserId = application.UserId,
                    Personnel = User.Identity.Name
                });
                if (decline)
                    application.Status = EntityStatus.Declined;

                application.DateUpdated = DateTime.Now;
                await _context.SaveChangesAsync();
                var subject = decline ? $"Application {application.Code} Declined" : $"{application.Code} Comment";
                await _emailSender.SendEmailAsync(application.Email, subject, model.Comment);
            }

            TempData.SetData(AlertLevel.Success, tag, decline ? "Application Declined" : "Comment Posted");
            return RedirectToAction(nameof(ViewApplication), new { application.Id });
        }

        public IActionResult UpdateDocumentsUnisol(Guid id)
        {
            var response = _unisolService.UpdateDocuments(id, _hostingEnvironment.WebRootPath);
            TempData.SetData(response.Success ? AlertLevel.Success : AlertLevel.Warning, "Update Unisol", response.Success ? response.Data : response.Message);
            return RedirectToAction(nameof(ViewApplication), new { id });
        }

        public async Task<IActionResult> SyncUnisolPayment(Guid id, string code)
        {
            try
            {
                var receiptsRes = await _unisolService.GetApplicantReceiptsAsync(code);
                if (!receiptsRes.Success)
                {
                    TempData.SetData(AlertLevel.Warning, "Sync Payments", receiptsRes.Message);
                    return RedirectToAction(nameof(ViewApplication), new { id });
                }
                var count = 0;
                foreach (var response in receiptsRes.Data)
                {
                    var res = await ReceiptAsync(response, id);
                    if (res.Success)
                        count++;
                }
                var total = receiptsRes.Data.Count();
                TempData.SetData(count.Equals(total) ? AlertLevel.Success : AlertLevel.Warning, "Sync Unisol Payments", $"Updated {count} of {total}");
                return RedirectToAction(nameof(ViewApplication), new { id });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"error syncing payments :- {ex}");
                TempData.SetData(AlertLevel.Error, "Sync Payments", "Error occured. Try again later");
                return RedirectToAction(nameof(ViewApplication), new { id });
            }
        }

        public async Task<FileContentResult> ExportAll(FilterApplicationsViewModel filter)
        {
            try
            {
                var package = new ExcelPackage();
                var client = _clientSettingsService.Read().Data;
                package.Workbook.Properties.Title = $"{client.ClientName} Applications Export";
                var user = await _userManager.GetUserAsync(User);
                package.Workbook.Properties.Author = $"{user.Names}-{user.Email}";
                package.Workbook.Properties.Subject = "Applications Summary";
                var worksheet = package.Workbook.Worksheets.Add($"Applications By {DateTime.Now:dd-MMMM-yyyy}");
                var columns = new List<string>
                    {
                        "REF #","Date Applied", "Name","Gender","Id Document", "Id Number","Email Address","Home Address",
                        "Phone Number", "Date of Birth","Nationality","County","Sub County", "Languages" ,"Disability",
                        "Medical Information", "Marital Status","Religion", "Kin Name","Kin Relationship","Kin Email","Kin Phone",
                        "Kin Notes","Level","Study Mode","Status","Fully Paid","Intake","Programme","Department"
                    };
                if (!client.IsSaga)
                {
                    var indexColumns = new List<string>
                        {
                            "KCSE Index No","KCSE Year","KCPE Index No","KCPE Year","KCSE Qualification"
                        };
                    columns.AddRange(indexColumns);
                }
                for (var i = 0; i < columns.Count; i++)
                {
                    worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                    worksheet.Cells[1, i + 1].Value = columns[i];
                }
                var cRow = 1;
                var index = 1;
                var intakes = await _dropDownService.Intakes();
                var applications = await GetApplicationsAsync(filter);
                foreach (var application in applications)
                {
                    worksheet.Cells[index + cRow, 1].Value = application.Code;
                    worksheet.Cells[index + cRow, 2].Value = application.DateCreatedStr;
                    worksheet.Cells[index + cRow, 3].Value = application.Names;
                    worksheet.Cells[index + cRow, 4].Value = application.Gender;
                    worksheet.Cells[index + cRow, 5].Value = application.IdDocument;
                    worksheet.Cells[index + cRow, 6].Value = application.NationalId;
                    worksheet.Cells[index + cRow, 7].Value = application.Email;
                    worksheet.Cells[index + cRow, 8].Value = application.HomeAddress;
                    worksheet.Cells[index + cRow, 9].Value = application.TelNo;
                    worksheet.Cells[index + cRow, 10].Value = application.DobStr;
                    worksheet.Cells[index + cRow, 11].Value = application.Nationality;
                    worksheet.Cells[index + cRow, 12].Value = application.County;
                    worksheet.Cells[index + cRow, 13].Value = application.SubCounty;
                    worksheet.Cells[index + cRow, 14].Value = application.Language;
                    worksheet.Cells[index + cRow, 15].Value = application.Disability;
                    worksheet.Cells[index + cRow, 16].Value = application.Special;
                    worksheet.Cells[index + cRow, 17].Value = application.Marital;
                    worksheet.Cells[index + cRow, 18].Value = application.Religion;
                    worksheet.Cells[index + cRow, 19].Value = application.EMName;
                    worksheet.Cells[index + cRow, 20].Value = application.EMRel;
                    worksheet.Cells[index + cRow, 21].Value = application.EMEmail;
                    worksheet.Cells[index + cRow, 22].Value = application.EMTel;
                    worksheet.Cells[index + cRow, 23].Value = application.EMRemarks;
                    worksheet.Cells[index + cRow, 24].Value = application.GradeType;
                    worksheet.Cells[index + cRow, 25].Value = application.StudyMode;
                    worksheet.Cells[index + cRow, 26].Value = application.StatusStr;
                    worksheet.Cells[index + cRow, 27].Value = application.IsFullyPaid ? "YES" : "NO";
                    worksheet.Cells[index + cRow, 28].Value = intakes
                        .FirstOrDefault(i => Guid.Parse(i.Id).Equals(application.IntakeId))?.Text;
                    if (application.ApplicationProgrammes.Any())
                    {
                        worksheet.Cells[index + cRow, 29].Value = application.ApplicationProgrammes
                                                .First().Programme.Name;
                        worksheet.Cells[index + cRow, 30].Value = application.ApplicationProgrammes
                            .First().Programme.Department;
                    }
                    else
                    {
                        worksheet.Cells[index + cRow, 29].Value = "";
                        worksheet.Cells[index + cRow, 30].Value = "";
                    }
                    if (!client.IsSaga)
                    {
                        worksheet.Cells[index + cRow, 31].Value = application.IndexNo;
                        worksheet.Cells[index + cRow, 32].Value = application.KcseYear;
                        worksheet.Cells[index + cRow, 33].Value = application.KcpeIndexNo;
                        worksheet.Cells[index + cRow, 34].Value = application.KcpeYear;
                        worksheet.Cells[index + cRow, 35].Value = application.KcseQualification;
                    }
                    index++;
                }
                var modelTable = worksheet.Cells[1, 1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column];
                modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                var title = $"{client.ClientName} Applications".Replace(" ", "_");
                var tbl = worksheet.Tables.Add(
                    new ExcelAddressBase(1, 1, worksheet.Dimension.End.Row,
                        worksheet.Dimension.End.Column), title);
                tbl.ShowHeader = true;
                //tbl.TableStyle = TableStyles.Dark1;
                var excelName = title + "-" + Guid.NewGuid().ToString().Substring(0, 6);
                return File(package.GetAsByteArray(), XlsxContentType, excelName + ".xlsx");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public async Task<FileContentResult> Export(FilterApplicationsViewModel filter)
        {
            try
            {
                var package = new ExcelPackage();
                var client = _clientSettingsService.Read().Data;
                package.Workbook.Properties.Title = $"{client.ClientName} Applications Export";
                package.Workbook.Properties.Author = User.Identity.Name;
                package.Workbook.Properties.Subject = "Programme Applications Summary";
                var columns = new List<string>
                    {
                        "REF #","Date Applied", "Name","Gender","Id Document", "Id Number","Email Address","Home Address",
                        "Phone Number", "Date of Birth","Nationality","County","Sub County", "Languages" ,"Disability",
                        "Medical Information", "Marital Status","Religion", "Kin Name","Kin Relationship","Kin Email","Kin Phone",
                        "Kin Notes","Level","Study Mode","Status","Fully Paid","Intake","Programme","Department"
                    };
                if (!client.IsSaga)
                {
                    var indexColumns = new List<string>
                        {
                            "KCSE Index No","KCSE Year","KCPE Index No","KCPE Year","KCSE Qualification"
                        };
                    columns.AddRange(indexColumns);
                }

                var intakes = await _dropDownService.Intakes();
                var applications = await GetApplicationsAsync(filter);
                var x = new List<Programme>();
                applications.ForEach(a =>
                {
                    var progs = a.ApplicationProgrammes.Select(p => p.Programme).ToList();
                    x.AddRange(progs);
                });
                var programmes = x.Distinct().ToList();
                foreach (var programme in programmes)
                {
                    var worksheet = package.Workbook.Worksheets.Add($"{programme.Code}");
                    var clustersRes = _unisolService.ProgrammeClusters(programme.Code);
                    var clusters = clustersRes.Success ? clustersRes.Data : new List<ProgrammeCluster>();
                    clusters.ForEach(c =>
                    {
                        columns.Add(c.Subject);
                    });
                    for (var i = 0; i < columns.Count; i++)
                    {
                        worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        worksheet.Cells[1, i + 1].Value = columns[i];
                    }
                    var cRow = 1;
                    var index = 1;

                    var programmeApplications = new List<Application>();
                    applications.ForEach(a =>
                    {
                        if (a.ApplicationProgrammes.Any(p => p.ProgrammeId.Equals(programme.Id)))
                        {
                            programmeApplications.Add(a);
                        }
                    });
                    foreach (var application in programmeApplications)
                    {
                        worksheet.Cells[index + cRow, 1].Value = application.Code;
                        worksheet.Cells[index + cRow, 2].Value = application.DateCreatedStr;
                        worksheet.Cells[index + cRow, 3].Value = application.Names;
                        worksheet.Cells[index + cRow, 4].Value = application.Gender;
                        worksheet.Cells[index + cRow, 5].Value = application.IdDocument;
                        worksheet.Cells[index + cRow, 6].Value = application.NationalId;
                        worksheet.Cells[index + cRow, 7].Value = application.Email;
                        worksheet.Cells[index + cRow, 8].Value = application.HomeAddress;
                        worksheet.Cells[index + cRow, 9].Value = application.TelNo;
                        worksheet.Cells[index + cRow, 10].Value = application.DobStr;
                        worksheet.Cells[index + cRow, 11].Value = application.Nationality;
                        worksheet.Cells[index + cRow, 12].Value = application.County;
                        worksheet.Cells[index + cRow, 13].Value = application.SubCounty;
                        worksheet.Cells[index + cRow, 14].Value = application.Language;
                        worksheet.Cells[index + cRow, 15].Value = application.Disability;
                        worksheet.Cells[index + cRow, 16].Value = application.Special;
                        worksheet.Cells[index + cRow, 17].Value = application.Marital;
                        worksheet.Cells[index + cRow, 18].Value = application.Religion;
                        worksheet.Cells[index + cRow, 19].Value = application.EMName;
                        worksheet.Cells[index + cRow, 20].Value = application.EMRel;
                        worksheet.Cells[index + cRow, 21].Value = application.EMEmail;
                        worksheet.Cells[index + cRow, 22].Value = application.EMTel;
                        worksheet.Cells[index + cRow, 23].Value = application.EMRemarks;
                        worksheet.Cells[index + cRow, 24].Value = application.GradeType;
                        worksheet.Cells[index + cRow, 25].Value = application.StudyMode;
                        worksheet.Cells[index + cRow, 26].Value = application.StatusStr;
                        worksheet.Cells[index + cRow, 27].Value = application.IsFullyPaid ? "YES" : "NO";
                        worksheet.Cells[index + cRow, 28].Value = intakes
                            .FirstOrDefault(i => Guid.Parse(i.Id).Equals(application.IntakeId))?.Text;
                        worksheet.Cells[index + cRow, 29].Value = programme.Name;
                        worksheet.Cells[index + cRow, 30].Value = programme.Department;
                        if (!client.IsSaga)
                        {
                            worksheet.Cells[index + cRow, 31].Value = application.IndexNo;
                            worksheet.Cells[index + cRow, 32].Value = application.KcseYear;
                            worksheet.Cells[index + cRow, 33].Value = application.KcpeIndexNo;
                            worksheet.Cells[index + cRow, 34].Value = application.KcpeYear;
                            worksheet.Cells[index + cRow, 35].Value = application.KcseQualification;
                        }
                        if (clusters.Any())
                        {
                            var scores = await _context.KcseScores.Where(s => s.UserId.Equals(application.UserId)).ToListAsync();
                            var ci = 1;
                            foreach (var cluster in clusters)
                            {
                                var score = scores.Where(s => s.SubjectCode.Equals(cluster.SubjectCode)).Select(s => s.Grade).FirstOrDefault();
                                worksheet.Cells[index + cRow, 34 + ci].Value = score ?? "--";
                                ci++;
                            }
                        }
                        index++;
                    }
                    var modelTable = worksheet.Cells[1, 1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column];
                    modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    var tbl = worksheet.Tables.Add(
                        new ExcelAddressBase(1, 1, worksheet.Dimension.End.Row,
                            worksheet.Dimension.End.Column), $"prog_{Guid.NewGuid().ToString().Substring(0, 6)}_{programme.UniId}");
                    tbl.ShowHeader = true;
                }

                var title = $"{client.ClientName} Applications".Replace(" ", "_");
                var excelName = title + "-" + Guid.NewGuid().ToString().Substring(0, 6);
                return File(package.GetAsByteArray(), XlsxContentType, excelName + ".xlsx");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public async Task<ActionResult> ExportPdf(FilterApplicationsViewModel filter)
        {
            var model = new PdfPreviewViewModel();
            try
            {
                var applications = await GetApplicationsPdfAsync(filter);
                if (!string.IsNullOrEmpty(filter.IntakeId) && !string.IsNullOrEmpty(filter.ProgrammeId) && !string.IsNullOrEmpty(filter.GradeTypeId))
                {
                    var intake = _context.Intakes.FirstOrDefault(x => x.Id == new Guid(filter.IntakeId));
                    var clientSetting = _context.ClientSettings.FirstOrDefault();
                    var programme = _context.Programmes.FirstOrDefault(x => x.Id == new Guid(filter.ProgrammeId));
                    var school = _unisolService.GetSchool(programme.Department).Data;
                    var name = _context.GradeCertificateTypes.FirstOrDefault(x => x.Id == new Guid(filter.GradeTypeId))?.CertType;
                    var isPostGrad = false;

                    if (name.Contains("Master") || name.Contains("Doctor") || name.Contains("Phd"))
                    {
                        isPostGrad = true;
                    }

                    model.Applications = applications;
                    model.ClientSetting = clientSetting;
                    model.Programme = programme;
                    model.Intake = intake;
                    model.School = school;
                    model.Success = true;
                    model.IsPostGrad = isPostGrad;

                    return View(model);
                }

                model.Message = "To print to pdf you have to select a programme, level and intake. If you have selected this and you are still seeing this contact the admin.";

                return View(model);
            }
            catch (Exception e)
            {
                model.Success = false;
                model.Message = "An error occured please contact the admin.";
                Console.WriteLine(e);
                return View();
            }
        }

        public async Task<ActionResult> ExportDeferPdf(DateTime startDate, DateTime endDate)
        {


            var model = new DeferPdfApplicationViewModel();
            try
            {
                var deferments = new List<DeferApplicationViewModel>();
                var clientSetting = _context.ClientSettings.FirstOrDefault();
                _context.Deferments
                    .Where(x => x.DateCreated >= startDate && x.DateCreated <= endDate)
                   .Select(x => new DefermentViewModel(x)).ToList().ForEach(item =>
                   {
                       var reason = _context.Reasons.FirstOrDefault(x => x.Id == item.Reason);
                       //var period = _context.DeferPeriods.FirstOrDefault(x => x.Id == item.DeferPeriod);
                       var application = _context.Applications.FirstOrDefault(x => x.Id == item.ApplicationId);
                       var unisolApp = _unisolService.GetApplicantByNationalId(application.NationalId);

                       //item.PeriodString = period?.Name;
                       item.ReasonString = reason?.Description;
                       var dModel = new DeferApplicationViewModel(application, item);
                       dModel.AdmNo = unisolApp.AdmnNo;
                       deferments.Add(dModel);
                   });
                var user = await _userManager.GetUserAsync(User);


                model.DeferApplications = deferments;
                model.ClientSetting = clientSetting;
                model.ApplicationUser = user;
                model.Success = true;
                model.SignaturePathUrl = user?.SignatureUrl;
                return View(model);
            }
            catch (Exception e)
            {
                model.Success = false;
                model.Message = "An error occured please contact the admin.";
                Console.WriteLine(e);
                return View();
            }
        }


        private async Task<ReturnData<string>> ReceiptAsync([FromBody] StkPayResponse stk, Guid appId)
        {
            var response = new ReturnData<string>();
            try
            {
                _logger.Info($"Received callback from daraja-STK Response : {JsonConvert.SerializeObject(stk)} ");
                if (!stk.IsCompleted)
                {
                    response.Message = "Payment not Completed";
                    response.ErrorMessage = stk.ResultDesc;

                    var message = $"{stk.ResultDesc} : Payment not Completed.";
                    _smsService.Send(new List<string> { stk.PhoneNumber }, message);

                    _logger.Warn($"Receipt failed: {message}");
                    return response;
                }

                var receiptNoExists = _context.ApplicationPayments
                    .Any(p => p.MpesaReceiptNumber.Equals(stk.ReceiptNumber));
                if (receiptNoExists)
                {
                    var message = $"{stk.ReceiptNumber} already exists";
                    _logger.Warn($"Receipt failed: {message}");
                    return response;
                }
                var payment = new ApplicationPayment(stk)
                {
                    Reference = GenerateReceipt(),
                    Personnel = User.Identity.Name
                };
                var application = await ApplicationViewModelAsync(appId);
                payment.AppRef = application.Application.Code;
                payment.ApplicationId = application.Application.Id;
                payment.UserId = application.Application.UserId;
                payment.IntakeId = application.Application.IntakeId;
                var prevBalance = application.Balance;
                payment.Balance = prevBalance - stk.Amount;
                payment.Amount = stk.Amount;
                var dbApplication = await _context.Applications.FirstOrDefaultAsync(a => a.Id.Equals(appId));
                if (dbApplication != null)
                {
                    dbApplication.IsFullyPaid = payment.Balance <= 0;
                    dbApplication.DateUpdated = DateTime.Now;
                    if (payment.Balance <= 0) {
                        dbApplication.Status = EntityStatus.Approved;
                    }
                    _context.ApplicationPayments.Add(payment);
                    var log = new Log
                    {
                        Key = LogKey.ApplicationPayment,
                        ApplicationId = dbApplication.Id,
                        UserId = dbApplication.UserId,
                        Message = $"Payment {payment.Reference} of KES {payment.Amount} has been made for Application Ref {dbApplication.Code}. Balance KES {payment.Balance}",
                        Success = payment.IsCompleted
                    };
                    _context.Logs.Add(log);
                    _context.SaveChanges();
                    var recipients = new List<string> { payment.PhoneNumber };
                    if (!payment.PhoneNumber.Equals(dbApplication.TelNo))
                        recipients.Add(dbApplication.TelNo);

                    var message =
                        $"RCPT: {payment.Reference}. KES {payment.Amount} received for {dbApplication.Code}-{dbApplication.GradeType} via M-PESA {payment.MpesaReceiptNumber}. Balance KES {payment.Balance}";
                    _smsService.Send(recipients, message);

                    _logger.Info($"Receipt Success: {message}");
                    response.Success = true;
                    response.Message = "Receipt success";
                    response.Data = payment.Reference;
                    return response;
                }
                response.Message = "Application not Found";
                _logger.Warn("Receipt failed: Application not found");
                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.Message = "Error Occured. Payment not Received";
                response.ErrorMessage = e.Message;
                _logger.Error($"Receipt error occured: {e.Message}");
                return response;
            }
        }

        private string GenerateReceipt()
        {
            var timeStamp = DateTime.UtcNow.ToString("yyyyMM");
            try
            {
                var nextReceipt = "";
                var exists = true;
                var i = 0;
                while (exists)
                {
                    var nextCount = _context.ApplicationPayments.Count() + 1;
                    nextReceipt = timeStamp + "" + (nextCount + i);
                    exists = _context.ApplicationPayments.Any(a => a.Reference.Equals(nextReceipt));
                    i++;
                }
                return nextReceipt;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error Generating Receipt" + e.Message);
                var rand = new Random().Next(9, 999999999);
                return timeStamp + rand.ToString("D5");
            }
        }
        private ReturnData<string> PaybillIsSelf()
        {
            var res = new ReturnData<string>();
            var mpesa = _context.Settings.First(s => s.Key.Equals(SettingKey.Mpesa));
            var setting = JsonConvert.DeserializeObject<MpesaSetting>(mpesa.Data);
            res.Success = !string.IsNullOrEmpty(setting.AbnClientId);
            res.Message = res.Success ? "Self Managed" : "Not Self Managed";
            res.Data = setting.BusinessShortCode;
            return res;
        }

        private async Task<ApplicationViewModel> ApplicationViewModelAsync(Guid appId)
        {
            var model = new ApplicationViewModel();
            try
            {
                var application = await _context.Applications
                    .Include(a => a.ApplicationProgrammes)
                    .Include(a => a.ApplicationUploads)
                    .Include(a => a.ApplicationPayments)
                    .Include(a => a.ApplicationComments)
                    .Include(a => a.ApplicationWaivers)
                    .FirstOrDefaultAsync(a => a.Id.Equals(appId));
                var unisolApp = _unisolService.GetApplicantByNationalId(application.NationalId);


                if (application == null)
                {
                    model.Message = "Application not Found";
                    return model;
                }
                var intakeGrType = await _context.IntakeGradeTypes
                    .FirstOrDefaultAsync(g => g.IntakeId.Equals(application.IntakeId)
                    && g.GradeTypeId.Equals(application.GradeTypeId));
                if (intakeGrType == null)
                {
                    model.Message = "Applied Grade not Found";
                    return model;
                }
                var deferments = new List<DefermentViewModel>();
                _context.Deferments.Where(x => x.ApplicationId == application.Id)
                   .Select(x => new DefermentViewModel(x)).ToList().ForEach(item => {
                       var reason = _context.Reasons.FirstOrDefault(x => x.Id == item.Reason);
                       var period = _context.DeferPeriods.FirstOrDefault(x => x.Id == item.DeferPeriod);
                       item.PeriodString = period?.Name;
                       item.ReasonString = reason?.Description;
                       deferments.Add(item);
                   });
                model.Deferments = deferments;
                var paybillRes = PaybillIsSelf();
                model.Paybill = paybillRes.Data;
                model.PaybillIsSelf = paybillRes.Success;
                model.Application = application;
                if (unisolApp != null)
                {
                    model.UniStatus = unisolApp.Status;
                }
                model.TotalPaid = application.ApplicationPayments.Where(p => p.IsCompleted).Sum(p => p.Amount);
                model.TotalWaiver = application.ApplicationWaivers
                    .Where(p => p.Status.Equals(EntityStatus.Active)).Sum(p => p.Amount);
                model.TotalExpected = intakeGrType.ApplicationFee;
                model.GradeType = new IntakeGradeTypeViewModel
                {
                    ApplicationFee = intakeGrType.ApplicationFee,
                    Name = application.GradeType,
                    MaxUnits = intakeGrType.MaxUnits
                };
                var programmes = application.ApplicationProgrammes
                    .Join(_context.Programmes, ap => ap.ProgrammeId, p => p.Id, (ap, p) =>
                           new IntakeGradeTypeProgrammeViewModel
                           {
                               Code = p.Code,
                               Name = p.Name,
                               Requirements = p.Requirements,
                               CertType = p.CertType,
                               GradeType = p.GradeType,
                               IsApproved = ap.IsApproved,
                               IsMain = ap.IsMain,
                               ProgrammeId = p.Id
                           }).ToList();
                model.Programmes = programmes;
                if (application.ApplicationUploads.Any())
                {
                    var dIds = application.ApplicationUploads.Select(a => a.DocumentId)
                        .ToList();
                    var documents = _context.Documents
                        .Where(d => dIds.Contains(d.Id)).ToList();
                    application.ApplicationUploads.ForEach(a =>
                    {
                        a.Name = documents.FirstOrDefault(d => d.Id.Equals(a.DocumentId))?.Name ?? a.Name;
                        a.Description = documents.FirstOrDefault(d => d.Id.Equals(a.DocumentId))?.Description ?? a.Description;
                    });
                }
                model.Intake = await _context.Intakes.FirstOrDefaultAsync(i => i.Id.Equals(application.IntakeId));
                model.Success = true;
                model.Message = "Found";
                return model;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error getting application model :-{ex.Message}");
                model.Message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, "Application", model.Message);
                return model;
            }
        }
        private async Task<ReturnData<Guid>> AddOrUpdateApplicationAsync(IntakeGradeTypeViewModel model)
        {
            var res = new ReturnData<Guid>();
            try
            {
                if (string.IsNullOrEmpty(model.ClassTypeId))
                {
                    res.Message = "Class Type is Required";
                    return res;
                }
                if (string.IsNullOrEmpty(model.CampusId))
                {
                    res.Message = "Campus is Required";
                    return res;
                }

                if (!model.Programmes.Any(p => p.IsAdded))
                {
                    res.Message = "No programme Added";
                    return res;
                }

                var appliedProgs = model.Programmes.Count(p => p.IsAdded);
                if (appliedProgs > model.MaxUnits)
                {
                    var suffix = model.MaxUnits > 1 ? "s" : "";
                    res.Message = $"Maximum allowed Programme{suffix} for {model.Name} is {model.MaxUnits}";
                    return res;
                }

                var campus = await _context.Campuses.FirstOrDefaultAsync(c => c.Id.Equals(Guid.Parse(model.CampusId)));
                if (campus == null)
                {
                    res.Message = "Campus provided not Found";
                    return res;
                }
                var classType = await _context.ClassTypes.FirstOrDefaultAsync(c => c.Id.Equals(Guid.Parse(model.ClassTypeId)));
                if (classType == null)
                {
                    res.Message = "Class Type provided not Found";
                    return res;
                }
                var profileSummary = await _profileController.ProfileDetailsSummary(_userManager.GetUserId(User));
                if (!profileSummary.IsComplete)
                {
                    res.Message = $"Profile is not Complete :- {profileSummary.Message}";
                    return res;
                }

                var personnel = User.Identity.Name;
                var personalDetails = profileSummary.PersonalDetail;
                var kinDetails = profileSummary.NextOfKin;
                var emDetails = profileSummary.Emergency;
                var locationDetails = profileSummary.Location;
                var grades = await _dropDownService.KcseGrades();
                var scores = await _context.KcseScores.Where(s => s.UserId.Equals(_userManager.GetUserId(User))).ToListAsync();

                if (model.IsEditMode)
                {
                    var dbApplication = await _context.Applications
                        .Include(p => p.ApplicationProgrammes)
                        .FirstOrDefaultAsync(a => a.Id.Equals(model.ApplicationId));

                    if (dbApplication == null)
                    {
                        res.Message = "Application provided not Found";
                        return res;
                    }

                    if (!dbApplication.Status.Equals(EntityStatus.Pending))
                    {
                        res.Message = $"Application {dbApplication.Code} is already {dbApplication.Status}";
                        return res;
                    }

                    dbApplication.Names = $"{personalDetails.OtherNames} {personalDetails.SurName}";
                    dbApplication.NationalId = personalDetails.IdDocumentNumber;
                    dbApplication.IdDocument = personalDetails.IdDocumentName;
                    dbApplication.Dob = personalDetails.DateOfBirth;
                    dbApplication.Gender = personalDetails.Gender;
                    dbApplication.Marital = personalDetails.MaritalStatusName;
                    dbApplication.Nationality = personalDetails.NationalityName;
                    dbApplication.TelNo = personalDetails.PrimaryPhoneNumber;
                    dbApplication.Email = personalDetails.PrimaryEmailAddress;
                    dbApplication.Religion = personalDetails.ReligionName;
                    dbApplication.Language = personalDetails.Language;
                    dbApplication.IndexNo = personalDetails.IndexNo;
                    dbApplication.KcpeIndexNo = personalDetails.KcpeIndexNo;
                    dbApplication.KcpeYear = personalDetails.KcpeYear;
                    dbApplication.KcseYear = personalDetails.KcseYear;
                    dbApplication.Activity = personalDetails.Activities;
                    dbApplication.Disability = personalDetails.DisabilityCondition;
                    dbApplication.Special = personalDetails.Special;
                    //Emergency
                    dbApplication.EMAddress = $"{emDetails.PostalAddress} {emDetails.AddressName} {emDetails.Town}";
                    dbApplication.EMEmail = emDetails.EmailAddress;
                    dbApplication.EMName = $"{emDetails.OtherNames} {emDetails.SurName}";
                    dbApplication.EMTel = emDetails.PhoneNumber;
                    dbApplication.EMRel = emDetails.RelationShipName;
                    dbApplication.EMRemarks = emDetails.Notes;
                    //Kin
                    dbApplication.KinAddress = $"{kinDetails.PostalAddress} {kinDetails.AddressName} {kinDetails.Town}";
                    dbApplication.KinEmail = kinDetails.EmailAddress;
                    dbApplication.KinName = $"{kinDetails.OtherNames} {kinDetails.SurName}";
                    dbApplication.KinTel = kinDetails.PhoneNumber;
                    dbApplication.KinRel = kinDetails.RelationShipName;
                    dbApplication.KinRemarks = kinDetails.Notes;

                    dbApplication.County = locationDetails.CountyName;
                    dbApplication.Constituency = locationDetails.Constituency;
                    dbApplication.SubCounty = locationDetails.SubCountyName;
                    dbApplication.HomeAddress = $"{locationDetails.PostalAddress} {locationDetails.Town}";
                    dbApplication.GradeType = model.Name;
                    dbApplication.GradeTypeId = model.GradeTypeId;
                    dbApplication.IntakeId = model.IntakeId;
                    dbApplication.Campus = campus.Name;
                    dbApplication.CampusId = campus.Id;
                    dbApplication.ClassTypeId = classType.Id;
                    dbApplication.StudyMode = classType.Name;
                    dbApplication.DateUpdated = DateTime.Now;
                    dbApplication.Source = model.Source;
                    dbApplication.KcseQualification = personalDetails.KcseQualification;

                    var prevProgrammes = dbApplication.ApplicationProgrammes;
                    var uniqueProgIds = model.Programmes
                        .Where(p => p.IsAdded).Select(c => c.ProgrammeId).Distinct().ToList();
                    foreach (var u in uniqueProgIds)
                    {
                        if (!prevProgrammes.Any(p => p.ProgrammeId.Equals(u)))
                        {
                            var uProgrammeCode = _context.Programmes.Where(p => p.Id.Equals(u))
                                .Select(p => p.Code).FirstOrDefault();
                            var uQualified = IsQualified(uProgrammeCode, scores, grades);
                            if (!uQualified.Success)
                            {
                                res.Message = $"You cannot Update Application : {uQualified.Message}";
                                ViewData["ResData"] = uQualified.Data;
                                return res;
                            }
                            dbApplication.ApplicationProgrammes.Add(new ApplicationProgramme
                            {
                                ProgrammeId = u,
                                Personnel = personnel
                            });
                        }
                    }
                    dbApplication.ApplicationProgrammes
                        .RemoveAll(c => !uniqueProgIds.Contains(c.ProgrammeId));
                    await _context.SaveChangesAsync();

                    res.Success = true;
                    res.Message = "Updated";
                    res.Data = dbApplication.Id;
                    return res;
                }

                var client = _clientSettingsService.Read().Data;
                var prefix = string.IsNullOrEmpty(client.AppRefPrefix) ? "APP" : client.AppRefPrefix;
                var nextRef = GenerateRef(prefix);
                var application = new Application
                {
                    Code = nextRef,
                    Names = $"{personalDetails.OtherNames} {personalDetails.SurName}",
                    NationalId = personalDetails.IdDocumentNumber,
                    IdDocument = personalDetails.IdDocumentName,
                    Dob = personalDetails.DateOfBirth,
                    Gender = personalDetails.Gender,
                    Marital = personalDetails.MaritalStatusName,
                    Nationality = personalDetails.NationalityName,
                    TelNo = personalDetails.PrimaryPhoneNumber,
                    Email = personalDetails.PrimaryEmailAddress,
                    Religion = personalDetails.ReligionName,
                    Language = personalDetails.Language,
                    IndexNo = personalDetails.IndexNo,
                    KcseYear = personalDetails.KcseYear,
                    KcpeYear = personalDetails.KcpeYear,
                    KcpeIndexNo = personalDetails.KcpeIndexNo,
                    Activity = personalDetails.Activities,
                    Disability = personalDetails.DisabilityCondition,
                    Special = personalDetails.Special,

                    EMAddress = $"{ emDetails.PostalAddress} {emDetails.AddressName} {emDetails.Town}",
                    EMEmail = emDetails.EmailAddress,
                    EMName = $"{kinDetails.OtherNames} {emDetails.SurName}",
                    EMTel = emDetails.PhoneNumber,
                    EMRel = emDetails.RelationShipName,
                    EMRemarks = emDetails.Notes,

                    KinAddress = $"{ kinDetails.PostalAddress} {kinDetails.AddressName} {kinDetails.Town}",
                    KinEmail = kinDetails.EmailAddress,
                    KinName = $"{kinDetails.OtherNames} {kinDetails.SurName}",
                    KinTel = kinDetails.PhoneNumber,
                    KinRel = kinDetails.RelationShipName,
                    KinRemarks = kinDetails.Notes,

                    County = locationDetails.CountyName,
                    Constituency = locationDetails.Constituency,
                    SubCounty = locationDetails.SubCountyName,
                    HomeAddress = $"{locationDetails.PostalAddress} {locationDetails.Town}",
                    GradeType = model.Name,
                    GradeTypeId = model.GradeTypeId,
                    IntakeId = model.IntakeId,
                    Campus = campus.Name,
                    CampusId = campus.Id,
                    ClassTypeId = classType.Id,
                    StudyMode = classType.Name,
                    DateCreated = DateTime.Now,
                    UserId = _userManager.GetUserId(User),
                    Personnel = personnel,
                    ApplicationProgrammes = new List<ApplicationProgramme>(),
                    Sponsor = "PSSP",
                    Source = model.Source,
                    KcseQualification = personalDetails.KcseQualification
                };

                var selProgIds = model.Programmes
                    .Where(p => p.IsAdded)
                    .Select(p => p.ProgrammeId).Distinct().ToList();
                foreach (var s in selProgIds)
                {
                    var sProgrammeCode = _context.Programmes.Where(p => p.Id.Equals(s))
                                    .Select(p => p.Code).FirstOrDefault();
                    var sQualified = IsQualified(sProgrammeCode, scores, grades);
                    if (!sQualified.Success)
                    {
                        res.Message = $"You cannot Add Application : {sQualified.Message}";
                        ViewData["ResData"] = sQualified.Data;
                        return res;
                    }
                    application.ApplicationProgrammes.Add(new ApplicationProgramme
                    {
                        ProgrammeId = s,
                        Personnel = personnel
                    });
                }
                application.ApplicationProgrammes.First().IsMain = true;
                _context.Applications.Add(application);
                await _context.SaveChangesAsync();

                var paybillRes = PaybillIsSelf();
                if (!paybillRes.Success)
                {
                    var uniRes = await _unisolService.AddOnlineApplicantAsync(application);
                    Console.WriteLine($"upload to unisol {application.Code} : {uniRes.Message}");
                }
                res.Success = true;
                res.Message = "Added";
                res.Data = application.Id;
                return res;
            }
            catch (Exception ex)
            {
                var opt = model.IsEditMode ? "Updating" : "Adding";
                Console.WriteLine(ex);
                _logger.Error($"Error {opt} Application :-{ex.Message}");
                res.Message = "Error occured. Try again";
                return res;
            }
        }

        private string FullPathUrl(string pathUrl)
        {
            if (string.IsNullOrEmpty(pathUrl))
                return string.Empty;
            var http = HttpContext.Request.IsHttps ? "https://" : "http://";
            var baseURL = $"{http}{ HttpContext.Request.Host}";
            return $"{baseURL}/{pathUrl}";
        }
        private string GenerateRef(string prefix)
        {
            try
            {
                var nextRef = "";
                var exists = true;
                var i = 0;
                while (exists)
                {
                    var nextCount = _context.Applications.Count() + 1;
                    nextRef = prefix + (nextCount + i).ToString("D5");
                    exists = _context.Applications.Any(a => a.Code.Equals(nextRef));
                    i++;
                }
                return nextRef;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                var rand = new Random().Next(9, 999999999);
                return prefix + "R" + rand.ToString("D5");
            }
        }
        private string GetContentType(string ext)
        {
            var types = MimeTypes;
            return types[ext.ToLower()];
        }
        private Dictionary<string, string> MimeTypes => new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };

        private async Task<PaginatedList<Application>> GetApplicationsAsync(FilterApplicationsViewModel filter)
        {
            ViewData["NameSortParm"] = string.IsNullOrEmpty(filter.SortOrder) ? "name_desc" : "";
            ViewData["EmailSortParm"] = filter.SortOrder == "Email" ? "email_desc" : "Email";
            ViewData["TelSortParm"] = filter.SortOrder == "TelNo" ? "tel_desc" : "TelNo";
            ViewData["IdSortParm"] = filter.SortOrder == "IdNo" ? "id_desc" : "IdNo";
            ViewData["CurrentSort"] = filter.SortOrder;
            ViewData["CurrentFilter"] = filter.SearchString;
            ViewData["StartDate"] = filter.StartDate;
            ViewData["EndDate"] = filter.EndDate;
            ViewData["GradeTypeId"] = filter.GradeTypeId;
            ViewData["IntakeId"] = filter.IntakeId;
            ViewData["ProgrammeId"] = filter.ProgrammeId;
            ViewData["Status"] = filter.Status;
            ViewData["ClassTypeId"] = filter.ClassTypeId;
            ViewData["Paid"] = filter.Paid;
            ViewData["Pushed"] = filter.Pushed;
            ViewData["Disability"] = filter.Disability;
            ViewData["Gender"] = filter.Gender;
            var appsQry = _context.Applications
                .Include(i => i.ApplicationProgrammes)
                .AsQueryable();
            if (filter.SearchString != null) filter.PageNumber = 1;
            else filter.SearchString = filter.CurrentFilter;
            if (!string.IsNullOrEmpty(filter.SearchString))
                appsQry = appsQry
                    .Where(s => s.Code.Contains(filter.SearchString)
                                               || s.Names.Contains(filter.SearchString)
                                               || s.TelNo.Contains(filter.SearchString)
                                               || s.Email.Contains(filter.SearchString)
                                               || s.NationalId.Contains(filter.SearchString));
            var hasIntakeId = Guid.TryParse(filter.IntakeId, out var intakeId);
            var hasProgrammeId = Guid.TryParse(filter.ProgrammeId, out var programmeId);
            var hasGradeId = Guid.TryParse(filter.GradeTypeId, out var gradeId);
            var hasStartDate = DateTime.TryParse(filter.StartDate, out var sDate);
            var hasEndDate = DateTime.TryParse(filter.EndDate, out var eDate);
            var hasClassTypeId = Guid.TryParse(filter.ClassTypeId, out var classTypeId);
            if (hasIntakeId)
                appsQry = appsQry.Where(a => a.IntakeId.Equals(intakeId));
            if (hasProgrammeId)
                appsQry = appsQry.Where(a => a.ApplicationProgrammes.Any(x => x.ProgrammeId.Equals(programmeId)));
            if (hasGradeId)
                appsQry = appsQry.Where(a => a.GradeTypeId.Equals(gradeId));
            if (hasClassTypeId)
                appsQry = appsQry.Where(a => a.ClassTypeId.Equals(classTypeId));
            if (hasStartDate)
                appsQry = appsQry.Where(a => a.DateCreated.Date >= sDate.Date);
            if (hasEndDate)
                appsQry = appsQry.Where(a => a.DateCreated.Date <= eDate.Date);
            if (!string.IsNullOrEmpty(filter.Paid))
            {
                var paid = filter.Paid.Equals("Yes");
                appsQry = appsQry.Where(a => a.IsFullyPaid.Equals(paid));
            }
            if (!string.IsNullOrEmpty(filter.Gender))
                appsQry = appsQry.Where(a => a.Gender.Equals(filter.Gender));
            if (!string.IsNullOrEmpty(filter.Disability))
                appsQry = appsQry.Where(a => a.Disability.Equals(filter.Disability));
            if (filter.Status != null)
                appsQry = appsQry.Where(a => a.Status.Equals(filter.Status.Value));
            if (!string.IsNullOrEmpty(filter.Pushed))
            {
                var pushed = filter.Pushed.Equals("Yes");
                appsQry = appsQry.Where(a => a.IsUploaded.Equals(pushed));
            }
            var progIds = new List<Guid>();
            if (User.IsInRole(PrimaryRole.Staff))
            {
                var settings = _clientSettingsService.Read().Data;
                if (settings.ViewSchoolOnly)
                {
                    var department = _context.Users
                    .Where(u => u.Id.Equals(_userManager.GetUserId(User)))
                    .Select(u => u.Department).FirstOrDefault();
                    var schoolDeptsRes = await _unisolService.DepartmentsAsync(department);
                    progIds = _context.Programmes.Where(p => schoolDeptsRes.Data.Contains(p.Department))
                        .Select(p => p.Id).ToList();
                }
            }
            switch (filter.SortOrder)
            {
                case "name_desc":
                    appsQry = appsQry.OrderByDescending(s => s.Names);
                    break;
                case "Email":
                    appsQry = appsQry.OrderBy(s => s.Email);
                    break;
                case "email_desc":
                    appsQry = appsQry.OrderByDescending(s => s.Email);
                    break;
                case "TelNo":
                    appsQry = appsQry.OrderBy(s => s.TelNo);
                    break;
                case "tel_desc":
                    appsQry = appsQry.OrderByDescending(s => s.TelNo);
                    break;
                case "IdNo":
                    appsQry = appsQry.OrderBy(s => s.NationalId);
                    break;
                case "id_desc":
                    appsQry = appsQry.OrderByDescending(s => s.NationalId);
                    break;
                default:
                    appsQry = appsQry.OrderByDescending(s => s.DateCreated);
                    break;
            }
            var dataQry = progIds.Any() ? appsQry.Include(a => a.ApplicationProgrammes
            .Where(p => progIds.Contains(p.ProgrammeId)))
                            .ThenInclude(a => a.Programme)
                            : appsQry.Include(a => a.ApplicationProgrammes)
                            .ThenInclude(a => a.Programme);
            var applications = await PaginatedList<Application>
                .CreateAsync(dataQry.AsNoTracking(), filter.PageNumber ?? 1, filter.ItemsPerPage);
            return applications;
        }

        private async Task<List<ApplicationPdfViewModel>> GetApplicationsPdfAsync(FilterApplicationsViewModel filter)
        {
            ViewData["NameSortParm"] = string.IsNullOrEmpty(filter.SortOrder) ? "name_desc" : "";
            ViewData["EmailSortParm"] = filter.SortOrder == "Email" ? "email_desc" : "Email";
            ViewData["TelSortParm"] = filter.SortOrder == "TelNo" ? "tel_desc" : "TelNo";
            ViewData["IdSortParm"] = filter.SortOrder == "IdNo" ? "id_desc" : "IdNo";
            ViewData["CurrentSort"] = filter.SortOrder;
            ViewData["CurrentFilter"] = filter.SearchString;
            ViewData["StartDate"] = filter.StartDate;
            ViewData["EndDate"] = filter.EndDate;
            ViewData["GradeTypeId"] = filter.GradeTypeId;
            ViewData["IntakeId"] = filter.IntakeId;
            ViewData["ProgrammeId"] = filter.ProgrammeId;
            ViewData["Status"] = filter.Status;
            ViewData["ClassTypeId"] = filter.ClassTypeId;
            ViewData["Paid"] = filter.Paid;
            ViewData["Pushed"] = filter.Pushed;
            ViewData["Disability"] = filter.Disability;
            ViewData["Gender"] = filter.Gender;
            var appsQry = _context.Applications
                .Include(i => i.ApplicationProgrammes)
                .AsQueryable();
            if (filter.SearchString != null) filter.PageNumber = 1;
            else filter.SearchString = filter.CurrentFilter;
            if (!string.IsNullOrEmpty(filter.SearchString))
                appsQry = appsQry
                    .Where(s => s.Code.Contains(filter.SearchString)
                                               || s.Names.Contains(filter.SearchString)
                                               || s.TelNo.Contains(filter.SearchString)
                                               || s.Email.Contains(filter.SearchString)
                                               || s.NationalId.Contains(filter.SearchString));
            var hasIntakeId = Guid.TryParse(filter.IntakeId, out var intakeId);
            var hasProgrammeId = Guid.TryParse(filter.ProgrammeId, out var programmeId);
            var hasGradeId = Guid.TryParse(filter.GradeTypeId, out var gradeId);
            var hasStartDate = DateTime.TryParse(filter.StartDate, out var sDate);
            var hasEndDate = DateTime.TryParse(filter.EndDate, out var eDate);
            var hasClassTypeId = Guid.TryParse(filter.ClassTypeId, out var classTypeId);
            if (hasIntakeId)
                appsQry = appsQry.Where(a => a.IntakeId.Equals(intakeId));
            if (hasProgrammeId)
                appsQry = appsQry.Where(a => a.ApplicationProgrammes.Any(x => x.ProgrammeId.Equals(programmeId)));
            if (hasGradeId)
                appsQry = appsQry.Where(a => a.GradeTypeId.Equals(gradeId));
            if (hasClassTypeId)
                appsQry = appsQry.Where(a => a.ClassTypeId.Equals(classTypeId));
            if (hasStartDate)
                appsQry = appsQry.Where(a => a.DateCreated.Date >= sDate.Date);
            if (hasEndDate)
                appsQry = appsQry.Where(a => a.DateCreated.Date <= eDate.Date);
            if (!string.IsNullOrEmpty(filter.Paid))
            {
                var paid = filter.Paid.Equals("Yes");
                appsQry = appsQry.Where(a => a.IsFullyPaid.Equals(paid));
            }
            if (!string.IsNullOrEmpty(filter.Gender))
                appsQry = appsQry.Where(a => a.Gender.Equals(filter.Gender));
            if (!string.IsNullOrEmpty(filter.Disability))
                appsQry = appsQry.Where(a => a.Disability.Equals(filter.Disability));
            if (filter.Status != null)
                appsQry = appsQry.Where(a => a.Status.Equals(filter.Status.Value));
            if (!string.IsNullOrEmpty(filter.Pushed))
            {
                var pushed = filter.Pushed.Equals("Yes");
                appsQry = appsQry.Where(a => a.IsUploaded.Equals(pushed));
            }
            var progIds = new List<Guid>();
            if (User.IsInRole(PrimaryRole.Staff))
            {
                var settings = _clientSettingsService.Read().Data;
                if (settings.ViewSchoolOnly)
                {
                    var department = _context.Users
                    .Where(u => u.Id.Equals(_userManager.GetUserId(User)))
                    .Select(u => u.Department).FirstOrDefault();
                    var schoolDeptsRes = await _unisolService.DepartmentsAsync(department);
                    progIds = _context.Programmes.Where(p => schoolDeptsRes.Data.Contains(p.Department))
                        .Select(p => p.Id).ToList();
                }
            }
            switch (filter.SortOrder)
            {
                case "name_desc":
                    appsQry = appsQry.OrderByDescending(s => s.Names);
                    break;
                case "Email":
                    appsQry = appsQry.OrderBy(s => s.Email);
                    break;
                case "email_desc":
                    appsQry = appsQry.OrderByDescending(s => s.Email);
                    break;
                case "TelNo":
                    appsQry = appsQry.OrderBy(s => s.TelNo);
                    break;
                case "tel_desc":
                    appsQry = appsQry.OrderByDescending(s => s.TelNo);
                    break;
                case "IdNo":
                    appsQry = appsQry.OrderBy(s => s.NationalId);
                    break;
                case "id_desc":
                    appsQry = appsQry.OrderByDescending(s => s.NationalId);
                    break;
                default:
                    appsQry = appsQry.OrderByDescending(s => s.DateCreated);
                    break;
            }
            var dataQry = progIds.Any() ? appsQry.Include(a => a.ApplicationProgrammes
            .Where(p => progIds.Contains(p.ProgrammeId)))
                            .ThenInclude(a => a.Programme)
                            : appsQry.Include(a => a.ApplicationProgrammes)
                            .ThenInclude(a => a.Programme);
            var applications = dataQry.ToList();
            var appList = new List<ApplicationPdfViewModel>();
            if (applications.Count > 0)
            {
                applications.ForEach(x => {
                    var workExperiences = _context.WorkExperiences.Where(i => i.UserId == x.UserId).ToList();
                    var educationExperiences = _context.EducationExperiences.Where(i => i.UserId == x.UserId).ToList();
                    var kcseScores = _context.KcseScores.Where(i => i.UserId == x.UserId).ToList();
                    appList.Add(
                        new ApplicationPdfViewModel()
                        {
                            Application = x,
                            EducationExperiences = educationExperiences,
                            WorkExperiences = workExperiences,
                            KcseScores = kcseScores
                        });
                });
            }
            return appList;
        }

        private ReturnData<string> IsQualified(string programmeCode, List<KcseScore> kcseScores, List<KcseGrade> grades)
        {
            var res = new ReturnData<string>();
            var hasQualified = false;
            var clustersRes = _unisolService.ProgrammeClusters(programmeCode);
            var clusters = clustersRes.Data;
            var uniqueClusters = clusters.Select(c => c.Cluster).Distinct().ToList();
            var resData = "";
            if (!uniqueClusters.Any())
            {
                res.Success = true;
                res.Message = $"No clusters Found :: {clustersRes.Message}";
                res.Data = $"Programme {programmeCode} has no Clusters";
                return res;
            }

            foreach (var cluster in uniqueClusters)
            {
                var choices = clusters.Where(c => c.Cluster.Equals(cluster)).Select(c => c.Choice)
                    .Distinct().ToList();
                foreach (var choice in choices)
                {
                    if (choice.Equals("MANDATORY"))
                    {
                        var mandatoryClusters = clusters.Where(c => c.Choice.Equals("MANDATORY")).ToList();
                        if (!mandatoryClusters.Any())
                            continue;
                        var meanGrade = mandatoryClusters.FirstOrDefault(m => m.Subject.Equals("MEAN GRADE"));
                        if (meanGrade != null)
                        {
                            var grade = grades.FirstOrDefault(g => g.Name.Equals(meanGrade.MinValue));
                            hasQualified = kcseScores.Any(k => k.IsMeanGrade && k.Rank >= grade?.Rank);
                            if (!hasQualified)
                                resData += $"{choice} : {meanGrade.Subject} of at least {meanGrade.MinValue} is required for {programmeCode}</br>";
                            mandatoryClusters.Remove(meanGrade);
                        }
                        var mandatorySubs = mandatoryClusters.Select(e => e.SubjectCode).ToList();
                        var appMandatorySubs = kcseScores.Where(k => mandatorySubs.Contains(k.SubjectCode)).ToList();
                        if (!appMandatorySubs.Any())
                            continue;
                        foreach (var mandatory in mandatoryClusters)
                        {
                            var grade = grades.FirstOrDefault(g => g.Name.Equals(mandatory.MinValue));
                            var subject = appMandatorySubs.FirstOrDefault(a => a.SubjectCode.Equals(mandatory.SubjectCode));
                            if (subject == null)
                            {
                                hasQualified = false;
                                resData += $"{choice} : {mandatory.MinValue} in {mandatory.Subject} is required for {programmeCode}. Enter scores. </br>";
                                break;
                            }
                            hasQualified = subject.Rank >= grade?.Rank;
                            if (!hasQualified)
                            {
                                resData += $"{choice} : {mandatory.Subject} of at least {mandatory.MinValue} is required for {programmeCode}</br>";
                                break;
                            }
                        }
                    }

                    if (choice.Equals("ANY OF"))
                    {
                        var anyOfClusters = clusters.Where(c => c.Choice.Equals("ANY OF")).ToList();
                        if (!anyOfClusters.Any())
                            continue;
                        var meanGrade = anyOfClusters.FirstOrDefault(m => m.Subject.Equals("MEAN GRADE"));
                        if (meanGrade != null)
                        {
                            var grade = grades.FirstOrDefault(g => g.Name.Equals(meanGrade.MinValue));
                            hasQualified = kcseScores.Any(k => k.IsMeanGrade && k.Rank >= grade?.Rank);
                            anyOfClusters.Remove(meanGrade);
                        }
                        var anyOfSubs = anyOfClusters.Select(a => a.SubjectCode).ToList();
                        var appAnyOfSubs = kcseScores.Where(k => anyOfSubs.Contains(k.SubjectCode)).ToList();
                        if (!appAnyOfSubs.Any() && anyOfSubs.Any())
                        {
                            var x = $"Enters scores for {choice} </br>";
                            anyOfClusters.ForEach(a =>
                            {
                                x += $"{a.Subject}  {a.MinValue} OR ";
                            });
                            resData = x.Substring(0, x.Length - 3);
                            hasQualified = false;
                            break;
                        }
                        foreach (var anyOf in anyOfClusters)
                        {
                            var grade = grades.FirstOrDefault(g => g.Name.Equals(anyOf.MinValue));
                            var rank = grade?.Rank ?? 0;
                            hasQualified = appAnyOfSubs.Any(
                                k => k.SubjectCode.Equals(anyOf.SubjectCode) && k.Rank >= rank);
                            if (hasQualified)
                                break;
                        }
                    }
                }
            }

            res.Success = hasQualified;
            res.Message = res.Success ? "Qualified" : $"Not Qualified for {programmeCode}. Check Clusters";
            res.Data = resData;
            return res;
        }

        private static string ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            string name = "";
            switch (_Number)
            {

                case 1:
                    name = "one";
                    break;
                case 2:
                    name = "two";
                    break;
                case 3:
                    name = "three";
                    break;
                case 4:
                    name = "four";
                    break;
                default:
                    name = "zero";
                    break;
            }
            return name;
        }

        private DateTime SemCalculator(Deferment defer, UniApplicant applicant)
        {
            var month = 0;
            if (applicant.RMonth == "September")
            {
                month = 09;
            }
            else if (applicant.RMonth == "January")
            {
                month = 01;
            }
            else if (applicant.RMonth == "May")
            {
                month = 05;
            }

            DateTime reportingDate = DateTime.ParseExact(applicant.RYear + "-0" + month + "-01 14:40:52,531", "yyyy-MM-dd HH:mm:ss,fff", System.Globalization.CultureInfo.InvariantCulture);
            var monthsInSems = defer.Count * 4;
            var newDate = reportingDate.AddMonths(monthsInSems);
            return newDate;
        }
    }
}
