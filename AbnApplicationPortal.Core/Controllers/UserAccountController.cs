﻿using AbnApplicationPortal.Core.CommProviders.Email;
using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Data.Unisol;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models;
using AbnApplicationPortal.Core.Models.AccountVm;
using AbnApplicationPortal.Core.Services;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers.Clearance
{
	[Authorize(Roles = "Admin, SuperAdmin")]
	public class UserAccountController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IEmailSender _emailSender;
		private readonly ILog _logger;
		private readonly AbnApplicationPortalContext _context;
		private readonly UnisolService _unisolService;

		public UserAccountController(AbnApplicationPortalContext context, UserManager<ApplicationUser> userManager,
			IEmailSender emailSender, UnisolDbContext unisol)
		{
			_logger = AbnLogManager.Logger(typeof(UserAccountController));
			_userManager = userManager;
			_emailSender = emailSender;
			_context = context;
			_unisolService = new UnisolService(unisol, context);
		}

		public async Task<IActionResult> Index()
		{
			ViewData["CPage"] = "Index";
			var model = new AccountCountViewModel
			{
				ApplicantAccounts = await _context.Users.CountAsync(u => u.PrimaryRole.Equals(PrimaryRole.Applicant)),
				StaffAccounts = await _context.Users.CountAsync(u => u.PrimaryRole.Equals(PrimaryRole.Staff)),
				AdminAccounts = await _context.Users.CountAsync(u => u.PrimaryRole.Equals(PrimaryRole.Admin))
			};
			return View(model);
		}

		public async Task<IActionResult> Accounts(string type, string sortOrder, string currentFilter, string searchString, int? pageNumber)
		{
			try
			{
				ViewData["CPage"] = string.IsNullOrEmpty(type) ? "All" : type;
				ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
				ViewData["EmailSortParm"] = sortOrder == "Email" ? "email_desc" : "Email";
				ViewData["TelSortParm"] = sortOrder == "TelNo" ? "tel_desc" : "TelNo";
				ViewData["CurrentSort"] = sortOrder;
				ViewData["CurrentFilter"] = searchString;
				var qry = _context.Users
					.Select(u => new UserAccountViewModel(u));
				if (searchString != null) pageNumber = 1;
				else searchString = currentFilter;

				if (!string.IsNullOrEmpty(searchString))
					qry = qry.Where(s => StringContains(s.Names, searchString)
												  || StringContains(s.PhoneNumber, searchString)
												  || StringContains(s.EmailAddress, searchString));
				if (!string.IsNullOrEmpty(type))
					qry = qry.Where(a => a.PrimaryType.Equals(type));
				switch (sortOrder)
				{
					case "name_desc":
						qry = qry.OrderByDescending(s => s.Names);
						break;
					case "Email":
						qry = qry.OrderBy(s => s.EmailAddress);
						break;
					case "email_desc":
						qry = qry.OrderByDescending(s => s.EmailAddress);
						break;
					case "TelNo":
						qry = qry.OrderBy(s => s.PhoneNumber);
						break;
					case "tel_desc":
						qry = qry.OrderByDescending(s => s.PhoneNumber);
						break;
					default:
						qry = qry.OrderByDescending(s => s.Names);
						break;
				}

				var userAccounts = await PaginatedList<UserAccountViewModel>
					.CreateAsync(qry.AsNoTracking(), pageNumber ?? 1, 10);
				return View(userAccounts);
			}
			catch (Exception ex)
			{
				ViewData["CPage"] = "All";
				Console.WriteLine(ex);
				_logger.Error($"Error getting Accounts :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Accounts", message);
				var res = new PaginatedList<UserAccountViewModel>(new List<UserAccountViewModel>(), 1, 1, 1);
				return View(res);
			}
		}

		private bool StringContains(string stringA, string stringB)
		{
			if (string.IsNullOrEmpty(stringA) || string.IsNullOrEmpty(stringB))
				return false;
			return stringA.ToLower().Contains(stringB.ToLower());
		}

		public IActionResult Create(string role = null)
		{
			ViewData["CPage"] = PrimaryRole.Staff;
			var model = new RegisterViewModel { Role = role };
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create(RegisterViewModel model, string returnUrl = null)
		{
			try
			{
				ViewData["CPage"] = PrimaryRole.Staff;
				ViewData["ReturnUrl"] = returnUrl;
				if (ModelState.IsValid)
				{
					UserAccountViewModel uniUser;
					var emRes = _unisolService.GetEmployeeByRefOrEmail(model.Email);
					if (!emRes.Success)
					{
						ModelState.AddModelError(string.Empty,
							$"Failed to get staff with email {model.Email} :-{emRes.Message}");
						return View(model);
					}
					uniUser = new UserAccountViewModel(emRes.Data);
					var user = new ApplicationUser
					{
						UserName = model.Email,
						Email = model.Email,
						PhoneNumber = uniUser.PhoneNumber,
						RefNo = uniUser.RefNo,
						OtherNames = uniUser.Names,
						PrimaryRole = model.Role,
						Personnel = User.Identity.Name
					};

					user.Department = string.IsNullOrEmpty(model.Department) ? uniUser.Department : model.Department;
					var result = await _userManager.CreateAsync(user, model.Password);
					if (result.Succeeded)
					{
						TempData.SetData(AlertLevel.Success, "Accounts", "User created a new account with password.");
						var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
						var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
						await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);

						await _userManager.AddToRoleAsync(user, model.Role);
						_logger.Info($"User created a new account with password and role {model.Role}.");
						return RedirectToAction(nameof(Accounts), new { type = model.Role });
					}

					AddErrors(result);
				}

				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				ModelState.AddModelError(string.Empty, $"Error Occured. Try again later");
				return View(model);
			}
		}

		public async Task<IActionResult> ResetPassword(string id)
		{
			var userResult = await _userManager.FindByIdAsync(id);
			ViewData["CPage"] = userResult.PrimaryRole;
			var code = await _userManager.GeneratePasswordResetTokenAsync(userResult);
			var model = new ResetPasswordViewModel { Email = userResult.Email, Code = code };
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					return View(model);
				}
				var user = await _userManager.FindByEmailAsync(model.Email);
				if (user == null)
				{
					ModelState.AddModelError(string.Empty, $"No user with Email '{model.Email}'.");
					return View(model);
				}

				var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
				if (!result.Succeeded)
				{
					AddErrors(result);
					return View(model);
				}

				TempData.SetData(AlertLevel.Success, "Reset Password", "Password Reset Success");
				return RedirectToAction("Accounts", new { type = user.PrimaryRole });
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				var message = $"Error occured. Try again later";
				ModelState.AddModelError(string.Empty, message);
				TempData.SetData(AlertLevel.Error, "Reset Password", message);

				return View(model);
			}
		}

		[Authorize(Roles = "Admin, SuperAdmin")]
		public async Task<IActionResult> Edit(string id)
		{
			var user = new EditUserAccountViewModel { Id = id };
			try
			{
				var accountResult = await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
				ViewData["CPage"] = accountResult?.PrimaryRole;
				if (accountResult == null)
				{
					TempData.SetData(AlertLevel.Warning, "Edit User", "Failed:- The user is not found");
					return View(user);
				}

				user = new EditUserAccountViewModel(accountResult); ;
				return View(user);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				TempData.SetData(AlertLevel.Error, "Edit User", "Error Occured:- Try again later");
				return View(user);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(EditUserAccountViewModel edit)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					return View(edit);
				}
				var account = await _context.Users
					.FirstOrDefaultAsync(u => u.Id.Equals(edit.Id));
				ViewData["CPage"] = account.PrimaryRole;
				if (account == null)
				{
					ModelState.AddModelError(string.Empty, $"Failed:- User account not found");
					return View(edit);
				}
				if (!edit.Role.Equals(account.PrimaryRole))
				{
					await _userManager.RemoveFromRoleAsync(account, account.PrimaryRole);
					await _userManager.AddToRoleAsync(account, edit.Role);
					account.PrimaryRole = edit.Role;
				}
				account.UserName = edit.UserName;
				account.NormalizedUserName = edit.UserName.ToUpper();
				account.Email = edit.EmailAddress;
				account.NormalizedEmail = edit.EmailAddress.ToUpper();
				account.EmailConfirmed = edit.EmailConfirmed;
				account.PhoneNumber = edit.PhoneNumber;

				await _context.SaveChangesAsync();

				TempData.SetData(AlertLevel.Success, "Edit User", "Update Success");
				return RedirectToAction("Accounts", new { type = account.PrimaryRole });
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				var message = $"Error Occured:- Try again later";
				ModelState.AddModelError(string.Empty, message);
				TempData.SetData(AlertLevel.Error, "Edit User", message);
				return View(edit);
			}
		}


		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError(string.Empty, error.Description);
			}
		}
	}
}
