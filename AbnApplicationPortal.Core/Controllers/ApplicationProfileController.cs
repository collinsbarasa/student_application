﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models.AppProfileVm;
using AbnApplicationPortal.Shared.Models.Applicants;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers.Applications
{
	[Authorize]
	[Route("[controller]/[action]")]
	public class ApplicationProfileController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly AbnApplicationPortalContext _context;
		private readonly ILog _logger;

		public ApplicationProfileController(AbnApplicationPortalContext context, 
			UserManager<ApplicationUser> userManager)
		{
			_logger = AbnLogManager.Logger(typeof(ApplicationKcseScoresController));
			_context = context;
			_userManager = userManager;
		}

		public async Task<IActionResult> PersonalDetails(Guid? appId)
		{
			try
			{
				ViewData["AppId"] = appId;
				var user = await _userManager.GetUserAsync(User);
				var details = await _context.PersonalDetails
					.FirstOrDefaultAsync(p => p.UserId.Equals(user.Id));
				if (details == null)
				{
					var nwModel = new PersonalDetailViewModel()
					{
						Success = true,
						SurName = user.SurName,
						OtherNames = user.OtherNames,
						AlternateEmailAddress = user.Email,
						PrimaryPhoneNumber = user.PhoneNumber,
						PrimaryEmailAddress = user.UserName,
						ApplicationId = appId
					};
					return View(nwModel);
				}

				var dbModel = new PersonalDetailViewModel(details)
				{
					Success = true,
					SurName = user.SurName,
					OtherNames = user.OtherNames,
					PrimaryPhoneNumber = user.PhoneNumber,
					AlternateEmailAddress = user.Email,
					ApplicationId = appId
				};
				return View(dbModel);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error get personal details :-{ex.Message}");
				var model = new PersonalDetailViewModel()
				{ Message = "Error occured. Try again", ErrorMessage = ex.Message };
				TempData.SetData(AlertLevel.Error, "Personal Details", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> PersonalDetails(PersonalDetailViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					return View(model);
				}
				var user = await _userManager.GetUserAsync(User);
				user.SurName = model.SurName;
				user.OtherNames = model.OtherNames;
				user.PhoneNumber = model.PrimaryPhoneNumber;
				//user.Email = model.AlternateEmailAddress;
				var settings = _context.ClientSettings.FirstOrDefault();
				var hasDob = DateTime.TryParse(model.DateOfBirth.ToString(), out var dob);
				if (!hasDob || dob.Date > DateTime.Now.Date)
				{
					ModelState.AddModelError("DateOfBirth", "Invalid date of Birth");
					return View(model);
				}
				var dbModel = await _context.PersonalDetails
					.FirstOrDefaultAsync(p => p.UserId.Equals(user.Id));

				if (dbModel != null)
				{
					if (!settings.SimpleProfile)
					{
						dbModel.AlternatePhoneNumber = model.AlternatePhoneNumber;
						dbModel.ReligionName = model.ReligionName;
						dbModel.Ethnicity = model.Ethnicity;
						dbModel.Language = model.Language;
						dbModel.DateOfBirth = dob;
						dbModel.NationalityName = model.NationalityName;
						dbModel.MaritalStatusName = model.MaritalStatusName;
						dbModel.DisabilityCondition = model.DisabilityCondition;
						dbModel.Activities = model.Activities;
						dbModel.IndexNo = model.IndexNo;
						dbModel.KcpeYear = model.KcpeYear;
						dbModel.KcseYear = model.KcseYear;
						dbModel.KcpeIndexNo = model.KcpeIndexNo;
						dbModel.Special = model.Special;
						dbModel.KcseQualification = model.KcseQualification;
					}
					else {
						dbModel.Institution = model.Institution;
					}
					dbModel.Gender = model.Gender;
					dbModel.Title = model.Title;
					dbModel.IdDocumentName = model.IdDocumentName;
					dbModel.IdDocumentNumber = model.IdDocumentNumber;
					dbModel.PhoneNumber = model.PrimaryPhoneNumber;
					dbModel.DateUpdated = DateTime.Now;

					await _context.SaveChangesAsync();
					await UpdateApplicationPersonalDetails(model);
					TempData.SetData(AlertLevel.Success, "Personal Details", "Information Updated");
					return RedirectToAction("Profile");
				}

				var detail = new PersonalDetail
				{
					Gender = model.Gender,
					Title = model.Title,
					AlternatePhoneNumber = model.AlternatePhoneNumber,
					ReligionName = model.ReligionName,
					Ethnicity = model.Ethnicity,
					Language = model.Language,
					DateOfBirth = dob,
					NationalityName = model.NationalityName,
					MaritalStatusName = model.MaritalStatusName,
					IdDocumentName = model.IdDocumentName,
					IdDocumentNumber = model.IdDocumentNumber,
					DisabilityCondition = model.DisabilityCondition,
					UserId = user.Id,
					Personnel = user.UserName,
					Activities = model.Activities,
					IndexNo = model.IndexNo,
					KcpeIndexNo = model.KcpeIndexNo,
					KcseYear = model.KcseYear,
					KcpeYear = model.KcpeYear,
					KcseQualification = model.KcseQualification,
					Special = model.Special
				};

				await _context.PersonalDetails.AddAsync(detail);
				await _context.SaveChangesAsync();
				await UpdateApplicationPersonalDetails(model);
				TempData.SetData(AlertLevel.Success, "Personal Details", "Information Added");
				return RedirectToAction("Profile");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error updating personal details :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Personal Details", model.Message);
				return View(model);
			}
		}

		public async Task<IActionResult> LocationDetails(Guid? appId)
		{
			try
			{
				ViewData["AppId"] = appId;
				var details = await _context.PersonalDetails
					.FirstOrDefaultAsync(p => p.UserId.Equals(_userManager.GetUserId(User)));
				if (details == null)
				{
					var nwModel = new LocationViewModel()
					{
						Success = true,
						ApplicationId = appId
					};
					return View(nwModel);
				}

				var dbModel = new LocationViewModel(details) { ApplicationId = appId };
				return View(dbModel);
			}
			catch (Exception ex)
			{
				_logger.Error($"Error get location details :-{ex.Message}");
				var model = new LocationViewModel()
				{ Message = "Error occured. Try again", ErrorMessage = ex.Message };
				TempData.SetData(AlertLevel.Error, "Location Details", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> LocationDetails(LocationViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					return View(model);
				}
				var userId = _userManager.GetUserId(User);
				var dbModel = await _context.PersonalDetails
					.FirstOrDefaultAsync(p => p.UserId.Equals(userId));
				model.UserId = userId;
				if (dbModel != null)
				{
					dbModel.CountyName = model.CountyName;
					dbModel.SubCountyName = model.SubCountyName;
					dbModel.Constituency = model.Constituency;
					dbModel.Town = model.Town;
					dbModel.PostalAddress = model.PostalAddress;
					dbModel.AddressName = model.AddressName;
					dbModel.DateUpdated = DateTime.UtcNow;

					await _context.SaveChangesAsync();
					await UpdateApplicationLocationDetails(model);
					TempData.SetData(AlertLevel.Success, "Location Details", "Information Updated");
					return RedirectToAction("Profile");
				}

				var detail = new PersonalDetail
				{
					CountyName = model.CountyName,
					SubCountyName = model.SubCountyName,
					Constituency = model.Constituency,
					Town = model.Town,
					PostalAddress = model.PostalAddress,
					AddressName = model.AddressName,
					UserId = userId
				};

				await _context.PersonalDetails.AddAsync(detail);
				await _context.SaveChangesAsync();
				await UpdateApplicationLocationDetails(model);
				TempData.SetData(AlertLevel.Success, "Location Details", "Information Added");
				return RedirectToAction("Profile");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error get location details :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Location Details", model.Message);
				return View(model);
			}
		}

		public async Task<IActionResult> NextOfKinDetails(Guid? appId)
		{
			try
			{
				ViewData["AppId"] = appId;
				var details = await _context.NextOfKins
					.FirstOrDefaultAsync(p => p.UserId.Equals(_userManager.GetUserId(User)));
				if (details == null)
				{
					var nwModel = new NextOfKinDetailViewModel()
					{
						Success = true,
						ApplicationId = appId
					};
					return View(nwModel);
				}

				var dbModel = new NextOfKinDetailViewModel(details) { ApplicationId = appId };
				return View(dbModel);
			}
			catch (Exception ex)
			{
				_logger.Error($"Error getting next of kin details :-{ex.Message}");
				var model = new LocationViewModel()
				{ Message = "Error occured. Try again", ErrorMessage = ex.Message };
				TempData.SetData(AlertLevel.Error, "Next of Kin", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> NextOfKinDetails(NextOfKinDetailViewModel model)
		{
			try
			{
				ViewData["AppId"] = model.ApplicationId;
				if (!ModelState.IsValid)
				{
					return View(model);
				}
				var userId = _userManager.GetUserId(User);
				model.UserId = userId;
				var dbModel = await _context.NextOfKins
					.FirstOrDefaultAsync(p => p.UserId.Equals(userId));
				if (dbModel != null)
				{
					dbModel.Title = model.Title;
					dbModel.Gender = model.Gender;
					dbModel.IdDocumentName = model.IdDocumentName;
					dbModel.IdDocumentNumber = model.IdDocumentNumber;
					dbModel.Town = model.Town;
					dbModel.PostalAddress = model.PostalAddress;
					dbModel.AddressName = model.AddressName;
					dbModel.SurName = model.SurName;
					dbModel.OtherNames = model.OtherNames;
					dbModel.EmailAddress = model.EmailAddress;
					dbModel.PhoneNumber = model.PhoneNumber;
					dbModel.RelationShipName = model.RelationShipName;
					dbModel.Notes = model.Notes;
					dbModel.DateUpdated = DateTime.UtcNow;

					await _context.SaveChangesAsync();
					await UpdateApplicationKinDetails(model);
					TempData.SetData(AlertLevel.Success, "Next Of Kin", "Information Updated");
					return RedirectToAction("Profile");
				}

				var detail = new NextOfKin
				{
					Title = model.Title,
					Gender = model.Gender,
					IdDocumentName = model.IdDocumentName,
					IdDocumentNumber = model.IdDocumentNumber,
					Town = model.Town,
					PostalAddress = model.PostalAddress,
					AddressName = model.AddressName,
					UserId = userId,
					PhoneNumber = model.PhoneNumber,
					SurName = model.SurName,
					OtherNames = model.OtherNames,
					EmailAddress = model.EmailAddress,
					RelationShipName = model.RelationShipName,
					Notes = model.Notes,
					Personnel = User.Identity.Name,
					
				};

				await _context.NextOfKins.AddAsync(detail);
				await _context.SaveChangesAsync();
				await UpdateApplicationKinDetails(model);
				TempData.SetData(AlertLevel.Success, "Next Of Kin", "Information Added");
				return RedirectToAction("Profile");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error get Next Of Kin Details :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Next Of Kin", model.Message);
				return View(model);
			}
		}

		public async Task<IActionResult> EmergencyContactDetails(Guid? appId, string esameAsKin = null)
		{
			try
			{
				ViewData["AppId"] = appId;
				var details = await _context.EmergencyContacts
					.FirstOrDefaultAsync(p => p.UserId.Equals(_userManager.GetUserId(User)));
				var nwModel = new EmergencyContactDetailViewModel()
				{
					Success = true,
					ApplicationId = appId,
					SameAsKin = false
				};
				if (details == null)
				{
					if (esameAsKin == "Yes")
					{
						var kin = await _context.NextOfKins
					.FirstOrDefaultAsync(p => p.UserId.Equals(_userManager.GetUserId(User)));
						if (kin != null)
						{
							nwModel.Title = kin.Title;
							nwModel.Gender = kin.Gender;
							nwModel.IdDocumentName = kin.IdDocumentName;
							nwModel.IdDocumentNumber = kin.IdDocumentNumber;
							nwModel.Town = kin.Town;
							nwModel.PostalAddress = kin.PostalAddress;
							nwModel.SurName = kin.SurName;
							nwModel.OtherNames = kin.OtherNames;
							nwModel.EmailAddress = kin.EmailAddress;
							nwModel.PhoneNumber = kin.PhoneNumber;
							nwModel.RelationShipName = kin.RelationShipName;
							nwModel.Notes = kin.Notes;
							nwModel.SameAsKin = true;
						}
					}
					return View(nwModel);
				}
				else {
					if (esameAsKin == "Yes") {
						var kin = await _context.NextOfKins
					.FirstOrDefaultAsync(p => p.UserId.Equals(_userManager.GetUserId(User)));
						if (kin != null)
						{
							nwModel.Title = kin.Title;
							nwModel.Gender = kin.Gender;
							nwModel.IdDocumentName = kin.IdDocumentName;
							nwModel.IdDocumentNumber = kin.IdDocumentNumber;
							nwModel.Town = kin.Town;
							nwModel.PostalAddress = kin.PostalAddress;
							nwModel.SurName = kin.SurName;
							nwModel.OtherNames = kin.OtherNames;
							nwModel.EmailAddress = kin.EmailAddress;
							nwModel.PhoneNumber = kin.PhoneNumber;
							nwModel.RelationShipName = kin.RelationShipName;
							nwModel.Notes = kin.Notes;
							nwModel.SameAsKin = true;
						}
						
					}
					else
					{
						nwModel = new EmergencyContactDetailViewModel(details);
					}
				}
				nwModel.ApplicationId = appId;
				return View(nwModel);
			}
			catch (Exception ex)
			{
				_logger.Error($"Error getting emergency contact details :-{ex.Message}");
				var model = new LocationViewModel()
				{ Message = "Error occured. Try again", ErrorMessage = ex.Message };
				TempData.SetData(AlertLevel.Error, "Emergency Contact", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EmergencyContactDetails(EmergencyContactDetailViewModel model)
		{
			try
			{
				ViewData["AppId"] = model.ApplicationId;
				if (!ModelState.IsValid)
				{
					return View(model);
				}
				var userId = _userManager.GetUserId(User);
				var dbModel = await _context.EmergencyContacts
					.FirstOrDefaultAsync(p => p.UserId.Equals(userId));
				model.UserId = userId;
				if (dbModel != null)
				{
					dbModel.Title = model.Title;
					dbModel.Gender = model.Gender;
					dbModel.IdDocumentName = model.IdDocumentName;
					dbModel.IdDocumentNumber = model.IdDocumentNumber;
					dbModel.Town = model.Town;
					dbModel.PostalAddress = model.PostalAddress;
					dbModel.AddressName = model.AddressName;
					dbModel.SurName = model.SurName;
					dbModel.OtherNames = model.OtherNames;
					dbModel.EmailAddress = model.EmailAddress;
					dbModel.PhoneNumber = model.PhoneNumber;
					dbModel.RelationShipName = model.RelationShipName;
					dbModel.Notes = model.Notes;
					dbModel.DateUpdated = DateTime.UtcNow;
					dbModel.SameAsKin = model.SameAsKin;

					await _context.SaveChangesAsync();
					await UpdateApplicationEmergencyContactDetails(model);
					TempData.SetData(AlertLevel.Success, "Emergency Contact", "Information Updated");
					return RedirectToAction("Profile");
				}

				var detail = new EmergencyContact
				{
					Title = model.Title,
					Gender = model.Gender,
					IdDocumentName = model.IdDocumentName,
					IdDocumentNumber = model.IdDocumentNumber,
					Town = model.Town,
					PostalAddress = model.PostalAddress,
					AddressName = model.AddressName,
					UserId = userId,
					PhoneNumber = model.PhoneNumber,
					SurName = model.SurName,
					OtherNames = model.OtherNames,
					EmailAddress = model.EmailAddress,
					RelationShipName = model.RelationShipName,
					Notes = model.Notes,
					Personnel = User.Identity.Name,
					SameAsKin = model.SameAsKin
				};

				await _context.EmergencyContacts.AddAsync(detail);
				await _context.SaveChangesAsync();
				await UpdateApplicationKinDetails(model);
				TempData.SetData(AlertLevel.Success, "Emergency Contact", "Information Added");
				return RedirectToAction("Profile");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error get Emergency Contact Details :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Emergency Contact", model.Message);
				return View(model);
			}
		}

		private async Task<List<EducationExperience>> UserEducationExperiences(string userId)
		{
			return await _context.EducationExperiences
						.OrderByDescending(e => e.EducationLevelRank)
						.Where(e => e.UserId.Equals(userId))
						.ToListAsync();
		}

		public async Task<IActionResult> EducationBackground(Guid? id, Guid? appId)
		{
			var model = new EducationDetailViewModel();
			try
			{
				ViewData["AppId"] = appId;
				if (id != null)
				{
					var dbExperience = await _context.EducationExperiences
						.FirstOrDefaultAsync(e => e.Id.Equals(id));
					if (dbExperience != null)
					{
						var level = await _context.EducationLevels
							.FirstOrDefaultAsync(l => l.Id.Equals(dbExperience.EducationLevelId));
						model.IsEditMode = true;
						model.Success = true;
						model.Id = dbExperience.Id;
						model.Institution = dbExperience.Institution;
						model.CourseStudied = dbExperience.CourseStudied;
						model.StartDateStr = dbExperience.StartDateStr;
						model.EndDateStr = dbExperience.EndDateStr;
						model.EducationLevelId = level?.Id.ToString();
						model.Grade = dbExperience.Grade;
						model.StartDate = dbExperience.StartDate;
						model.EndDate = dbExperience.EndDate;
						model.IndexNo = dbExperience.IndexNo;
						model.Type = dbExperience.QualificationType;
						model.Notes = dbExperience.Notes;
					}
				}
				var uId = _userManager.GetUserId(User);
				var experiences = await UserEducationExperiences(uId);
				if (experiences.Any())
					model.EducationExperiences = experiences;
				model.ApplicationId = appId;
				return View(model);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Education Background :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Education Background", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EducationBackground(EducationDetailViewModel model)
		{
			try
			{
				ViewData["AppId"] = model.ApplicationId;
				var id = _userManager.GetUserId(User);
				var experiences = await UserEducationExperiences(id);
				if (experiences.Any())
					model.EducationExperiences = experiences;
				if (!ModelState.IsValid)
				{
					return View(model);
				}

				

				var sDate = DateTime.Parse(model.StartDate.ToString());
				var eDate = DateTime.Parse(model.EndDate.ToString());
				if (sDate > eDate)
				{
					ModelState.AddModelError("EndDateStr", "Invalid date. Confirm");
					TempData.SetData(AlertLevel.Warning, "Education Background", "End Date should be greater");
					return View(model);
				}

				var level = await _context.EducationLevels
					.FirstOrDefaultAsync(g => g.Id.Equals(Guid.Parse(model.EducationLevelId)));
				if (level == null)
				{
					ModelState.AddModelError("EducationLevelId", "Invalid level. Confirm");
					TempData.SetData(AlertLevel.Warning, "Education Background", "Level not Found");
					return View(model);
				}

				if (model.IsEditMode)
				{
					var dbExperience = await _context.EducationExperiences
						.FirstOrDefaultAsync(e => e.Id.Equals(model.Id));
					if (dbExperience == null)
					{
						TempData.SetData(AlertLevel.Warning, "Education Background", "Provided record not Found");
						return View(model);
					}

					dbExperience.EducationLevelId = level.Id;
					dbExperience.EducationLevel = level.Name;
					dbExperience.EducationLevelRank = level.Rank;
					dbExperience.CourseStudied = model.CourseStudied;
					dbExperience.Institution = model.Institution;
					dbExperience.StartDate = sDate;
					dbExperience.EndDate = eDate;
					dbExperience.Grade = model.Grade;
					dbExperience.DateUpdated = DateTime.Now;
					dbExperience.Notes = model.Notes;
					dbExperience.IndexNo = model.IndexNo;
					dbExperience.QualificationType = model.Type;

					await _context.SaveChangesAsync();
					TempData.SetData(AlertLevel.Success, "Education Background", "Record Updated");
					return RedirectToAction("Profile");
				}

				var experience = new EducationExperience
				{
					EducationLevelId = level.Id,
					EducationLevel = level.Name,
					EducationLevelRank = level.Rank,
					CourseStudied = model.CourseStudied,
					Institution = model.Institution,
					StartDate = sDate,
					EndDate = eDate,
					Grade = model.Grade,
					Personnel = User.Identity.Name,
					UserId = _userManager.GetUserId(User),
					Notes = model.Notes,
					QualificationType = model.Type,
					IndexNo = model.IndexNo
				};

				await _context.EducationExperiences.AddAsync(experience);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Education Background", "Record Added");
				return RedirectToAction("Profile");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error updating Education Background :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Education Background", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteEducationBackground(EducationDetailViewModel model)
		{
			try
			{
				var record = await _context.EducationExperiences
					.FirstOrDefaultAsync(s => s.Id.Equals(model.Id));
				if (record == null)
				{
					TempData.SetData(AlertLevel.Warning, "Education Background", "Provided record not Found");
					return RedirectToAction(nameof(EducationBackground));
				}

				_context.EducationExperiences.Remove(record);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Education Background", "Deleted");
				return RedirectToAction(nameof(EducationBackground));

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error deleting Education Background - {model.Id} :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Education Background", model.Message);
				return RedirectToAction(nameof(EducationBackground));
			}
		}

		private async Task<List<WorkExperience>> UserWorkExperiences(string userId)
		{
			return await _context.WorkExperiences
						.OrderByDescending(e => e.EndDate)
						.Where(e => e.UserId.Equals(userId))
						.ToListAsync();
		}

		public async Task<IActionResult> WorkBackground(Guid? id, Guid? appId)
		{
			var model = new WorkDetailViewModel();
			try
			{
				ViewData["AppId"] = appId;
				if (id != null)
				{
					var dbExperience = await _context.WorkExperiences
						.FirstOrDefaultAsync(e => e.Id.Equals(id));
					if (dbExperience != null)
					{
						model.IsEditMode = true;
						model.Id = dbExperience.Id;
						model.Employer = dbExperience.Employer;
						model.Designation = dbExperience.Designation;
						model.StartDateStr = dbExperience.StartDateStr;
						model.EndDateStr = dbExperience.EndDateStr;
						model.AssignmentNature = dbExperience.AssignmentNature;
						model.StartDate = dbExperience.StartDate;
						model.EndDate = dbExperience.EndDate;
					}
				}
				var uId = _userManager.GetUserId(User);
				var experiences = await UserWorkExperiences(uId);
				if (experiences.Any())
					model.WorkExperiences = experiences;
				model.ApplicationId = appId;
				TempData.SetData(AlertLevel.Success, "Education Background Saved Successfully", "Record Updated");
				return View(model);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Work Experience :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Work Experience", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> WorkBackground(WorkDetailViewModel model)
		{
			try
			{
				ViewData["AppId"] = model.ApplicationId;
				var id = _userManager.GetUserId(User);
				var experiences = await UserWorkExperiences(id);
				if (experiences.Any())
					model.WorkExperiences = experiences;
				if (!ModelState.IsValid)
				{
					return View(model);
				}

				var sDate = DateTime.Parse(model.StartDate.ToString());
				var eDate = DateTime.Parse(model.EndDate.ToString());
				if (sDate > eDate)
				{
					ModelState.AddModelError("EndDateStr", "Invalid date. Confirm");
					TempData.SetData(AlertLevel.Warning, "Education Background", "End Date should be greater");
					return View(model);
				}

				if (model.IsEditMode)
				{
					var dbExperience = await _context.WorkExperiences
						.FirstOrDefaultAsync(e => e.Id.Equals(model.Id));
					if (dbExperience == null)
					{
						TempData.SetData(AlertLevel.Warning, "Work Experience", "Provided record not Found");
						return View(model);
					}

					dbExperience.Employer = model.Employer;
					dbExperience.Designation = model.Designation;
					dbExperience.AssignmentNature = model.AssignmentNature;
					dbExperience.StartDate = sDate;
					dbExperience.EndDate = eDate;
					dbExperience.DateUpdated = DateTime.Now;

					await _context.SaveChangesAsync();
					TempData.SetData(AlertLevel.Success, "Work Experience", "Record Updated");
					return RedirectToAction("Profile");
				}

				var experience = new WorkExperience
				{
					Employer = model.Employer,
					Designation = model.Designation,
					AssignmentNature = model.AssignmentNature,
					StartDate = sDate,
					EndDate = eDate,
					Personnel = User.Identity.Name,
					UserId = _userManager.GetUserId(User),
				};

				await _context.WorkExperiences.AddAsync(experience);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Work Experience", "Record Added");
				return RedirectToAction("Profile");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error updating Work Experience :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Work Experience", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteWorkBackground(WorkDetailViewModel model)
		{
			try
			{
				var record = await _context.WorkExperiences
					.FirstOrDefaultAsync(s => s.Id.Equals(model.Id));
				if (record == null)
				{
					TempData.SetData(AlertLevel.Warning, "Work Experience", "Provided record not Found");
					return RedirectToAction(nameof(WorkBackground));
				}

				_context.WorkExperiences.Remove(record);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Work Experience", "Deleted");
				return RedirectToAction(nameof(WorkBackground)); 

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error deleting Work Experience - {model.Id} :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Work Experience", model.Message);
				return RedirectToAction(nameof(WorkBackground));
			}
		}

		public async Task<IActionResult> LevelDetails(Guid? appId)
		{
			try
			{
				ViewData["AppId"] = appId;
				var details = await _context.Levels
					.Include(x=>x.Referees)
					.FirstOrDefaultAsync(p => p.UserId.Equals(_userManager.GetUserId(User)));
				if (details == null)
				{
					var nwModel = new LevelViewModel()
					{
						Success = true,
						ApplicationId = appId,
						Referees = InitializeReferees(),
						IsEditMode = false,
						Financing = "",
						ResearchInsitute = "",
						Message = "",
						ApplicationLevel = "",
						UserId = "",
						ErrorMessage = "",
						
					};
					return View(nwModel);
				}

				var dbModel = new LevelViewModel(details) { ApplicationId = appId };
				TempData.SetData(AlertLevel.Success, "Work Experience Saved Successfully", "Record Updated");
				return View(dbModel);
			}
			catch (Exception ex)
			{
				_logger.Error($"Error getting level details :-{ex.Message}");
				var model = new LocationViewModel()
				{ Message = "Error occured. Try again", ErrorMessage = ex.Message };
				TempData.SetData(AlertLevel.Error, "Level", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> LevelDetails(LevelViewModel model)
		{
			try
			{
				ViewData["AppId"] = model.ApplicationId;
				
				var userId = _userManager.GetUserId(User);
				var dbModel = await _context.Levels
					.Include(x=>x.Referees)
					.FirstOrDefaultAsync(p => p.UserId.Equals(userId));
				if (dbModel != null)
				{
					dbModel.ApplicationLevel = model.ApplicationLevel;
					dbModel.Financing = model.Financing;
					dbModel.ResearchInsitute = model.ResearchInsitute;
					dbModel.Referees = model.Referees;
					dbModel.DateUpdated = DateTime.UtcNow;

					await _context.SaveChangesAsync();
					TempData.SetData(AlertLevel.Success, "Level", "Information Updated");
					return RedirectToAction("Profile");
				}

				var detail = new Level
				{
					
					ApplicationLevel = model.ApplicationLevel,
					Financing = model.Financing,
					ResearchInsitute = model.ResearchInsitute,
					Referees = model.Referees,
					ApplicationId = model.ApplicationId,
					UserId = userId,
					Personnel = User.Identity.Name
				};

				await _context.Levels.AddAsync(detail);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Level", "Information Added");
				return RedirectToAction("Profile");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error get Level Details :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "Level", model.Message);
				return View(model);
			}
		}

		public async Task<ProfileDetailsSummaryViewModel> ProfileDetailsSummary(string userId)
		{
			var model = new ProfileDetailsSummaryViewModel();
			try
			{
				var user = await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
				var settings = _context.ClientSettings.FirstOrDefault();
				var pDetails = await _context.PersonalDetails
					.FirstOrDefaultAsync(p => p.UserId.Equals(user.Id));
				if (!settings.SimpleProfile)
                {
					var experiences = await UserEducationExperiences(userId);
					if (experiences.Any())
					{
						model.Education.EducationExperiences = experiences;
						model.Education.Success = true;
					}
					else
						model.Education.Message = "Education background details not Found";

					var workExperiences = await UserWorkExperiences(userId);
					if (workExperiences.Any())
					{
						model.Work.WorkExperiences = workExperiences;
						model.Work.Success = true;
					}
					else
						model.Work.Message = "Work experiences details not Found";


					var nextOfKin = await _context.NextOfKins
						.FirstOrDefaultAsync(p => p.UserId.Equals(user.Id));
					if (nextOfKin != null)
						model.NextOfKin = new NextOfKinDetailViewModel(nextOfKin);
					else model.NextOfKin.Message = "Next of Kin details not Found";

					var level = await _context.Levels
						.Include(x => x.Referees)
						.FirstOrDefaultAsync(p => p.UserId.Equals(user.Id));
					if (level != null)
						model.Level = new LevelViewModel(level);
					else model.Level.Message = "Level details not Found";

					var emergency = await _context.EmergencyContacts
						.FirstOrDefaultAsync(p => p.UserId.Equals(user.Id));
					if (emergency != null)
						model.Emergency = new EmergencyContactDetailViewModel(emergency);
					else model.Emergency.Message = "Emergency contact details not Found";

					if (pDetails != null)
						model.Location = new LocationViewModel(pDetails);

					else model.Location.Message = "Location details not Found";

					if (pDetails != null)
						model.PersonalDetail = new PersonalDetailViewModel(pDetails);
					else
						model.PersonalDetail.Message = "Personal details not Found";
					if (string.IsNullOrEmpty(user.PhoneNumber) || string.IsNullOrEmpty(pDetails.Gender))
					{
						model.PersonalDetail.Message = "Complete personal details";
						model.PersonalDetail.Success = false;
					}
				}

				if (settings.SimpleProfile) {
					if (pDetails != null) {
						model.PersonalDetail = new PersonalDetailViewModel(pDetails);
						model.Location = new LocationViewModel(pDetails);
						model.NextOfKin.Success = true;
						model.Emergency.Success = true;
						model.PersonalDetail.Success = true;
						model.Level.Success = true;
						model.Location.Success = true;
					}

					if (string.IsNullOrEmpty(user.PhoneNumber) || string.IsNullOrEmpty(pDetails.Gender))
					{
						model.PersonalDetail.Message = "Complete personal details";
						model.PersonalDetail.Success = false;
					}
					if (string.IsNullOrEmpty(pDetails.CountyName) || string.IsNullOrEmpty(pDetails.SubCountyName))
					{
						model.Location.Message = "Complete location details";
						model.Location.Success = false;
					}

				}
				model.PersonalDetail.SurName = user.SurName;
				model.PersonalDetail.OtherNames = user.OtherNames;
				model.PersonalDetail.AlternateEmailAddress = user.Email;
				model.PersonalDetail.PrimaryEmailAddress = user.UserName;
				model.PersonalDetail.PrimaryPhoneNumber = user.PhoneNumber;

				return model;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"error getting profile details summary {ex} ");
				return model;
			}
		}

		public async Task<IActionResult> Summary(Guid? appId)
		{
			try
			{
				if (appId != null)
				{
					return RedirectToAction("ViewApplication", "IntakeApplications", new { id = appId });
				}
				ViewData["AppId"] = appId;
				var pModel = await ProfileDetailsSummary(_userManager.GetUserId(User));
				return View(pModel);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Summary:-{ex.Message}");
				var model = new ProfileDetailsSummaryViewModel
				{
					Message = "Error occured. Try again",
					ErrorMessage = ex.Message
				};
				TempData.SetData(AlertLevel.Error, "Summary", model.Message);
				return View(model);
			}
		}

		public async Task<IActionResult> Profile(Guid? appId)
		{
			try
			{
				if (appId != null)
				{
					return RedirectToAction("ViewApplication", "IntakeApplications", new { id = appId });
				}
				ViewData["AppId"] = appId;
				var pModel = await ProfileDetailsSummary(_userManager.GetUserId(User));
				return View(pModel);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Summary:-{ex.Message}");
				var model = new ProfileDetailsSummaryViewModel
				{
					Message = "Error occured. Try again",
					ErrorMessage = ex.Message
				};
				TempData.SetData(AlertLevel.Error, "Summary", model.Message);
				return View(model);
			}
		}

		private async Task UpdateApplicationPersonalDetails(PersonalDetailViewModel model)
		{
			try
			{
				if (model.ApplicationId == null)
				{
					return;
				}
				var application = await _context.Applications
					.FirstOrDefaultAsync(a => a.Id.Equals(model.ApplicationId.Value));
				if (application == null)
				{
					return;
				}
				application.Gender = model.Gender;
				application.Language = model.Language;
				application.Dob = model.DateOfBirth;
				application.Nationality = model.NationalityName;
				application.Marital = model.MaritalStatusName;
				application.Religion = model.ReligionName;
				application.IdDocument = model.IdDocumentName;
				application.NationalId = model.IdDocumentNumber;
				application.Disability = model.DisabilityCondition;
				application.IndexNo = model.IndexNo;
				application.Activity = model.Activities;
				application.KcseYear = model.KcseYear;
				application.KcpeIndexNo = model.KcpeIndexNo;
				application.KcpeYear = model.KcpeYear;
				application.KcseQualification = model.KcseQualification;
				application.Special = model.Special;
				application.Email = model.AlternateEmailAddress;
				application.TelNo = model.PrimaryPhoneNumber;
				application.Names = $"{model.OtherNames} {model.SurName}";
				application.DateUpdated = DateTime.Now;
				await _context.SaveChangesAsync();
				return;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"error updating application personal details :-{ex}");
				return;
			}
		}

		private async Task UpdateApplicationLocationDetails(LocationViewModel model)
		{
			try
			{
				if (model.UserId == null)
				{
					return;
				}
				var application = await _context.Applications
					.FirstOrDefaultAsync(a => a.UserId.Equals(model.UserId));
				if (application == null)
				{
					return;
				}
				application.SubCounty = model.SubCountyName;
				application.Constituency = model.Constituency;
				application.HomeAddress = $"{model.PostalAddress} {model.AddressName} {model.Town}";
				application.DateUpdated = DateTime.Now;
				await _context.SaveChangesAsync();
				return;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error updating application location details :- {ex}");
				return;
			}
		}

		private async Task UpdateApplicationKinDetails(NextOfKinDetailViewModel model)
		{
			try
			{
				if (model.UserId == null)
				{
					return;
				}

				var application = await _context.Applications
					.FirstOrDefaultAsync(a => a.UserId.Equals(model.UserId));
				if (application == null)
				{
					return;
				}
				application.KinAddress = $"{model.PostalAddress} {model.AddressName} {model.Town}";
				application.KinEmail = model.EmailAddress;
				application.KinName = $"{model.OtherNames} {model.SurName}";
				application.KinTel = model.PhoneNumber;
				application.KinRel = model.RelationShipName;
				application.KinRemarks = model.Notes;
				application.DateUpdated = DateTime.Now;
				await _context.SaveChangesAsync();
				return;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error updating application next of kin details :- {ex}");
				return;
			}
		}

		private async Task UpdateApplicationEmergencyContactDetails(EmergencyContactDetailViewModel model)
		{
			try
			{
				if (model.UserId == null)
				{
					return;
				}

				var application = await _context.Applications
					.FirstOrDefaultAsync(a => a.UserId.Equals(model.UserId));
				if (application == null)
				{
					return;
				}
				application.EMAddress = $"{model.PostalAddress} {model.AddressName} {model.Town}";
				application.EMEmail = model.EmailAddress;
				application.EMName = $"{model.OtherNames} {model.SurName}";
				application.EMTel = model.PhoneNumber;
				application.EMRel = model.RelationShipName;
				application.EMRemarks = model.Notes;
				application.DateUpdated = DateTime.Now;
				await _context.SaveChangesAsync();
				return;
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error updating application emergency contact details :- {ex}");
				return;
			}
		}

		private List<Referee> InitializeReferees()
		{
			var refs = new List<Referee>();
			refs.Add(new Referee
			{
				Title = _context.UserTitles.FirstOrDefault().Name,
				Name = "Referee 1",
				Phone = "254",
				Email = "example@email.com"
			});
			refs.Add(new Referee
			{
				Title = _context.UserTitles.FirstOrDefault().Name,
				Name = "Referee 2",
				Phone = "254",
				Email = "example@email.com"
			});
			return refs;
		}
	}
}
