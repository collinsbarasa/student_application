﻿using AbnApplicationPortal.Core.CommProviders.Email;
using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Data.Unisol;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models;
using AbnApplicationPortal.Core.Models.AccountVm;
using AbnApplicationPortal.Core.Models.DashboardVm;
using AbnApplicationPortal.Core.Services;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Services;
using log4net;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers.Clearance
{
	public class HomeController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly IEmailSender _emailSender;
		private readonly ILog _logger;
		private readonly AbnApplicationPortalContext _context;
		private readonly UnisolService _unisolService;
		public HomeController(UserManager<ApplicationUser> userManager, AbnApplicationPortalContext context,
			SignInManager<ApplicationUser> signInManager, IEmailSender emailSender, UnisolDbContext unisolDbContext)
		{
			_logger = AbnLogManager.Logger(typeof(HomeController));
			_userManager = userManager;
			_signInManager = signInManager;
			_emailSender = emailSender;
			_context = context;
			_unisolService = new UnisolService(unisolDbContext, context);
		}

		[TempData]
		public string ErrorMessage { get; set; }

		public IActionResult Index(string returnUrl = null)
		{
			StoreCaptcha();
			ViewData["ReturnUrl"] = returnUrl;
			return View();
		}

		public async Task<IActionResult> Login(string returnUrl = null)
		{
			await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
			ViewData["ReturnUrl"] = returnUrl;
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
		{
			ViewData["ReturnUrl"] = returnUrl;
			if (ModelState.IsValid)
			{
				var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe,
					lockoutOnFailure: true);
				if (result.Succeeded)
				{
					TempData.SetData(AlertLevel.Success, "Login", "Login Success");
					_logger.Info("User logged in.");
					return RedirectToLocal(returnUrl);
				}

				if (result.RequiresTwoFactor)
				{
					return RedirectToAction(nameof(LoginWith2fa), new { returnUrl, model.RememberMe });
				}

				if (result.IsLockedOut)
				{
					var msg = "User account locked out";
					_logger.Warn($"Login failed: {msg}");
					TempData.SetData(AlertLevel.Warning, "Login", $"{msg}");
					return RedirectToAction(nameof(Lockout));
				}
				else
				{
					var msg = "Invalid login attempt.";
					ModelState.AddModelError(string.Empty, msg);
					TempData.SetData(AlertLevel.Warning, "Login", $"{msg}");
					return View(model);
				}
			}

			return View(model);
		}

		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> LoginWith2fa(bool rememberMe, string returnUrl = null)
		{
			// Ensure the user has gone through the username & password screen first
			var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

			if (user == null)
			{
				throw new ApplicationException($"Unable to load two-factor authentication user.");
			}

			var model = new LoginWith2faViewModel { RememberMe = rememberMe };
			ViewData["ReturnUrl"] = returnUrl;

			return View(model);
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> LoginWith2fa(LoginWith2faViewModel model, bool rememberMe,
			string returnUrl = null)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
			if (user == null)
			{
				throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
			}

			var authenticatorCode = model.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

			var result =
				await _signInManager.TwoFactorAuthenticatorSignInAsync(authenticatorCode, rememberMe,
					model.RememberMachine);

			if (result.Succeeded)
			{
				_logger.Info($"User with ID {user.Id} logged in with 2fa.");
				return RedirectToLocal(returnUrl);
			}
			else if (result.IsLockedOut)
			{
				_logger.Warn($"User with ID {user.Id} account locked out.");
				return RedirectToAction(nameof(Lockout));
			}
			else
			{
				_logger.Warn($"Invalid authenticator code entered for user with ID {user.Id}.");
				ModelState.AddModelError(string.Empty, "Invalid authenticator code.");
				return View();
			}
		}

		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> LoginWithRecoveryCode(string returnUrl = null)
		{
			// Ensure the user has gone through the username & password screen first
			var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
			if (user == null)
			{
				throw new ApplicationException($"Unable to load two-factor authentication user.");
			}

			ViewData["ReturnUrl"] = returnUrl;

			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> LoginWithRecoveryCode(LoginWithRecoveryCodeViewModel model,
			string returnUrl = null)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
			if (user == null)
			{
				throw new ApplicationException($"Unable to load two-factor authentication user.");
			}

			var recoveryCode = model.RecoveryCode.Replace(" ", string.Empty);

			var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

			if (result.Succeeded)
			{
				_logger.Info("User with ID {UserId} logged in with a recovery code.");
				return RedirectToLocal(returnUrl);
			}

			if (result.IsLockedOut)
			{
				_logger.Warn("User with ID {UserId} account locked out.");
				return RedirectToAction(nameof(Lockout));
			}
			else
			{
				_logger.Warn("Invalid recovery code entered for user with ID {UserId}");
				ModelState.AddModelError(string.Empty, "Invalid recovery code entered.");
				return View();
			}
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult Lockout()
		{
			return View();
		}
		[HttpGet]
		[AllowAnonymous]
		public IActionResult Dashboard()
		{

			var applications = _context.Applications.Count();

			var pendingApp = _context.Applications.Where(x => x.StatusStr == "Pending").Count();

			var approvedApp = _context.Applications.Where(e => e.StatusStr == "Approved").Count();

			var deferments = _context.Deferments.Count();
			var Prog = _context.Programmes.Count();

			var model = new DashboardViewModel()
			{
				ApplicationCount = applications,
				DefermentCount=deferments,
				ProgramCount=Prog,
				PendingCount=pendingApp,
				ApprovedCount=approvedApp
				
			};
		
			 


			return View(model);
		}


		[HttpGet]
		[AllowAnonymous]
		public IActionResult ProgramDetails(Guid id)
		{
			var programe = _context.Programmes.FirstOrDefault(x=>x.Id == id);
			return View(programe);
		}
		[HttpGet]
		[AllowAnonymous]
		public IActionResult PublicProgramDetails(Guid id)
		{
			var programe = _context.Programmes.FirstOrDefault(x => x.Id == id);
			return View(programe);
		}


		[HttpGet]
		[AllowAnonymous]
		public IActionResult Register(string returnUrl = null)
		{
			ViewData["ReturnUrl"] = returnUrl;
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
		{
			ViewData["ReturnUrl"] = returnUrl;
			if (ModelState.IsValid)
			{
				var clientSetting = _context.ClientSettings.FirstOrDefault();
				if (!clientSetting.SimpleProfile) {
					var anyIdNumber = await _context.PersonalDetails
						.AnyAsync(p => p.IdDocumentNumber.ToLower().Equals(model.IdDocumentNumber.ToLower()));
					if (anyIdNumber)
					{
						var idMsg = $"{model.IdDocumentName} {model.IdDocumentNumber} already Exists";
						ModelState.AddModelError("IdDocumentNumber", idMsg);
						TempData.SetData(AlertLevel.Warning, "Register", idMsg);
						return View(model);
					}
				}
					
				var user = new ApplicationUser
				{
					UserName = model.Email,
					Email = model.Email,
					PrimaryRole = model.Role,
					SurName = model.SurName,
					OtherNames = model.OtherNames,
					Personnel = model.Email
				};
				user.PersonalDetail = new PersonalDetail
				{
					IndexNo = model.KcseIndexNo,
					UserId = user.Id,
					Personnel = model.Email,
					DateOfBirth = DateTime.Now,
				};
				if (!clientSetting.SimpleProfile) {
					user.PersonalDetail.IdDocumentName = model.IdDocumentName;
					user.PersonalDetail.IdDocumentNumber = model.IdDocumentNumber;
				}
				var result = await _userManager.CreateAsync(user, model.Password);
				if (result.Succeeded)
				{
					var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
					var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
					await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);

					await _userManager.AddToRoleAsync(user, model.Role);
					await _signInManager.SignInAsync(user, isPersistent: false);
					TempData.SetData(AlertLevel.Success, "Register", $"Check email to confirm Account");
					_logger.Info($"User created a new account with password and role {model.Role}.");
					return RedirectToLocal(returnUrl);
				}

				AddErrors(result);
			}

			return View(model);
		}

		public async Task<IActionResult> Logout()
		{
			await _signInManager.SignOutAsync();
			_logger.Info("User logged out.");
			return RedirectToAction("Index");
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public IActionResult ExternalLogin(string provider, string returnUrl = null)
		{
			// Request a redirect to the external login provider.
			var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new { returnUrl });
			var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
			return Challenge(properties, provider);
		}

		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
		{
			if (remoteError != null)
			{
				ErrorMessage = $"Error from external provider: {remoteError}";
				return RedirectToAction(nameof(Login));
			}

			var info = await _signInManager.GetExternalLoginInfoAsync();
			if (info == null)
			{
				return RedirectToAction(nameof(Login));
			}

			// Sign in the user with this external login provider if the user already has a login.
			var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey,
				isPersistent: false, bypassTwoFactor: true);
			if (result.Succeeded)
			{
				_logger.Info($"User logged in with {info.LoginProvider} provider.");
				return RedirectToLocal(returnUrl);
			}

			if (result.IsLockedOut)
			{
				return RedirectToAction(nameof(Lockout));
			}
			else
			{
				// If the user does not have an account, then ask the user to create an account.
				ViewData["ReturnUrl"] = returnUrl;
				ViewData["LoginProvider"] = info.LoginProvider;
				var email = info.Principal.FindFirstValue(ClaimTypes.Email);
				return View("ExternalLogin", new ExternalLoginViewModel { Email = email });
			}
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginViewModel model,
			string returnUrl = null)
		{
			if (ModelState.IsValid)
			{
				// Get the information about the user from the external login provider
				var info = await _signInManager.GetExternalLoginInfoAsync();
				if (info == null)
				{
					throw new ApplicationException("Error loading external login information during confirmation.");
				}

				var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
				var result = await _userManager.CreateAsync(user);
				if (result.Succeeded)
				{
					result = await _userManager.AddLoginAsync(user, info);
					if (result.Succeeded)
					{
						await _signInManager.SignInAsync(user, isPersistent: false);
						_logger.Info($"User created an account using {info.LoginProvider} provider.");
						return RedirectToLocal(returnUrl);
					}
				}

				AddErrors(result);
			}

			ViewData["ReturnUrl"] = returnUrl;
			return View(nameof(ExternalLogin), model);
		}

		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> ConfirmEmail(string userId, string code)
		{
			if (userId == null || code == null)
			{
				return RedirectToAction("Index");
			}

			var user = await _userManager.FindByIdAsync(userId);
			if (user == null)
			{
				throw new ApplicationException($"Unable to load user with ID '{userId}'.");
			}

			var result = await _userManager.ConfirmEmailAsync(user, code);
			return View(result.Succeeded ? "ConfirmEmail" : "Error");
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult ForgotPassword()
		{
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
		{
			if (ModelState.IsValid)
			{
				var user = await _userManager.FindByEmailAsync(model.Email);
				if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
				{
					// Don't reveal that the user does not exist or is not confirmed
					return RedirectToAction(nameof(ForgotPasswordConfirmation));
				}

				// For more information on how to enable account confirmation and password reset please
				// visit https://go.microsoft.com/fwlink/?LinkID=532713
				var code = await _userManager.GeneratePasswordResetTokenAsync(user);
				var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
				await _emailSender.SendEmailAsync(model.Email, "Reset Password",
					$"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
				return RedirectToAction(nameof(ForgotPasswordConfirmation));
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult ForgotPasswordConfirmation()
		{
			return View();
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult ResetPassword(string code = null)
		{
			if (code == null)
			{
				throw new ApplicationException("A code must be supplied for password reset.");
			}

			var model = new ResetPasswordViewModel { Code = code };
			return View(model);
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var user = await _userManager.FindByEmailAsync(model.Email);
			if (user == null)
			{
				// Don't reveal that the user does not exist
				return RedirectToAction(nameof(ResetPasswordConfirmation));
			}

			var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
			if (result.Succeeded)
			{
				return RedirectToAction(nameof(ResetPasswordConfirmation));
			}

			AddErrors(result);
			return View();
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult ResetPasswordConfirmation()
		{
			return View();
		}


		[HttpGet]
		public IActionResult AccessDenied()
		{
			return View();
		}

		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<JsonResult> SendMessage(SendMessageViewModel model)
		{
			var res = new ReturnData<string>();
			try
			{
				StoreCaptcha();
				if (!ModelState.IsValid)
				{
					res.Message = "Fill all fields to Send";
					return Json(res);
				}
				var validCaptcha = Encrypter.Encrypt(model.Result.Trim()).Equals(model.IntResult);
				if (!validCaptcha)
				{
					res.Message = "Wrong result of the arithmetic operation";
					return Json(res);
				}
				var contactEmail = await _context.ClientSettings
					.Select(e => e.ContactEmail)
					.FirstOrDefaultAsync();
				if (string.IsNullOrEmpty(contactEmail))
				{
					res.Message = "Not Sent. Contact email not set";
					return Json(res);
				}

				var result = await _emailSender.SendContactEmailAsync(model.Name, model.Email, model.Message);
				res.Success = result.Success;
				res.Message = result.Message;
				res.ErrorMessage = result.ErrorMessage;
				return Json(res);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"error sending email message :-{ex.Message}");
				res.Message = "Error occured. Try again later";
				res.ErrorMessage = ex.Message;
				return Json(res);
			}
		}

		public async Task<IActionResult> Programmes(string sortOrder, string currentFilter,
			string searchString, int? pageNumber, int perPage = 10000, string college = "")
		{
			var data = await GetProgrammesAsync(sortOrder, currentFilter, searchString, pageNumber, perPage, college);
			return View(data);
		}

		public async Task<IActionResult> PublicProgrammes(string sortOrder, string currentFilter,
			string searchString, int? pageNumber, int perPage = 10000, string college = "")
		{
			var data = await GetProgrammesAsync(sortOrder, currentFilter, searchString, pageNumber, perPage, college);
			return View(data);
		}

		public async Task<JsonResult> ProgrammesAjax(string sortOrder, string currentFilter,
			string searchString, int? pageNumber, int perPage = 6, string college = "")
		{
			var data = await GetProgrammesAsync(sortOrder, currentFilter, searchString, pageNumber, perPage, college);
			var y = data.PageIndex + 1;
			var x = new ReturnData<PaginatedList<Programme>> { Message = $"{y}", Success = true, Data = data };
			return Json(x);
		}

		private async Task<PaginatedList<Programme>> GetProgrammesAsync(string sortOrder, string currentFilter,
			string searchString, int? pageNumber, int perPage = 6, string college = "")
		{
			try
			{
				ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
				ViewData["CodeSortParm"] = sortOrder == "Code" ? "code_desc" : "Code";
				ViewData["CurrentSort"] = sortOrder;
				ViewData["College"] = college;
				if (searchString != null) pageNumber = 1;
				else searchString = currentFilter;
				ViewData["CurrentFilter"] = searchString;

				var programmesQry = _context.Programmes.AsQueryable();
				if (!string.IsNullOrEmpty(searchString))
					programmesQry = programmesQry.Where(p => p.Name.Contains(searchString)
					|| p.Code.Contains(searchString) || p.CertType.Contains(searchString)
					|| p.Requirements.Contains(searchString));

				if (!string.IsNullOrEmpty(college))
				{
					var departmentsRes = await _unisolService.CollegeDepartmentsAsync(college);
					programmesQry = programmesQry.Where(p => departmentsRes.Data.Contains(p.Department));
				}
				switch (sortOrder)
				{
					case "name_desc":
						programmesQry = programmesQry.OrderByDescending(p => p.Name);
						break;
					case "Code":
						programmesQry = programmesQry.OrderBy(p => p.Code);
						break;
					case "code_desc":
						programmesQry = programmesQry.OrderByDescending(p => p.Code);
						break;
					default:
						programmesQry = programmesQry.OrderBy(s => s.Name);
						break;
				}

				var data = await PaginatedList<Programme>.CreateAsync(programmesQry.AsNoTracking(), pageNumber ?? 1, perPage);
				if (data.TotalItems.Equals(0))
				{
					TempData.SetData(AlertLevel.Warning, "Programmes", "No programme Found");
					return data;
				}
				return data;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting programmes :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Programmes", message);
				var dData = new List<Programme>();
				var errData = await PaginatedList<Programme>.CreateAsync(dData.AsQueryable(), pageNumber ?? 1, perPage);
				return errData;
			}
		}

		[HttpGet]
		public string Encrypt(string text)
		{
			return Encrypter.Encrypt(text);
		}

		[HttpGet]
		public string Decrypt(string text)
		{
			return Encrypter.Decrypt(text);
		}

		#region Helpers
		private void StoreCaptcha()
		{
			var intA = new Random().Next(1, 100);
			var intB = new Random().Next(1, 100);
			ViewData["IntA"] = intA;
			ViewData["IntB"] = intB;
			var total = $"{intA + intB}";
			ViewData["IntTotal"] = Encrypter.Encrypt(total);
		}

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError(string.Empty, error.Description);
			}
		}

		private IActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			else
			{
				return RedirectToAction("Index", "IntakeApplications");
			}
		}

		#endregion
	}
}
