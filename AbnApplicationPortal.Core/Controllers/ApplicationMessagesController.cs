﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Log4Net;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers.Applications
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class ApplicationMessagesController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AbnApplicationPortalContext _context;
        private readonly ILog _logger;

        public ApplicationMessagesController(UserManager<ApplicationUser> userManager,
            AbnApplicationPortalContext context)
        {
            _logger = AbnLogManager.Logger(typeof(ApplicationMessagesController));
            _userManager = userManager;
            _context = context;
        }


        public async Task<IActionResult> Inbox()
        {
            return View();
        }

        public async Task<IActionResult> Compose()
        {
            return View();
        }
    }
}
