﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.Unisol;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models.IntakeSettingsVm;
using AbnApplicationPortal.Shared.Models;
using AbnApplicationPortal.Shared.Models.Applicants;
using AbnApplicationPortal.Shared.Models.Intakes;
using AbnApplicationPortal.Shared.Models.Settings;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers.Applications
{

	[Authorize(Roles = "Admin,SuperAdmin")]
	[Route("[controller]/[action]")]
	public class IntakeSettingsController : Controller
	{
		private readonly AbnApplicationPortalContext _context;
		private readonly ILog _logger;
		private readonly UnisolService _unisolService;
		public IntakeSettingsController(AbnApplicationPortalContext context, UnisolDbContext unisolDbContext)
		{
			_logger = AbnLogManager.Logger(typeof(IntakeSettingsController));
			_context = context;
			_unisolService = new UnisolService(unisolDbContext, context);
		}

		public async Task<IActionResult> Index()
		{
			var model = new IntakeSettingsSummaryViewModel();
			try
			{
				model.Campuses = await _context.Campuses.CountAsync();
				model.Programmes = await _context.Programmes.CountAsync();
				model.ClassTypes = await _context.ClassTypes.CountAsync();
				model.GradeTypes = await _context.GradeCertificateTypes.CountAsync();
				model.Documents = await _context.Documents.CountAsync();
				model.KcseSubjects = await _context.KcseSubjects.CountAsync();
				return View(model);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Utilities :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Utilities", message);
				return View(model);
			}
		}

		public async Task<IActionResult> Campuses()
		{
			var model = new List<CampusViewModel>();
			try
			{
				var personnel = User.Identity.Name;
				var uniCampus = _unisolService.Campuses();
				if (!uniCampus.Success)
				{
					var message = "Failed to get Campuses from Unisol";
					TempData.SetData(AlertLevel.Warning, "Campuses", message);
					return View(model);
				}

				foreach (var campus in uniCampus.Data)
				{
					var dbCampus = _context.Campuses.FirstOrDefault(c => c.UniId.Equals(campus.ID));
					if (dbCampus == null)
					{
						_context.Campuses.Add(new Campus
						{
							Personnel = personnel,
							Address = campus.Address,
							Description = campus.Notes,
							Website = campus.Website,
							Name = campus.Names,
							Contact = campus.Contact,
							Telephone = campus.Tel,
							Email = campus.Email,
							UniId = campus.ID,
							Status = campus.Closed ? EntityStatus.Inactive : EntityStatus.Active
						});
					}
					else
					{
						dbCampus.Address = campus.Address;
						dbCampus.Description = campus.Notes;
						dbCampus.Website = campus.Website;
						dbCampus.Name = campus.Names;
						dbCampus.Contact = campus.Contact;
						dbCampus.Telephone = campus.Tel;
						dbCampus.Email = campus.Email;
						dbCampus.Status = campus.Closed ? EntityStatus.Inactive : EntityStatus.Active;
						dbCampus.DateUpdated = DateTime.Now;
					}
				}

				await _context.SaveChangesAsync();
				var campuses = await _context.Campuses
						.Select(c => new CampusViewModel(c))
						.ToListAsync();
				if (!campuses.Any())
				{
					TempData.SetData(AlertLevel.Error, "Campuses", "Not Found");
					return View(model);
				}
				return View(campuses);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting campuses :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Campuses", message);
				return View(model);
			}
		}

		public async Task<IActionResult> Programmes()
		{
			var model = new List<ProgrammeViewModel>();
			try
			{
				var personnel = User.Identity.Name;
				var uniProgrammes = _unisolService.Programmes();
				var gradeTyes = uniProgrammes.Data
					.Select(e => new { e.GradeType, e.CertType }).Distinct()
					.Select(g => new GradeCertificateType { GradeType = g.GradeType, CertType = g.CertType })
					.ToList();
				var dbGradeTypes = _context.GradeCertificateTypes.ToList();
				foreach (var item in gradeTyes)
				{
					var dbGrade = gradeTyes.FirstOrDefault(g => g.GradeType.Equals(item.GradeType));
					if (dbGrade == null)
					{
						var nGrade = new GradeCertificateType
						{
							CertType = item.CertType,
							GradeType = item.GradeType
						};
						dbGradeTypes.Add(nGrade);
						_context.GradeCertificateTypes.Add(nGrade);
					}
				}
				var dbProgrammes = _context.Programmes.ToList();
				foreach (var uniProgramme in uniProgrammes.Data)
				{
					var dbProgramme = dbProgrammes.FirstOrDefault(p => p.UniId.Equals(uniProgramme.ID));
					if (dbProgramme == null)
					{
						var programme = new Programme
						{
							Code = uniProgramme.Code,
							Name = uniProgramme.Names,
							Notes = uniProgramme.Notes,
							CertType = uniProgramme.CertType,
							GradeType = uniProgramme.GradeType,
							Requirements = uniProgramme.AdminReq,
							UniId = uniProgramme.ID,
							Period = uniProgramme.Period,
							Department = uniProgramme.Department,
							Personnel = personnel
						};
						_context.Programmes.Add(programme);
						dbProgrammes.Add(programme);
					}
					else
					{
						dbProgramme.Code = uniProgramme.Code;
						dbProgramme.Name = uniProgramme.Names;
						dbProgramme.Notes = uniProgramme.Notes;
						dbProgramme.CertType = uniProgramme.CertType;
						dbProgramme.GradeType = uniProgramme.GradeType;
						dbProgramme.Requirements = uniProgramme.AdminReq;
						dbProgramme.Period = uniProgramme.Period;
						dbProgramme.Department = uniProgramme.Department;
						dbProgramme.DateUpdated = DateTime.Now;
					}
				}
				await _context.SaveChangesAsync();
				var programmes = dbProgrammes
						.Select(p => new ProgrammeViewModel(p))
						.ToList();
				if (!programmes.Any())
				{
					TempData.SetData(AlertLevel.Error, "Programmes", "Not Found");
					return View(model);
				}
				return View(programmes);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Programmes :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Programmes", message);
				return View(model);
			}
		}

		public async Task<IActionResult> ClassTypes()
		{
			var model = new List<ClassTypeViewModel>();
			try
			{
				var personnel = User.Identity.Name;
				var uniClassTypes = _unisolService.ClassTypes();
				if (!uniClassTypes.Success)
				{
					var message = "Failed to get Class Types from Unisol";
					TempData.SetData(AlertLevel.Warning, "Class Types", message);
					return View(model);
				}

				foreach (var classType in uniClassTypes.Data)
				{
					var dbType = _context.ClassTypes.FirstOrDefault(c => c.UniId.Equals(classType.ID));
					if (dbType == null)
					{
						_context.ClassTypes.Add(new ClassType
						{
							Personnel = personnel,
							Description = classType.notes,
							Name = classType.Names,
							UniId = classType.ID,
							Status = classType.Closed ? EntityStatus.Inactive : EntityStatus.Active
						});
					}
					else
					{
						dbType.Description = classType.notes;
						dbType.Name = classType.Names;
						dbType.Status = classType.Closed ? EntityStatus.Inactive : EntityStatus.Active;
						dbType.DateUpdated = DateTime.Now;
					}
				}

				await _context.SaveChangesAsync();
				var classTypes = await _context.ClassTypes
						.Select(c => new ClassTypeViewModel(c))
						.ToListAsync();
				if (!classTypes.Any())
				{
					TempData.SetData(AlertLevel.Error, "Class Types", "Not Found");
					return View(model);
				}
				return View(classTypes);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting ClassTypes :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Class Types", message);
				return View(model);
			}
		}

		public async Task<IActionResult> GradeTypes()
		{
			var model = new List<GradeTypeViewModel>();
			try
			{
				var personnel = User.Identity.Name;
				var uniGrade = _unisolService.GradeTypes();
				if (!uniGrade.Success)
				{
					var message = "Failed to get Grades from Unisol";
					TempData.SetData(AlertLevel.Warning, "Grades", message);
					return View(model);
				}

				foreach (var grade in uniGrade.Data)
				{
					var dbGrade = _context.GradeCertificateTypes
						.FirstOrDefault(c => c.GradeType.Equals(grade.GradeType));
					if (dbGrade == null)
					{
						_context.GradeCertificateTypes.Add(new GradeCertificateType
						{
							Personnel = personnel,
							CertType = grade.CertType,
							GradeType = grade.GradeType
						});
					}
					else
					{
						dbGrade.CertType = grade.CertType;
						dbGrade.DateUpdated = DateTime.Now;
					}
				}
				await _context.SaveChangesAsync();
				var gradeTypes = await _context.GradeCertificateTypes
						.OrderBy(x=>x.CertType)
						.Select(c => new GradeTypeViewModel(c))
						.ToListAsync();
				if (!gradeTypes.Any())
				{
					TempData.SetData(AlertLevel.Error, "Grade Types", "Not Found");
					return View(model);
				}
				return View(gradeTypes);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting GradeTypes :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Grade Types", message);
				return View(model);
			}
		}

		public async Task<IActionResult> Documents()
		{
			var model = new List<DocumentViewModel>();
			try
			{
				var documents = await _context.Documents
						.Select(c => new DocumentViewModel(c))
						.ToListAsync();
				if (!documents.Any())
				{
					TempData.SetData(AlertLevel.Error, "Documents", "Not Found");
					return View(model);
				}
				return View(documents);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Documents :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Documents", message);
				return View(model);
			}
		}

		public async Task<IActionResult> EditDocument(Guid? id)
		{
			var model = new DocumentViewModel();
			try
			{
				if (id == null)
					return View(model);

				var document = await _context.Documents.FirstOrDefaultAsync(d => d.Id.Equals(id));
				if (document == null)
				{
					var message = "Document not Found";
					TempData.SetData(AlertLevel.Warning, "Documents", message);
					return View(model);
				}
				return View(new DocumentViewModel(document));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error getting document :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Documents", message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EditDocument(DocumentViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
					return View(model);
				var personnel = User.Identity.Name;
				if (!model.IsEditMode)
				{
					var document = new Document
					{
						Name = model.Name,
						Description = model.Description,
						Category=model.Category,
						Personnel = personnel
					};
					_context.Documents.Add(document);
					await _context.SaveChangesAsync();
					TempData.SetData(AlertLevel.Success, "Documents", "Document Added");
					return RedirectToAction(nameof(Documents));
				}

				var dbDocument = await _context.Documents.FirstOrDefaultAsync(d => d.Id.Equals(model.Id));
				if (dbDocument == null)
				{
					var message = "Document not Found";
					TempData.SetData(AlertLevel.Warning, "Documents", message);
					return View(model);
				}
				dbDocument.Name = model.Name;
				dbDocument.Description = model.Description;
				dbDocument.Category = model.Category;
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Documents", "Document Updated");
				return RedirectToAction(nameof(Documents));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error editing document :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Documents", message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteDocument(DocumentViewModel model)
		{
			try
			{
				var document = await _context.Documents
					.FirstOrDefaultAsync(d => d.Id.Equals(model.Id));
				if (document == null)
				{
					var message = "Document not Found";
					TempData.SetData(AlertLevel.Warning, "Documents", message);
					return RedirectToAction(nameof(Documents));
				}

				var anyDocument = _context.ApplicationUploads.Any(u => u.DocumentId.Equals(model.Id));
				if (anyDocument)
				{
					var message = $"Document {document.Name} already in use";
					TempData.SetData(AlertLevel.Warning, "Documents", message);
					return RedirectToAction(nameof(Documents));
				}

				_context.Documents.Remove(document);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Documents", "Document Deleted");
				return RedirectToAction(nameof(Documents));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error deleting document :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Documents", message);
				return RedirectToAction(nameof(Documents));
			}
		}

		//categories
		public async Task<IActionResult> Categories()
		{
			var model = new List<CategoryViewModel>();
			try
			{
				var documents = await _context.Categories
						.Select(c => new CategoryViewModel(c))
						.ToListAsync();
				if (!documents.Any())
				{
					TempData.SetData(AlertLevel.Error, "Categories", "Not Found");
					return View(model);
				}
				return View(documents);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Categories :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Categories", message);
				return View(model);
			}
		}

		public async Task<IActionResult> EditCategory(Guid? id)
		{
			var model = new CategoryViewModel();
			try
			{
				if (id == null)
					return View(model);

				var document = await _context.Categories.FirstOrDefaultAsync(d => d.Id.Equals(id));
				if (document == null)
				{
					var message = "Ca not Found";
					TempData.SetData(AlertLevel.Warning, "Categories", message);
					return View(model);
				}
				return View(new CategoryViewModel(document));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error getting category :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Category", message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EditCategory(CategoryViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
					return View(model);
				var personnel = User.Identity.Name;
				if (!model.IsEditMode)
				{
					var document = new Category
					{
						Name = model.Name,
						Description = model.Description,
						Owner = model.Owner,
						Personnel = personnel
					};
					_context.Categories.Add(document);
					await _context.SaveChangesAsync();
					TempData.SetData(AlertLevel.Success, "Categories", "Category Added");
					return RedirectToAction(nameof(Categories));
				}

				var dbDocument = await _context.Categories.FirstOrDefaultAsync(d => d.Id.Equals(model.Id));
				if (dbDocument == null)
				{
					var message = "Category not Found";
					TempData.SetData(AlertLevel.Warning, "Categories", message);
					return View(model);
				}
				dbDocument.Name = model.Name;
				dbDocument.Description = model.Description;
				dbDocument.Owner = model.Owner;
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Categories", "Category Updated");
				return RedirectToAction(nameof(Categories));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error editing category :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Categories", message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteCategory(CategoryViewModel model)
		{
			try
			{
				var document = await _context.Categories
					.FirstOrDefaultAsync(d => d.Id.Equals(model.Id));
				if (document == null)
				{
					var message = "Category not Found";
					TempData.SetData(AlertLevel.Warning, "Categories", message);
					return RedirectToAction(nameof(Categories));
				}


				_context.Categories.Remove(document);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Categories", "Category Deleted");
				return RedirectToAction(nameof(Categories));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error deleting document :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Categories", message);
				return RedirectToAction(nameof(Categories));
			}
		}

		public async Task<IActionResult> Subjects()
		{
			var model = new List<KcseSubjectViewModel>();
			try
			{
				var kcseSubjects = await _context.KcseSubjects
					.OrderByDescending(c => c.IsCompulsory)
					.OrderByDescending(c => c.IsMeanGrade)
						.Select(c => new KcseSubjectViewModel(c))
						.ToListAsync();
				if (!kcseSubjects.Any())
				{
					TempData.SetData(AlertLevel.Error, "Subjects", "Not Found");
					return View(model);
				}
				return View(kcseSubjects);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Subjects :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Subjects", message);
				return View(model);
			}
		}

		public async Task<IActionResult> EditSubject(Guid? id)
		{
			var model = new KcseSubjectViewModel();
			try
			{
				if (id == null)
					return View(model);

				var subject = await _context.KcseSubjects.FirstOrDefaultAsync(d => d.Id.Equals(id));
				if (subject == null)
				{
					var message = "Subject not Found";
					TempData.SetData(AlertLevel.Warning, "Subjects", message);
					return View(model);
				}
				return View(new KcseSubjectViewModel(subject));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error getting subject :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Subjects", message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EditSubject(KcseSubjectViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
					return View(model);
				var personnel = User.Identity.Name;
				if (!model.IsEditMode)
				{
					var subject = new KcseSubject
					{
						Code = model.Code,
						Name = model.Name,
						Abbreviation = model.Abbreviation,
						Personnel = personnel,
						IsCompulsory = model.IsCompulsory,
						IsMeanGrade = model.IsMeanGrade
					};
					_context.KcseSubjects.Add(subject);
					await _context.SaveChangesAsync();
					TempData.SetData(AlertLevel.Success, "Subjects", "Subjects Added");
					return RedirectToAction(nameof(Subjects));
				}

				var kcseSubject = await _context.KcseSubjects.FirstOrDefaultAsync(d => d.Id.Equals(model.Id));
				if (kcseSubject == null)
				{
					var message = "Subject not Found";
					TempData.SetData(AlertLevel.Warning, "Subjects", message);
					return View(model);
				}
				kcseSubject.Code = model.Code;
				kcseSubject.Name = model.Name;
				kcseSubject.Abbreviation = model.Abbreviation;
				kcseSubject.IsCompulsory = model.IsCompulsory;
				kcseSubject.IsMeanGrade = model.IsMeanGrade;
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Subjects", "Subject Updated");
				return RedirectToAction(nameof(Subjects));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error editing subject :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Subjects", message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteSubject(KcseSubjectViewModel model)
		{
			try
			{
				var subject = await _context.KcseSubjects.FirstOrDefaultAsync(d => d.Id.Equals(model.Id));
				if (subject == null)
				{
					var message = "Document not Found";
					TempData.SetData(AlertLevel.Warning, "Documents", message);
					return RedirectToAction(nameof(Subjects));
				}
				var anySubject = _context.KcseScores.Any(u => u.SubjectId.Equals(model.Id));
				if (anySubject)
				{
					var message = $"Subject {subject.Name} already in use";
					TempData.SetData(AlertLevel.Warning, "Subjects", message);
					return RedirectToAction(nameof(Subjects));
				}

				_context.KcseSubjects.Remove(subject);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Subjects", "Subject Deleted");
				return RedirectToAction(nameof(Subjects));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error deleting Subject :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Subjects", message);
				return RedirectToAction(nameof(Subjects));
			}
		}

        public async Task<IActionResult> DeferPeriods()
        {
            var model = new List<DeferPeriodViewModel>();
            try
            {
                var deferPeriods = await _context.DeferPeriods
                    .OrderByDescending(c => c.EndDate)
                        .Select(c => new DeferPeriodViewModel(c))
                        .ToListAsync();
                if (!deferPeriods.Any())
                {
                    TempData.SetData(AlertLevel.Error, "Defer Periods", "Not Found");
                    return View(model);
                }
                return View(deferPeriods);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _logger.Error($"Error getting Defer Periods :-{ex.Message}");
                var message = "Error occured. Try again";
                TempData.SetData(AlertLevel.Error, "Defer Periods", message);
                return View(model);
            }
        }

		public async Task<IActionResult> EditDeferPeriod(Guid? id)
		{
			var model = new DeferPeriodViewModel();
			try
			{
				if (id == null)
					return View(model);

				var period = await _context.DeferPeriods.FirstOrDefaultAsync(d => d.Id.Equals(id));
				if (period == null)
				{
					var message = "Deferment period not Found";
					TempData.SetData(AlertLevel.Warning, "DeferPeriods", message);
					return View(model);
				}
				return View(new DeferPeriodViewModel(period));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error getting Deferment period :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "DeferPeriods", message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EditDeferPeriod(DeferPeriodViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
					return View(model);
				var personnel = User.Identity.Name;
				if (!model.IsEditMode)
				{
					var period = new DeferPeriod
					{
						Name = model.Name,
						StartDate = model.StartDate,
						Personnel = personnel,
						EndDate = model.EndDate,
						Active = model.Active
					};
					_context.DeferPeriods.Add(period);
					await _context.SaveChangesAsync();
					TempData.SetData(AlertLevel.Success, "DeferPeriods", "Deferment Periods Added");
					return RedirectToAction(nameof(DeferPeriods));
				}

				var defPeriod = await _context.DeferPeriods.FirstOrDefaultAsync(d => d.Id.Equals(model.Id));
				if (defPeriod == null)
				{
					var message = "Period not Found";
					TempData.SetData(AlertLevel.Warning, "DeferPeriods", message);
					return View(model);
				}
				
				defPeriod.Name = model.Name;
				defPeriod.StartDate = model.StartDate;
				defPeriod.EndDate = model.EndDate;
				defPeriod.Active = model.Active;
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "DeferPeriods", "DeferPeriod Updated");
				return RedirectToAction(nameof(DeferPeriods));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error editing period :- {ex}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "DeferPeriods", message);
				return View(model);
			}
		}

	}
}
