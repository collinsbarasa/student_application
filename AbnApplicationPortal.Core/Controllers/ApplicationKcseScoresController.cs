﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models.AppProfileVm;
using AbnApplicationPortal.Core.Services;
using AbnApplicationPortal.Shared.Models.Applicants;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers.Applications
{


	[Authorize]
	[Route("[controller]/[action]")]
	public class ApplicationKcseScoresController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly AbnApplicationPortalContext _context;
		private readonly ILog _logger;
		private readonly IHostingEnvironment _hostingEnvironment;

		public ApplicationKcseScoresController(AbnApplicationPortalContext context,
			UserManager<ApplicationUser> userManager, IHostingEnvironment hostingEnvironment)
		{
			_logger = AbnLogManager.Logger(typeof(ApplicationKcseScoresController));
			_context = context;
			_userManager = userManager;
			_hostingEnvironment = hostingEnvironment;
		}

		public async Task<IActionResult> Index(Guid? id = null)
		{
			var model = new KcseScoreDetailViewModel();
			try
			{
				var scores = await UserKcseScores();
				if (scores.Any())
					model.KcseScores = scores;
				if (id == null)
					return View(model);

				var score = scores.FirstOrDefault(s => s.Id.Equals(id));
				if (score == null)
				{
					TempData.SetData(AlertLevel.Error, "KSCE Scores", "Provided score not Found");
					_logger.Error($"Unable to get KSCE Score with id :-{id}");
					return View(model);
				}

				model.SubjectId = score.SubjectId.ToString();
				model.GradeId = score.GradeId.ToString();
				model.IsEditMode = true;
				model.Id = score.Id;
				model.Points = score.Points;

				return View(model);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting KSCE Scores :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "KSCE Scores", model.Message);
				return View(model);
			}
		}

		private async Task<List<KcseScore>> UserKcseScores()
		{
			var userId = _userManager.GetUserId(User);
			return await _context.KcseScores
				.Where(s => s.UserId.Equals(userId))
				.ToListAsync();
		}

		private string FullPathUrl(string pathUrl)
		{
			if (string.IsNullOrEmpty(pathUrl))
				return string.Empty;
			var http = HttpContext.Request.IsHttps ? "https://" : "http://";
			var baseURL = $"{http}{ HttpContext.Request.Host}";
			return $"{baseURL}/{pathUrl}";
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Index(KcseScoreDetailViewModel model)
		{
			try
			{
				var scores = await UserKcseScores();
				if (scores.Any())
					model.KcseScores = scores;
				var subject = await _context.KcseSubjects
					.FirstOrDefaultAsync(s => s.Id.Equals(Guid.Parse(model.SubjectId)));
				if (subject == null)
				{
					TempData.SetData(AlertLevel.Warning, "KSCE Scores", "Provided subject not Found");
					return View(model);
				}

				var grade = await _context.KcseGrades
					.FirstOrDefaultAsync(g => g.Id.Equals(Guid.Parse(model.GradeId)));
				if (grade == null)
				{
					TempData.SetData(AlertLevel.Warning, "KSCE Scores", "Provided grade not Found");
					return View(model);
				}

				if (model.IsEditMode)
				{
					var dbScore = await _context.KcseScores
											.FirstOrDefaultAsync(g => g.Id.Equals(model.Id));
					if (dbScore == null)
					{
						TempData.SetData(AlertLevel.Warning, "KSCE Scores", "Provided score not Found");
						return View(model);
					}

					dbScore.Points = model.Points;
					dbScore.SubjectCode = subject.Code;
					dbScore.SubjectId = subject.Id;
					dbScore.IsMeanGrade = subject.IsMeanGrade;
					dbScore.Grade = grade.Name;
					dbScore.GradeId = grade.Id;
					dbScore.Rank = grade.Rank;
					dbScore.DateUpdated = DateTime.Now;

					await _context.SaveChangesAsync();

					TempData.SetData(AlertLevel.Success, "KSCE Scores", "Updated");
					return RedirectToAction(nameof(Index));
				}

				var score = new KcseScore
				{
					UserId = _userManager.GetUserId(User),
					Personnel = User.Identity.Name,
					Points = model.Points,
					SubjectCode = subject.Code,
					SubjectName = subject.Name,
					IsMeanGrade = subject.IsMeanGrade,
					Grade = grade.Name,
					GradeId = grade.Id,
					Rank = grade.Rank,
					SubjectId = subject.Id
				};

				await _context.KcseScores.AddAsync(score);
				await _context.SaveChangesAsync();

				TempData.SetData(AlertLevel.Success, "KSCE Scores", "Updated");
				return RedirectToAction(nameof(Index));

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error updating KSCE Scores :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "KSCE Scores", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Delete(KcseScoreDetailViewModel model)
		{
			try
			{
				var score = await _context.KcseScores
					.FirstOrDefaultAsync(s => s.Id.Equals(model.Id));
				if (score == null)
				{
					TempData.SetData(AlertLevel.Warning, "KSCE Scores", "Provided score not Found");
					return RedirectToAction(nameof(Index));
				}

				_context.KcseScores.Remove(score);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "KSCE Scores", "Deleted");
				return RedirectToAction(nameof(Index));

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error deleting KSCE Scores :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "KSCE Scores", model.Message);
				return RedirectToAction(nameof(Index));
			}
		}

		public async Task<IActionResult> Upload()
		{
			try
			{
				var cert = await _context.KCSECertificates
					.FirstOrDefaultAsync(c => c.UserId.Equals(_userManager.GetUserId(User)));
				if (cert == null)
				{
					TempData.SetData(AlertLevel.Error, "KSCE Upload", "No document Uploaded");
					return View(new KcseCertificateViewModel());
				}
				return View(new KcseCertificateViewModel(cert) { FullPathUrl = FullPathUrl(cert.PathUrl) });
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error updating KSCE Scores :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "KCSE Upload", message);
				return View(new KcseCertificateViewModel());
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Upload(KcseCertificateViewModel model)
		{
			try
			{
				var userId = _userManager.GetUserId(User);
				if (model.IsEditMode)
				{
					var dbCert = await _context.KCSECertificates
								   .FirstOrDefaultAsync(c => c.UserId.Equals(userId));
					if (dbCert != null)
					{
						var upRes = await FileUploadService.Upload(model.File, _hostingEnvironment.WebRootPath);
						if (!upRes.Success)
						{
							TempData.SetData(AlertLevel.Warning, "KCSE Upload", $"Not uploaded. {upRes.Message}");
							return View(model);
						}
						dbCert.PathUrl = upRes.Data.Path;
						dbCert.Size = upRes.Data.Size;
						dbCert.Extension = upRes.Data.Extension;
						dbCert.Description = model.Description;
						dbCert.DateUpdated = DateTime.Now;
						await _context.SaveChangesAsync();
						TempData.SetData(AlertLevel.Success, "KCSE Upload", "Updated");
						return View(new KcseCertificateViewModel(dbCert) { FullPathUrl = dbCert.PathUrl });
					}

					TempData.SetData(AlertLevel.Warning, "KCSE Upload", "Provided document not Found");
					return View(model);
				}

				var upRes_ = await FileUploadService.Upload(model.File, _hostingEnvironment.WebRootPath);
				if (!upRes_.Success)
				{
					var msg = $"Unable to upload Document :- {upRes_.Message}";
					TempData.SetData(AlertLevel.Warning, "KCSE Upload", msg);
					return View(model);
				}

				var data = upRes_.Data;
				var cert = new KCSECertificate
				{
					Extension = data.Extension,
					PathUrl = data.Path,
					Size = data.Size,
					Description = model.Description,
					UserId = userId
				};

				_context.KCSECertificates.Add(cert);
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "KCSE Upload", "Uploaded");
				return View(new KcseCertificateViewModel(cert) { FullPathUrl = cert.PathUrl });
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error uploading KCSE document :-{ex.Message}");
				model.Message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "KCSE Upload", model.Message);
				return View(model);
			}
		}
	}
}