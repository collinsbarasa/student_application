﻿using AbnApplicationPortal.Core.CommProviders.Email;
using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models.ManageVm;
using AbnApplicationPortal.Core.Services;
using AbnApplicationPortal.Shared.Models.Applicants;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers.Clearance
{
	[Authorize]
	[Route("[controller]/[action]")]
	public class ManageController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly UrlEncoder _urlEncoder;
		private readonly IEmailSender _emailSender;
		private readonly ILog _logger;
		private readonly AbnApplicationPortalContext _context;
		private readonly IHostingEnvironment _hostingEnvironment;
        private string _tag;
        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
		private const string RecoveryCodesKey = nameof(RecoveryCodesKey);

		public ManageController(
		  UserManager<ApplicationUser> userManager, IHostingEnvironment hostingEnvironment,
		  SignInManager<ApplicationUser> signInManager, IEmailSender emailSender,
		  UrlEncoder urlEncoder, AbnApplicationPortalContext context)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_urlEncoder = urlEncoder;
			_emailSender = emailSender;
			_logger = AbnLogManager.Logger(typeof(ManageController));
			_context = context;
			_hostingEnvironment = hostingEnvironment;
		}

		[TempData]
		public string StatusMessage { get; set; }

		[HttpGet]
		public async Task<IActionResult> Index()
		{
			var user = await _userManager.GetUserAsync(User);
			var userId = _userManager.GetUserId(User);
			var notifications = await _context.Notifications
				.Where(n => n.UserId.Equals(userId))
				.OrderByDescending(n => n.DateUpdated)
				.ToListAsync();
			if (!notifications.Any())
				TempData.SetData(AlertLevel.Warning, _tag, "Not Found");
			

			if (user == null)
			{
				TempData.SetData(AlertLevel.Warning, "User Account", "Provided user not Found");
				_logger.Error($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
				return View(new IndexViewModel());


			}
		


			var model = new IndexViewModel
			{
				Email = user.Email,
				PhoneNumber = user.PhoneNumber,
				IsEmailConfirmed = user.EmailConfirmed,
				Names = user.Names,
				ProfilePhotoPath = user.ImageUrl,
				SignaturePhotoPath = user.SignatureUrl,
				Notifications= notifications,
				CreatedAt=user.DateCreated
				
			};

			return View(model);

		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Index(IndexViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				TempData.SetData(AlertLevel.Warning, "User Account", "Provided user not Found");
				_logger.Error($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
				return View(model);
			}

			var email = user.Email;
			if (model.Email != email)
			{
				var userNameResult = await _userManager.SetUserNameAsync(user, model.Email);
				if (!userNameResult.Succeeded)
				{
					await _userManager.SetEmailAsync(user, model.Email);
					TempData.SetData(AlertLevel.Warning, "User Account", "Failed to Update email");
					_logger.Error($"Unexpected error occurred setting email for user with ID '{user.Id}'.");
					return View(model);
				}
			}

			var phoneNumber = user.PhoneNumber;
			if (model.PhoneNumber != phoneNumber)
			{
				var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, model.PhoneNumber);
				if (!setPhoneResult.Succeeded)
				{
					TempData.SetData(AlertLevel.Warning, "User Account", "Failed to Update Phone number");
					_logger.Error($"Unexpected error occurred setting phone number for user with ID '{user.Id}'.");
					return View(model);
				}
			}

			StatusMessage = "Your profile has been updated";
			TempData.SetData(AlertLevel.Success, "User Account", StatusMessage);
			return RedirectToAction(nameof(Index));
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> UploadPicture(IndexViewModel model)
		{
			try
			{
				var hasFile = model.File != null;
				if (!hasFile)
				{
					var msg = $"No file to upload";
					TempData.SetData(AlertLevel.Warning, "Profile", msg);
					return RedirectToAction(nameof(Index));
				}

				var userId = _userManager.GetUserId(User);
				var user = await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
				if (user == null)
				{
					var msg = $"Unable to load user account";
					TempData.SetData(AlertLevel.Warning, "Profile", msg);
					return RedirectToAction(nameof(Index));
				}

				var upRes = await FileUploadService.Upload(model.File, _hostingEnvironment.WebRootPath);
				if (!upRes.Success)
				{
					var msg = $"Unable to upload image :- {upRes.Message}";
					TempData.SetData(AlertLevel.Warning, "Profile", msg);
					return RedirectToAction(nameof(Index));
				}

				user.ImageUrl = upRes.Data.Path;
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Profile", "Image uploaded");
				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error uploading photo :- {ex.Message}");
				TempData.SetData(AlertLevel.Error, "Profile", "Error occured. Try later");
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> UploadSignature(IndexViewModel model)
		{
			try
			{
				var hasFile = model.File != null;
				if (!hasFile)
				{
					var msg = $"No file to upload";
					TempData.SetData(AlertLevel.Warning, "Profile", msg);
					return RedirectToAction(nameof(Index));
				}

				var userId = _userManager.GetUserId(User);
				var user = await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
				if (user == null)
				{
					var msg = $"Unable to load user account";
					TempData.SetData(AlertLevel.Warning, "Profile", msg);
					return RedirectToAction(nameof(Index));
				}

				var upRes = await FileUploadService.Upload(model.File, _hostingEnvironment.WebRootPath);
				if (!upRes.Success)
				{
					var msg = $"Unable to upload image :- {upRes.Message}";
					TempData.SetData(AlertLevel.Warning, "Profile", msg);
					return RedirectToAction(nameof(Index));
				}

				user.SignatureUrl = upRes.Data.Path;
				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, "Profile", "Image uploaded");
				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.Error($"Error uploading signature :- {ex.Message}");
				TempData.SetData(AlertLevel.Error, "Profile", "Error occured. Try later");
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> SendVerificationEmail(IndexViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				TempData.SetData(AlertLevel.Warning, "User Account", "Provided user not Found");
				_logger.Error($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
				return RedirectToAction(nameof(Index));
			}

			var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
			var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
			var email = user.Email;
			await _emailSender.SendEmailConfirmationAsync(email, callbackUrl);

			StatusMessage = "Verification email sent. Please check your email.";
			TempData.SetData(AlertLevel.Success, "User Account", StatusMessage);
			return RedirectToAction(nameof(Index));
		}

		[HttpGet]
		public async Task<IActionResult> ChangePassword()
		{
			var user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
			}

			var model = new ChangePasswordViewModel { StatusMessage = StatusMessage };
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				TempData.SetData(AlertLevel.Warning, "User Account", "Provided user not Found");
				_logger.Error($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
				return View(model);
			}

			var changePasswordResult = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
			if (!changePasswordResult.Succeeded)
			{
				TempData.SetData(AlertLevel.Warning, "User Account", "Unable to change Password. ");
				AddErrors(changePasswordResult);
				return View(model);
			}

			await _signInManager.SignInAsync(user, isPersistent: false);
			_logger.Info("User changed their password successfully.");
			StatusMessage = "Your password has been changed.";
			TempData.SetData(AlertLevel.Success, "User Account", StatusMessage);
			return RedirectToAction(nameof(ChangePassword));
		}


		#region Helpers

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError(string.Empty, error.Description);
			}
		}

		private string FormatKey(string unformattedKey)
		{
			var result = new StringBuilder();
			int currentPosition = 0;
			while (currentPosition + 4 < unformattedKey.Length)
			{
				result.Append(unformattedKey.Substring(currentPosition, 4)).Append(" ");
				currentPosition += 4;
			}
			if (currentPosition < unformattedKey.Length)
			{
				result.Append(unformattedKey.Substring(currentPosition));
			}

			return result.ToString().ToLowerInvariant();
		}

		private string GenerateQrCodeUri(string email, string unformattedKey)
		{
			return string.Format(
				AuthenticatorUriFormat,
				_urlEncoder.Encode("Identity"),
				_urlEncoder.Encode(email),
				unformattedKey);
		}
		#endregion
	}
}
