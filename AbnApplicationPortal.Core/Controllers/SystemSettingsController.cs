﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AbnApplicationPortal.Core.CommProviders.Email;
using AbnApplicationPortal.Core.CommProviders.Sms;
using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Data.Soap;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models.SettingsVm;
using AbnApplicationPortal.Core.Services;
using AbnApplicationPortal.Shared.Models.Settings;
using AbnApplicationPortal.Shared.Requests.Application;
using AbnApplicationPortal.Shared.Requests.Settings;
using AbnApplicationPortal.Shared.Services;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RestSharp;

namespace AbnApplicationPortal.Core.Controllers.Applications
{
	[Authorize(Roles = "Admin,SuperAdmin")]
	[Route("[controller]/[action]")]
	public class SystemSettingsController : Controller
	{
		private readonly AbnApplicationPortalContext _context;
		private readonly ILog _logger;
		private readonly IClientSettingsService _clientSettingsService;
		private readonly IHostingEnvironment _hostingEnvironment;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IEmailSender _emailSender;
		private readonly ISmsService _smsService;
		public SystemSettingsController(AbnApplicationPortalContext context, IClientSettingsService clientSettingsService,
			IHostingEnvironment hostingEnvironment, UserManager<ApplicationUser> userManager,
			IEmailSender emailSender, ISmsService smsService)
		{
			_logger = AbnLogManager.Logger(typeof(SystemSettingsController));
			_context = context;
			_clientSettingsService = clientSettingsService;
			_hostingEnvironment = hostingEnvironment;
			_userManager = userManager;
			_emailSender = emailSender;
			_smsService = smsService;
		}

		public async Task<IActionResult> Index()
		{
			var model = new ClientSettingsViewModel();
			try
			{
				var client = await _context.ClientSettings.FirstOrDefaultAsync();
				if (client == null)
				{
					TempData.SetData(AlertLevel.Error, "Client Settings", "No Settings Found");
					return View(model);
				}
				return View(new ClientSettingsViewModel(client));
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting client settings :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Client Settings", message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Index(ClientSettingsViewModel model)
		{
			try
			{
				if (model.File != null)
				{
					var currentPath = _clientSettingsService.Read().Data.LogoUrl;
					var res = await FileUploadService.Upload(model.File, _hostingEnvironment.WebRootPath);
					if (!res.Success)
					{
						ModelState.AddModelError("File", res.Message);
						return View(model);
					}
					model.IsNewImg = res.Success;
					model.LogoUrl = res.Success ? res.Data.Path : string.Empty;
				}

				

				if (model.LFImage != null)
				{
					var currentPath = _clientSettingsService.Read().Data.ImageUrl;
					var res = await FileUploadService.Upload(model.LFImage, _hostingEnvironment.WebRootPath);
					if (!res.Success)
					{
						ModelState.AddModelError("LFImage", res.Message);
						return View(model);
					}
					model.IsNewLFImg = res.Success;
					model.ImageUrl = res.Success ? res.Data.Path : string.Empty;
				}
				var settings = _clientSettingsService.Update(model, User.Identity.Name);
				var resModel = settings.Success ? settings.Data
					: new ClientSettingsViewModel();
				model.Success = settings.Success;
				model.Message = settings.Message;
				TempData.SetData(model.Success ? AlertLevel.Success : AlertLevel.Warning, "Client Settings", model.Message);
				return View(resModel);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error updating client settings :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Client Settings", message);
				return View(model);
			}
	
		}

		public async Task<IActionResult> Email()
		{
			var model = new EmailSettingsViewModel();
			try
			{
				var setting = await _context.Settings.FirstOrDefaultAsync(s => s.Key.Equals(SettingKey.Email));
				if (setting != null)
				{
					var emailSetting = JsonConvert.DeserializeObject<EmailSetting>(setting.Data);
					model = new EmailSettingsViewModel(emailSetting);
				}

				model.Success = true;
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				_logger.Error($"Error fetching email settings :- {e.Message}");
				model.Message = "Error Occured. Try again later";
				TempData.SetData(AlertLevel.Error, "E-Mail Settings", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Email(EmailSettingsViewModel model)
		{
			try
			{
				var data = JsonConvert.SerializeObject(model);
				var setting = new Setting { Key = model.Key };
				setting.Data = data;
				var res = await Update(setting);
				model.Message = res ? "Email Settings Updated" : "Email Settings not Updated";
				model.Success = res;
				TempData.SetData(res ? AlertLevel.Success : AlertLevel.Warning, "E-Mail Settings", model.Message);
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				model.Message = "Error occured. Try again later";
				TempData.SetData(AlertLevel.Error, "E-Mail Settings", model.Message);
				return View(model);
			}
		}

		public async Task<IActionResult> SendTestEmail()
		{
			var user = await _userManager.GetUserAsync(User);
			return View(new TestEmailViewModel
			{
				Names = user.Names,
				Email = user.Email,
				Subject = $"Test E-mail  :- {DateTime.Now}"
			});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> SendTestEmail(TestEmailViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					TempData.SetData(AlertLevel.Warning, "Test Email", "Invalid Request");
					return View(model);
				}
				var result = await _emailSender.SendEmailAsync(model.Email, model.Subject, model.Content);
				TempData.SetData(result.Success ? AlertLevel.Success : AlertLevel.Warning, "Test Email", result.Message);
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				model.Message = "Error occured. Try again later";
				TempData.SetData(AlertLevel.Error, "Test Email", model.Message);
				return View(model);
			}
		}

		public async Task<IActionResult> PustTestStk()
		{
			var user = await _userManager.GetUserAsync(User);
			var phone = $"254{user.PhoneNumber.Substring(user.PhoneNumber.Length - 9)}";
			return View(new TestStkViewModel
			{
				Phone = phone,
				Amount = 1
			});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> PustTestStk(TestStkViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					TempData.SetData(AlertLevel.Warning, "Test Stk", "Invalid Request");
					return View(model);
				}
				var application = await _context.Applications.FirstOrDefaultAsync();
				var appId = application?.Id ?? Guid.Empty;
				var refNo = application?.Code ?? "TEST_APP";
				var setting = await _context.Settings.FirstOrDefaultAsync(s => s.Key.Equals(SettingKey.Mpesa));
				var mpesa = JsonConvert.DeserializeObject<MpesaSetting>(setting.Data);
				var lipa = new LipaRequest()
				{
					CallBackURL = $"{mpesa.CallBackURL}?code={Encrypter.Encrypt(appId.ToString())}",
					PassKey = mpesa.PassKey,
					BusinessShortCode = mpesa.BusinessShortCode,
					TransactionDesc = refNo,
					ConsumerKey = mpesa.ConsumerKey,
					ConsumerSecret = mpesa.ConsumerSecret,
					//AuthUrl = mpesa.AuthUrl,
					//StkUrl = mpesa.StkUrl,
					ClientId = mpesa.AbnClientId,
					AccountReference = refNo,
					Amount = $"{model.Amount}",
					PhoneNumber = model.Phone
				};

				var expressUrl = mpesa.AbnExpressUrl;
				var client = new RestClient(expressUrl);
				var expressRequest = new RestRequest("pay", Method.POST) { RequestFormat = DataFormat.Json };
				expressRequest.AddJsonBody(lipa);
				var data = await client.ExecutePostTaskAsync(expressRequest);

				if (data.StatusCode.Equals(HttpStatusCode.OK))
				{
					var content = data.Content;
					var result = JsonConvert.DeserializeObject<InitiateStkResponse>(content);
					model.Success = result.Success;
					model.Message = result.Success ? "STK Push Success" : $"STK Failed :-{result.Message}";
					TempData.SetData(model.Success ? AlertLevel.Success : AlertLevel.Warning, "Test Stk", model.Message);
					return View(model);
				}
				model.Message = "STK Push Failed:- Incomplete";
				TempData.SetData(AlertLevel.Warning, "Test Stk", model.Message);
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				model.Message = "Error occured. Try again later";
				TempData.SetData(AlertLevel.Error, "Test Stk", model.Message);
				return View(model);
			}
		}

		public async Task<IActionResult> SendTestSms()
		{
			var user = await _userManager.GetUserAsync(User);
			return View(new TestSmsViewModel
			{
				Phone = user.PhoneNumber
			});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult SendTestSms(TestSmsViewModel model)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					TempData.SetData(AlertLevel.Warning, "Test Sms", "Invalid Request");
					return View(model);
				}
				var sentRes = _smsService.Send(new List<string> { model.Phone }, model.Content);
				TempData.SetData(sentRes.Success ? AlertLevel.Success : AlertLevel.Warning, "Test Sms", sentRes.Message);
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				model.Message = "Error occured. Try again later";
				TempData.SetData(AlertLevel.Error, "Test Sms", model.Message);
				return View(model);
			}
		}


		public async Task<IActionResult> Sms()
		{
			var model = new SmsSettingsViewModel();
			try
			{
				var setting = await _context.Settings.FirstOrDefaultAsync(s => s.Key.Equals(SettingKey.Sms));
				if (setting != null)
				{
					var smsSetting = JsonConvert.DeserializeObject<SmsSetting>(setting.Data);
					model = new SmsSettingsViewModel(smsSetting);
				}

				model.Success = true;
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				_logger.Error($"Error fetching SMS settings :- {e.Message}");
				model.Message = "Error Occured. Try again later";
				TempData.SetData(AlertLevel.Error, "SMS Settings", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Sms(SmsSettingsViewModel model)
		{
			try
			{
				var data = JsonConvert.SerializeObject(model);
				var setting = new Setting { Key = model.Key };
				setting.Data = data;
				var res = await Update(setting);
				model.Message = res ? "SMS Settings Updated" : "SMS Settings not Updated";
				model.Success = res;
				TempData.SetData(res ? AlertLevel.Success : AlertLevel.Warning, "SMS Settings", model.Message);
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				model.Message = "Error occured. Try again later";
				TempData.SetData(AlertLevel.Error, "SMS Settings", model.Message);
				return View(model);
			}
		}

		public async Task<IActionResult> Mpesa()
		{
			var model = new MpesaSettingsViewModel();
			try
			{
				var setting = await _context.Settings.FirstOrDefaultAsync(s => s.Key.Equals(SettingKey.Mpesa));
				if (setting != null)
				{
					var mpesaSetting = JsonConvert.DeserializeObject<MpesaSetting>(setting.Data);
					model = new MpesaSettingsViewModel(mpesaSetting);
				}

				model.Success = true;
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				_logger.Error($"Error fetching Mpesa settings :- {e.Message}");
				model.Message = "Error Occured. Try again later";
				TempData.SetData(AlertLevel.Error, "M-PESA Settings", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Mpesa(MpesaSettingsViewModel model)
		{
			try
			{
				var data = JsonConvert.SerializeObject(model);
				var setting = new Setting { Key = model.Key };
				setting.Data = data;
				var res = await Update(setting);
				model.Message = res ? "Mpesa Settings Updated" : "Mpesa Settings not Updated";
				model.Success = res;
				TempData.SetData(res ? AlertLevel.Success : AlertLevel.Warning, "M-PESA Settings", model.Message);
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				_logger.Error($"Error editing M-PESA settings :- {e.Message}");
				model.Message = "Error occured. Try again later";
				TempData.SetData(AlertLevel.Error, "M-PESA Settings", model.Message);
				return View(model);
			}
		}

		public async Task<IActionResult> Layouts()
		{
			var layout = await _context.LayoutSettings.FirstOrDefaultAsync();
			var model = layout != null ? new LayoutSettingViewModel(layout) : new LayoutSettingViewModel();
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Layouts(LayoutSettingViewModel model)
		{
			var tag = "Layout Settings";
			try
			{
				if (model.File != null)
				{
					var res = await FileUploadService.Upload(model.File, _hostingEnvironment.WebRootPath);
					if (!res.Success)
					{
						ModelState.AddModelError("File", res.Message);
						return View(model);
					}
					model.IsNewImg = res.Success;
					model.CurrentIntakeImage = res.Success ? res.Data.Path : string.Empty;
				}
				var layout = await _context.LayoutSettings.FirstOrDefaultAsync();
				if (layout == null)
				{
					_context.LayoutSettings.Add(new LayoutSetting
					{
						Personnel = User.Identity.Name,
						HomePage = model.HomePage,
						CreateAccount = model.CreateAccount,
						FindCourse = model.FindCourse,
						SubmitApplication = model.SubmitApplication,
						ContactUs = model.ContactUs,
						RecommendedCourses = model.RecommendedCourses,
						CurrentIntakes = model.CurrentIntakeImage
					});
					await _context.SaveChangesAsync();
					TempData.SetData(AlertLevel.Success, tag, "Settings Added");
					return View(model);
				}
				layout.HomePage = model.HomePage;
				layout.CreateAccount = model.CreateAccount;
				layout.FindCourse = model.FindCourse;
				layout.SubmitApplication = model.SubmitApplication;
				layout.ContactUs = model.ContactUs;
				layout.RecommendedCourses = model.RecommendedCourses;
				if (model.IsNewImg)
					layout.CurrentIntakes = model.CurrentIntakeImage;
				layout.DateUpdated = DateTime.Now;

				await _context.SaveChangesAsync();
				TempData.SetData(AlertLevel.Success, tag, "Settings Updated");
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				_logger.Error($"Error editing {tag} :- {e.Message}");
				var message = "Error occured. Try again later";
				TempData.SetData(AlertLevel.Error, tag, message);
				return View(model);
			}
		}

		public async Task<IActionResult> Soap()
		{
			var model = new SoapSettingViewModel();
			try
			{
				var setting = await _context.Settings.FirstOrDefaultAsync(s => s.Key.Equals(SettingKey.Soap));
				if (setting != null)
				{
					var soapSetting = JsonConvert.DeserializeObject<SoapSetting>(setting.Data);
					model = new SoapSettingViewModel(soapSetting);
				}

				model.Success = true;
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				_logger.Error($"Error fetching soap settings :- {e.Message}");
				model.Message = "Error Occured. Try again later";
				TempData.SetData(AlertLevel.Error, "SOAP Settings", model.Message);
				return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Soap(SoapSettingViewModel model)
		{
			try
			{
				var data = JsonConvert.SerializeObject(model);
				var setting = new Setting { Key = model.Key };
				setting.Data = data;
				var res = await Update(setting);
				model.Message = res ? "SOAP Settings Updated" : "SOAP Settings not Updated";
				model.Success = res;
				TempData.SetData(res ? AlertLevel.Success : AlertLevel.Warning, "SOAP Settings", model.Message);
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				model.Message = "Error occured. Try again later";
				TempData.SetData(AlertLevel.Error, "SOAP Settings", model.Message);
				return View(model);
			}
		}

		public IActionResult SendTestSoap()
		{
			return View(new TestSoapViewModel
			{
				ModeNo = DateTime.UtcNow.ToString("yyyyMMddHHmmssffff"),
				Amount = 1
			});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> SendTestSoap(TestSoapViewModel model)
		{
			try
			{

				var setting = await _context.Settings
					.FirstOrDefaultAsync(s => s.Key.Equals(SettingKey.Soap));
				if (setting == null)
				{
					TempData.SetData(AlertLevel.Warning, "Test SOAP", "Settings not Added");
					return View(model);
				}
				var soapSettings = JsonConvert.DeserializeObject<SoapSetting>(setting.Data);
				var service = new UnisolBankService(soapSettings.UserName, soapSettings.Password, soapSettings.Url);
				var result = await service.ProcessStudentPaymentAsync(model.RegNo, model.Amount, model.ModeNo);

				model.Message = result.Message;
				var message = result.Success ? $"{result.Message} :- {result.Data.StrRcptNo}" : result.Message;
				model.Success = result.Success;
				TempData.SetData(model.Success ? AlertLevel.Success : AlertLevel.Warning, "Test SOAP", message);
				return View(model);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				model.Message = "Error occured. Try again later";
				TempData.SetData(AlertLevel.Error, "Test SOAP", model.Message);
				return View(model);
			}
		}

		private async Task<bool> Update(Setting setting)
		{
			try
			{
				var dbSetting = _context.Settings.FirstOrDefault(s => s.Key.Equals(setting.Key));
				if (dbSetting != null)
				{
					dbSetting.Data = setting.Data;
					dbSetting.DateUpdated = DateTime.UtcNow;
				}
				else
				{
					_context.Settings.Add(setting);
				}
				await _context.SaveChangesAsync();
				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				_logger.Error($"Error Updating {setting.Key} settings :- {e.Message}");
				return false;
			}
		}
	}
}
