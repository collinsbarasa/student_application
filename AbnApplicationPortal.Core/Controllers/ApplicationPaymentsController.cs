﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AbnApplicationPortal.Core.Models.PaymentVm;
using Microsoft.EntityFrameworkCore;
using AbnApplicationPortal.Core.Data.AppDb;
using Microsoft.AspNetCore.Identity;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Models.ApplicationVm;
using AbnApplicationPortal.Core.Services;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;

namespace AbnApplicationPortal.Core.Controllers.Applications
{
	[Authorize]
	[Route("[controller]/[action]")]
	public class ApplicationPaymentsController : Controller
	{
		private readonly AbnApplicationPortalContext _context;
		private readonly ILog _logger;
		private readonly UserManager<ApplicationUser> _userManager;
		private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		private readonly IClientSettingsService _clientSettingsService;

		public ApplicationPaymentsController(AbnApplicationPortalContext context, UserManager<ApplicationUser> userManager,
			IClientSettingsService clientSettingsService)
		{
			_logger = AbnLogManager.Logger(typeof(ApplicationPaymentsController));
			_context = context;
			_userManager = userManager;
			_clientSettingsService = clientSettingsService;
		}

		public async Task<IActionResult> Index(FilterApplicationsViewModel filter)
		{
			try
			{
				filter.ItemsPerPage = 10;
				var data = await GetPaymentsAsync(filter);
				return View(data);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Payments :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Payments", message);
				var res = new PaginatedList<PaymentViewModel>(new List<PaymentViewModel>(), 1, 1, 1);
				return View(res);
			}
		}

		public async Task<FileContentResult> Export(FilterApplicationsViewModel filter)
		{
			try
			{
				var package = new ExcelPackage();
				var client = _clientSettingsService.Read().Data;
				package.Workbook.Properties.Title = $"{client.ClientName} Payments Export";
				var user = await _userManager.GetUserAsync(User);
				package.Workbook.Properties.Author = $"{user.Names}-{user.Email}";
				package.Workbook.Properties.Subject = "Payments Summary";
				var worksheet = package.Workbook.Worksheets.Add($"Payments By {DateTime.Now:dd-MMMM-yyyy}");

				var columns = new List<string> { "Payment Ref #", "Receipt #", "Application Ref #", "Mode/Phone #", "Amount", "Date", "Status" };
				for (var i = 0; i < columns.Count; i++)
				{
					worksheet.Cells[1, i + 1].Style.Font.Bold = true;
					worksheet.Cells[1, i + 1].Value = columns[i];
				}
				var cRow = 1;
				var index = 1;
				var data = await GetPaymentsAsync(filter);
				foreach (var payment in data)
				{
					worksheet.Cells[index + cRow, 1].Value = payment.Reference;
					worksheet.Cells[index + cRow, 2].Value = payment.MpesaReceiptNumber;
					worksheet.Cells[index + cRow, 3].Value = payment.AppRef;
					worksheet.Cells[index + cRow, 4].Value = payment.PhoneNumber;
					worksheet.Cells[index + cRow, 5].Value = payment.Amount;
					worksheet.Cells[index + cRow, 6].Value = payment.DateStr;
					worksheet.Cells[index + cRow, 7].Value = payment.IsCompleted ? "COMPLETE" : "INCOMPLETE";
					index++;
				}
				var modelTable = worksheet.Cells[1, 1, worksheet.Dimension.End.Row, worksheet.Dimension.End.Column];
				modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
				modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
				modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
				modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

				var title = $"{client.ClientName} Payments";
				var tbl = worksheet.Tables.Add(
					new ExcelAddressBase(1, 1, worksheet.Dimension.End.Row,
						worksheet.Dimension.End.Column), title.Replace(" ", "_"));
				tbl.ShowHeader = true;
				//tbl.TableStyle = TableStyles.Dark9;
				var excelName = "payments-" + Guid.NewGuid().ToString().Substring(0, 6);
				return File(package.GetAsByteArray(), XlsxContentType, excelName + ".xlsx");
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return null;
			}
		}

		public async Task<IActionResult> Mine()
		{
			var userId = _userManager.GetUserId(User);
			var model = new ReturnData<List<PaymentViewModel>>
			{ Data = new List<PaymentViewModel>() };
			try
			{
				var data = await _context.ApplicationPayments
					.OrderByDescending(p => p.DateCreated)
					.Where(p => p.UserId.Equals(userId))
					.Select(p => new PaymentViewModel(p))
				.ToListAsync();
				model.Success = data.Any();
				if (model.Success)
					model.Data = data;
				return View(model);
			}
			catch (Exception ex)
			{
				ViewData["CPage"] = "All";
				Console.WriteLine(ex);
				_logger.Error($"Error getting Payments for {userId} :-{ex.Message}");
				model.Message = "Error occured. Try again";
				model.ErrorMessage = ex.Message;
				TempData.SetData(AlertLevel.Error, "My Payments", model.Message);
				return View(model);
			}
		}

		private async Task<PaginatedList<PaymentViewModel>> GetPaymentsAsync(FilterApplicationsViewModel filter)
		{
			ViewData["CodeSortParm"] = string.IsNullOrEmpty(filter.SortOrder) ? "code_desc" : "";
			ViewData["RefSortParm"] = filter.SortOrder == "RefNo" ? "ref_desc" : "RefNo";
			ViewData["PhoneSortParm"] = filter.SortOrder == "PhoneNo" ? "phone_desc" : "PhoneNo";
			ViewData["CurrentSort"] = filter.SortOrder;
			ViewData["CurrentFilter"] = filter.SearchString;
			ViewData["StartDate"] = filter.StartDate;
			ViewData["EndDate"] = filter.EndDate;
			if (filter.SearchString != null) filter.PageNumber = 1;
			else filter.SearchString = filter.CurrentFilter;
			var paymentQry = _context.ApplicationPayments.AsQueryable();
			if (User.IsInRole(PrimaryRole.Applicant) || User.IsInRole(PrimaryRole.Student))
				paymentQry = paymentQry.Where(p => p.UserId.Equals(_userManager.GetUserId(User)));
			if (!string.IsNullOrEmpty(filter.SearchString))
				paymentQry = paymentQry
					.Where(p => p.AppRef.Contains(filter.SearchString)
					|| p.MpesaReceiptNumber.Contains(filter.SearchString)
					|| p.PhoneNumber.Contains(filter.SearchString));
			if (!string.IsNullOrEmpty(filter.StartDate))
			{
				var sDate = DateTime.Parse(filter.StartDate);
				paymentQry = paymentQry.Where(p => p.DateCreated.Date >= sDate.Date);
			}
			if (!string.IsNullOrEmpty(filter.EndDate))
			{
				var eDate = DateTime.Parse(filter.EndDate);
				paymentQry = paymentQry.Where(p => p.DateCreated.Date <= eDate.Date);
			}
			switch (filter.SortOrder)
			{
				case "code_desc":
					paymentQry = paymentQry.OrderByDescending(s => s.AppRef);
					break;
				case "RefNo":
					paymentQry = paymentQry.OrderBy(s => s.MpesaReceiptNumber);
					break;
				case "ref_desc":
					paymentQry = paymentQry.OrderByDescending(s => s.MpesaReceiptNumber);
					break;
				case "PhoneNo":
					paymentQry = paymentQry.OrderBy(s => s.PhoneNumber);
					break;
				case "phone_desc":
					paymentQry = paymentQry.OrderByDescending(s => s.PhoneNumber);
					break;
				default:
					paymentQry = paymentQry.OrderByDescending(s => s.DateCreated);
					break;
			}
			var payments = paymentQry
				.Select(c => new PaymentViewModel(c));
			var data = await PaginatedList<PaymentViewModel>
			   .CreateAsync(payments.AsNoTracking(), filter.PageNumber ?? 1, filter.ItemsPerPage);
			var sum = await paymentQry.Where(p => p.IsCompleted).SumAsync(p => p.Amount);
			var total = string.Format("{0:n2}", sum);
			ViewData["Total"] = total;
			return data;
		}
	}
}
