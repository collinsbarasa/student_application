﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Shared.Models.Applicants;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers
{
	[Authorize]
	public class NotificationsController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly ILog _logger;
		private readonly AbnApplicationPortalContext _context;
		private readonly string _tag = "Notifications";
		public NotificationsController(AbnApplicationPortalContext context, UserManager<ApplicationUser> userManager)
		{
			_logger = AbnLogManager.Logger(typeof(NotificationsController));
			_userManager = userManager;
			_userManager = userManager;
			_context = context;
		}

		public async Task<IActionResult> Index()
		{
			try
			{
				var userId = _userManager.GetUserId(User);
				var notifications = await _context.Notifications
					.Where(n => n.UserId.Equals(userId))
					.OrderByDescending(n => n.DateUpdated)
					.ToListAsync();
				if (!notifications.Any())
					TempData.SetData(AlertLevel.Warning, _tag, "Not Found");
				return View(notifications);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"error getting notifications :- {ex.Message}");
				TempData.SetData(AlertLevel.Error, _tag, "Error occured. Try again later");
				return View(new List<Notification>());
			}
		}
	}
}
