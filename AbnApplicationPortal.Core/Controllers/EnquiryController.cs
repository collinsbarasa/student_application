﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Extensions;
using AbnApplicationPortal.Core.Log4Net;
using AbnApplicationPortal.Core.Models.EnquiryVm;
using AbnApplicationPortal.Core.Models.IntakeSettingsVm;
using AbnApplicationPortal.Shared.Models.Enquiry;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class EnquiryController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AbnApplicationPortalContext _context;
        private readonly ILog _logger;

        public EnquiryController(AbnApplicationPortalContext context,
            UserManager<ApplicationUser> userManager)
        {
            _logger = AbnLogManager.Logger(typeof(EnquiryController));
            _context = context;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
			var model = new List<EnquiryViewModel>();
			try
			{
				var enquiries = await _context.Enquiries
						.Select(x=> new EnquiryViewModel(x))
						.ToListAsync();
				if (!enquiries.Any())
				{
					TempData.SetData(AlertLevel.Error, "Enquiries", "Not Found");
					return View(model);
				}
				return View(enquiries);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Categories :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Enquiries", message);
				return View(model);
			}
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<JsonResult> SaveEnquiry(EnquiryViewModel model)
		{
			var res = new ReturnData<string>();
			try
			{
				if (!ModelState.IsValid)
                {
					res.Message = "Fill all fields to Send";
					return Json(res);
				}
					

				var enq = new Enquiry() {
					Name = model.Name,
					Email = model.Email,
					Phone = model.Phone,
					Message = model.Message,
					CategoryId = model.CategoryId
				};
				_context.Enquiries.Add(enq);
				await _context.SaveChangesAsync();
				res.Success = true;
				res.Message = "Your message has been received.";
				
				TempData.SetData(AlertLevel.Success, "Enquiry", "Enquiry Saved Successfully");
				return Json(res);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"error sending email message :-{ex.Message}");
				res.Message = "Error occured. Try again later";
				res.ErrorMessage = ex.Message;
				return Json(res);
			}
		}

		public async Task<IActionResult> EnquiryDetail(Guid id) {
			var model = new EnquiryDetailViewModel();
			model.EnquiryFeedBacks = new List<FeedBack>();
			try
			{

				var enquiry = await _context.Enquiries
					.FirstOrDefaultAsync(x => x.Id == id);
				var category = _context.Categories.FirstOrDefault(x=>x.Id == enquiry.CategoryId);
				model.Enquiry = new EnquiryViewModel(enquiry);
				model.Enquiry.CategoryStr = category?.Name;
				return View(model);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_logger.Error($"Error getting Categories :-{ex.Message}");
				var message = "Error occured. Try again";
				TempData.SetData(AlertLevel.Error, "Enquiries", message);
				return View(model);
			}
		}
	}
}
