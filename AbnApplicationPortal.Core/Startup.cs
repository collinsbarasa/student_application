﻿using AbnApplicationPortal.Core.CommProviders.Email;
using AbnApplicationPortal.Core.CommProviders.Sms;
using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Data.AppDb;
using AbnApplicationPortal.Core.Data.Unisol;
using AbnApplicationPortal.Core.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;
using System;

namespace AbnApplicationPortal.Core
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			var unisolString = DbSetting.ConnectionString(Configuration, "Unisol");
			services.AddDbContext<UnisolDbContext>(
				options => options.UseSqlServer(unisolString));

			var portalString = DbSetting.ConnectionString(Configuration, "Portal");
			services.AddDbContext<AbnApplicationPortalContext>(
				options => options.UseSqlServer(portalString));

			services.AddIdentity<ApplicationUser, IdentityRole>()
				.AddEntityFrameworkStores<AbnApplicationPortalContext>()
				.AddDefaultTokenProviders();

			services.AddTransient<IEmailSender, EmailSender>();
			services.AddTransient<IClientSettingsService, ClientSettingsService>();
			services.AddTransient<IDropDownService, DropDownService>();
			services.AddTransient<ISmsService, SmsService>();

			services.Configure<IdentityOptions>(options =>
			{
				options.Password.RequireDigit = false;
				options.Password.RequireLowercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = false;
				options.Password.RequiredLength = 2;
				options.Password.RequiredUniqueChars = 0;

				options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
				options.Lockout.MaxFailedAccessAttempts = 5;
				options.Lockout.AllowedForNewUsers = true;

				options.User.AllowedUserNameCharacters =
					"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
				options.User.RequireUniqueEmail = true;
			});

			services.ConfigureApplicationCookie(options =>
			{
				options.Cookie.HttpOnly = true;
				options.ExpireTimeSpan = TimeSpan.FromMinutes(35);

				options.LoginPath = "/Home/Index";
				options.AccessDeniedPath = "/Home/AccessDenied";
				options.SlidingExpiration = true;
			});

			services.AddAuthorization(options =>
			{
				options.AddPolicy("RequireAdminsOnly", policy => policy.RequireRole("Admin,SuperAdmin"));
			});

			services.AddMvc(options =>
			{
			}).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

			services.AddSignalR();
		}

		private static void UpdateDatabase(IApplicationBuilder app)
		{
			using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
			{
				var context = serviceScope.ServiceProvider.GetRequiredService<AbnApplicationPortalContext>();
                context.Database.Migrate();
                //context.EnsureDatabaseSeeded();
                context.Database.EnsureCreated();
            }
		}
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.UseDefaultFiles();
			app.UseStaticFiles(new StaticFileOptions
			{
				OnPrepareResponse = ctx =>
				{
					const int durationInSeconds = 60 * 60 * 24;
					ctx.Context.Response.Headers[HeaderNames.CacheControl] =
						"public,max-age=" + durationInSeconds;
				}
			});
			app.UseAuthentication();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}");
			});

			app.UseForwardedHeaders(new ForwardedHeadersOptions
			{
				ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
			});


			app.UseHttpsRedirection();
			app.UseMvc();

			UpdateDatabase(app);
		}
	}
}
