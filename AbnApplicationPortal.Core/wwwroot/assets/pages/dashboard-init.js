/**
* Theme: Syntra Admin Template
* Author: Mannat-themes
* Dashboard
*/

// Dashboard 1 Morris-chart
$( function () {
    "use strict";
     
   
   // Morris donut chart
    Morris.Donut( {
        element: 'morris-donut-chart', data: [ {
            label: "Completed Applications", value: 100,
        }
        , {
            label: "Pending Applications", value: 28
        }
        , {
            label: "Rejected Applications", value: 12
        }
        ], resize: true, colors: [ '#A8EFD5', '#F6E2B9', '#F77A87']
    }
    );
    
    // Extra chart
    Morris.Area( {
        element: 'extra-area-chart', data: [ {
            period: '2015', Applications: 0
        }
        , {
            period: '2016', Applications: 90
        }
        , {
            period: '2017', Applications: 40
        }
        , {
            period: '2018', Applications: 30
        }
        , {
            period: '2019', Applications: 150
        }
        , {
            period: '2020', Applications: 25 
        }
        , {
            period: '2021', Applications: 10
        }
        ], lineColors: [ '#2e61f2'], xkey: 'period', ykeys: [ 'Applications'], labels: [ 'Applications'], pointSize: 0, lineWidth: 0, resize: true, fillOpacity: 0.8, behaveLikeLine: true, gridLineColor: '#e0e0e0', hideHover: 'auto'
    }
    );



    // Morris bar chart
    Morris.Bar( {
        element: 'morris-bar-chart', data: [ {
                y: '2021', a: 100, b:120, c:123, d:145, e:34
        }
        
        ], xkey: 'y', ykeys: [ 'a', 'b', 'c','d','e'], labels: [ 'Certificate', 'Diploma', 'Bachelors','Masters','Phd'], barColors: [ '#8596a7', '#242440', '#123456','#675421','#456281'], hideHover: 'auto', gridLineColor: '#eef0f2', resize: true
     }
    );
   

     // LINE CHART
    var line=new Morris.Line( {
        element: 'morris-line-chart', data: [ {
            period: '2015', Revenue: 1200
        }
        , {
            period: '2016', Revenue: 6000
        }
        , {
            period: '2017', Revenue: 2000
        }
        , {
            period: '2018', Revenue: 6500
        }
        , {
            period: '2019', Revenue: 1200
        }
        , {
            period: '2020', Revenue: 2500
        }
        , {
            period: '2021', Revenue: 3000
        }
        ], xkey: 'period', ykeys: [ 'Revenue'], labels: [ 'Revenue  '], pointSize: 3, fillOpacity: 0, pointStrokeColors: ['#FFA500'], behaveLikeLine: true, gridLineColor: '#e0e0e0', lineWidth: 3, hideHover: 'auto', lineColors: [ '#00FF12' ], resize: true
    }
    );
   
}

);


function chartData() {

    $.ajax({
        url: '',
        method: 'Get',
        dataType: 'json',
        data: {

        }
    },
        }
      

