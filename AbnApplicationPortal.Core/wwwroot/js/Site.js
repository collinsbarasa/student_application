﻿$(document).ready(function () {
	$("#frmSendMessage").submit(function (e) {
		e.preventDefault();
		var form = $(this);
		var url = form.attr('action');
		var btn = $("#btnSendMessage");
		var text = btn.text();
		btn.prop("disabled", true);
		btn.text("sending ...");
		$.ajax({
			type: "POST",
			url: url,
			data: form.serialize(),
			success: function (data) {
				btn.prop("disabled", false);
				btn.text(text);
				alert(data.message);
				if (data.success) {
					form.trigger("reset");
				}

			},
			error: function (error) {
				btn.prop("disabled", false);
				btn.text(text);
			}
		});
	});

	$('.auto-cap').keyup(function () {
		$(this).val($(this).val().substr(0, 1).toUpperCase() + $(this).val().substr(1));
	});

	$("#dataTable").dataTable();
	var table = $('.intake-pr-table').DataTable({
		"columnDefs": [
			{ "orderable": false, "targets": 0 }
		],
		responsive: true,
		//stateSave: true
	})
		.columns.adjust()
		.responsive.recalc();

	$(".check-all").on("change", function () {
		var id = $(this).attr("gr-id");
		toggle(this, id);
	});
	var toggle = function (source, id) {
		table.$('.intake-pr-check-' + id).prop('checked', source.checked);
		table.$('.intake-pr-check-' + id).attr('value', source.checked);
	}

	$("#ChkPayConfirm").change(function () {
		if ($(this).is(':checked')) {
			$("#ShowPay").show();
			$("#HidePay").hide();
		}
		else {
			$("#ShowPay").hide();
			$("#HidePay").show();
		}
	});

	$('.intake-pr-form').on('submit', function (e) {
		$('.intake-pr-table').DataTable().destroy();
	});

	$(".numbers-only").keypress(function (evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
		if (iKeyCode !== 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;
		return true;
	});

	$("#SelectCounty").change(function () {
		var items = $('option:selected', this).attr("data-sub-counties");
		if (items) {
			var data = JSON.parse(items);
			var select = $("#SelectSubCounty").empty()
				.append($('<option>').text("--select sub-county here--").attr('value', ""));;
			$.each(data, function () {
				select.append($('<option>').text(this.Name).attr('value', this.Name));
			});
		}
	});

	$(window).scroll(function () {
		var pageNo = $("#inpPageNumber").val();
		var search = $("#search-toggle").val();
		if (pageNo) {
			var scrollHeight = $(document).height();
			var scrollPos = $(window).height() + $(window).scrollTop();
			if (((scrollHeight - 300) >= scrollPos) / scrollHeight == 0) {
				var college = $("#selCollege").val() ?? "";
				$("#loader").html('<i class="fa fa-spinner fa-spin"></i> loading. please wait ...');
				$.ajax({
					type: "get",
					url: '/Home/ProgrammesAjax?college=' + college + '&pageNumber=' + pageNo + '&searchString=' + search + '',
					success: function (data) {
						$("#loader").html('--');
						$("#inpPageNumber").val(data.message);
						data.data.forEach(function (p) {
							var programme = '<div class="xl:w-1/3 md:w-1/2 p-4">'
								+ '<div class=" md:flex-1 p-4 rounded shadow-lg bg-white border-b border-r border-grey-dark7 overflow-hidden" >'
								+ '<img class="center"  src="../assets/images/educ.jpg"></img>'
								+ '	<span class="absolute bg-yellow-300 px-2 py-1 text-xs inline-flex ml-64 mb-4 rounded-full text-white font-medium">'
								+ '' + p.period + '</span>'
								+ '<div class="w-10 h-10 inline-flex items-center justify-center rounded-full bg-green-100 text-green-500 mb-4 font-bold">'
								+ '' + p.code + '</div>'
								+ '			<h2 class="text-lg text-gray-900 font-medium title-font mb-2">'
								+ '' + p.name + '</h2>'
								+ '			<p class="h-10 md:h-4 lg:h-20 overflow-hidden">'
								+ '' + p.requirements + ' </p>'
								+ ' <span class="mt-3 text-green-500 inline-flex items-center">'
								+ '' + p.certType + ' </span>'
								+ ' <span class="mt-3 inline-flex items-center">'
								+ '<a href="/Home/ProgramDetails?id='+ p.id+'"'
								+ ' class="mx-auto mt-2  inline-flex text-white bg-yellow-500 rounded-full py-3 px-6 content-center focus:outline-none hover:bg-gray-300 rounded text-lg  ">View More</a>'
		                            '</span> </div > </div >';
							$("#progData").append(programme);
							changeColor();
						});
					},
					error: function (error) {
						$("#loader").html("Error occured. Try later");
					},
					
				});
			}
		}
		function changeColor() {
            var pcolor = $('#primary-color').val();
            var scolor = $('#secondary-color').val();

           
            $('.bg-green-500').css('background-color', pcolor);
            $('.bg-green-600').css('background-color', pcolor);
            $('.bg-green-700').css('background-color', pcolor);
            $('.bg-green-800').css('background-color', pcolor);
            $('.bg-green-200').css('background-color', pcolor);
            $('.text-gray-200').css('background-color', pcolor);
            $('.bg-yellow-200').css('background-color', pcolor);

            
            $('.text-green-500').css('color', pcolor);
            $('.bg-yellow-500').css('background-color', scolor);
            $('.bg-yellow-300').css('background-color', scolor);
            $('.bg-gray-200').css('background-color', scolor);
            $('.bg-gray-300').css('background-color', pcolor);
            $('.bg-gray-800').css('background-color', scolor);
            $('.bg-gray-700').css('background-color', scolor);
            $('.bg-indigo-400').css('background-color', pcolor);

          
            
            $('.bg-yellow-600').css('secondary-color', scolor);
        }

	});

});