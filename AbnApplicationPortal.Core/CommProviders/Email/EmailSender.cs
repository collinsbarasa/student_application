﻿using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Core.Services;
using AbnApplicationPortal.Shared.Models.Settings;
using AbnApplicationPortal.Shared.Requests.Settings;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using MimeKit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AbnApplicationPortal.Core.CommProviders.Email
{
	public interface IEmailSender
	{
		Task<ReturnData<string>> SendEmailAsync(string email, string subject, string message);
		Task<ReturnData<string>> SendContactEmailAsync(string name, string email, string message);
	}

	public class EmailSender : IEmailSender
	{
		private readonly AbnApplicationPortalContext _context;
		private readonly IClientSettingsService _settingsService;
		private readonly IHostingEnvironment _hostingEnvironment;
		public EmailSender(AbnApplicationPortalContext context,
			IClientSettingsService settingsService, IHostingEnvironment hostingEnvironment)
		{
			_context = context;
			_settingsService = settingsService;
			_hostingEnvironment = hostingEnvironment;
		}
		public async Task<ReturnData<string>> SendEmailAsync(string email, string subject, string message)
		{
			var emailMessage = new EmailMessage
			{
				ToAddresses = new List<EmailAddress> { new EmailAddress { Address = email } },
				Subject = subject,
				Content = message
			};
			var res = await SendEmailAsync(emailMessage, EmailSetting());
			return res;
		}

		private EmailSetting EmailSetting(string senderName = "", string senderEmail = "")
		{
			var emailSetting = _context.Settings
				   .FirstOrDefault(s => s.Key.Equals(SettingKey.Email));
			if (emailSetting == null)
			{
				return null;
			}
			var settings = JsonConvert.DeserializeObject<EmailSetting>(emailSetting?.Data);
			if (!string.IsNullOrEmpty(senderName))
				settings.SenderFromName = senderName;
			if (!string.IsNullOrEmpty(senderEmail))
				settings.SenderFromEmail = senderEmail;
			return settings;
		}

		private async Task<ReturnData<string>> SendEmailAsync(EmailMessage emailMessage, EmailSetting settings)
		{
			var res = new ReturnData<string>();
			try
			{
				if (settings == null)
				{
					res.Message = "Email settings not set";
					return res;
				}
				var img = $"<img src='cid:logoId' style='width:100px;'/><br/>";
				emailMessage.Content = $"{img}{emailMessage.Content}";
				var message = new MimeMessage();
				message.To.AddRange(emailMessage.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
				message.From.Add(new MailboxAddress(settings.SenderFromName, settings.SenderFromEmail));
				message.Subject = emailMessage.Subject;

				var clSetings = _settingsService.Read().Data;
				var builder = new BodyBuilder();
				if (!string.IsNullOrEmpty(clSetings.LogoUrl))
				{
					var url = Path.Combine(_hostingEnvironment.WebRootPath, clSetings.LogoUrl);
					var image = builder.LinkedResources.Add(url);
					image.ContentId = "logoId";
				}
				builder.HtmlBody = emailMessage.Content;
				message.Body = builder.ToMessageBody();
				using (var emailClient = new SmtpClient())
				{
					var smtpServer = settings.SmtpServer;
					var smtpPort = int.Parse(settings.SmtpPort);
					var smtpUsername = settings.SmtpUsername;
					var smtpPassword = settings.SmtpPassword;
					try
					{
						emailClient.Connect(smtpServer, smtpPort, settings.SocketOptions);
						emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
						emailClient.Authenticate(smtpUsername, smtpPassword);
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex);
						res.Message = "Mail Error occured. Try again later";
						res.ErrorMessage = ex.Message;
						return res;
					}
					await emailClient.SendAsync(message);
					await emailClient.DisconnectAsync(true);
				}

				res.Success = true;
				res.Message = "Sent";
				res.Data = message.MessageId;
				return res;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				res.Message = "Error occured. Try again later";
				res.ErrorMessage = ex.Message;
				return res;
			}
		}

		public async Task<ReturnData<string>> SendContactEmailAsync(string name, string email, string message)
		{
			var settings = _settingsService.Read().Data;
			var emailMessage = new EmailMessage
			{
				ToAddresses = new List<EmailAddress> { new EmailAddress { Address = settings.ContactEmail, Name = settings.ClientName } },
				Subject = $"Contact Email - { DateTime.Now:yyyyMMddHHmmssffff}",
				Content = $"{message}. From :-{name} {email}"
			};
			var res = await SendEmailAsync(emailMessage, EmailSetting(name, email));
			return res;
		}
	}

}
