﻿namespace AbnApplicationPortal.Core.CommProviders.Email
{
    public class EmailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
