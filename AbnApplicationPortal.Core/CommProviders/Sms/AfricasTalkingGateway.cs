﻿using System;
using System.Collections;
using System.Text;
using System.Web;
using System.IO;
using System.Net;
using Newtonsoft.Json;


namespace AbnApplicationPortal.Core.CommProviders.Sms
{
    public class AfricasTalkingGatewayException : Exception
    {
        public AfricasTalkingGatewayException(string message)
            : base(message)
        {
        }

        public AfricasTalkingGatewayException(Exception ex) : base(ex.Message, ex)
        {

        }
    }

    public class AfricasTalkingGateway
    {
        private string _username;
        private string _apiKey;
        private string _environment;
        private int responseCode;

        private bool DEBUG = false;

        public AfricasTalkingGateway(string username_, string apiKey_)
        {
            _username = username_;
            _apiKey = apiKey_;
            _environment = "production";
        }

        public AfricasTalkingGateway(string username, string apiKey, string environment)
        {
            _username = username;
            _apiKey = apiKey;
            _environment = environment;
        }

        public string sendMessage(string to_, string message_, string from_ = null, int bulkSMSMode_ = 1,
            Hashtable options_ = null)
        {
            var data = new Hashtable();
            data["username"] = _username;
            data["to"] = to_;
            data["message"] = message_;

            if (!string.IsNullOrEmpty(from_))
            {
                data["from"] = from_;
                data["bulkSMSMode"] = Convert.ToString(bulkSMSMode_);

                if (options_ != null)
                {
                    if (options_.Contains("keyword"))
                    {
                        data["keyword"] = options_["keyword"];
                    }

                    if (options_.Contains("linkId"))
                    {
                        data["linkId"] = options_["linkId"];
                    }

                    if (options_.Contains("enqueue"))
                    {
                        data["enqueue"] = options_["enqueue"];
                    }

                    if (options_.Contains("retryDurationInHours"))
                        data["retryDurationInHours"] = options_["retryDurationInHours"];
                }
            }

            var response = sendPostRequest(data, SMS_URLString);
            if (responseCode == (int)HttpStatusCode.Created)
            {
                return "sent";
                //var json = JsonConvert.DeserializeObject<dynamic>(response);
                //var recipients = json["SMSMessageData"]["Recipients"]; 
                //if (recipients.Length > 0)
                //{
                //    return recipients;
                //}
                //if (recipients != null)
                //{
                //    return recipients;
                //} 
                //  throw new AfricasTalkingGatewayException(json["SMSMessageData"]["Message"]);
            }

            throw new AfricasTalkingGatewayException(response);
        }

        public dynamic fetchMessages(int lastReceivedId_)
        {
            string url = SMS_URLString + "?username=" + _username + "&lastReceivedId=" +
                         Convert.ToString(lastReceivedId_);
            string response = sendGetRequest(url);
            if (responseCode == (int)HttpStatusCode.OK)
            {
                dynamic json = JsonConvert.DeserializeObject<dynamic>(response);
                return json["SMSMessageData"]["Messages"];
            }

            throw new AfricasTalkingGatewayException(response);
        }

        private string sendPostRequest(Hashtable dataMap_, string urlString_)
        {
            try
            {
                var dataStr = "";
                foreach (string key in dataMap_.Keys)
                {
                    if (dataStr.Length > 0) dataStr += "&";
                    var value = (string)dataMap_[key];
                    dataStr += HttpUtility.UrlEncode(key, Encoding.UTF8);
                    dataStr += "=" + HttpUtility.UrlEncode(value, Encoding.UTF8);
                }

                var byteArray = Encoding.UTF8.GetBytes(dataStr);

                ServicePointManager.ServerCertificateValidationCallback =
                    RemoteCertificateValidationCallback;
                var webRequest = (HttpWebRequest)WebRequest.Create(urlString_);

                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = byteArray.Length;
                webRequest.Accept = "application/json";

                webRequest.Headers.Add("apiKey", _apiKey);

                var webpageStream = webRequest.GetRequestStream();
                webpageStream.Write(byteArray, 0, byteArray.Length);
                webpageStream.Close();

                var httpResponse = (HttpWebResponse)webRequest.GetResponse();
                responseCode = (int)httpResponse.StatusCode;
                var webpageReader = new StreamReader(httpResponse.GetResponseStream());
                var response = webpageReader.ReadToEnd();

                if (DEBUG)
                    Console.WriteLine("Full response: " + response);

                return response;

            }
            catch (WebException ex)
            {
                if (ex.Response == null)
                    throw new AfricasTalkingGatewayException(ex.Message);
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var response = reader.ReadToEnd();

                    if (DEBUG)
                        Console.WriteLine("Full response: " + response);

                    return response;
                }
            }

            catch (AfricasTalkingGatewayException ex)
            {
                throw ex;
            }
        }

        private string sendGetRequest(string urlString_)
        {
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    RemoteCertificateValidationCallback;

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlString_);
                webRequest.Method = "GET";
                webRequest.Accept = "application/json";
                webRequest.Headers.Add("apiKey", _apiKey);

                HttpWebResponse httpResponse = (HttpWebResponse)webRequest.GetResponse();
                responseCode = (int)httpResponse.StatusCode;
                StreamReader webpageReader = new StreamReader(httpResponse.GetResponseStream());

                string response = webpageReader.ReadToEnd();

                if (DEBUG)
                    Console.WriteLine("Full response: " + response);

                return response;

            }

            catch (WebException ex)
            {
                if (ex.Response == null)
                    throw new AfricasTalkingGatewayException(ex.Message);

                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    string response = reader.ReadToEnd();

                    if (DEBUG)
                        Console.WriteLine("Full response: " + response);

                    return response;
                }
            }

            catch (AfricasTalkingGatewayException ex)
            {
                throw ex;
            }
        }

        private bool RemoteCertificateValidationCallback(object sender,
           System.Security.Cryptography.X509Certificates.X509Certificate certificate,
           System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors errors)
        {
            return true;
        }

        private string ApiHost
        {
            get
            {
                return (string.ReferenceEquals(_environment, "sandbox")
                    ? "https://api.sandbox.africastalking.com"
                    : "https://api.africastalking.com");
            }
        }

        private string SMS_URLString
        {
            get { return ApiHost + "/version1/messaging"; }
        }

    }
}
