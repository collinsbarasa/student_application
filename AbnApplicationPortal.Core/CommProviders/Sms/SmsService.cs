﻿using System;
using System.Collections.Generic;
using System.Linq;
using AbnApplicationPortal.Core.Data;
using AbnApplicationPortal.Shared.Models.Logs;
using AbnApplicationPortal.Shared.Models.Settings;
using AbnApplicationPortal.Shared.Requests.Settings;
using Newtonsoft.Json;

namespace AbnApplicationPortal.Core.CommProviders.Sms
{

	public interface ISmsService
	{
		ReturnData<string> Send(List<string> phoneList, string message);
	}

	public class SmsService : ISmsService
	{
		private readonly AbnApplicationPortalContext _context;

		public SmsService(AbnApplicationPortalContext context)
		{
			_context = context;
		}

		public ReturnData<string> Send(List<string> phoneList, string message)
		{
			var response = new ReturnData<string>();
			var recipients = string.Join(",", phoneList.ToArray());
			try
			{
				var settings = Settings();
				if (string.IsNullOrEmpty(settings.Username) || string.IsNullOrEmpty(settings.ApiKey))
				{
					response.Message = "Provide both Username and Api Key";
					return response;
				}

				var gateway = new AfricasTalkingGateway(settings.Username, settings.ApiKey);
				var res = gateway.sendMessage(recipients, message, settings.SenderId);
				var sent = res.Equals("sent");

				var smsSentLog = new SmsSentLog
				{
					Recipients = recipients,
					Message = message,
					Response = res,
					Success = sent
				};

				AddSmsLog(smsSentLog);
				response.Success = sent;
				response.Message = response.Success ? "Sent" : "Not Sent";
				response.Data = res;
				return response;
			}
			catch (AfricasTalkingGatewayException e)
			{
				var smsSentLog = new SmsSentLog
				{
					Recipients = recipients,
					Message = message,
					Response = "AT:-" + e.Message,
					Success = false
				};

				AddSmsLog(smsSentLog);
				response.Message = "AT Error occured. Try again";
				response.Data = e.Message;
				return response;
			}
			catch (Exception ex)
			{
				var smsSentLog = new SmsSentLog
				{
					Recipients = recipients,
					Message = message,
					Response = ex.Message,
					Success = false
				};

				AddSmsLog(smsSentLog);
				response.Message = "Error occured. Try again";
				response.Data = ex.Message;
				return response;
			}
		}

		private void AddSmsLog(SmsSentLog smsSentLog)
		{
			try
			{
				_context.SmsSentLogs.Add(smsSentLog);
				_context.SaveChanges();
			}
			catch (Exception e)
			{
				Console.WriteLine("SMS not Sent :-" + e.Message);
				Console.WriteLine(e);
			}
		}

		private SmsSetting Settings()
		{
			try
			{
				var smsSetting = _context.Settings.FirstOrDefault(s => s.Key.Equals(SettingKey.Sms));
				if (smsSetting == null) return new SmsSetting();
				var settings = JsonConvert.DeserializeObject<SmsSetting>(smsSetting.Data);
				return settings;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return new SmsSetting();
			}
		}
	}
}
