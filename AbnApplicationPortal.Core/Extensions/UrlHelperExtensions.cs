﻿using AbnApplicationPortal.Core.Controllers.Clearance;
using Microsoft.AspNetCore.Mvc;

namespace AbnApplicationPortal.Core.Extensions
{
	public static class UrlHelperExtensions
	{
		public static string EmailConfirmationLink(this IUrlHelper urlHelper, string userId, string code, string scheme)
		{
			return urlHelper.Action(
				action: nameof(HomeController.ConfirmEmail),
				controller: "Home",
				values: new { userId, code },
				protocol: scheme);
		}

		public static string ResetPasswordCallbackLink(this IUrlHelper urlHelper, string userId, string code, string scheme)
		{
			return urlHelper.Action(
				action: nameof(HomeController.ResetPassword),
				controller: "Home",
				values: new { userId, code },
				protocol: scheme);
		}
	}
}
