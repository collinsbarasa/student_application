﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace AbnApplicationPortal.Core.Extensions
{
    public static class TempDataExtensions
    {
        public static void SetData(this ITempDataDictionary @this, AlertLevel type, string title, string message)
        {
            @this["Message"] = message;
            @this["Title"] = title;
            @this["Type"] = type.ToString();
        }
    }

    public enum AlertLevel
    {
        Error,
        Success,
        Warning
    }
}
